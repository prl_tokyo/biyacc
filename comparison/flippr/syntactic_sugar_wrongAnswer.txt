-- data E = Minus E E | Div E E | One | Zero | IfZ E E E
--

pprMain x = nil <> ppr 5 x <> nil;
ppr  i x = manyParens (ppr' i x);

ppr' i One         = text "1" ;

ppr' i Zero        = text "0" ;

ppr' i (Minus x y) = group $
  ppr 5 x <> nest 2 (line' <> text "-" <> space' <> ppr 6 y);

ppr' i (Div   x y) = group $
  ppr 6 x <> nest 2 (line' <> text "/" <> space' <> ppr 7 y) ;

ppr' i (IfZ cond One e2) = group $
  ppr 5 cond <> text "||" <> space' <> ppr 6 e2 ;

ppr' i (IfZ cond e1 e2) = group $
  text "if" <> space' <> ppr 5 cond <> space' <>
    text "then" <> space' <> ppr 6 e1 <> space' <>
    text "else" <> space' <> ppr 6 e2 ;


parensIf b x = if b then parens x else x;

manyParens d = d <+ parens (manyParens d);

parens d = text "(" <> nest 1 (nil <> d <> nil) <> text ")";

space = (text " " <+ charOf nospace) <> nil;

nil = text "" <+ (charOf spaceChars <> nil);

nospace = spaceChars `butnot` chars " ";

line'  = line <+ text "" ;
space' = space <+ text "";

