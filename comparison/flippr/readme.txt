To test these files,
in addition to following the instructions (the README file) given by Flippr,
we might need to do some fixing:
1 add "import Examples.Defs" to the generated parser/printer file (Haskell file)
2 give a proper type to the function parse_pprMain. This could done by
  2-1: give the type   parse_pprMain :: String -> Maybe a
  2-2, reload the file, ghci will tell you the actual type that  (a)  should be.
In the minus.txt example, the type is   parse_pprMain :: String -> Maybe Examples.Defs.E


Test for syntactic sugar (WRONG***):
>>> parse_pprMain "if 1 then 1 else 1"
IfZ One One One
>>> pprMain (IfZ One One One)
1|| 1


results from unambiguous grammar:
(Print -> Parse) (* Good)
>>> pprMain (Div (Minus One One) One)
(1 - 1) / 1

>>> parse_pprMain "(1 - 1) / 1"
Just (Div (Minus One One) One)


(Parse -> Print) (* pretty but not reflective)
>>> parse_pprMain "((1 - 1) / 1)"
Just (Div (Minus One One) One)

>>> pprMain (Div (Minus One One) One)
(1 - 1) / 1



results from ambiguous grammar:
(Print -> Parse) (***wrong)
>>> pprMain (Minus (Div (Minus One One) One) One)
1 - 1 / 1 - 1

>>> parse_pprMain "1 - 1 / 1 - 1"
Just (Minus (Minus One (Div One One)) One)

(Parse -> Print) (***wrong)
>>> parse_pprMain "(1 - 1) / 1 - 1"
Just (Minus (Div (Minus One One) One) One)

>>> pprMain (Minus (Div (Minus One One) One) One)
1 - 1 / 1 - 1


