in order to use the parser and printer.
we should add
> import Text.Syntax.Parser.Naive
> import Text.Syntax.Printer.Naive as TSPN
to the Example.lhs file.


-------------Test2-----------
with syntactic sugar. (WRONG***)
>>> parseM expression " (1) land (2) "
IfZero (Literal 1) (Literal 2) (Literal 0)

>>> TSPN.printM expression (IfZero (Literal 1) (Literal 2) (Literal 0))
"ifzero (1) (2) else (0)"

-------------Test------------
Invertible syntax:
syntax with priority ()

Seems handle unambiguous grammar only. (use priority and chainl.)

Since there is no source information, of course
(print . parse === id) is not satisfied

If priority is not used, the parsing becomes nonderterministic.
(printM t = Just s => parseM s = Just t) does not hold.
--------------------------
unambiguous grammar:
(Parse -> Print) (* Good)
>>> parseM expression "(1 + 2 + 3)"
BinOp (BinOp (Literal 1) AddOp (Literal 2)) AddOp (Literal 3)

>>> TSPN.printM expression (BinOp (BinOp (Literal 1) AddOp (Literal 2)) AddOp (Literal 3))
"1 + 2 + 3"

Print-Parse (* Good)
trivial.
---------------------------
ambiguous grammar:
(Print -> Parse) (*** Property not hold. The printer should reject the input AST.)
>>> TSPN.printM expression (BinOp (BinOp (Literal 1) AddOp (Literal 2)) AddOp (Literal 3))
"1 + 2 + 3"

>>> parseM expression "1 + 2 + 3"
*** Exception: user error (ambiguous input)

(Parse -> Print) (* Good. the parser rejects the input string)
>>> parseM expression "1 + 2 + 3"
*** Exception: user error (ambiguous input)

