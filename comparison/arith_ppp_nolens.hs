import Test.QuickCheck
import Control.Monad

-- I think I do not need to adjust the size of the generated data.
-- The judgement of PutGet and GetPut seems more associated with the structure of the data but not the content of the data.
testArgs :: Args
testArgs = stdArgs {maxSuccess=100}


getput = quickCheckWith testArgs prop_GetPut
putget = quickCheckWith testArgs prop_PutGet


-- points need to be paid attention to when using QuickCheck.
-- some counter examples.
--counterE :: [Int] -> Bool
--counterE = flip (<) 1000 . sum

--newtype BigInt= BigInt Int

--instance Show BigInt where
--  show (BigInt n) = show n

--instance Arbitrary BigInt where
--  arbitrary = liftM BigInt (choose (0,9999999))

--counterEN :: [BigInt] -> Bool
--counterEN = flip (<) 1000000 . sum'
--  where sum' = foldr (\(BigInt n) done-> n + done) 0

data Arith =
    AAdd Arith Arith
  | ASub Arith Arith
  | AMul Arith Arith
  | ADiv Arith Arith
  | ANum String
  deriving (Show, Eq)


data CExpr =
    CAdd  CExpr CTerm
  | CSub  CExpr CTerm
  | ET    CTerm
  | ENull
  deriving (Show, Eq)

data CTerm =
    CMul    CTerm CFactor
  | CDiv    CTerm CFactor
  | TF      CFactor
  | TNull
  deriving (Show, Eq)

data CFactor =
    CNeg   CFactor
  | FE     CExpr
  | CNum   String
  | FNull
  deriving (Show, Eq)


-- data ExprLens s a = ExprLens
--   { p  :: s -> a
--   , pp :: a -> s -> s}

-- exprLens :: ExprLens CExpr Arith
-- exprLens = ExprLens pExpr ppArithE0


-- a simple parser
pExpr :: CExpr -> Arith
pExpr (CAdd l r) = AAdd (pExpr l) (pTerm r)
pExpr (CSub l r) = ASub (pExpr l) (pTerm r)
pExpr (ET t) = pTerm t

pTerm :: CTerm -> Arith
pTerm (CMul l r) = AMul (pTerm l) (pFactor r)
pTerm (CDiv l r) = ADiv (pTerm l) (pFactor r)
pTerm (TF f) = pFactor f

pFactor :: CFactor -> Arith
pFactor (CNeg f) = ASub (ANum "0") (pFactor f)
pFactor (FE e) = pExpr e
pFactor (CNum s) = ANum s



-- a "reflective" printer
-- the first approach I tried is that: divide the printer into three parts according to the concrete datatype defined above.
-- write the simple one that does not handle "reflective printing parenthese and negation" first.
-- then extend the printer with these features,
-- however, trying to keep the negation and the extra parentheses is more tricky than I think.
-- and they make the former simple pinter in a mess.
-- I could not conceive how mess and difficult it is to build a printer with more complex behaviour.
-- Now I think that the tricky part of BiYacc is not so tricky...
-- Finally it fails the QuickCheck and I have no idea to fix the bugs.


-- the second approach is to follow the behaviour of the program written in BiYacc's exactly.
-- it turned out to be a very good method and it passed the QuickCheck almost once it was done.
-- the problem is that usually it is too hard to come up with this nice idea without the prior knowledge of simultaneous pattern matching and adaptation.
-- another important issue is that it somehow fails to follow the structure of the naive printer.
-- also the parser is not derived but wrote by human.
-- and it cannot guarantee the round-trip properties by proving.
ppArithE :: Arith -> CExpr -> CExpr
ppArithE (AAdd ax ay) (CAdd cx cy) = CAdd (ppArithE ax cx)  (ppArithT ay cy)
ppArithE (ASub ax ay) (CSub cx cy) = CSub (ppArithE ax cx)  (ppArithT ay cy)
ppArithE a (ET t)                  = ET (ppArithT a t)
ppArithE a _ =
  case a of
    AAdd l r -> CAdd (ppArithE l defaultExpr) (ppArithT r defaultTerm)
    ASub l r -> CSub (ppArithE l defaultExpr) (ppArithT r defaultTerm)
    _        -> ET   (ppArithT a defaultTerm)

--
ppArithT :: Arith -> CTerm -> CTerm
ppArithT (AMul ax ay) (CMul cx cy) = CMul (ppArithT ax cx)  (ppArithF ay cy)
ppArithT (ADiv ax ay) (CDiv cx cy) = CDiv (ppArithT ax cx)  (ppArithF ay cy)
ppArithT a (TF t)                  = TF (ppArithF a t)
ppArithT a _ =
  case a of
    AMul l r -> CMul (ppArithT l defaultTerm) (ppArithF r defaultFactor)
    ADiv l r -> CDiv (ppArithT l defaultTerm) (ppArithF r defaultFactor)
    _        -> TF $ ppArithF a defaultFactor

--
ppArithF :: Arith -> CFactor -> CFactor
ppArithF (ASub (ANum "0") r) (CNeg f) = CNeg (ppArithF r f)
ppArithF (ANum as) (CNum cs)          = CNum as
ppArithF a (FE e') = FE (ppArithE a e')
ppArithF a _ =
  case a of
    (ASub (ANum "0") r ) -> CNeg (ppArithF r defaultFactor)
    ANum as              -> CNum as
    _                    -> FE   (ppArithE a defaultExpr)


-- generate default subtree structure. compared to "adaptation"
defaultExpr :: CExpr
defaultExpr = ENull


defaultTerm :: CTerm
defaultTerm = TNull


defaultFactor :: CFactor
defaultFactor = FNull



--test round trip properties by hands.
getPut :: CExpr -> Bool
getPut cterm =
  if cterm == ppArithE (pExpr cterm) cterm
     then True
     else False

putGet :: Arith -> CExpr -> Bool
putGet aterm cterm =
  if aterm == pExpr (ppArithE aterm cterm)
     then True
     else False


-- quick check
prop_GetPut :: CExpr -> Bool
prop_GetPut cterm = ppArithE (pExpr cterm) cterm == cterm
  where types = cterm :: CExpr

-- high potential to have problems here
-- maybe one of the two parameters should be fixed.
-- prop_PutGetFixed :: Arith-> Bool
-- prop_PutGetFixed aterm = pExpr (ppArithE aterm cterm) == aterm
--   where
--     cterm =
--       ET (TF (FE ( CSub (ET (TF (CNum "0"))) (TF (CNum "33")))))

prop_PutGet :: Arith -> CExpr -> Bool
prop_PutGet aterm cterm = pExpr (ppArithE aterm cterm) == aterm



-- data generator for CExpr
cexprg :: Gen CExpr
cexprg = sized cexprg'

cexprg' :: Int -> Gen CExpr
cexprg' 0 = liftM ET (ctermg' 0)
cexprg' n | n > 0 =
  oneof [liftM2 CAdd subcexprg subctermg
        ,liftM2 CSub subcexprg subctermg
        ,liftM ET subctermg]
  where
    subcexprg = cexprg' (n `div` 2)
    subctermg = ctermg' (n `div` 2)

ctermg :: Gen CTerm
ctermg = sized ctermg'

ctermg' :: Int -> Gen CTerm
ctermg' 0 = liftM TF (cfactorg' 0)
ctermg' n | n > 0 =
  oneof [liftM2 CMul subctermg subcfactorg
        ,liftM2 CDiv subctermg subcfactorg
        ,liftM TF subcfactorg]
  where
    subctermg = ctermg' (n `div` 2)
    subcfactorg = cfactorg' (n `div` 2)

cfactorg :: Gen CFactor
cfactorg = sized cfactorg'

cfactorg' :: Int -> Gen CFactor
cfactorg' 0 = liftM CNum arbitrary
cfactorg' n | n>0 =
  oneof [liftM CNeg subcfactorg
        ,liftM FE subcexprg
        ,liftM CNum arbitrary]
  where
    subcfactorg = cfactorg' (n `div` 2)
    subcexprg = cexprg' (n `div` 2)


instance Arbitrary CExpr where
  arbitrary = cexprg
-----------
-- data generator for Arith
arithg :: Gen Arith
arithg = sized arithg'

arithg' :: Int -> Gen Arith
arithg' 0 = liftM ANum arbitrary
arithg' n | n > 0 =
  frequency [(1, liftM2 AAdd subarithg subarithg)
            ,(1, liftM2 ASub subarithg subarithg)
            ,(1, liftM2 AMul subarithg subarithg)
            ,(1, liftM2 ADiv subarithg subarithg)
            ,(3, liftM ANum arbitrary)]
  where
    subarithg = arithg' (n `div` 2)

instance Arbitrary Arith where
  arbitrary = arithg





