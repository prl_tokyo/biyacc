% !TEX root = biyacc.tex

\section{Foundation for \biyacc: putback-based bidirectional transformations}
\label{sec:foundation}

The behaviour of \biyacc\ is totally nontrivial:
Not only do we need to generate two different programs from one, but we also need to guarantee that the two generated programs are consistent with each other, i.e., satisfy the properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}) stated in Section \ref{sec:introduction}.
It is possible to separately implement the $\mathit{print}$ and $\mathit{parse}$ semantics in an ad hoc way, but verifying the two consistency properties takes extra effort.
The implementation we present, however, is systematic and guarantees consistency by construction, thanks to the well-developed theory of \emph{bidirectional transformations} (BXs for short).
(See \cite{czarnecki2009bidirectional} and \cite{hu2011dagstuhl} for a comprehensive introduction.)


\subsection{Parsing and printing as bidirectional transformations}

The $\mathit{parse}$ and $\mathit{print}$ semantics of \biyacc\ programs are potentially \emph{partial} --- for example, if the actions in a \biyacc\ program do not cover all possible forms of program text and abstract syntax trees, $\mathit{parse}$ and $\mathit{print}$ will fail for those uncovered inputs.
Thus we should take partiality into account when choosing a BX framework in which to model $\mathit{parse}$ and $\mathit{print}$. The framework we use in this paper is an explicitly partial version of asymmetric lenses~\cite{foster2007combinators}:
A (well-behaved) \emph{lens} between a \emph{source} type~$S$ and a \emph{view} type~$V$ is a pair of functions $\mathit{get}$ and $\mathit{put}$, where
\begin{itemize}
\item the function $\mathit{get} :: S \to \mathsf{Maybe}\;V$ extracts a part of a source of interest to the user as a view, and
\item the function $\mathit{put} :: S \to V \to \mathsf{Maybe}\;S$ takes a source and a view and produces an updated source incorporating information from the view.
\end{itemize}
Partiality is explicitly represented by wrapping the result types in the $\mathsf{Maybe}$ monad.
The pair of functions should satisfy the \emph{well-behavedness} laws:
\begin{align*}
\mathit{put}\;s\;v = \mathsf{Just}\;\mathrlap{s'}\phantom{v} &\Rightarrow\quad \phantom{\mathit{put}\;s\;v}\mathllap{\mathit{get}\;s'} = \mathsf{Just}\;v \tag{\textsc{PutGet}} \label{equ:PutGet} \\
\mathit{get}\;s = \mathsf{Just}\;v &\Rightarrow\quad \mathit{put}\;s\;v = \mathsf{Just}\;s \tag{\textsc{GetPut}} \label{equ:GetPut}
\end{align*}
Informally, the \ref{equ:PutGet} law enforces that $\mathit{put}$ must embed all information of the view into the updated source, so the view can be recovered from the source by $\mathit{get}$, while the \ref{equ:GetPut} law prohibits $\mathit{put}$ from performing unnecessary updates by requiring that putting back a view directly extracted from a source by $\mathit{get}$ must produce the same, unmodified source.
The $\mathit{parse}$ and $\mathit{print}$ semantics of a \biyacc\ program will be the pair of functions $\mathit{get}$ and $\mathit{put}$ in a BX, satisfying the \ref{equ:PutGet} and \ref{equ:GetPut} laws by definition.
The well-behavedness laws are then exactly the consistency properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}) reformulated for a partial setting.


\subsection{Putback-based bidirectional programming}
%\todo{in order to understand the details about the implementation of \biyacc, we first introduce BX then \bigul.}

Having rephrased parsing and printing in terms of BXs, we can now easily construct consistent pairs of parsers and printers using \emph{bidirectional programming} techniques, in which the programmer writes a single program to denote the two directions of a well-behaved BX.
Specifically, \biyacc\ programs are compiled to the \emph{putback-based} bidirectional programming language \bigul~\cite{ko2016bigul}.
It has been formally verified in \textsc{Agda} \cite{norell2007towards} that \bigul\ programs always denote well-behaved BXs, and \bigul\ has been ported to \textsc{Haskell} as an embedded DSL library, which will be introduced in more detail in Section \ref{sec:programming-in-BiGUL}.
\bigul\ is putback-based, meaning that a \bigul\ program describes a $\mathit{put}$ function, but --- since \bigul\ is bidirectional --- can also be executed as the corresponding $\mathit{get}$ function.
The advantage of putback-based bidirectional programming lies in the following theorem~\cite{foster2010bidirectional}:
\begin{theorem}
Given a $\mathit{put}$ function, there is at most one $\mathit{get}$ function that forms a (well-behaved) BX with this $\mathit{put}$ function.
\end{theorem}
\noindent That is, once we describe a $\mathit{put}$ function in \bigul, not only can we immediately obtain a $\mathit{get}$ function satisfying \ref{equ:PutGet} and \ref{equ:GetPut} with the $\mathit{put}$ function, but also guarantee that the $\mathit{get}$ function is unique, i.e., completely determined by the $\mathit{put}$ function.
Due to this theorem, in this paper we can focus solely on the printing ($\mathit{put}$) behaviour, leaving the parsing ($\mathit{get}$) behaviour only implicitly (but unambiguously) specified.


%
\label{sec:programming-in-BiGUL}
%
\bigul~\cite{ko2016bigul} is a \emph{putback-based} bidirectional programming language.
It has been formally verified in \textsc{Agda} \cite{norell2007towards} that \bigul\ programs always denote well-behaved BXs.
\bigul\ is putback-based, meaning that a \bigul\ program describes a $\mathit{put}$ function, but --- since \bigul\ is bidirectional --- can also be executed as the corresponding $\mathit{get}$ function.
The specific \bigul\ that we used for the \biyacc\ implementation, is an embedded DSL library of the \textsc{Haskell} port version.
Here we introduce three constructs in \bigul\, to which a \biyacc\ program is compiled to.

A \bigul\ program has type \lstinline{BiGUL s v}, where \lstinline{s} and \lstinline{v} are respectively the source and view types; its $\mathit{put}$ interpreter can then be given the type
\begin{lstlisting}
put :: BiGUL s v -> s -> v -> Maybe s
\end{lstlisting}
%
\paragraph*{Replace.}
The simplest \bigul\ operation we use is
\begin{lstlisting}
Replace :: BiGUL s s
\end{lstlisting}
which discards the original source and returns the view --- which has the same type as the source --- as the updated source.
That is,
\begin{lstlisting}
put Replace _ v = v
\end{lstlisting}

%
\paragraph*{Update.}
The next operation \lstinline{update} is more complex, and is implemented with the help of Template Haskell \cite{sheard2002template}.
The general form of the operation is
\begin{lstlisting}
   $(update [p| (*@$\mathit{spat}$@*) |] [p| (*@$\mathit{vpat}$@*) |] [d| (*@$\mathit{bs}$@*) |])
:: BiGUL s v
\end{lstlisting}
This operation decomposes the source and view by pattern matching with the patterns $\mathit{spat}$ and $\mathit{vpat}$ respectively, pairs the source and view components as specified by the patterns (see below), and performs further \bigul\ operations listed in $\mathit{bs}$ on the source--view pairs.
The way to determine which source and view components are paired and which operation is performed on a pair is by looking for the same names in the three arguments --- for example, this \lstinline{update} operation
\begin{lstlisting}
$(update [p| (x, _) |] [p| x |] [d| x = Replace |])
\end{lstlisting}
pairs first component of the source with the view, since both are matched with~\lstinline{x};
the \lstinline{Replace} operation is then performed on this pair,
since it is the operation associated with~\lstinline{x} in the (singleton) list of operations%
\footnote{This representation of lists of named \bigul\ operations is admittedly an abuse of syntax, but simplifies prototyping this system with Template Haskell.}.
In general, any (type-correct) \bigul\ program can be used in the list of further updates, not just the primitive \lstinline{Replace}.
In the source pattern, the part marked by underscore (\lstinline{_}) simply means that it will be skipped during the update.

%
\paragraph*{Case.}
The most complex operation we use is \lstinline{Case} for doing case analysis on the source and view:
\begin{lstlisting}
Case :: [Branch s v] -> BiGUL s v
\end{lstlisting}
\lstinline{Case} takes a list of branches, of which there are two kinds: \emph{normal} branches and \emph{adaptive} branches.
For a normal branch, we should specify a main condition using a source pattern $\mathit{spat}$ and a view pattern $\mathit{vpat}$, and an exit condition using a source pattern $\mathit{spat'}$:
\begin{lstlisting}
$(normalSV [p| (*@$\mathit{spat}$@*) |] [p| (*@$\mathit{vpat}$@*) |] [p| (*@$\mathit{spat'}$@*) |]) ::
BiGUL s v -> Branch s v
\end{lstlisting}
An adaptive branch, on the other hand, only needs a main condition:
\begin{lstlisting}
$(adaptiveSV [p| (*@$\mathit{spat}$@*) |] [p| (*@$\mathit{vpat}$@*) |]) ::
(s -> v -> s) -> Branch s v
\end{lstlisting}
Their $\mathit{put}$ semantics are as follows: A branch is applicable when the source and view respectively match $\mathit{spat}$ and $\mathit{vpat}$ in its main condition.
Execution of a \lstinline{Case} chooses the first applicable branch from the list of branches, and continues with that branch.
When the chosen branch is normal, the associated \bigul\ operation is performed, and the updated source should satisfy the exit condition $\mathit{spat'}$ (otherwise it is a runtime error)%
\footnote{The exit condition is an over-approximation of the range of this branch, so it is possible to check that the ranges of the branches in the same \lstinline{Case} statement are disjoint, as is standard for bidirectional or reversible programs.
In general, \bigul's semantics incorporates several kinds of runtime checks for guaranteeing bidirectionality, and it is the programmer's responsibility to ensure that these checks can succeed.};
when the chosen branch is adaptive, the associated function is applied to the source and view to compute an adapted source, and the whole \lstinline{Case} is rerun on the adapted source and the view, and should go into a normal branch this time.
Think of an adaptive branch as bringing a source that is too mismatched with the view to a suitable shape so that a normal branch --- which deals with sources and views in some sort of correspondence --- can take over.
This adaptation mechanism is used by \biyacc\ to print an AST when the source program text is too different from the AST or even nonexistent at all.
