\section{Language design of \biyacc}
\label{sec:design}

This section presents the language design of \biyacc\ by a running example handling arithmetic expressions previously shown in Figure \ref{fig:exp_reflective_printing}.
We briefly summarise the definition of the syntax of \biyacc\ programs in Section \ref{sec:def_biyacc_syntax}.
In Section \ref{sec:biyacc_program}, we present the structure, the syntax, and the semantics of \biyacc\ programs from a high-level aspect by writing the program for the arithmetic expression problem.

%
\subsection{Definition of \biyacc's syntax}
\label{sec:def_biyacc_syntax}
%
The syntax of \biyacc\ programs is shown in Figure \ref{fig:biyaccSyntax}, where the definition is rather straightforward and can be treated as production rules.
Anything wrapped in single quotes is string literal, which is basically keywords and built-in symbols of \biyacc.
$\nt{Terminal}$s are usually keywords and symbols in a user-defined language (such as `+', `-', `let', etc) and should be put within single quotes in a \biyacc\ program,
A $\nt{Nonterminal}$ is a symbol that could be expanded to a list of $\nt{Symbol}$s in the usual sense.
$\nt{HsDeclarations}$ are Haskell declarations, and $\nt{HsPattern}$ and $\nt{HsType}$ denotes respectively a Haskell pattern and a Haskell datatype that should conform to the datatype definitions in the $\nt{HsDeclarations}$ part.
Their definitions follow the syntax of Haskell and are thus omitted here.
A rare notation can be found in $\nt{ProductionBody}^+\{\t{|}\}$ where the bracket part means that one or more occurrence of $\nt{ProductionBody}$ is divided by the vertical bar ($\t{|}$).
Spaces (including newline) merely means string concatenation.



\begin{figure}[h]
\begin{small}
\begin{align*}
\nt{Program} &\Coloneqq \t{Abstract}\ \nt{HsDeclarations} \\[-0.75ex]
             &\phantom{\null\Coloneqq\null} \t{Concrete}\ \nt{ProductionGroup}^+ \\[-0.75ex]
             &\phantom{\null\Coloneqq\null} \rlap{\t{Actions}}\phantom{\t{Concrete}}\ \nt{ActionGroup}^+ \\
\nt{ProductionGroup} &\Coloneqq \nt{Nonterminal}\ \t{->}\ \\[-0.75ex]
 &\phantom{\null\Coloneqq\null}\nt{ProductionBody}^+\{\t{|}\}\ \t{;} \\
\nt{ProductionBody} &\Coloneqq Symbol^+ \\
\nt{Symbol} &\Coloneqq \nt{Primitive} \mid \nt{Terminal} \mid \nt{Nonterminal} \\
%\nt{Primitive} &\Coloneqq \texttt{Name} \mid \texttt{String} \mid \texttt{Int} \mid \texttt{Float} \\
%
\nt{ActionGroup} &\Coloneqq \nt{HsType}\ \t{+>}\ \nt{Nonterminal} \\[-0.75ex]
                 &\phantom{\null\Coloneqq\null} \nt{Action}^+ \\
\nt{Action} &\Coloneqq \nt{HsPattern}\ \t{+>}\ \nt{Update}^+\ \t{;} \\
\nt{Update} &\Coloneqq \nt{Symbol} \\
            &\rlap{\hskip.75em$|$}\phantom{\null\Coloneqq\null} \t{(}\ \nt{HsVariable}\ \t{+>}\ \\[-0.75ex]
            &\phantom{\Coloneqq\t{(}\ } \nt{UpdateCondition}\ \t{)} \\
            &\rlap{\hskip.75em$|$}\phantom{\null\Coloneqq\null} \t{(}\ \nt{Nonterminal}\ \t{->}\ \nt{Update}^+\ \t{)} \\
\nt{UpdateCondition} &\Coloneqq \nt{Symbol} \\
       &\rlap{\hskip.75em$|$}\phantom{\null\Coloneqq\null} \t{(}\ \nt{Nonterminal}\ \t{->}\ \\[-0.75ex]
       &\phantom{\Coloneqq\t{(}\ } \nt{UpdateCondition}^+\ \t{)}
\end{align*}
\end{small}
\caption{Syntax of \biyacc\ programs}
\small{Nonterminals with prefix $\mathit{Hs}$ denote Haskell entities and follow the Haskell syntax; the notation $\mathit{nt}^+\{\mathit{sep}\}$ denotes a nonempty sequence of the same nonterminal $\mathit{nt}$ separated by $\mathit{sep}$; \lstinline{Name} represents an identifier starting with a character and followed by characters and numbers.}
\label{fig:biyaccSyntax}
\end{figure}



%
\subsection{Programs in \biyacc}
\label{sec:biyacc_program}

A \biyacc\ program consists of three parts: abstract syntax definition, concrete syntax definition, and actions describing how to update a concrete syntax tree (CST) with an AST.



\subsubsection{Defining the abstract and concrete syntax}
%
The abstract syntax part --- which starts with the keyword \lstinline{Abstract} --- is just one or more definitions of Haskell datatypes.
In our example, the abstract syntax is defined in lines 1--9 by a single datatype \lstinline{Arith} whose elements are constructed from integer constants, variables, and the arithmetic operators.
Different constructors --- namely \lstinline{Add}, \lstinline{Sub}, \lstinline{Mul}, \lstinline{Div}, and \lstinline{Num} --- are used to construct different kinds of expressions, and in the definition each constructor is followed by the types of arguments it takes.
Hence the constructors \lstinline{Add}, \lstinline{Sub}, \lstinline{Mul}, and \lstinline{Div} take two subexpressions (of type \lstinline{Arith}) as arguments,
while the constructors \lstinline{Var} and \lstinline{Num} respectively takes an \lstinline{String} and \lstinline{Int} as their arguments.
For instance, the expression ``$1 + 2 * 3 - 4$'' can be represented as this AST of type \lstinline{Arith}:
\begin{lstlisting}
Sub (Add (Num 1)
         (Mul (Num 2)
              (Num 3)))
    (Num 4)
\end{lstlisting}

On the other hand, the concrete syntax --- which is defined in the second part beginning with the keyword \lstinline{Concrete} --- is defined by a context-free grammar,
i.e., a set of production rules specifying how nonterminal symbols can be expanded to sequences of terminal and nonterminal symbols.
For our expression example, in lines 16--27 we use a standard grammatical structure to encode operator precedence and order of association, which involves three nonterminal symbols \lstinline{Expr}, \lstinline{Term}, and \lstinline{Factor}:
An \lstinline{Expr} can produce a left-leaning tree of \lstinline{Term}s, each of which can in turn produce a left-leaning tree of \lstinline{Factor}s.
To produce right-leaning trees or operators of lower precedence under those with higher precedence, the only way is to reach for the last production rule \lstinline[breaklines=true]{Factor -> '(' Expr ')'}, resulting in parentheses in the produced program text.
(There is also a nonterminal $\mathit{Name}$, which produces identifiers.)

\paragraph*{Syntax for comments.}
Syntax for comments can be declared at the beginning of this part before any production rules.
For example,
line~13 shows that the syntax for a single line comments is ``\lstinline!//!'', while line~14 states that ``\lstinline!/*!'' and ``\lstinline!*/!'' are respectively the beginning mark and ending mark for a block comment.

\begin{figure}[h]
\begin{center}
\begin{tabular}{c}
{\begin{lstlisting}[numbers=left, numberstyle=\tiny, emphstyle={\bfseries}]
Abstract

data Arith = Num Int
           | Var String
           | Add Arith Arith
           | Sub Arith Arith
           | Mul Arith Arith
           | Div Arith Arith
  deriving (Show, Eq, Read)

Concrete

%commentLine  '//'      ;
%commentBlock '/*' '*/' ;

Expr   -> Expr '+' Term
        | Expr '-' Term
        | Term ;

Term   -> Term '*' Factor
        | Term '/' Factor
        | Factor ;

Factor -> '-' Factor
        | Int
        | Name
        | '(' Expr ')' ;
\end{lstlisting}}
\end{tabular}
\end{center}
\caption{Syntax definition parts of a \biyacc\ program}
\label{fig:expr}
\end{figure}


%
\subsubsection{Defining actions}

The last and main part of a \biyacc\ program starts with the keyword \lstinline{Actions}, and describes how to update a CST --- i.e., a piece of program text --- with an AST.
For our expression example, the actions are defined in lines 29--45 in Figure \ref{fig:expr_actions}.
Before explaining the actions, we should emphasise that we are identifying program text with CSTs:
Conceptually, whenever we write a piece of program text, we are actually describing a CST rather than just a sequence of characters.
Technically, it is almost effortless to convert a piece of program text to a CST with existing parser technologies
(we follow the normal convention for parser-generating systems that grammars must be unambiguous);
the reverse direction is even easier, requiring only a traversal of the CST.
By integrating with existing parser technologies, \biyacc\ actions can focus on describing conversions between CSTs and ASTs --- the more interesting part in the tasks of parsing and printing.
We will expound on this identification of program text with CSTs in Section \ref{sec:concrete_ppr}.

\begin{figure}[h]
\begin{center}
\begin{tabular}{c}
{\begin{lstlisting}[numbers=left, numberstyle=\tiny, emphstyle={\bfseries}, firstnumber=29]
Actions

Arith +> Expr
Add x y  +>  (x +> Expr) '+' (y +> Term);
Sub x y  +>  (x +> Expr) '-' (y +> Term);
arith    +>  (arith +> Term);

Arith +> Term
Mul x y  +>  (x +> Term) '*' (y +> Factor);
Div x y  +>  (x +> Term) '/' (y +> Factor);
arith    +>  (arith +> Factor);

Arith +> Factor
Sub (Num 0) y  +>  '-' (y +> Factor);
Num i          +>  (i +> Int);
Var n          +>  (n +> Name);
arith          +>  '(' (arith +> Expr) ')';
\end{lstlisting}}
\end{tabular}
\end{center}
\caption{Synchronisation strategy part of a \biyacc\ program}
\label{fig:expr_actions}
\end{figure}

The \lstinline{Actions} part consists of groups of actions, and each group begins with a ``type declaration'' of the form $\mathit{hsType}$ $\t{+>}$ $\mathit{nonterminal}$ stating that the actions in this group specify updates on CSTs generated from $\mathit{nonterminal}$ using ASTs of type $\mathit{hsType}$.
Informally, given an AST and a CST, the semantics of an action is to perform pattern matching simultaneously on both trees, and then use components of the AST to update corresponding parts of the CST, possibly recursively.
(The syntax~`\lstinline{+>}' suggests that information from the left-hand side is embedded into the right-hand side.)
Usually the nonterminals in a right-hand side pattern are overlaid with update instructions, which are also denoted by \t{+>}.

%
Let us look at a specific action --- the first one for the expression example, at line~32 of Figure \ref{fig:expr_actions}:
\begin{lstlisting}
Add x y +> (x +> Expr) '+' (y +> Term);
\end{lstlisting}
For the view pattern \lstinline{Add x y}, an AST (of type \lstinline{Arith}) is said to match the pattern when it starts with the constructor \lstinline{Add} (because the variable pattern, for example \lstinline{x} and \lstinline{y}, matches anything);
if the match succeeds, the two arguments of the constructor (i.e., the two subexpressions of the addition expression) are then respectively bound to the variables \lstinline{x}~and~\lstinline{y}.
As for the CST pattern, the main intention is to refer to the production rule \lstinline{Expr -> Expr '+' Term}
and use it to match those CSTs produced by this rule.
Since the action belongs to the group \lstinline[breaklines=true]{Arith +> Expr}, the part `\lstinline{Expr ->}' of the production rule can be inferred, and thus is not included in the CST pattern.
Finally we overlay `\lstinline{x +>}' and `\lstinline{y +>}' on the nonterminal symbols \lstinline{Expr} and \lstinline{Term} to indicate that, after the simultaneous pattern matching succeeds, the subtrees \lstinline{x}~and~\lstinline{y} of the AST are respectively used to update the left and right subtrees of the CST.

%
\paragraph*{Semantics of the program.} Having explained what an action means, we can now explain the semantics of the entire program.
Given an AST and a CST as input, first a group of actions is chosen according to the types of the trees.
Then the actions in the group are tried in order, from top to bottom, by performing simultaneous pattern matching on both trees.
If pattern matching for an action succeeds, the updating operations specified by the action is executed; otherwise the next action is tried.
Execution of the program ends when the matched action specifies either no updating operations or only updates to primitive datatypes such as \lstinline{Int}.
\biyacc's most interesting behaviour shows up when all actions in the chosen group fail to match --- in this case a suitable CST will be created.
The specific approach adopted by \biyacc\ is to perform pattern matching on the AST only and choose the first matched action.
A suitable CST conforming to the CST pattern is then created, and after that the whole group of actions is tried again.
This time the pattern matching should succeed at the action used to create the CST, and the program will be able to make further progress.


%
\paragraph*{Complex patterns.}
It is interesting to note that, by using more complex patterns, we can write actions that establish nontrivial relationships between CSTs and ASTs.
For example, the action at line~42 of Figure \ref{fig:expr_actions} associates abstract subtraction expressions whose left operand is zero with concrete negated expressions; this action is the key to preserving negated expressions in the CST.
For an example of a more complex CST pattern:
Suppose that we want to write a pattern that matches those CSTs produced by the rule \lstinline[breaklines=true]{Factor -> '-' Factor},
where the inner nonterminal \lstinline{Factor} produces a further \lstinline{'-' Factor} using the same rule.
This pattern is written by overlaying the production rule on the first nonterminal \lstinline{Factor} (an additional pair of parentheses is required for the expanded nonterminal): \lstinline{'-' (Factor -> '-' Factor)}.
More examples involving this kind of deep patterns will be presented in Section \ref{sec:case_studies}.


%
\paragraph*{Layouts and comments preservation.}
The reflective printer generated by \biyacc\ is capable of preserving layouts and comments, but, perhaps mysteriously, in Figure \ref{fig:expr} and Figure \ref{fig:expr_actions} there is no clue as to how layouts and comments are preserved.
This is because we decide to hide layout preservation from the programmer, so that the more important logic of abstract and concrete syntax synchronisation is not cluttered with layout preserving instructions.
Our current approach is fairly simplistic:
We store layout information following each terminal in an additional field in the CST implicitly, and treat comments in the same way as layouts%
\footnote{One might argue, though, that layouts and comments should in fact be handled differently, since comments are usually attached to some entities which they describe.
For example, when a function declaration is moved to somewhere else (e.g., by a refactoring tool), we will want the comment describing that function to be moved there as well.
We leave the proper treatment of comments as future work.}.
During the printing stage, if the pattern matching on an action succeeds, the layouts and comments after the terminals shown in the right-hand side of that action are preserved;
on the other hand, layouts and comments are dropped when a CST is created in the situation where pattern matching fails for all actions in a group.
The layouts and comments before the first terminal are always kept during the printing.
More details about this treatment and some exceptions can be found in Section \ref{sec:implementation}.


%
% \paragraph*{Usefulness of the concrete syntax.}
% Observant readers may have noticed that, the production rules in the concrete syntax part resemble the right-hand sides of the actions.
% In fact these production rules in the concrete syntax part can be omitted, and they will be automatically generated by extracting the right-hand sides of all actions.
% However, we recommend the users still write these production rules, so that \biyacc\ can perform some simple checks:
% It will make sure that in an action group, users have covered all of the production rules of the nonterminal appearing in the ``type declaration'', and will never use undefined production rules.

% \subsubsection{Running the program on a specific input}
% \label{running_exp}
% %
% To get a solid feeling of how \biyacc\ works, let us go through the execution of the program on the program text and optimised AST shown previously in Figure \ref{fig:exp_reflective_printing} to see how the updated program text is produced.

% %
% Initially the types of the two trees are assumed to match those declared for the first group of actions, and hence we try the first action in the group, at line~32.
% However, since the AST is a \lstinline{Mul} and the first production rule used for the CST is \lstinline[breaklines=true]{Expr -> Term} (followed by \lstinline[breaklines=true]{Term -> Term '*' Factor}), pattern matching fails on both the abstract and concrete sides.
% So, other actions in this group are tried in order, from top to bottom: We fail at line~33 but succeed at line~34.
% Now the update specified by this action is to proceed with updating the subtree produced from \lstinline{Term}, so we move on to the second group.
% This time the action at line~37 matches, meaning that we should update the subtrees \lstinline{-a}~and \lstinline{(1 + 1 + (a))} with \lstinline[breaklines=true]{Sub (Num 0) (Var "a")} and \lstinline{Add (Num 2) (Var "a")} respectively.
% Note that at this point the layout information after the terminal \lstinline{*} is preserved.

% For the update on the left-hand side of the multiplication, we again try the second group (according to its ``type'': a \lstinline{Term}), and the action at line~39 matches.
% Then we go through the actions at line~42 --- preserving the negation --- and line~44, eventually updating the variable \lstinline{a} in the CST with \lstinline{Var "a"} in the AST and preserving the comments after it.
% (The update is still performed even though the concrete and abstract sides are the same.)
% %\todo{why? because the information in the view should be fully embedded into the source.}
% As for the update on the right-hand side of the multiplication, the action at line~45 matches.
% Now the parentheses are kept and we are updating the subtree \lstinline{1 + 1 + (a)} produced from the \lstinline{Expr} inside the parentheses with the AST \lstinline{Add (Num 2) (Var "a")}.

% %\todo{Revise.}
% Similarly, the action at line~32 matches, so \lstinline{1 + 1} is then to be updated by \lstinline{Num 2}, and \lstinline{(a)} by \lstinline{Var "a"}.
% As for \lstinline{1 + 1} and \lstinline{Num 2}, since none of the actions matches, a CST in the form of \lstinline{Expr -> Term} is created according to the AST pattern at line~34, and we go into the second group.
% The remaining execution sequences are as follows: Actions at lines 37, 38, and~39 fail to match, so the CST is reshaped to a \lstinline{Factor} at line~39.
% Actions at lines 42, 43, 44, and~45 fail, and the CST is adapted to an \lstinline{Int} at line~43.
% The newly created \lstinline{Int} is eventually replaced by the integer $2$ in the AST, and a default layout (a space) is inserted after it.
% This is why the updated result for this part is ``\lstinline{2 }''.
% For the update on \lstinline{(a)} with \lstinline{Var "a"} , we are going to update \lstinline{(a)} with \lstinline{Var "a"}.
% The program goes through actions at lines 39, 45, 34, 39, and~44.



\paragraph*{Parsing semantics.}
So far we have been describing the reflective printing semantics of the \biyacc\ program, but we may also work out its parsing semantics intuitively by interpreting the actions from right to left, converting the production rules to the corresponding constructors.
(This might remind the reader of the usual \textsc{Yacc} actions.)
In fact, this paper will not define the parsing semantics formally, because the parsing semantics is completely determined by the reflective printing semantics:
If the actions are written with the intention of establishing some relation between the CSTs and ASTs, then BiYacc will be able to derive the only well-behaved parser, which respects that relation.
We will explain how this is achieved in the next section.
