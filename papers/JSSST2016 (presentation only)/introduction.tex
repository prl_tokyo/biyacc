% !TEX root = biyacc.tex


\section{Introduction}
\label{sec:introduction}

Whenever we come up with a new programming language, as the front-end part of the system we need to design and implement a parser and a printer to convert between program text and an internal representation.
A piece of program text, while conforming to a \emph{concrete syntax} specification, is a flat string that can be easily edited by the programmer.
A parser extracts the tree structure from such a string to a concrete syntax tree (CST), and converts it to an \emph{abstract syntax} tree (AST),
which is a structured and simplified representation and is easier for the back-end to manipulate.
On the other hand, a printer converts an AST back to a piece of program text, which can be understood by the user of the system;
this is useful for debugging the system, reporting internal information to the user, or observing the optimisations performed on the AST by the back-end of the compiler \cite{pombrio2014resugaring}.

%
Parsers and printers do conversions in opposite directions and are intimately related --- for instance, the program text printed from an AST should be parsed to the same tree.
It is certainly far from being economical to write parsers and printers separately:
The parser and printer need to be revised from time to time as the language evolves,
and each time we must revise both of the two components and keep them consistent with each other, which is a time-consuming and error-prone task.
In response to this problem, many domain-specific languages~\cite{Bout96,Viss97,rendel2010invertible,duregaard2011embedded,matsuda2013flippr} have been proposed,
in which the user can describe both a parser and a printer in a single program.
By unifying these two pieces of software and deriving them from single and centralised code, we are creating a unified environment,
which is easier to maintain and update, therefore respecting the ``Don't Repeat Yourself'' principle of software development~\cite{HuntThomas99}.

%
Despite their advantages, these domain-specific languages cannot deal with synchronisation between program text and ASTs,
in the sense that a printer will always produce a new piece of program text from scratch.
Let us look at a concrete example in Figure \ref{fig:exp_reflective_printing}:
%
\begin{figure}
Original program text:
\begin{lstlisting}
// the expression evaluates...
-a /* a is the variable denoting(*@\ldots@*)*/ *
(1 + 1 + (a))
\end{lstlisting}
%
Abstract syntax tree:
\begin{lstlisting}
Mul (Sub (Num 0) (Var "a"))
    (Add (Add (Num 1) (Num 1)) (Var "a"))
\end{lstlisting}
%
Optimised abstract syntax tree:
\begin{lstlisting}
Mul (Sub (Num 0) (Var "a"))
    (Add (Num 2) (Var "a"))
\end{lstlisting}
%
Printed result from a \emph{conventional} printer:
\begin{lstlisting}
(0 - a) * (2 + a)
\end{lstlisting}
%
Printed result from an ideal printer:
\begin{lstlisting}
// the expression evaluates...
-a /* a is the variable denoting(*@\ldots@*) */ *
(2 + (a))
\end{lstlisting}
\caption{Conventional printer and reflective printer}
\label{fig:exp_reflective_printing}
\end{figure}
%
The original program text is an arithmetic expression, containing negation, (redundant) parentheses, and some comments.
It is first parsed to an AST (supposing that addition is left-associative) where the negation is desugared to a subtraction, parentheses are implicitly represented by the tree structure, and the comments are thrown away.
Then the AST is optimised by replacing \lstinline{Add (Num 1) (Num 1)} with a constant \lstinline{Num 2}.
The user may want to observe the optimisation made by the compiler, but the AST is an internal representation that is hard for humans to read and is usually not exposed to the user.
%
So a natural idea is to \emph{reflect} the change on the AST back to the program text to make it easier for the user to check where the changes are.
With a conventional printer, immediately a problem occurs,
as the printed result will likely mislead the programmer into thinking that the negation is replaced by a subtraction by the compiler;
in addition, since the comments are not preserved, it will be harder for the programmer to compare the updated and original versions of the text.
The problem illustrated here also occurs in many other practical situations where the parser and printer are used as a bridge between the system and the user, for example,
\begin{itemize}
  \item in program debugging where part of the original program in an error message is displayed much differently from its original form \cite{de2002pretty,kort2003parse}, and
  \item in program refactoring where the comments and layouts in the original program are completely lost after the AST is transformed by the system \cite{de2011algorithm}.
\end{itemize}

%
To make the printed result better, many attempts have been made to enrich ASTs with more information.
To see a particular instance, Pombrio and Krishnamurthi \cite{pombrio2014resugaring} propose the notion \emph{resugaring}, which devotes to representing evaluation sequences in a core language in terms of its surface syntax.
Despite the fact that by applying these methods, the syntactic sugar (\lstinline{-a}) in this example can be preserved during the printing,
these methods contaminate ASTs by injecting more than necessary information.
As a results, the compiler is hard to perform optimisation, and other tools designed for the language must also carefully handle the additional information in AST.

%
We aim to address these problems:
\begin{itemize}
  \item unifying the design of parsers and printers,
  \item reflecting changes properly into the original program text when printing, and
  \item keeping the AST clean and neat.
\end{itemize}
by proposing a domain-specific language \biyacc, which lets the user describe both a parser and a \emph{reflective} printer for an unambiguous grammar in a single program.
Different from a conventional printer, a reflective printer will take a piece of program text and an AST,
which is usually slightly modified from the AST corresponding to the program text, and reflects the modification to the program text.
Meanwhile the comments (and layouts) in the unmodified parts of the program text can all be preserved.
A taste of the reflective printing can be seen clearly from the above arithmetic expression example as shown in Figure \ref{fig:exp_reflective_printing}.
It is worth noting that reflective printing is a generalisation of the conventional notion of printing, because a reflective printer is able to accept an AST and an \emph{empty} piece of program text,
in which case it will behave as a conventional printer producing a new piece of program text depending on the AST only.


From a \biyacc\ program we can generate a parser and a reflective printer; in addition, we want to guarantee that the two generated programs are \emph{consistent} with each other.
Specifically, we want to ensure two inverse-like properties:
Firstly, a piece of program text~$s$ printed from an abstract syntax tree~$t$ should be parsed to the same tree~$t$, i.e.,
\begin{equation}
\mathit{parse}\;(\mathit{print}\;s\;t) = t \label{equ:BXPrintParse}
\end{equation}
Secondly, updating a piece of program text~$s$ with an abstract syntax tree parsed from~$s$ should leave~$s$ unmodified (including formatting details like parentheses and spaces), i.e.,
\begin{equation}
\mathit{print}\;s\;(\mathit{parse}\;s) = s \label{equ:BXParsePrint}
\end{equation}
These two properties are inspired by the theory of \emph{bidirectional transformations} \cite{czarnecki2009bidirectional,hu2011dagstuhl}, and are guaranteed by construction for all \biyacc\ programs.

%
An online tool that implements the approach described in this paper can be accessed at {\small\url{http://www.prg.nii.ac.jp/project/biyacc.html}},
which contains two input examples with various test cases used in this paper.

%
The rest of the paper is organised as follows:
\begin{itemize}[leftmargin=*]
  \item We first give an overview of \biyacc\ in Section \ref{sec:design}, explaining how to describe in a single program both a parser and a reflective printer for synchronising program text and its abstract syntax representation.
  \item After reviewing some background on bidirectional transformations in Section \ref{sec:foundation}, in particular the bidirectional programming language \bigul~\cite{ko2016bigul}, we give the semantics of \biyacc\ by compiling it to \bigul\ in Section \ref{sec:implementation}, guaranteeing the properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}) by construction.
  \item We present a case study in Section \ref{sec:case_studies},
        showing that even though \biyacc\ is currently restricted to dealing with unambiguous grammars, it is capable of describing \tiger\ \cite{Appel98}, which shares many similarities with full grown, widely used languages.
        We demonstrate that \biyacc\ can handle syntactic sugar, partially subsume Pombrio and Krishnamurthi's ``resugaring'' \cite{pombrio2014resugaring}, and facilitate language evolution.
  \item Related work including detailed comparison with other systems is presented in Section \ref{sec:related_work} and in Section \ref{sec:conclusion_future_work} we conclude the paper and present some future work.
\end{itemize}

