\section{Implementation of \biyacc}
\label{sec:implementation}

This section provides a prototype implementation of \biyacc\ in detail.
The architecture of \biyacc\ is shown in Figure \ref{fig:biyaccArchitecture}:
On the surface, the programmer supplies a three-part \biyacc\ program as described in Section \ref{sec:biyacc_program}, which is compiled into an executable for performing synchronisation between program text and ASTs.
Since it is difficult to synchronise non-structured program text and structured ASTs directly,
we make use of existing technologies and separate the synchronisation into two phases.
The synchronisation now becomes the composition of two bidirectional transformations, one between program text and CSTs and the other between CSTs and ASTs.
The second BX is constructed as a \bigul\ program and is well-behaved by construction; we will explain in Section \ref{sec:programming-in-BiGUL} the basics of \bigul\ and in Section \ref{sec:gen-BiGUL} how this \bigul\ program is derived.
%
The first BX, on the other hand, is in fact an isomorphism, which we construct in a more ad hoc manner.
More specifically, we derive a lexer and a parser (using the parser generator \textsc{Happy}) for converting program text to CSTs, and a printer for flattening CSTs to program text; this isomorphism, which we call \emph{concrete parsing and printing}, will be explained in Section \ref{sec:inverse-properties} and Section \ref{sec:concrete_ppr}.
As is well known, isomorphisms are special cases of BXs, and the composition of two BXs is again a BX, so the composite transformations are still well-behaved.

%
The grammar accepted by \biyacc\ is restricted to pure LALR (1) without disambiguation rules in the current implementation.
And the well-behavedness of a \biyacc\ program is guaranteed by the compiled \bigul\ program,
which means that sometimes \bigul\ will raise a runtime error for the bad transformations not satisfying the properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}).
In the future, we plan to extend \biyacc\ by making more static checks, and supporting disambiguation declarations or employing other parser generators so that it can handle a wider class of grammars.


\begin{figure}
\centering
\includegraphics[scale=0.8, clip = true, trim = 6cm 10.8cm 6cm 11cm]{img/biyacc-arch.pdf}
\caption{\small{Architecture of \biyacc}}
\label{fig:biyaccArchitecture}
\end{figure}


%
\subsection{Generating the \bigul\ program}
\label{sec:gen-BiGUL}

\begin{figure*}
% \setlength{\mathindent}{0em}
\begin{equation*}
\begin{aligned}
&\sem{\t{Abstract}\ \var{decls}\ \t{Concrete}\ \var{pgs}\ \t{Action}\ \var{ags}}_\nt{Program} = \\[-1ex]
&\quad \nt{decls}\ \iter{\var{pg}}{\var{pgs}}{\sem{\var{pg}}_\var{ProductionGroup}}\ \iter{\var{ag}}{\var{ags}}{\sem{\var{ag}}_\nt{ActionGroup}} \\
&\sem{\var{nt}\ \t{->}\ \var{bodies}}_\nt{ProductionGroup} = \\[-1ex]
&\quad \t{data}\ \var{nt}\ \t{=} \ \iter{\var{syms}}{\var{bodies}}{\fun{con}(\var{nt}, \var{syms})\ \iter{\var{s}}{\var{syms}}{\fun{field}(\var{s})}\ \t|} \fun{nullCon}(\var{nt}) \\
%
&\sem{\var{vt}\ \t{+>}\ \var{st}\ \var{acts}}_\nt{ActionGroup} = \\[-1ex]
&\quad \fun{prog}(\var{vt}, \var{st})\ \t{::}\ \t{BiGUL}\ \var{st}\ \var{vt} \\[-1ex]
&\quad \fun{prog}(\var{vt}, \var{st})\ \t{=}\ \t{Case} \t{[}\ \iter{\var a}{\var{acts}}{\sem{a}_\nt{Action}^{\mathsf N, \var{vt}, \var{st}}\ \t{,}}\ \iter{\var a}{\var{acts}}{\sem{a}_\nt{Action}^{\mathsf A,\, \var{st}}}\{\t{,}\}\ \t{]} \\
%
&\sem{\var{vpat}\ \t{+>}\ \var{updates}}_\nt{Action}^{\mathsf N, \var{vt}, \var{st}} = \\[-1ex]
&\quad \t{\$(normalSV} \\[-1ex]
&\qquad \t{[p|}\ \fun{srcCond}(\fun{eraseVars}(\t(\ \var{st}\ \t{->}\ \var{updates}\ \t))_\nt{Update})\ \t{|]} \t{[p|}\ \var{vpat}\ \t{|]} \\[-1ex]
&\qquad \t{[p|}\ \fun{srcCond}(\fun{eraseVars}(\t(\ \var{st}\ \t{->}\ \var{updates}\ \t))_\nt{Update})\ \t{|])} \\[-1ex]
%
&\qquad\quad \t{\$(update}\ \t{[p|}\ \fun{removeAs}(\var{vpat})\ \t{|]} \ \t{[p|}\ \fun{srcPat}(\t(\ \var{st}\ \t{->}\ \var{updates}\ \t))_\nt{Update}\ \t{|]} \\[-1ex]
&\qquad\quad \phantom{\t{\$(update}\ \text`}\llap{`}\texttt{[d|}\text'\ \iter{u}{\var{updates}}{\sem{\var u}_\nt{Update}^{\var{vt}, \var{vpat}}}\ \t{|])} \\
%
&\sem{\t(\ \var{var}\ \t{+>}\ \var{uc}_\nt{Primitive}\ \t)}_\nt{Update}^{\var{vt}, \var{vpat}} = \var{var}\ \t{= Replace;} \\[-1ex]
&\sem{\t(\ \var{var}\ \t{+>}\ \var{uc}_\nt{Nonterminal}\ \t)}_\nt{Update}^{\var{vt}, \var{vpat}} = \var{var}\ \t{=}\ \fun{prog}(\fun{varType}(\var{vt}, \var{vpat}, \var{var}), \var{uc})\ \t; \\[-1ex]
&\sem{\t(\ \var{var}\ \t{+>}\ \t(\ \var{nt}\ \t{->}\ \ldots \t)\ \t)}_\nt{Update}^{\var{vt}, \var{vpat}} = \sem{\t(\ \var{var}\ \t{+>}\ \var{nt}\ \t)}_\nt{Update}^{\var{vt}, \var{vpat}}\\[-1ex]
&\sem{\t(\ \ldots\ \t{->}\ \var{updates}\ \t)}_\nt{Update}^{\var{vt}, \var{vpat}} = \iter{\var u}{\var{updates}}{\sem{u}_\nt{Update}^{\var{vt}, \var{vpat}}\ \t;} \\[-1ex]
&\sem{symbol}_\nt{Update}^{\var{vt}, \var{vpat}} = \t{} \\
%
&\sem{\var{vpat}\ \t{+>}\ \var{updates}}_\nt{Action}^{\mathsf A,\, \var{st}} = \\[-1ex]
&\quad \t{\$(adaptiveSV} \ \t{[p| \char`_\ |]} \ \t{[p|} \ \var{vpat}\ \t{|])} \\[-1ex]
&\qquad \t{(\char92\char95\;\char95\;->}\ \fun{defaultExpr} (\fun{eraseVars}(\t(\ \var{st}\ \t{->}\ \var{updates}\ \t)))\ \t{)}
\end{aligned}
\end{equation*}
%
%
\begin{equation*}
\begin{aligned}
% \begin{split}
&\fun{field}(\var{nt})_\nt{Nonterminal} = \var{nt} \\[-1ex]
&\rlap{$\fun{field}(\var{t})_\nt{Terminal}$}\phantom{\fun{field}(\var{nt})_\nt{Nonterminal}} = \t{String} \\[-1ex]
&\rlap{$\fun{field}(\var{p})_\nt{Primitive}$}\phantom{\fun{field}(\var{nt})_\nt{Nonterminal}} = \t{(}\ \var p\ \t{, String)} \\
% \end{split} \\
%
%
&\fun{eraseVars}(\t(\ \var{var}\ \t{+>}\ \var{uc}\ \t))_\nt{Update} = \var{uc} \\[-1ex]
&\fun{eraseVars}(\t(\ \var{nt}\ \t{->}\ \var{updates}\ \t))_\nt{Update} = \\[-1ex]
&\quad \t(\ \var{nt}\ \t{->}\ \iter{\var u}{\var{updates}}{\fun{eraseVars}(u)}\ \t) \ \fun{eraseVars}(\var{symbol})_\nt{Update} = \var{symbol} \\
%
&\fun{srcCond}(\t(\ \var{nt}\ \t{->}\ \var{uconds}\ \t))_\nt{UpdateCondition} = \\[-1ex]
&\quad \t(\ \fun{con}(\var{nt}, \iter{\var{uc}}{\var{uconds}}{\fun{condHead}(\var{uc})}) \ \iter{\var{uc}}{\var{uconds}}{\fun{srcCond}(\var{uc})}\ \t) \\[-1ex]
&\fun{srcCond}(\var{symbol})_\nt{UpdateCondition} = \t{\char95} \\
%
&\fun{condHead}(\t(\ \var{nt}\ \t{->}\ \ldots\ \t))_\nt{UpdateCondition} = \var{nt} \\[-1ex]
&\fun{condHead}(\var{symbol})_\nt{UpdateCondition} = \var{symbol} \\
%
&\fun{srcPat}(\t(\ \var{var}\ \t{+>}\ \var{uc}_\nt{Primitive}\ \t))_\nt{Update} = \t(\ \var{var}\ \t{, \char95)} \\[-1ex]
&\fun{srcPat}(\t(\ \var{var}\ \t{+>}\ \var{uc}_\nt{Nonterminal}\ \t))_\nt{Update} = \var{var} \\[-1ex]
&\fun{srcPat}(\t(\ \var{nt}\ \t{->}\ \var{updates}\ \t))_\nt{Update} = \\[-1ex]
&\quad \t(\ \fun{con}(\var{nt}, \iter{\var{uc}}{\fun{eraseVars}(\var{updates})}{\fun{condHead}(\var{uc})})\ \iter{\var{u}}{\var{updates}}{\fun{srcPat}(\var{u})}\ \t) \\[-1ex]
&\fun{srcPat}(\var{symbol})_\nt{Symbol} = \t{\char95} \\
%
&\fun{defaultExpr}(\var{symbol})_\nt{Primitive} = \t{(undefined, " ")} \\[-1ex]
&\fun{defaultExpr}(\var{symbol})_\nt{Nonterminal} = \fun{nullCon}(\var{symbol}) \\[-1ex]
&\fun{defaultExpr}(\var{symbol})_\nt{Terminal} = \t{" "} \\[-1ex]
&\fun{defaultExpr}(\t(\ \var{nt}\ \t{->}\ \var{uconds}\ \t))_\nt{UpdateCondition} = \\[-1ex]
&\quad \fun{con}(\var{nt}, \iter{\var{uc}}{\var{uconds}}{\fun{condHead}(\var{uc})}) \ \iter{\var{uc}}{\var{uconds}}{\fun{defaultExpr}(\var{uc})}
\end{aligned}
\end{equation*}
\caption{Semantics of \biyacc\ programs (as \bigul\ programs)}
\label{biyaccSemantics}
\end{figure*}


The semantics of \biyacc, shown in Figure \ref{biyaccSemantics}, is defined by source-to-source compilation to \bigul.
Compilation rules are defined with the semantic bracket ($\sem{\cdot}$), and refer to some auxiliary functions, whose names are in \textsc{small caps}.
A nonterminal in subscript gives the ``type'' of the argument or metavariable before it, and additional arguments to the semantic bracket are typeset in superscript.


%
\paragraph*{Top-level structure.}
A \biyacc\ \textit{Program} has the form
\begin{lstlisting}
(*@$\t{Abstract}\ \var{decls}\ \t{Concrete}\ \var{pgs}$ $\t{Action}\ \var{ags}$@*)
\end{lstlisting}
and is compiled to a three-part Haskell program by copying $\var{decls}$ (which is already valid Haskell code), converting each group of production rules in $\var{pgs}$ to a datatype, and each action group in $\var{ags}$ to a small \bigul\ program.
The angle bracket notation $\iter{e}{\var{es}}{f\;e}$ denotes the generation of a list of entities of the form $f\;e$ for each element~$e$ in the list $\var{es}$, in the order of their appearance in $\var{es}$.
Comment syntax declarations (\lstinline{%commentLine} and \lstinline{%commentBlock}) are only relevant to concrete parsing and printing, and are ignored in Figure \ref{biyaccSemantics}.


%
\paragraph*{CST datatypes.}
\label{sec:cst}
The production rules in a context-free grammar dictate how to generate strings from nonterminals, and a CST can be regarded as encoding one particular way of generating a string using the production rules.
In Haskell, we represent CSTs starting from a nonterminal $\mathit{nt}$ as a datatype named $\mathit{nt}$, whose constructors represent the production rules for $\mathit{nt}$.
%For each group of production rules, we generate a Haskell datatype for it by the nonterminal symbol ($\var{nt}$) on the left side of $\t{->}$.
For each constructor we generate a unique name, which is denoted by $\fun{con}(\var{nt},\var{syms})$.
The fields of a constructor are generated from the right-hand side of the corresponding production rule in the way described by the auxiliary function $\fun{field}$:
Nonterminals are left unchanged (using their names for datatypes), terminal symbols are dropped, and an additional \lstinline{String} field is added for terminals and primitives for storing layout information (whitespaces and comments) appearing after them in the program text.
The last step is to insert an additional empty constructor, the unique name generated for which is denoted by $\fun{nullCon}(\var{nt})$; this empty constructor is used as a default value to a \biyacc\ printer whenever we want to create a new piece of program text depending on the view only.


For instance, the third group of the concrete syntax defined in Figure \ref{fig:expr} is translated to the following Haskell declarations:
\begin{lstlisting}[language=Haskell]
data Factor = Factor0 String Factor
            | Factor1 (Int, String)
            | Factor2 (Name, String)
            | Factor3 String Expr String
            | FactorNull2
  deriving (Show, Eq, Read)
\end{lstlisting}
Note that the first \lstinline{String} field of \lstinline{Factor0} stores the whitespaces appearing after a negation sign in the program text.


\paragraph*{Action groups.}
Each group of actions is translated into a small \bigul\ program, whose name is determined by the view type~$\var{vt}$ and source type~$\var{st}$ and denoted by $\fun{prog}(\var{vt}, \var{st})$.
The \bigul\ program has one single \lstinline{Case} statement, and each action is translated into two branches in this \lstinline{Case} statement, one normal and the other adaptive.
All the adaptive branches are gathered in the second half of the \lstinline{Case} statement, so that normal branches will be tried first.
For example, the third group of type \lstinline[breaklines=true]{Arith +> Factor} is compiled to
% (the string ``\lstinline{bigul}'' is always added to the name of a generated function):
%
\begin{lstlisting}
bigulArithFactor :: BiGUL Factor Arith
bigulArithFactor = Case [(*@\,\textrm{\ldots}@*)]
\end{lstlisting}

\paragraph*{Normal branches.}
We said in Section \ref{sec:design} that the semantics of an action is to perform pattern matching on both the source and view, and then update parts of the source with parts of the view.
This semantics is implemented with a normal branch:
The source and view patterns are compiled to the entry condition, and, together with the updates overlaid on the source pattern, also to an \lstinline{update} operation.
For example, the first action in the \lstinline{Arith}--\lstinline{Factor} group
\begin{lstlisting}
Sub (Num 0) y  +>  '-' (y +> Factor)
\end{lstlisting}
is compiled to
\begin{lstlisting}
$(normalSV [p| (Factor0 _ _) |] [p| Sub (Num 0) y |]
           [p| (Factor0 _ _) |])
  $(update [p| Sub (Num 0) y |] [p| (Factor0 _ y) |]
           [d| y = bigulArithFactor; |])
\end{lstlisting}
When the CST is a \lstinline{Factor0} and the AST matches \lstinline{Sub (Num 0) y}, we enter this branch, decompose the source and view by pattern matching, and use the view's right subtree~\lstinline{y} to update the second field of the source while skipping the first field (which stores whitespaces); the name of the \bigul\ program for performing the update is determined by the type of the smaller source~\lstinline{y} (deduced by $\fun{varType}$) and that of the smaller view.


\paragraph*{Adaptive branches.}
When all actions in a group fail to match, we should adapt the source into a proper shape to correspond to the view.
This is done by generating adaptive branches from the actions during compilation.
For example, the first action in the \lstinline{Arith}--\lstinline{Factor} group is compiled to
\begin{lstlisting}
$(adaptiveSV [p| _ |] [p| Sub (Num 0) _ |])
  (\ _ _ ->  Factor0 " " FactorNull2)
\end{lstlisting}
The body of the adaptation function is generated by the auxiliary function $\fun{defaultExpr}$, which creates a skeletal value that matches the source pattern.


\paragraph*{Entry point.}
The entry point of the program is chosen to be the \bigul\ program compiled from the first group of actions.
This corresponds to our assumption that the initial input concrete and abstract syntax trees are of the types specified for the first action group.
It is rather simple so the rules are not shown in the figure.
For the expression example, we generate a definition
\begin{lstlisting}
entrance = bigulArithExpr
\end{lstlisting}
which is invoked in the \lstinline{main} program.


%%%%%%%%%%%%%%%
\subsection{Inverse properties between program text and CSTs}
\label{sec:inverse-properties}
Having explained the bidirectional transformation between CSTs and ASTs, the remaining task is to establish an isomorphism between program text and CSTs.
Assuming that the grammar is unambiguous and the parser generator is ``correct'', we will show that there exists an isomorphism between program text and CSTs:
\begin{align}
& \mathit{parse}\;\mathit{text} = \mathsf{Just}\;\mathit{cst} \quad\Rightarrow\quad \mathit{print}\;\mathit{cst} = \mathit{text} \label{equ:ParsePrint} \\
& \mathit{parse}\;(\mathit{print}\;\mathit{cst}) = \mathsf{Just}\;\mathit{cst} \label{equ:PrintParse}
\end{align}
One direction is a partial ($\mathsf{Maybe}$-valued) function $\mathit{parse}$ that converts program text into CSTs according to the grammar,
and the other is a total function $\mathit{print}$ from CSTs to program text.

%
To see the meaning of (\ref{equ:ParsePrint}), we should first clarify what $\mathit{print}$ does:
Our CSTs, as described in Section \ref{sec:gen-BiGUL}, encode precisely the derivation trees, with the CST constructors representing the production rules used;
what $\mathit{print}$ does is simply traversing the CSTs and applying the encoded production rules to produce the derived program text.
Now consider what $\mathit{parse}$ is supposed to do:
It should take a piece of program text and find a derivation tree for it, i.e., a CST which $\mathit{print}$s to that piece of program text.
This statement is exactly~(\ref{equ:ParsePrint}).
In other words, (\ref{equ:ParsePrint}) is the functional specification of parsing, which is satisfied if the parser generator we use behaves correctly.

%
\newcommand{\reason}[1]{\quad\{\,\text{#1}\,\}}
\setlength{\jot}{0pt}
For (\ref{equ:PrintParse}), since the grammar is unambiguous, for any piece of program text there is at most one CST that prints to it,
which is equivalent to saying that $\mathit{print}$ is injective.
In addition, it is reasonable to expect that a generated parser will be able to successfully parse any valid program text; that is, for any $\mathit{cst}$ we have
\[\phantom{\Rightarrow} \mathit{parse}\;(\mathit{print}\;\mathit{cst}) = \mathsf{Just}\;\mathit{cst'} \]
for some $\mathit{cst'}$.
This is already close to~(\ref{equ:PrintParse}); what remains to be shown is that $\mathit{cst'}$ is exactly $\mathit{cst}$, which is indeed the case because
\begin{align*}
& \mathit{parse}\;(\mathit{print}\;\mathit{cst}) = \mathsf{Just}\;\mathit{cst'} \\
\Rightarrow& \reason{(\ref{equ:ParsePrint})} \\
& \mathit{print}\;\mathit{cst'} = \mathit{print}\;\mathit{cst} \\
\Rightarrow& \reason{$\mathit{print}$ is injective} \\
& \mathit{cst'} = \mathit{cst}
\end{align*}
% \end{itemize}



%
\subsection{Generating concrete parser and printer}
\label{sec:concrete_ppr}

In current \biyacc, the implementation of the $\mathit{parse}$ function is further separated into two phases: tokenising and parsing.
In both phases, the layout information (whitespaces and comments) is automatically preserved, which makes the CSTs isomorphic to the program text.
In the following, we will show how the lexer and parser are constructed and later the printer.

\paragraph*{Generating Lexer.}
\label{subsec:lexer}

Apart from handling the terminal symbols appearing in a grammar, the lexer automatically derived by \biyacc\ can also recognise several kinds of literals, including integers, strings, and identifiers, respectively produced by the nonterminals \lstinline{Int}, \lstinline{String}, and \lstinline{Name}.
For now, the forms of these literals are pre-defined, but we take this as a step towards a lexerless grammar, in which strings produced by nonterminals can be specified in terms of regular expressions.
Furthermore, whitespaces and comments are carefully handled in the derived lexer, so they can be completely stored in CSTs and correctly recovered to the program text in printing.
This feature of \biyacc, which we explain below, makes layout preservation transparent to the programmer.

%
An assumption of \biyacc\ is that whitespaces are only considered as separators between other tokens.
(Although there exist some languages such as Haskell and Python where indentation does affect the meaning of a program, there are workarounds, e.g., writing a preprocessing program to insert explicit separators.)
Usually, token separators are thrown away in the lexing phase, but since we want to keep layout information in CSTs, which are built by the parser, the lexer should leave the separators intact and pass them to the parser.
The specific approach taken by \biyacc\ is wrapping a lexeme and the whitespaces following it into a single token so that the CST still contains these whitespaces and a trivial printer can be constructed to naturally transform the CST back to original program text.
Beginning whitespaces are treated separately from lexing and parsing, and are always preserved.
And in this prototype implementation, comments are also considered as whitespaces.


%
\paragraph*{Generating parser.}
\label{subsec:concreteParser}
The concrete parser is used to generate a CST from a list of tokens according to the production rules in the grammar.
Our parser is built using the parser generator \happy, which takes a BNF specification of a grammar and produces a \haskell\ module containing a parser function.
The grammar we feed into \happy\ is still essentially the one specified in a \biyacc\ program, but in addition to parsing and constructing CSTs, the \happy\ actions also transfer the whitespaces wrapped in tokens to corresponding places in the CSTs.
For example, the production rules for \lstinline{Factor} in the expression example, as shown on the left below, are translated to the \happy\ specification on the right:
\[ \begin{minipage}{.15\textwidth}
\begin{lstlisting}[xleftmargin=0pt]
Factor
  -> '-' Factor

   | Int

   | Name

   | '(' Expr ')';
(*@@*)
\end{lstlisting}
\end{minipage}
~~\leadsto~~
\begin{minipage}{.2\textwidth}
\begin{lstlisting}[xleftmargin=0pt]
Factor
  : token0 Factor
      { Factor0 $1 $2 }
  | tokenInt
      { Factor1 $1 }
  | tokenName
      { Factor2 $1 }
  | token1 Expr token2
      { Factor3 $1 $2 $3 }
\end{lstlisting}
\end{minipage} \]
We use the first expansion (\lstinline{token0 Factor}) to explain how whitespaces are transferred:
The generated \happy\ token \lstinline{token0} matches a `\lstinline{-}' token produced by the lexer, and extracts the whitespaces wrapped in the `\lstinline{-}' token; these whitespaces are bound to \lstinline{$1}, which is placed into the first field of \lstinline{Factor0} by the associated \haskell\ action.

\paragraph*{Generating printer.}
As mentioned in Section \ref{sec:inverse-properties}, a printer generated by \biyacc\ is supposed to traverse the CST and apply the encoded production rules to retrieve that piece of program text,
which is rather simple and straightforward.
For example, the CST for \lstinline{Factor} is flattened in the following way by defining the instance of \lstinline[language=Haskell]{Show} class in \haskell:
\begin{lstlisting}[language=Haskell]
instance Show Factor where
  show (Factor0 s0 s1) = "-" ++ s0 ++ show s1
  show (Factor1 s0) = fst s0 ++ snd s0
  show (Factor2 s0) = fst s0 ++ snd s0
  show (Factor3 s0 s1 s2) = s0 ++ show s1 ++ s2
  show (FactorNull2) = ""
\end{lstlisting}

