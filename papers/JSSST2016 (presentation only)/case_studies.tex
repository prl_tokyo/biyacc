% !TEX root = biyacc.tex
\section{Case studies}
\label{sec:case_studies}

The design of \biyacc\ may look simplistic and make the reader wonder how much it can describe.
However, in this section we demonstrate with a larger case study that, without any extension, \biyacc\ can already handle real-world language features.
Section \ref{sec:tigerOverview} gives a brief introduction to the \tiger\ language, where we also make comparison with the C programming language to show the complexity of \tiger.
In the following sections, some representative cases are presented to demonstrate what \biyacc\ and reflective printing can achieve, including preservation of syntactic sugar, constant propagation, Pombrio and Krishnamurthi's resugaring \cite{pombrio2014resugaring}, and language evolution.


\subsection{The \tiger\ language}
\label{sec:tigerOverview}
For this case study, we choose the \tiger\ language, which is a statically typed imperative language first introduced in Appel's textbook on compiler construction~\cite{Appel98}.
Since \tiger's purpose of design is pedagogical,
it is not too complex and yet covers many important language features
including conditionals, loops, variable declarations and assignments, and function definitions and calls.
Some of these features can be seen in this \tiger\ program:
%
\begin{lstlisting}
function foo() =
  (for i := 0 to 10
    do (print(if i < 5 then "smaller"
                       else "bigger");
        print("\n")))
\end{lstlisting}
%
To give a sense of \tiger's complexity, it takes an LALR grammar with 81 production rules to specify \tiger's syntax, while for C89 and C99 it takes 183 and 237 rules respectively (based on \cite{kernighan1988c} and the draft version of 1999 ISO C standard, excluding the preprocessing part).
The difference is basically due to the fact that C has more primitive types and various kinds of assignment statements.
\tiger\ is therefore a good case study with which we can test the potential of our BX-based approach to constructing parsers and reflective printers.


\begin{figure}[h]
\begin{lstlisting}[language=Haskell,emphstyle={\bfseries}]
Abstract

data TExp = TVarExp TVar | TString String
  | TInt Int | TSeqExp [TExp]
  | TLetExp [TDec] TExp | TNilExp
  | TIfExp TExp TExp (Maybe TExp)
  | TOpExp TExp TOper TExp
  | (*@\,\textrm{\ldots}@*)
  deriving (Show, Eq, Read)

data TVar = TSimpleVar TSymbol
          | TFieldVar (TVar,TSymbol)
          | TSubscriptVar (TVar,TExp)
  deriving (Eq,Show,Read)

data TOper = TPlusOp | TMinusOp | (*@\,\textrm{\ldots}@*)
           | TEqOp | TNeqOp | (*@\,\textrm{\ldots}@*)
  deriving (Eq,Show,Read)

data TDec = TVarDec TSymbol (Maybe TSymbol) TExp
          | TTypeDec TTyDec
          | TFunctionDec TFundec
  deriving (Eq,Show,Read)

data TFundec =
  TFundec TSymbol [TFieldDec] (Maybe TSymbol) TExp
  deriving (Eq,Show,Read)

type TSymbol = String
(*@\,\textrm{\ldots}@*)
\end{lstlisting}
\caption{An excerpt of \tiger's abstract syntax}
\label{lst:tiger1}
\end{figure}


%
\begin{figure}[h]
\begin{lstlisting}[emphstyle={\bfseries}]
Concrete

Exp -> 'break' | LetExp | ArrExp | Assignment
     |  ForExp | RecExp | IfThen | PrimitiveOpt
     | IfThenElse | WhileExp ;

VarDec -> 'var' Name          ':=' Exp
        | 'var' Name ':' Name ':=' Exp ;

LValue      -> Name | OtherLValue ;
OtherLValue -> Name '[' Exp ']'
  | OtherLValue '[' Exp ']' | LValue '.' Name;

SeqExp  -> '(' ')' | '(' ExpSeq ')' ;
ExpSeq  -> Exp     | Exp ';' ExpSeq ;

PrimitiveOpt  -> PrimitiveOpt '|' PrimitiveOpt1
               | PrimitiveOpt1 ;
(*@\,\textrm{\ldots}@*)
PrimitiveOpt3 -> PrimitiveOpt3 '+'  PrimitiveOpt4
               | (*@\,\textrm{\ldots}@*) | PrimitiveOpt4 ;
PrimitiveOpt5 -> 'nil'  | Int    | String
               | LValue | SeqExp | CallExp
               | '-' PrimitiveOpt5 ;

IfThen     -> 'if' Exp 'then' Exp 'end' ;
IfThenElse -> 'if' Exp 'then' Exp 'else' Exp ;
(*@\,\textrm{\ldots}@*)
\end{lstlisting}
\caption{An excerpt of \tiger's concrete syntax}
\label{lst:tiger2}
\end{figure}

%
Excerpts of the abstract and concrete syntax of \tiger\ are respectively shown in Figure \ref{lst:tiger1} and Figure \ref{lst:tiger2}
, where the usage of constructors can be guessed from its name easily.
The abstract syntax is substantially the same as the original one defined in Appel's textbook (page~98);
as for the concrete syntax, Appel does not specify the whole grammar in detail, so we use a version slightly adapted from Hirzel and Rose's lecture notes \cite{tigerSpec}.
Some changes are made to handle features that are not supported by current \biyacc:
For example, to make the grammar become unambiguous without disambiguation rules,
the operators are divided into several groups, with the highest-precedence terms (like literals) placed in the last group,
just like what we did in the arithmetic expression example (Figure \ref{fig:expr});
the AST constructors \lstinline{TFunctionDec} or \lstinline{TTypeDec} take a single function or type declaration instead of a list of adjacent declarations (for representing mutual recursion) as in Appel's book,
since we cannot handle the synchronisation between a list of lists (in ASTs) and a list (in CSTs) with \biyacc's current syntax;
finally, to circumvent the ``dangling else'' problem,
a terminal ``\lstinline{end}'' is added to mark the end of an \lstinline{if}-\lstinline{then} expression.
One remark is that the language shown in Figure \ref{fig:expr} is a subset of \tiger\ and is reused for building this example except for some changes on names of constructors and production rules,
which means that \biyacc\ programs can be developed in an incremental way.

We have tested our \biyacc\ program for \tiger\ on all the sample programs provided on the homepage of Appel's book\footnote{\url{https://www.cs.princeton.edu/~appel/modern/testcases/}}, including a merge sort implementation and an eight-queen solver, and there is no problem parsing and printing them.
The complete \biyacc\ program for \tiger\ can be found at {\small\url{http://www.prg.nii.ac.jp/project/biyacc.html}}.
In the following sections, we will highlight some nontrivial printing strategies used in that program to demonstrate what \biyacc, in particular reflective printing, can achieve.



%
\subsection{Syntactic sugar}
\label{sec:synSugar}
Syntactic sugar, which lets the programmer use some features in an alternative (perhaps conceptually higher-level) syntax, is pervasive in programming languages.
For instance, \tiger\ represents boolean values false and true respectively as zero and nonzero integers, and the logical operators \lstinline{&}~(``and'') and \lstinline{|}~(``or'') are converted to \lstinline{if} expressions in the abstract syntax:
\lstinline{e1 & e2} is desugared and parsed to \lstinline{TIf e1 e2 (TInt 0)} and \lstinline{e1 | e2} to \lstinline{TIf e1 (TInt 1) e2}.
The printing actions for them in \biyacc\ are:
%
\begin{lstlisting}
TExp +> PrimitiveOpt
TIf e1 (TInt 1) (Just e2) +>
    (e1 +> PrimOpt) '|' (e2 +> PrimOpt1) ;
t                         +> (t  +> PrimOpt1) ;

TExp +> PrimitiveOpt1
TIf e1 e2 (Just (TInt 0)) +>
    (e1 +> PrimOpt1) '&' (e2 +> PrimOpt2) ;
t                         +> (t  +> PrimOpt2) ;
\end{lstlisting}
%
The $\mathit{parse}$ function for these syntactic sugar is not injective, since the alternative syntax and the features being desugared into are both mapped to the latter.
A conventional printer --- which takes only the AST as input --- cannot reliably determine whether an abstract expression should be ensugared or not, whereas a reflective printer can make the decision by inspecting the CST.


%
\subsection{Constant propagation}
Constant propagation is an important compiler optimisation that substitutes the values of known constants in expressions at compile time rather than at runtime.
We use the example below of solving the eight-queen problem to show that \biyacc\ enables the observation of constant propagation performed on AST from program text.

Suppose the code snippet is:
\begin{lstlisting}
let var N := 8
    type intArray = array of int
    var row   := intArray [ N ] of 0
    (*@\,\textrm{\ldots}@*)
    function try(c:int)   = ((*@\,\textrm{\ldots}@*))
in  try(0)
end
\end{lstlisting}
where the variable \lstinline{N} denotes the size of the problem.
Since the length of the arrays only depends on the variable \lstinline{N}, it can be evaluated and optimised at compile time.
The right hand side of the variable declaration at line~3 is parsed to:
\begin{lstlisting}
TArrayExp "intArray" (TVarExp (TSimpleVar "N"))
                     (TInt 0)
\end{lstlisting}
After constant propagation, it is optimised to:
\begin{lstlisting}
TArrayExp "intArray" (TInt 8) (TInt 0)
\end{lstlisting}
Now we reflect the change back to program text using \biyacc\ by simply performing printing. The result is:
\begin{lstlisting}
let var N := 8
    type intArray = array of int
    var row   := intArray [ 8 ] of 0
    (*@\,\textrm{\ldots}@*)
    function try(c:int)   = ((*@\,\textrm{\ldots}@*))
in  try(0)
end
\end{lstlisting}
Since the change is precisely located in the right hand side of the variable declaration at line~3, other parts of the program including layouts remain intact.


%
\subsection{Resugaring}
The idea of \emph{resugaring}~\cite{pombrio2014resugaring} is to print evaluation sequences in a core language in terms of a surface syntax.
Here we show that, without any extension, \biyacc\ is already capable of reflecting to the concrete syntax some of the AST changes resulting from evaluation, subsuming a part of Pombrio and Krishnamurthi's work \cite{pombrio2014resugaring}.

We borrow Pombrio and Krishnamurthi's example of resugaring evaluation sequences for the logical operators ``or'' and ``not'', but recast the example in \tiger.
The ``or'' operator has been defined as syntactic sugar in Section \ref{sec:synSugar}.
For the ``not'' operator, which \tiger\ lacks, we introduce `\lstinline{~}', represented by \lstinline{TNot} in the abstract syntax.
Now consider the source expression
\begin{lstlisting}
~1 | ~0
\end{lstlisting}
which is parsed to
\begin{lstlisting}
TIf (TNot (TInt 1)) (TInt 1) (Just (TNot (TInt 0)))
\end{lstlisting}
%
A typical call-by-value evaluator will produce the following evaluation sequence given the above AST:
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) TIf (TNot (TInt 1)) (TInt 1) (Just (TNot (TInt 0)))
(*@$         \rightarrow $@*) TIf (TInt 0) (TInt 1) (Just (TNot (TInt 0)))
(*@$         \rightarrow $@*) TNot (TInt 0)
(*@$         \rightarrow $@*) TInt 1
\end{lstlisting}
%
If we perform reflective printing after every evaluation step using \biyacc, we will get the following evaluation sequence on the source:
\begin{lstlisting}
~1 | ~0    (*@$\rightarrow$@*)    0 | ~0    (*@$\rightarrow$@*)    ~0    (*@$\rightarrow$@*)    1
\end{lstlisting}
%
Due to the \ref{equ:PutGet} property, parsing these concrete terms will yield the corresponding abstract terms in the first evaluation sequence, and this is exactly Pombrio and Krishnamurthi' ``emulation'' property, which they have to prove for their system; for \biyacc, however, the emulation property holds by construction, since \biyacc's semantics is defined in terms of \bigul, whose programs are always well-behaved.
Also different from their approach is that we do not need to insert any additional information into the ASTs for remembering the form of the original sources.
The advantage of our approach is that we can keep the abstract syntax pure, so that other tools --- the evaluator in particular --- can process the abstract syntax without being modified,
whereas in their approach, the evaluator has to be adapted to work on the enriched abstract syntax.

Also note that the above resugaring for \tiger\ is achieved for free --- the programmer does not need to write additional, special actions to achieve that.
In general, \biyacc\ can easily and reliably reflect AST changes that involve only ``simplification'', i.e., replacing part of an AST with a simpler tree, so it should not be surprising that \biyacc\ can also reflect simplification-like optimisations such as constant propagation (we have mentioned) and dead code elimination, and some refactoring transformations such as variable renaming.
All these can be achieved by one ``general-purpose'' \biyacc\ program, which does not need to be tailored for each application.


\subsection{Language evolution}

We conclude this section by looking at a practical scenario in language evolution, incorporating all the applications we introduced in this section.
When a language evolves, some new features of the language (e.g., \lstinline{foreach} loops in Java~5) can be implemented by desugaring to some existing features (e.g., ordinary \lstinline{for} loops), so that the compiler does not need to be extended to handle the new features.
As a consequence, all the engineering work about refactoring or optimising transformations that has been developed for the abstract syntax remains valid.
%

Consider a kind of ``generalised-\lstinline{if}'' expression allowing more than two cases, resembling the alternative construct in Dijkstra's guarded command language \cite{dijkstra1975guarded}.
We extend \tiger's concrete syntax with the following production rules:
\begin{lstlisting}
Exp    -> (*@\,\textrm{\ldots}@*) | Guard | (*@\,\textrm{\ldots}@*) ;
Guard  -> 'guard' CaseBs 'end' ;
CaseBs -> CaseB CaseBs | CaseB ;
CaseB  -> LValue '=' Int '->' Exp ;
\end{lstlisting}
For simplicity, we restrict the predicate produced by \lstinline{CaseB} to the form \lstinline{LValue '=' Int}, but in general this can be any expression computing an integer.
The reflective printing actions for this new construct can still be written within \biyacc, but require much deeper pattern matching:
%
\begin{lstlisting}
TExp +> Guard
TIf (TOp (TVar lv) TEqOp (TInt i)) e1 Nothing
  +> 'guard' (CaseBs -> (CaseB ->
        (lv +> LValue) '=' (i +> Int) '->' (e1 +> Exp))
             ) 'end';
TIf (TOp (TVar lv) TEqOp (TInt i)) e1
    (Just if2@(TIf _ _ _))
  +> 'guard' (CaseBs -> (CaseB ->
        (lv +> LValue) '=' (i +> Int) '->' (e1 +> Exp))
                        (if2 +> CaseBs)
             ) 'end';

TExp +> CaseBs
TIf (TOp (TVar lv) TEqOp (TInt i)) e1 Nothing
  +> (CaseB ->
        (lv +> LValue) '='
          (i +> Int) '->' (e1 +> Exp));
TIf (TOp (TVar lv) TEqOp (TInt i)) e1
    (Just if2@(TIf _ _ _))
  +> (CaseB ->
        (lv +> LValue) '='
          (i +> Int) '->' (e1 +> Exp))
        (if2 +> CaseBs);
\end{lstlisting}
%
Though complex, these printing actions are in fact fairly straightforward:
The first group of type \lstinline{Tiger +> Guard} handles the enclosing \lstinline{guard}--\lstinline{end} pairs, distinguishes between single- and multi-branch cases, and delegates the latter case to the second group,
which prints a list of branches recursively.

This is all we have to do --- the corresponding parser is automatically derived and guaranteed to be consistent.
Now \lstinline{guard} expressions are desugared to nested \lstinline{if} expressions in parsing and preserved in printing,
and we can also resugar evaluation sequences on the ASTs to program text.
For instance, the following \lstinline{guard} expression
%
\begin{lstlisting}
guard  choice = 1  ->  4
       choice = 2  ->  8
       choice = 3  ->  16  end
\end{lstlisting}
%
is parsed to
%
\begin{lstlisting}
TIf (TEq (TName "choice") (TInt 1)) (TInt 4)
  (TIf (TEq (TName "choice") (TInt 2)) (TInt 8)
    (TIf (TEq (TName "choice") (TInt 3)) (TInt 16)
      TSeqNil))
\end{lstlisting}
Suppose that the value of the variable \lstinline{choice} is~$2$.
The evaluation sequence on the the AST will then be:
%\todo{there is a problem, since we do not write printing rules for the intermediate results, reflecting intermediate results such as \lstinline{TIf "True" (TInt 4) (...)} back to program text will fail the put}
%
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)          TEqOp (TInt 1)) (TInt 4)
(*@$\phantom{\rightarrow}$@*)   (Just (TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)                   TEqOp (TInt 2)) (TInt 8)
(*@$\phantom{\rightarrow}$@*)     (Just (TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)                     TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)        Nothing))))
(*@$         \rightarrow $@*) TIf (TInt 0) (TInt 4)
(*@$\phantom{\rightarrow}$@*)   (Just (TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)                   TEqOp (TInt 2)) (TInt 8)
(*@$\phantom{\rightarrow}$@*)     (Just (TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)                     TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)        Nothing))))
(*@$         \rightarrow $@*) TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)          TEqOp (TInt 2)) (TInt 8)
(*@$\phantom{\rightarrow}$@*)   (Just (TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)              TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)      Nothing))
(*@$         \rightarrow $@*) TIf (TInt 1) (TInt 8)
(*@$\phantom{\rightarrow}$@*)  (Just (TIf (TOp (TVar (TSV "choice"))
(*@$\phantom{\rightarrow}$@*)                  TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)     Nothing))
(*@$         \rightarrow $@*) TInt 8
\end{lstlisting}
%
And the reflected evaluation sequence on the concrete expression will be:
%
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) guard  choice = 1  ->  4
(*@$\phantom{\rightarrow}$@*)        choice = 2  ->  8
(*@$\phantom{\rightarrow}$@*)        choice = 3  ->  16  end
(*@$     \not\rightarrow $@*)
(*@$         \rightarrow $@*) guard  choice = 2  ->  8
(*@$\phantom{\rightarrow}$@*)        choice = 3 -> 16 end
(*@$     \not\rightarrow $@*)
(*@$         \rightarrow $@*) 8
\end{lstlisting}
%
Reflective printing fails for the first and third steps, but this behaviour in fact conforms to Pombrio and Krishnamurthi's ``abstraction'' property \cite{pombrio2014resugaring}, which demands that core evaluation steps that make sense only in the core language must not be reflected to the surface.
In our example, the first and third steps in the \lstinline{TIf}-sequence evaluate the condition to a constant, but conditions in guard expressions are restricted to a specific form and cannot be a constant; evaluation of guard expressions thus has to proceed in bigger steps, throwing away or going into a branch in each step, which corresponds to two steps for \lstinline{TIf}.

%\todo{the paragraph is in draft version}
The reader may have noticed that, after the \lstinline{guard} expression is reduced to two branches, the layout of the second branch is disrupted; this is because the second branch is in fact printed from scratch.
This problem is discussed in Section \ref{subsec:globalMatching}.
%
