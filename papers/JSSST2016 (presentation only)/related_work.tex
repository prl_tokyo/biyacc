
\section{Related work}
\label{sec:related_work}

\paragraph*{Comparison with our earlier prototype.}
This paper is primarily based on and significantly extends our previous tool demonstration paper~\cite{zhu2015biyacc},
which conducts an early experiment with the idea of casting parsing and reflective printing in the framework of bidirectional transformations.
Compared to our previous prototype, the current system has the following improvements:
\begin{itemize}[leftmargin=*]
\item The well-behavedness of the underlying bidirectional language \bigul~(Section \ref{sec:foundation}) we use in this version is formally verified~\cite{ko2016bigul}, so we can guarantee the well-behavedness of \biyacc\ programs much more confidently;
\item concrete parsers and printers between program text and CSTs are automatically derived (Section \ref{sec:concrete_ppr}), so \biyacc\ synchronises program text and ASTs,
not just CSTs and ASTs as with the previous version;
\item we present many nontrivial applications such as resugaring and language evolution (Section \ref{sec:case_studies});
\item our current system tries to preserve layouts and comments in a transparent manner.
\end{itemize}


\paragraph*{Unifying parsing and printing.}
Much research has been devoted to describing parsers and printers in a single program.
For example, both Rendel and Ostermann \cite{rendel2010invertible} and Matsuda and Wang \cite{matsuda2013flippr} adopt a combinator-based approach,
where small components describing both parsing and printing are glued together to yield more sophisticated behaviour,
and can guarantee inverse properties similar to (\ref{equ:ParsePrint})~and~(\ref{equ:PrintParse}) by construction (with CST replaced by AST in the equations).
%
In Rendel and Ostermann's system, both parsing and printing semantics are pre-defined in the combinators and the consistency is guaranteed by their partial isomorphisms,
whereas in Matsuda and Wang's system, the combinators describing pretty printing are lately removed by a semantic-preserving transformation to a syntax, which is further processed by their grammar-based inversion system to achieve the parsing semantics.
%
By specialising one of the syntax to be XML syntax, Brabrand et al. \cite{brabrand2008dual} present a tool XSugar that handles bijection between the XML syntax and any other syntax for a grammar, guaranteeing that the transformation is reversible.
However, the essential factor that distinguishes our system from others is that \biyacc\ is designed to perform synchronisation while others are all targeted at handling transformations.

%
Just like \biyacc, all of the systems described above handle pure unambiguous grammars without disambiguation declarations only.
When the user-defined grammar (or the derived grammar) is ambiguous, the behaviour of these systems is as follows:
Neither of Rendel and Ostermann's system (called ``invertible syntax descriptions'', which we shorten to ISDs henceforth) and Matsuda and Wang's system (called \flippr) will notify the user that the (derived) grammar is ambiguous.
For ISDs, property (\ref{equ:PrintParse}) will fail,
while for \flippr\ both properties (\ref{equ:ParsePrint})~and~(\ref{equ:PrintParse}) will fail.
(Since the discussion on ambiguous grammars has not been presented in their papers, we try the examples provided by their libraries and find these problems.)
In contrast, Brabrand et al. \cite{brabrand2008dual} give a detailed discussion about ambiguity detection, and XSugar can statically check that the transformations are reversible.
If any ambiguity in the program is detected, XSugar will notify the user of the precise location where ambiguity arises.
In \biyacc, the ambiguity analysis is performed by the parser generator employed in the system, and the result is reported at compile time.
If no warning is reported, the well-behavedness is always guaranteed, as explained in Section \ref{sec:inverse-properties}.

Even though all these systems handle unambiguous grammar only, there are design differences between them.
An ISD is more like a parser, while \flippr\ lets the user describe a printer: To handle operator priorities, for example,
the user of ISDs will assign priorities to different operators, consume parentheses, and use combinators such as \lstinline{chainl} to handle left recursion in parsing, while the user of \flippr\ will produce necessary parentheses according to the operator priorities.
In \biyacc, the user defines the concrete syntax that has a hierarchical structure (\lstinline{Expr}, \lstinline{Term}, and \lstinline{Factor}) to express operator priority, and write printing strategies to produce (preserve) necessary parentheses.
The user of XSugar will also likely need to use such a hierarchical structure.

It is interesting to note that, the part producing parentheses in \flippr\ essentially corresponds to the hierarchical structure of grammars.
For example, to handle arithmetic expressions in \flippr, we can write:
\begin{lstlisting}
ppr' i (Minus x y) =
 parensIf (i >= 6) $ group $
   ppr 5 x <> nest 2
     (line' <> text "-" <> space' <> ppr 6 y);
\end{lstlisting}
\flippr\ will automatically expand the definition and derive a group of \lstinline{ppr_i} functions indexed by the priority integer~\lstinline{i}, corresponding to the hierarchical grammar structure.
In other words, there is no need to specify the concrete grammar, which is already implicitly embedded in the printer program.
This makes \flippr\ programs neat and concise.
Following this approach, \biyacc\ programs can also be made more concise:
In a \biyacc\ program, the user is allowed to omit the production rules in the concrete syntax part,
and they will be automatically generated by extracting the terminals and nonterminals in the right-hand sides of all actions.
However, if these production rules are supplied, \biyacc\ will perform some sanity checks:
It will make sure that, in an action group, the user has covered all of the production rules of the nonterminal appearing in the ``type declaration'', and never uses undefined production rules.


\paragraph*{Bidirectional transformations (BXs).}
Our work is theoretically based on bidirectional transformations \cite{foster2007combinators,czarnecki2009bidirectional,hu2011dagstuhl},
particularly taking inspiration from the recent progress on putback-based bidirectional programming~\cite{pacheco2014monadic,pacheco2014biflux,hu2014validity,fischer2015essence,ko2016bigul}.
As explained in Section \ref{sec:foundation}, the purpose of bidirectional programming is to relieve the burden of thinking bidirectionally --- the programmer writes a program in only one direction, and a program in the other direction is derived automatically.
We call a language \emph{get-based} when programs written in the language denote $\mathit{get}$ functions, and call a language \emph{putback-based} when its programs denote $\mathit{put}$ functions.
In the context of parsing and reflecting printing, the get-based approach lets the programmer describe a parser, whereas the putback-based approach lets the programmer describe a printer.
Below we discuss in more depth how the putback-based methodology affects \biyacc's design by comparing \biyacc\ with a closely related, get-based system.


% One difference between Martins et al.'s system and ours is that their system performs synchronisation between two trees rather than program text and AST,
% and thus the preservation of layouts and comments is not considered.

% Another difference lies in the fact that, with their get-based system,
% certain decisions on the backward transformation are, by design, permanently encoded in the bidirectionalisation system and cannot be controlled by the user,
% whereas a putback-based system such as \biyacc\ can give the user fuller control.


\paragraph*{Get-based vs putback-based.}
Martins et al.~\cite{martins2014generating} introduces an attribute grammar--based BX system for defining transformations between two representations of languages (two grammars).
The utilisation is similar to \biyacc:
The programmer defines both grammars and a set of rules specifying a \emph{forward} transformation (i.e., $\mathit{get}$), with a backward transformation (i.e., $\mathit{put}$) being automatically generated.
For example, the \biyacc\ actions in lines 32--34 of Figure \ref{fig:expr} can be expressed in Martins et al.'s system as
\[ \setlength{\arraycolsep}{.2em}
\begin{array}{lcl}
\mathit{get}_A^{\,E}(\mathit{add}(l, \t{+}, r)) &\rightarrow& \mathrlap{\mathit{plus}}\phantom{\mathit{minus}}(\mathit{get}_A^{\,E}(l), \mathit{get}_A^{\,T}(r)) \\[.5ex]
\mathit{get}_A^{\,E}(\mathrlap{\mathit{sub}}\phantom{\mathit{add}}(l, \t{-}, r)) &\rightarrow& \mathit{minus}(\mathit{get}_A^{\,E}(l), \mathit{get}_A^{\,T}(r)) \\[.5ex]
\mathit{get}_A^{\,E}(\mathit{et}(t)) &\rightarrow& \mathit{get}_A^{\,T}(t)
%getAE (sub(l, op, r)) → minus(getAE (l), getAT (r)) getAE (et(t)) → getAT (t)
\end{array} \]
which describes how to convert certain forms of CSTs to corresponding ASTs.
The similarity is evident, and raises the question as to how get-based and putback-based approaches differ in the context of parsing and reflective printing.

The difference lies in the fact that, with a get-based system, certain decisions on the backward transformation are, by design, permanently encoded in the bidirectionalisation system and cannot be controlled by the user, whereas a putback-based system can give the user fuller control.
For example, when no source is given and more than one rules can be applied, Martins et al.'s system chooses, by design, the one that creates the most specialised version.
This might or might not be ideal for the user of the system.
For example:
Suppose that we port to Martins et al.'s system the \biyacc\ action that relates \tiger's concrete `\lstinline{&}'~operator with a specialised abstract \lstinline{if} expression in \ref{sec:synSugar}, coexisting with a more general rule that maps a concrete \lstinline{if} expression to an abstract \lstinline{if} expression.
Then printing the AST \lstinline{TIf (TName "a") (TName "b") 0} from scratch will and can only produce \lstinline{a & b}, as dictated by the system's hard-wired printing logic.
By contrast, the user of \biyacc\ can easily choose to print the AST from scratch as \lstinline{a & b} or \lstinline{if a then b else 0} by suitably ordering the actions.

This difference is somewhat subtle, and one might argue that Martins et al.'s design simply went one step too far --- if their system had been designed to respect the rule ordering as specified by the user, as opposed to always choosing the most specialised rule, the system would have given its user the same flexibility as \biyacc.
Interestingly, whether to let user-specified rule/action ordering affect the system's behaviour is, in this case, exactly the line between get-based and putback-based design.
The user of Martins et al.'s system writes rules to specify a forward transformation, whose semantics is the same regardless of how the rules are ordered, and thus it would be unpleasantly surprising if the rule ordering turned out to affect the system's behaviour.
We can view this from another angle:
If the user is required to specify a forward transformation while customising the backward behaviour by carefully ordering the rules, then the purpose of a bidirectionalisation system --- which is to reduce the problem of writing bidirectional transformations to unidirectional programming --- is largely defeated.
By contrast, the user of \biyacc\ only needs to think in one direction about the printing behaviour, for which it is natural to consider how the actions should be ordered when an AST has many corresponding CSTs; the parsing behaviour will then be automatically and uniquely determined.
In short, relevance of action ordering is incompatible with get-based design, but is a natural consequence of putback-based thinking.
Of course, it would be rather disappointing if all we can gain from adopting the putback-based approach was just the ability to customise the behaviour of printing from scratch.
We will make another case for putback-based thinking by sketching one potential extension to \biyacc\ in Section \ref{subsec:globalMatching}.

