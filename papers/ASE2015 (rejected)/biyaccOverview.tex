% !TEX root = BiYacc.tex

\section{A high-level introduction to \BiYacc}
\label{informal_introduction}

We give a high-level introduction to the structure, syntax, and semantics of \BiYacc\ using the \BiYacc\ program shown in \autoref{sec2_expr_all}, which deals with the example about arithmetic expressions mentioned in \autoref{introduction}.
At the end of this section, we highlight some distinct features of \BiYacc\ in terms of the example.

The program in \autoref{sec2_expr_all} consists of three parts:
\begin{itemize}
\item abstract syntax definition,
\item concrete syntax definition, and
\item actions describing how to update a concrete syntax tree with an abstract syntax tree.
\end{itemize}

Crucially, although the meaning of these actions are updates (i.e., extended pretty-printing), they can also be interpreted as parsing rules, so from a single \BiYacc\ program we can derive both an extended pretty-printer and a parser.
This is due to the power of the underlying theory of bidirectional transformations, which we will present in \autoref{biflux}.

\subsection{Defining the abstract syntax}

%The essence of \BiYacc\ is to write \emph{update rules} explicating how to convert the  abstract syntax tree to a pretty-printed string. So we first define the abstract syntax.
The abstract syntax part of a \BiYacc\ program starts with the keyword \lstinline{Abstract}, and can be seen as definitions of \emph{algebraic datatypes} commonly found in functional programming languages (see, e.g., \cite[Section~5]{Hudak-history-of-Haskell}).
For the expression example, we define a datatype \lstinline{Arith} of arithmetic expressions, where an arithmetic expression can be either an addition, a subtraction, a multiplication, a division, or a numeric literal.
For simplicity, we represent a literal as a string.
Different constructors --- namely \lstinline{ADD}, \lstinline{SUB}, \lstinline{MUL}, \lstinline{DIV}, and \lstinline{NUM} --- are used to construct different kinds of expressions, and in the definition each constructor is followed by the types of arguments it takes.
Hence the constructors \lstinline{ADD}, \lstinline{SUB}, \lstinline{MUL}, and \lstinline{DIV} take two subexpressions (of type \lstinline{Arith}) as arguments, while the last constructor \lstinline{NUM} takes a \lstinline{String} as argument.
(\lstinline{String} is a built-in datatype of strings.)
For instance, the expression ``$1 + 2 \times 3 - 4$'' can be represented as this abstract syntax tree of type \lstinline{Arith}:
\begin{lstlisting}
SUB (ADD (NUM "1")
         (MUL (NUM "2")
              (NUM "3")))
    (NUM "4")
\end{lstlisting}

\begin{figure}[t]
\begin{center}
\begin{tabular}{c}
{\begin{lstlisting}[numbers=left, numberstyle=\tiny, emphstyle={\color{blue}\bfseries}]
Abstract

Arith = ADD Arith Arith
      | SUB Arith Arith
      | MUL Arith Arith
      | DIV Arith Arith
      | NUM String

Concrete

Expr   -> Expr '+' Term
        | Expr '-' Term
        | Term

Term   -> Term '*' Factor
        | Term '/' Factor
        | Factor

Factor -> '-' Factor
        | String
        | '(' Expr ')'

Actions

Arith +> Expr
ADD x y  ->  (x => Expr) '+' (y => Term)
SUB x y  ->  (x => Expr) '-' (y => Term)
arith    ->  (arith => Term)

Arith +> Term
MUL x y  ->  (x => Term) '*' (y => Factor)
DIV x y  ->  (x => Term) '/' (y => Factor)
arith    ->  (arith => Factor)

Arith +> Factor
SUB (NUM "0") y  ->  '-' (y => Factor)
NUM n            ->  (n => String)
arith            ->  '(' (arith => Expr) ')'
\end{lstlisting}}
\end{tabular}
\end{center}
\caption{A \BiYacc\ program for the expression example.}
\label{sec2_expr_all}
\end{figure}


\subsection{Defining the concrete syntax}

Compared with an abstract syntax, the structure of a concrete syntax is more refined such that we can conveniently yet unambiguously represent a concrete syntax tree as a string.
For instance, we should be able to interpret the string ``$1 - 2 \times 3 + 4$'' unambiguously as a tree of the same shape as the abstract syntax tree at the end of the previous subsection.
This means that the concrete syntax for expressions should, in particular, somehow encode the conventions that multiplicative operators have higher precedence than additive operators and that operators of the same precedence associate to the left.

In a \BiYacc\ program, the concrete syntax is defined in the second part starting with the keyword \lstinline{Concrete}.
The definition is in the form of a context-free grammar, which is a set of production rules specifying how nonterminal symbols can be expanded to sequences of terminal or nonterminal symbols.
For the expression example, we use a standard syntactic structure to encode operator precedence and order of association, which involves three nonterminal symbols \lstinline{Expr}, \lstinline{Term}, and \lstinline{Factor}: an \lstinline{Expr} can produce a left-leaning tree of \lstinline{Term}s, each of which can in turn produce a left-leaning tree of \lstinline{Factor}s.
To produce right-leaning trees or operators of lower precedence under those with higher precedence, the only way is to reach for the last production rule \lstinline[breaklines=true]{Factor -> '(' Expr ')'}, resulting in parentheses in the produced string.

Note that there is one more difference between the concrete syntax and the abstract syntax in this example: the concrete syntax has a production rule \lstinline[breaklines=true]{Factor -> '-' Factor} for producing negated expressions, whereas in the abstract syntax we can only write subtractions.
This means that negative numbers will have to be converted to subtractions and these subtractions will have to be converted back to negative numbers in the opposite direction.
This mismatch can be complex to solve, but as we will see, can be easily handled in \BiYacc.

\subsection{Defining the actions}
The last and main part of a \BiYacc\ program starts with the keyword \lstinline{Actions}, and describes how to update a concrete syntax tree --- i.e., a well-formed string --- with an abstract syntax tree.
Note that we are identifying strings representing program text with concrete syntax trees:
Conceptually, whenever we write an expression as a string, we are actually describing a concrete syntax tree with the string (instead of just describing a sequence of characters).
Technically, it is almost effortless to convert a (well-formed) string to a concrete syntax tree with existing parser technology%(and we follow the normal convention for parser-generating systems that grammars must be unambiguous)
; the reverse direction is even easier, requiring only a traversal of the concrete syntax tree.
By integrating with existing parser technologies, \BiYacc\ actions can focus on describing conversions between concrete and abstract syntax trees --- the more interesting part in the tasks of parsing and pretty-printing.

The \lstinline{Actions} part consists of groups of actions, and each group of actions begins with a type declaration:
\begin{lstlisting}
~$\mathit{abstract\;syntax\;datatype}$~ +> ~$\mathit{concrete\;nonterminal\;symbol}$~
\end{lstlisting}
The symbol~`\lstinline{+>}' indicates that this group of actions describe how to put information from an abstract syntax tree of the specified datatype into a concrete syntax tree produced from the specified nonterminal symbol.
Each action takes the form
\begin{lstlisting}
~$\mathit{abstract\;syntax\;pattern}$~ -> ~$\mathit{concrete\;syntax\;update\;pattern}$~
\end{lstlisting}
The left-hand side pattern describes a particular shape for abstract syntax trees and the right-hand side one for concrete syntax trees; also the right-hand side pattern is overlaid with updating instructions denoted by~`\lstinline{=>}'.
For brevity, we call the left-hand side patterns \emph{view patterns} and the right-hand side ones \emph{source patterns} (in this case, representing an abstract and a concrete representation, respectively), hinting at their roles in terms of the underlying theory of bidirectional transformations.
Given an abstract syntax tree and a concrete syntax tree, the semantics of an action is to simultaneously perform \emph{pattern matching} on both trees (like in functional programming languages), and then use components of the abstract syntax tree to update parts of the concrete syntax tree, possibly recursively.

Let us look at a specific action --- the first one for the expression example, at line~26 of \autoref{sec2_expr_all}:
\begin{lstlisting}
ADD x y -> (x => Expr) '+' (y => Term)
\end{lstlisting}
For the view pattern \lstinline{ADD x y}, an abstract syntax tree (of type \lstinline{Arith}) is said to match the pattern when it starts with the constructor \lstinline{ADD}; if the match succeeds, the two arguments of the constructor (i.e., the two subexpressions of the addition expression) are then respectively bound to the variables \lstinline{x}~and~\lstinline{y}.
(\BiYacc\ adopts the naming convention in which variable names start with a lowercase letter and names of datatypes and nonterminal symbols start with an uppercase letter.)
For the source pattern of the action, the main intention is to refer to the production rule
\begin{lstlisting}
Expr -> Expr '+' Term
\end{lstlisting}
and use this to match those concrete syntax trees produced by first using this rule.
Since the action belongs to the group \lstinline[breaklines=true]{Arith +> Expr}, the part `\lstinline{Expr ->}' of the production rule can be inferred and hence is not included in the source pattern.
Finally we overlay `\lstinline{x =>}' and `\lstinline{y =>}' on the nonterminal symbols \lstinline{Expr} and \lstinline{Term} to indicate that, after the simultaneous pattern matching succeeds, the subtrees \lstinline{x}~and~\lstinline{y} of the abstract syntax tree are respectively used to update the left and right subtrees of the concrete syntax tree.

It is interesting to note that more complex view and source patterns are also supported, which can greatly enhance the flexibility of actions.
For example, the view pattern
\begin{lstlisting}
SUB (NUM "0") y
\end{lstlisting}
of the action at line~36 of \autoref{sec2_expr_all} accepts those subtraction expressions whose left subexpression is zero.
This action is the key to preserving negation expressions in the concrete syntax tree.
For an example of a more complex source pattern:
Suppose that in the \lstinline{Arith +> Factor} group we want to write a pattern that matches those concrete syntax trees produced by the rule \lstinline[breaklines=true]{Factor -> '-' Factor},
where the inner nonterminal \lstinline{Factor} produces a further \lstinline{'-' Factor} using the same rule.
This pattern is written by overlaying the production rule on the nonterminal \lstinline{Factor} in the top-level appearance of the rule:
\begin{lstlisting}
'-' (Factor -> '-' Factor)
\end{lstlisting}
It enables users to do some more precise pattern matching and thus enhance the expressive power of our language.

Now we can explain the semantics of the entire program.
Given an abstract syntax tree and a concrete syntax tree as input, first a group of actions is chosen according to the types of the trees.
Then the actions in the group are tried in order, by performing simultaneous pattern matching on both trees.
If pattern matching for an action succeeds, the update specified by the action is executed (recursively); otherwise the next action is tried.
(Execution of the program stops when the matched action specifies either no updating operations or only updates to \lstinline{String}.)
\BiYacc's most interesting behaviour shows up when pattern matching for all of the actions in the chosen group fail: in this case a suitable source will be created.
The specific approach here is to do pattern matching just on the abstract syntax tree and choose the first matching action.
A suitable concrete syntax tree matching the source pattern is then created, whose subtrees are recursively created according to the abstract syntax tree.

\subsection{Running the program on a specific input}
\label{running}

Let us go through the execution of the program in \autoref{sec2_expr_all} on the abstract syntax tree
\begin{lstlisting}
ADD (SUB (NUM 0) (NUM 4)) (NUM 5)
\end{lstlisting}
and the concrete syntax tree denoted by the string
\[ (-1 + 2 * 3) \]
The abstract syntax tree is obtained from the concrete syntax tree by ignoring the pair of parentheses, desugaring the negation to a subtraction, and replacing the number~$1$ with~$4$ and the multiplication subexpression with~$5$.
Executing the program will leave the pair of parentheses intact, update the number~$1$ in the concrete syntax tree with~$4$, preserving the negation (instead of changing the negation to a subtraction), and update the multiplication subexpression to~$5$.
Initially the types of the two trees are assumed to match those declared for the first group, and hence we try the first action in the group, at line~26.
The view-side pattern matching succeeds but the source-side one fails, because the first production rule used for the source is not \lstinline[breaklines=true]{Expr -> Expr '+' Term} but \lstinline[breaklines=true]{Expr -> Term} (followed by \lstinline[breaklines=true]{Term -> Factor} and then \lstinline{Factor -> '(' Expr ')'}, in order to produce the pair of parentheses).
So, instead, the action at line~28 is matched.
The update specified by this action is to proceed with updating the subtree produced from \lstinline{Term}, so we move on to the second group.
Similarly, the actions at lines 33~and~38 match, and we are now updating the subtree $-1 + 2 * 3$ produced from \lstinline{Expr} inside the parentheses.
Note that, at this point, the parentheses have been preserved.
For this subtree, we should again try the first group of actions, and this time the first action (at line~26) matches, meaning that we should update the subtrees $-1$~and $2 * 3$ with \lstinline[breaklines=true]{SUB (NUM 0) (NUM 4)} and \lstinline{NUM 5} respectively.
For the update of $-1$, we go through the actions at lines 28,~33, 36, and~37, eventually updating the number~$1$ with~$4$, preserving the negation.
As for the update of $2 * 3$, all the actions in the group \lstinline{Arith +> Term} fail, so we create a new concrete syntax tree from \lstinline{NUM 5} by going through the actions at lines 33~and~37.

So far we have been describing the putback semantics of the \BiYacc\ program, but we may also work out its get semantics by intuitively reading the actions in \autoref{sec2_expr_all} from right to left (which might remind the reader of the usual \textsc{Yacc} actions from this opposite angle):
The production rules for addition, subtraction, multiplication, and division expressions are converted to the corresponding constructors, and the production rule for negation expressions is converted to a subtraction whose left operand is zero.
The other production rules are ignored and do not appear in the resulting abstract syntax tree.

\subsection{Distinct features}
We conclude this section by highlighting the distinct features of \BiYacc\ listed in \autoref{introduction} with the expression example.

\subsubsection{Updating actions}
\BiYacc\ is distinct from conventional pretty-printers in that it does not pretty-print a new program text from scratch, but update the original program text with the abstract syntax tree, possibly preserving some syntactic structure.
For example, when a source expression has a pair of parentheses, the action \lstinline{arith -> '(' (arith => Expr) ')'}   will update the inner expression, leaving the parentheses intact.
%We can still do conventional pretty-printing with \BiYacc\ by using an empty source expression as input, thanks to the powerful adaptation mechanism.

% get semantic
% \subsubsection{The get semantics}
% As the arithmetic expression example shows, the get semantics is automatically derived from the putback semantics. It is the key feature for describing both a parser and a pretty-printer in a single program.

% adaptation
\subsubsection{Adaptation}
\label{hadaptation}

With the adaptation mechanism, the user only needs to write actions that make sense: for example, we relate addition expressions in the abstract and concrete syntax with the action \lstinline[breaklines=true]{ADD x y -> (x => Expr) '+' (y => Term)}, and do not need to worry about how an abstract addition is synchronised with concrete subtraction, multiplication, etc --- mismatching cases are automatically handled by adaptation.
As demonstrated in \autoref{running}, there is no action that can handle \lstinline{NUM 5} on the abstract side and $2 \times 3$ on the concrete side, but the adaptation mechanism would go through the user-defined actions and correctly change the concrete side to~$5$.

% deep pattern matching
\subsubsection{Pattern matching}
The pattern matching notation contributes significantly to the conciseness of \BiYacc\ programs.
For example, the action \lstinline[breaklines=true]{SUB (NUM "0") rhs -> '-' (rhs => Factor)} cleanly expresses several conditions on the view and the source, and how components of the view are embedded into the source.
The description would have been more verbose if pattern matching were not available.
(See \autoref{tiger} for more powerful examples.)

