% !TEX root = BiYacc.tex

\section{Introduction}
\label{introduction}

%\todo{A new story: 
%(1)parsers and pretty printers are important and should be designed at the sametime and envolved together; => one (ad-hoc) description;
%(2) parsers and pretty printers are not only used once: dynamically and repeadetely used when in reporting errors or showing execution to the users => update-based pp is required. So far, too ad hoc.
%(3) Two challenges: various pp update strategy, property-preservation, adaptation, compositional
%}

Whenever we come up with a new programming language, general or domain-specific, as the front-end part of the system we need to design and implement a parser and a pretty-printer to convert between program text and an internal representation.
A piece of program text, while conforming to a \emph{concrete syntax} specification, is a flat string that can be easily edited by the programmer.
The parser extracts the tree structure from such a string and parses it to an \emph{abstract syntax} tree, which is a structured and simplified representation that is easier for the backend to manipulate.
On the other hand, a pretty-printer flattens an abstract syntax tree to a string, which is typically in a human readable format.
This is useful for debugging the system or reporting internal information to the user.

Parsers and pretty-printers do conversions in opposite directions and are intimately related --- for example, a string pretty-printed from an abstract syntax tree should be parsed to the same tree. It is certainly far from being economical
to write parsers and pretty-printers separately: the parser and the pretty-printer need to be revised from time to time as the language evolves, and each time, we must revise the parser and the pretty-printer and also keep them consistent with each other, which is a time-consuming and error-prone task. 
In response to this problem, many domain-specific languages~\cite{Rant04,Bout96,Viss97,Rant11,DuJa11,ReOs10,MaWa13} have been proposed, in which the user can describe both a parser and a pretty-printer in a single program. By unifying these two pieces of software and deriving them from single, unambiguous and centralised code, we are creating a unified environment,
which is easier to maintain and update, therefore respecting the ``Don't Repeat Yourself'' principle of software development~\cite{HuntThomas99}.

Despite their advantages,  
these domain-specific languages cannot deal with synchronisation between the concrete and abstract syntax trees, in the sense that a pretty-printer will print the concrete syntax text from scratch without taking into account the original concrete syntax text even if it already exists. 
This has a serious disadvantage when the parser and the pretty-printer are used as a bridge between the system and the user in practice.
It is indeed troublesome, for example,
\begin{itemize}
\item in program debugging where 
part of the original program in an error message is displayed much differently from its original form \cite{deJo02,KoLa03}, 
\item in program refactoring where the comments and layout in the original program are completely lost after the abstract syntax tree is transformed by the system \cite{deVi11}, or
\item in language-based editors, as introduced by Reps~\cite{RTD83,RT89b}, where the user needs to interact with different pretty-printed representations of the same underlying abstract syntax tree.
\end{itemize}
Although many attempts have been made to enrich the abstract syntax trees with more information to make the pretty-printer print better, 
the printing algorithms are fixed and the systems are hard-coded, 
which prevents the user 
from both controlling information to be preserved and describing specific strategies to be used in pretty-printing.

To address the problem, we propose a new domain-specific language 
 \BiYacc, in which the user can describe both a parser and an \emph{extended} pretty-printer in a single program.
The extended pretty-printer generated from a \BiYacc\ program is unique and distinct from traditional pretty-printers: it takes not only an abstract syntax tree but also a piece of program text, and produces an updated piece of program text into which information from the abstract syntax tree is properly embedded. 

We illustrate this extended notion of pretty-printing with the following small but non-trivial example (which is also used as the running example in subsequent sections) about a simple language of arithmetic expressions.
The concrete syntax has the four elementary arithmetic operations, negation, and parentheses, while the abstract syntax has only the four elementary arithmetic operations --- negated expressions are represented in the abstract syntax as subtraction expressions whose left operand is zero, and parenthesised expressions are translated into tree structures.
Now suppose that we write an arithmetic expression as a plain string, and parse it to an abstract syntax tree.
Later, the abstract syntax tree is somehow modified (say, after some optimisation done by the compiler), and we want to pretty-print it back to a string for the user to compare with what was written originally (say, to understand what the compiler's optimisation does).%

To make it easier for the user to compare these two strings, we should try to maintain the syntactic characteristics of the original string when producing the updated string.
Here we may choose to
\begin{itemize}
  \item preserve all brackets --- even redundant ones --- in the original string, and
  \item preserve negation expressions in the original string instead of changing them to subtraction expressions.
\end{itemize}
For example, the string ``$((-1))$'' is parsed to an abstract syntax tree ``\lstinline{SUB (NUM 0) (NUM 1)}'' (a subtraction node whose left subtree is a numeral node~\lstinline{0} and right subtree is another numeral node~\lstinline{1}); if we change the abstract syntax tree to ``\lstinline{SUB (NUM 0) (NUM 2)}'' and update the string with the pretty-printer, we get ``$((-2))$'' instead of ``$0 - 2$''.
(\autoref{informal_introduction} will present a \BiYacc\ program that describes exactly this pretty-printing strategy.)
Our notion of extended pretty-printing is a generalisation of the traditional one, because our pretty-printer can accept an abstract syntax tree and an \emph{empty} string, in which case it will behave just like a traditional pretty-printer, producing a new string depending on the abstract syntax tree only.

The main contributions of the paper are summarised as follows.
\begin{itemize}
\item 
We design a novel language \BiYacc, which can describe both a parser and an extended pretty-printer, not only for parsing and pretty-printing as usual, but also for synchronising the concrete and abstract representations of programs. It is inspired by the parser generator \Yacc~\cite{Yacc}, utilises the new powerful putback-based bidirectional programming theory~\cite{HuPF14,FiHP15}, and achieves bidirectionality by providing three distinct language constructs: updating actions, adaptation mechanism, and pattern matching.

%\todo{So far, it is not clear how much we should add to parser to 
%achieve update-based pp. Our method shows that this can be 
%programmed and parser can be automatically derived.}

\item We propose a translation algorithm from \BiYacc\ to the bidirectional programming language \BiFluX~\cite{PaZH14}, automatically deriving a parser and 
a pretty-printer that satisfy the \emph{round-trip consistency property}: 
an unchanged abstract syntax
tree will have no effect in pretty-printing on the concrete syntax representation 
if it already exists, and an updated concrete syntax representation after 
printing will be parsed exactly to the same abstract syntax tree before pretty-printing.
%(See \autoref{biflux} for details.)

\item As a proof of the concept, we have successfully implemented and tested a prototype system\footnote{The system is available at \url{http://www.prg.nii.ac.jp/project/biyacc.html}, which contains an interactive demo and all source code.} with some interesting 
examples, including those discussed in this paper. 
Though more engineering work is required to make our system
really useful in practice (in particular, a more efficient underlying bidirectional transformation engine needs to be developed), we have started testing with a bigger example
(see Section \ref{tiger}) and got positive feedback in the expressiveness of \BiYacc.
% with small subsets.

\end{itemize}

The rest of the paper is organised as follows.
\autoref{informal_introduction} gives a high-level introduction to \BiYacc\, (syntax and semantics) using a simple example and explains its basic features.
\autoref{biflux} explains the putback-based bidirectional transformation foundation 
for realising \BiYacc\, together with the basics of \BiFluX, a putback-based bidirectional
transformation language. 
We present the details of how we translate the \BiYacc\ program to a runnable \BiFluX\ program in \autoref{translation}, and demonstrate the expressive power of \BiYacc\ using an example of the \Tiger\ language.
We highlight the future work in \autoref{future-work}, 
%we discusses some more interesting topics about \BiYacc: expressive power, time efficiency, the XML-free implementation, and useful functionalities to be fulfilled
discuss the related work in \autoref{sec:relatedWork},
and conclude the paper in \autoref{sec:conclusion}.


