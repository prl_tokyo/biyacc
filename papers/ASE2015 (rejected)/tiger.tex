% !TEX root = BiYacc.tex

\section{Expressive power of \BiYacc}
\label{tiger}

The design of \BiYacc\ may look simplistic and make the reader wonder how much it can describe.
However, in this section we demonstrate with a larger case study that, without any extension, \BiYacc\ can already handle real-world language features.

The \Tiger\ language is a simple, statically typed imperative language first presented in \cite{Appel98}. Despite being a small language, \Tiger\ contains many characteristics of bigger, well-known programming languages such as \textsc{Java} and \textsc{C}. These include, for example, conditionals, for-loops, variable declarations and usage, function definitions and function calls.
Some of these characteristics can be seen in this \Tiger\ program:
\begin{lstlisting}
function foo() =
    (for i := 0 to 10
     do (print(if i < 5 then "smaller"
                        else "bigger");
         print("\n"); ); )
\end{lstlisting}
Despite being a small language, the features of \Tiger\ make it frequently used in the academia to teach compiler construction.
(In fact it was first introduced as a running example in \cite{Appel98} with the same purpose, describing the various steps to compiler construction.)
For the same reasons, in the context of \BiYacc, \Tiger\ is a good scenario to test the potential of our BX-based approach to parsers and pretty-printers.

\begin{figure}[t]
\begin{lstlisting}[emphstyle={\color{blue}\bfseries}]
Concrete

EXPR -> 'nil'
      | 'break'
      | BINARYOP
      | LVALUE ':=' EXPR
      | 'for' String ':=' EXPR 'to' EXPR 'do' EXPR
      | 'while' EXPR 'do' EXPR
      | 'let' DECLARATIONLIST 'in' EXPRSEQ 'end'
      | '(' EXPRSEQ ')'
      | String '(' EXPRLIST ')'
      | String '{' FIELDLIST '}'
      | 'if' EXPR 'then' EXPR 'else' EXPR
      | 'if' EXPR 'then' EXPR

EXPRLIST -> EXPR
          | EXPRLIST ',' EXPR

FIELDLIST -> String '=' EXPR
           | FIELDLIST ',' String '=' EXPR

DECLARATIONLIST -> DECLARATION
                 | DECLARATION DECLARATIONLIST

DECLARATION -> TYPEDECLARATION
             | VARIABLEDECLARATION
             | FUNCTIONDECLARATION
~\ldots~
\end{lstlisting}
\caption{A partial \BiYacc\ representation of the concrete version of \Tiger.}
\label{partial-tiger}
\end{figure}

We present in \autoref{partial-tiger} part of the \BiYacc\ concrete representation of \Tiger\ written in \BiYacc.
The concrete representation of this language (representing the grammar) contains a number of nonterminal symbols and production rules that can test \BiYacc\ when transforming between its concrete and abstract versions.
Here, although multiple datatypes and instances are possible in the source, there are optimal representations produced by the bidirectional transformations maintaining the syntactic structure of the original source.
%\todo{maybe should use another word}

\begin{figure}[t]
\begin{lstlisting}[emphstyle={\color{blue}\bfseries}]
Actions
~\ldots~
ArithIfThenElse lhs mhs rhs
  -> 'if' (lhs => EXPR) 'then' (mhs => EXPR)
                        'else' (rhs => EXPR)
ArithIfThenElse lhs mhs ArithNil
  -> 'if' (lhs => EXPR) 'then' (mhs => EXPR)
~\ldots~
\end{lstlisting}
\caption{A partial \BiYacc\ representation of actions to transform between the concrete and the abstract representation of \Tiger.}
\label{tiger:2}
\end{figure}

Next we present a subset of the transformation between the concrete and abstract representations of \Tiger.
In \autoref{tiger:2} we can see that there are two actions differs in the right-hand side, while in the abstract representation of \Tiger\ there is only one constructor representing conditionals, \lstinline{ArithIfThenElse}.


When the abstract representation of \lstinline{if} ends with \lstinline{ArithNil} (second rule), this means the original source was an \lstinline{if}-\lstinline{then}, whereas in the first case the original source had an \lstinline{if}-\lstinline{then}-\lstinline{else}.
This is extremely useful, because it means the abstract representation is as simple as possible and easier to handle, but creates difficulties because the backward transformation should create a new source as similar as possible to the original.

This is a powerful representation of the simplification introduced by \BiYacc, both for the programmer of the transformation, for whom mapping between different data types is no longer a concern, and for the language engineer, that has a simple abstract data type to work with without paying the cost of having a poorer concrete representation of the language.
The bidirectional engine of \BiYacc\ saves time and programming effort in both scenarios.

Another scenario where the expressive power of our approach is helpful is in language evolution. Suppose we want to evolve \Tiger\ so that a new specific type of variable, \emph{pointer}, is available.
This new type represents an address that does not have to be initialised in memory, and will be helpful for programmers to optimise their code in the new target hardware \Tiger\ will be used in.

This introduction is extremely useful to programmers of \Tiger, but implies editing the complete ecosystem of the language.
The parser, the pretty printer, the transformation, all these artifacts have to evolve to support the new extension.
With \BiYacc\ however, evolving \Tiger\ is simplified.
We would have to necessarily introduce information on the concrete representation of the language, and a new transformation rule of the form:

\begin{center}
\begin{tabular}{c}
{\begin{lstlisting}[numbers=left, numberstyle=\tiny, emphstyle={\color{blue}\bfseries}]
ArithTypeField lhs ArithNil
                 -> 'pointer' (lhs => String)
\end{lstlisting}}
\end{tabular}
\end{center}

After introducing the information in the concrete representation and this instruction in the \lstinline{Action}, \BiYacc\ will automatically generate a pretty printer, a parser, and a transformation system that guarantees a generation of good concrete representations.

The biggest advantage however, is that the data type \lstinline{ArithTypeField} already existed in the abstract representation. This means all the engineering efforts regarding refactorings or optimisations on the language, which were developed in its abstract form for convenience, maintain their validity. BiYacc provides us the expressivity to evolve a language and, together with the bidirectional, putback-based engine, an ecosystem where this evolution is highly facilitated.

There will be scenarios where the evolution of the source language will enforce an evolution in the abstract representation as well, but in this cases the usage of \BiYacc\ will not be more complex that it is in a more traditional approach. We believe however that will be instances where this evolution can be guaranteed not to touch the abstract representation, thanks to the ability of \BiYacc\ on mapping between different, heterogeneous productions both in the concrete and in the abstract representations

The these features and examples on \Tiger\ can be easily extrapolated to work on other programming languages, and the language is complex enough to describe interesting transformations to simpler, abstract representations that have multiple choices on the source.
The backward transformation should have these concerns and make the best options.
In scenarios as the ones represented here with the \Tiger\ language, \emph{putback-based} bidirectional transformation allow an easy definition of transformation where redundancy is automatically solved.

The full \BiYacc\ program for \Tiger, with its concrete and abstract representations and the actions that describe the transformations between these two formats, can be found in \url{http://www.prg.nii.ac.jp/project/tiger.html}.

%@book{Appel:1997:MCI:275565,
% author = {Appel, Andrew W.},
% title = {Modern Compiler Implementation in Java},
% year = {1998},
% isbn = {0-521-58388-8},
% publisher = {Cambridge University Press},
% address = {New York, NY, USA},
%} 



%\begin{lstlisting}
%arith -> (arith => EXPRSEQ) ';' (EXPR -> 'nil')
%\end{lstlisting}
%or synchronising \lstinline{if}-\lstinline{then}-\lstinline{else} statements in the abstract syntax with \lstinline{if}-\lstinline{then} statements in the concrete syntax:
%\begin{lstlisting}
%ArithIfThenElse lhs mhs rhs ->
%  'if' (lhs => EXPR) 'then' (mhs => EXPR)
%                     'else' (rhs => EXPR)
%ArithIfThenElse lhs mhs ArithNil ->
%  'if' (lhs => EXPR) 'then' (mhs => EXPR)
%\end{lstlisting}







%\subsection{Expressive power of \BiYacc}
%\label{tiger}

%We have demonstrated \BiYacc\ with the small (but already non-trivial) arithmetic expression example.
%This is not all that \BiYacc\ can handle, however:
%even without extension, \BiYacc\ is powerful enough to deal with the \Tiger\ language~\cite{Appel98}.\todo{See appendix.}\
%\Tiger\ is a simplified imperative programming language but contains key features of real-world programming languages.
%It is straightforward to express both the concrete syntax and abstract syntax for \Tiger\ in \BiYacc, and we can describe non-trivial updating strategies such as skipping empty expressions in the concrete syntax:
%\begin{lstlisting}
%arith -> (arith => EXPRSEQ) ';' (EXPR -> 'nil')
%\end{lstlisting}
%or synchronising \lstinline{if}-\lstinline{then}-\lstinline{else} statements in the abstract syntax with \lstinline{if}-\lstinline{then} statements in the concrete syntax:
%\begin{lstlisting}
%ArithIfThenElse lhs mhs rhs ->
%  'if' (lhs => EXPR) 'then' (mhs => EXPR)
%                     'else' (rhs => EXPR)
%ArithIfThenElse lhs mhs ArithNil ->
%  'if' (lhs => EXPR) 'then' (mhs => EXPR)
%\end{lstlisting}
%The behaviour of the parser derived from the \BiYacc\ program becomes non-trivial too: when converting a concrete syntax tree to an abstract syntax tree, the parser removes all occurrences of empty expressions and desugars \lstinline{if}-\lstinline{then} statements to \lstinline{if}-\lstinline{then}-\lstinline{else} statements.
%Note how naturally such behaviour can be specified due to \BiYacc's strong support of pattern matching, which is capable of describing expression sequences whose last element is an empty expression and \lstinline{if}-\lstinline{then}-\lstinline{else} statements whose \lstinline{else}-branch is an empty statement.
