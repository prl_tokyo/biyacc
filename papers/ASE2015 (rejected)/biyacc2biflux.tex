% !TEX root = BiYacc.tex

\section{Translation to BiFluX}
\label{translation}

Having explained the architecture of the translation from \BiYacc\ to \BiFluX~(\autoref{architecture}) and the necessary \BiFluX\ operations in the previous section, we are now ready to explain the translation itself.
The three parts of a \BiYacc\ program are respectively translated to a DTD for view XMLs, a DTD for source XMLs, and a \BiFluX\ program for synchronising XML documents conforming to these two DTDs.

\subsection{Translating the abstract syntax to a DTD}

The \lstinline{Abstract} part of the \BiYacc\ program in \autoref{sec2_expr_all} is translated straightforwardly to the following DTD:
\begin{lstlisting}
<!DOCTYPE arith[

<!ELEMENT arith (add|sub|mul|div|num)>
<!ELEMENT add (arith, arith)>
<!ELEMENT sub (arith, arith)>
<!ELEMENT mul (arith, arith)>
<!ELEMENT div (arith, arith)>
<!ELEMENT num (biyaccpcdata)>
<!ELEMENT biyaccpcdata (#PCDATA)>

]>
\end{lstlisting}
Each datatype and its constructors are translated into XML element definitions.
For example, \lstinline{Arith} is translated into an element definition \lstinline{arith} whose child element can be one of the elements translated from \lstinline{ADD}, \lstinline{SUB}, \lstinline{MUL}, \lstinline{DIV}, and \lstinline{NUM}, and each of the elements translated from the first four constructors contains two \lstinline{arith} elements as its child elements.
(\BiFluX\ requires that the name of an XML element contains only lower case letters and digits, so \BiYacc\ changes all the names into lower case.)
The first datatype defined in the abstract syntax (\lstinline{arith} for this example) is declared as the XML document type (\lstinline{DOCTYPE}), i.e., the type of the topmost element in an XML document.

We use the primitive datatype (or nonterminal) \lstinline{String} in the abstract (or concrete) syntax wherever we need a string --- in the constructor \lstinline{NUM}, for example.
%This includes not only string in common sense but also \emph{Boolean}, \emph{Integer}, etc because all of the user input will be treated as string in our implementation.
In XML, a string is usually represented by \lstinline{#PCDATA}, but here we cannot directly translate an occurrence of \lstinline{String} to \lstinline{#PCDATA}, because \lstinline{#PCDATA} must be the only child or the first child of a group of choices of an element~\cite{bray1998extensible}, which may not be the case for constructors in general.
So, instead, we translate an occurrence of \lstinline{String} to a specially defined element \lstinline{biyaccpcdata}, whose only child is \lstinline{#PCDATA}.

\subsection{Translating concrete syntax to DTD file}

The concrete syntax defined in \autoref{sec2_expr_all} is translated to the following DTD:
\begin{lstlisting}
<!DOCTYPE expr[

<!ELEMENT expr (a0|a1|a2|null)>
<!ELEMENT a0 (expr, term)>
<!ELEMENT a1 (expr, term)>
<!ELEMENT a2 (term)>
~\ldots~
<!ELEMENT biyaccpcdata (#PCDATA)>
<!ELEMENT null EMPTY>

]>
\end{lstlisting}
The idea is to view nonterminals as datatypes and production rules as constructors.
Unlike the constructors in the abstract syntax, the production rules are not given names, so we generate distinct names for them in the form of a letter ``\lstinline{a}'' followed by a number.
Under each nonterminal we insert an additional empty constructor \lstinline{null}, so we can write, for example, \lstinline{<expr><null/></expr>} to denote an empty expression.
This is used as a default input to a \BiYacc\ pretty-printer wherever we wish to create a new source depending on the view only.
The rest of the translation is similar to the one for the abstract syntax.

\subsection{Translating actions to \BiFluX\ program}


In a nutshell: Each action (rather than a group of actions) is translated into a single procedure in \BiFluX, which contains two levels of pattern matching on the view and the source respectively.
If both matchings succeed, the update specified by the action is executed; otherwise, the procedure for the next action is called.
At the end of each group, a special adaptation action is implicitly inserted, which changes the source to a new form so one of the actions in the group can apply.
Below we go into a bit more detail of this translation.

\subsection*{Generating synchronisation procedures}
The name of the procedure translated from an action begins with ``\lstinline{sync}'', followed by the source and view types of the group to which the action belongs, and ends with a number corresponding to the position of the action in the group.
For example, the first action
\begin{lstlisting}
SUB (NUM "0") y -> '-' (y => Factor)
\end{lstlisting}
in the group \lstinline[breaklines=true]{Arith +> Factor} is translated into the following \BiFluX\ procedure:
\begin{lstlisting}
PROCEDURE syncFactorArith0(SOURCE $s AS factor,
                           VIEW   $v AS arith) =
  CASE $v OF
  { arith[sub[arith[num[biyaccpcdata["0"]]],
              $y AS arith]] ->
      CASE $source OF
      { factor[a6[$factor AS factor]] ->
          syncFactorArith0($factor, $y)
      | $wholeSource ->
          syncFactorArith1(
            $s,
            <arith><sub>
              <arith><num>
                <biyaccpcdata>0</biyaccpcdata>
              </num></arith>
              {$y}
            </sub></arith>)
      }
  | $wholeView ->
      syncFactorArith1($s, $wholeView)
  }
\end{lstlisting}
The simultaneous pattern matching is translated into a first pattern matching on the view~\lstinline{$v} and then a second pattern matching on the source~\lstinline{$s}.
\begin{itemize}
\item If both succeed, we update the inner \lstinline{$factor} of the source with the subtree~\lstinline{$y} of the view by calling the procedure \lstinline{syncFactorArith0} itself recursively.
In general, we may call a procedure translated from the first action of a different group, as determined by the types of the source and the view.
Also, not all updates are translated to procedure calls: an update to \lstinline{String} is translated to a replacing statement instead.
\item Otherwise, if either of the two pattern matchings fails (i.e., falling to one of the two default cases with only a variable pattern \lstinline{$wholeSource} or \lstinline{$wholeView}), we go to the next action in the same group by calling \lstinline{syncFactorArith1}.
\end{itemize}

\subsection*{Generating adaptation procedures}
When all of the actions in a group fail, we should try to create a new source such that one of the actions can be matched.
We may think of this behaviour as being handled by a special adaptation action that is implicitly added to the end of each action group.
For example, the procedure generated for this special action at the end of the group \lstinline{Arith +> Factor} is as follows:
\begin{lstlisting}
PROCEDURE syncFactorArith3(SOURCE $s AS factor,
                           VIEW   $v AS arith) =
  CASE $v OF
  { arith[sub[arith[num[biyaccpcdata["0"]]],
              $y AS arith]] ->
      CASE $source OF
      { factor[a6[$factor AS factor]] ->
          syncFactorArith0($factor, $y)
      | $wholeSource ->
          ADAPT SOURCE BY
            CREATE VALUE <factor><a6>
                           <factor><null/></factor>
                         </a6></factor>
      }
  | arith[num[biyaccpcdata[$n AS String]]] ->
      ~\ldots~
  ~\ldots~
  }
\end{lstlisting}
It first uses a view case statement to choose an action that matches the view only.
Once an action is chosen, a source case statement follows, which tries to match the source with the source pattern of the chosen action.
Normally this would fail (otherwise we would not have reached this special action), so we fall to the adaptation branch, which creates a new source of the same form as the source pattern.
Afterwards, the source case statement will be executed again, and this time it will match the source pattern.
The updates specified by the action will then be executed on this new source.

\subsection*{Generating the entry point}
Finally, we need to declare the entry point of the program, which we choose to be the procedure translated from the first action in the first group.
This corresponds to our assumption that the initial input concrete and abstract syntax trees are of the types specified for the first action group.

