% !TEX root = BiYacc.tex

\section{Future work}
\label{future-work}

%In this section we discuss and highlight more interesting topics about \BiYacc: expressive power, time efficiency, the XML-free implementation, and useful functionalities to be fulfilled. 

Although our implementation of \BiYacc\ is just a prototype, it has shown a lot of potential, as demonstrated in previous sections.
Below we list a few improvements that will make \BiYacc\ into a mature tool.

\subsection{Efficiency issue}

The efficiency of our prototype implementation of \BiYacc\ is not always satisfactory --- for deeply structured inputs like expressions wrapped inside multiple pairs of redundant parentheses, the parser may require time exponential in the depth of the structure to deliver its result.
While the translation algorithm of \BiYacc\ can deal with all the examples --- including the \Tiger\ language --- in this paper without difficulty, the inefficiency of the final executable prevents us from conducting more realistic experiments.
Nevertheless, we must emphasise that the problem lies at the underlying BX level, and is not an inherent problem of \BiYacc: the code is translated into deeply nested view case statements in \BiFluX, and currently the behaviour for view case statements in \BiFluX\ is an inefficient exhaustive search.
We believe that the efficiency of \BiFluX\ can be greatly improved in the near future, and then \BiYacc\ will be able to get a performance boost without essential changes.

%\subsection{XML-free implementation}
%
%It might raise concerns that \BiYacc\ --- implemented through \BiFluX\ --- is unnecessarily bound to XML.
%What we present in this paper is, however, only a prototype implementation for demonstrating the potential of our approach.
%We chose \BiFluX\ as the backend of \BiYacc\ simply for convenience: \BiFluX\ is the only putback-based bidirectional language in existence, and it just happens to be the case that \BiFluX\ is designed for processing XMLs.
%In a separate project, we are developing a datatype-generic putback-based bidirectional language (which also aims to address the efficiency problem mentioned in the previous subsection), and \BiYacc\ will be among the first applications to be implemented in this language, allowing a more direct and seamless integration with other parts of a compiler.

\subsection{Extensions to \BiYacc}

While \BiYacc\ in its current state is already quite expressive, there is still a lot of room for extension.
For example, we intend to support \emph{non-linear patterns} in the next version of \BiYacc:
Suppose that we extend the concrete syntax of arithmetic expressions with a production rule of squared expressions:
\begin{lstlisting}
Factor -> Factor '^2'
\end{lstlisting}
Then we may add an action linking squared expressions in the concrete syntax with multiplication of an expression with itself in the abstract syntax:
\begin{lstlisting}
MUL x x -> (x => Factor) '^2'
\end{lstlisting}
The duplicated appearances of~\lstinline{x} in the view pattern says concisely that, for an abstract syntax tree to match the pattern, its two subtrees must be exactly the same --- there is implicitly a conditional structure in this action.
In general, we might add more control structures to \BiYacc\ (either implicitly or explicitly), to express more sophisticated parsing and pretty-printing strategies.

\subsection{Parsers and pretty-printers to and from XMLs}
\label{generalised-parsing}

We mentioned that we need additional pairs of parsers and pretty-printers to do format conversion between well-formed strings and XML concrete syntax trees and between abstract syntax trees and their XML representations.
In particular, the parsers simply utilise existing parsing technology to extract tree structure from unstructured strings, and their role is independent from \BiYacc\ actions, which specify non-trivial synchronisation between concrete and abstract syntax trees.
It is thus possible to independently upgrade this component of \BiYacc\ and deal with more powerful grammars, particularly for the concrete syntax side.

Traditional \Yacc-like parser generators usually structure the language specification in two phases: the lexical phase, specified via regular expressions, and the syntactic phase, specified via (unambiguous) context-free grammars~(CFGs).
Moreover, semantic actions are associated to the grammar productions (usually written in the parser implementation language), which are executed at parse time.
Thus, the CFG is ``polluted'' with such code, limiting its
reusability.

For future versions of \BiYacc, we intend to follow recent advances in parsing technology where for
any (pure) CFG, powerful generalised parsing techniques \cite{glr2009,gll2015} are used to implement efficient parsers for the underlying language.
Such generalised parsers always produce concrete syntax trees as the result of processing an input text.
These parsing techniques are combined with disambiguation rules in order to define unambiguous parsers so that a single syntax tree is produced for a giving input.
Moreover, in a pure (scannerless) CFG setting, lexical symbols are defined in the productions rules via regular expressions.
Intuitively, a scannerless parser uses the power of CFGs instead of regular expressions to tokenise an input string.
This makes not only the language specification more concise and clear, but also provides a modular approach to language definition:
two grammars can easily be combined without the grammar writer having to deal with regular expressions defining similar language keywords/identifiers that may exist in the grammars/languages. For example, the production rule at line~20 of \autoref{sec2_expr_all} can be written instead as \lstinline{Factor -> '(0-9)+'}.
The pretty-printing of the concrete syntax tree can also be fully generated from the concrete CFG
definition~\cite{deJonge2002b}.
So it is feasible to automatically generate the pair of string parser and pretty-printer from the
concrete context-free grammar.


%This can be done mechanically by utilising existing tools such as \emph{Happy} \todo {maybe need ref?} since the concrete syntax we defined in the program are exactly production rules.
%For instance, \lstinline{EXPR -> LVALUE ':=' EXPR} can be translated to \lstinline!EXPR : LVALUE ':=' EXPR {ExprA3 $1 $3}!.
%We name the constructor in a way that it starts with the type ``Expr'' of the production rule, followed by the letter ``A'' and a number ``3'' indicating it is the third production rule of type ``Expr''.
%For every non-terminal symbols, we use a fold (i.e. \$1, and \$3 here) to represent them and for terminal symbols we just ignore them.
%As for the pretty-printer part, \lstinline{ExprA3 x y} can be translated to \lstinline{pprint x ++ ":=" ++ pprint y}, \todo{in fact I am not sure how to generate the PP. Is there any existing tools? Or we write this using template Haskell?}
%where all the terminal symbols and their position (here ``:='' only) can be inferred from the production rule and thus be inserted to the output string correctly.
%The function \lstinline{++} is used to join strings and the function \lstinline{pprint} will invoke proper pretty-printing functions according to the type of its parameters, and terminated when it finally prints a string. 
%So it is feasible to automatically generate the pair of string parser and pretty-printer.

%\subsection{Types and type systems}
%
%As our language aims to provide users with a pair of the parser and the pretty-printer, it will be convenient if users can make constraints on the atomic \emph{String} type by using regular expressions.
%E.g. the variable name should start with a ``\$'' symbol and the type constructor must begin with an upper case character, etc.
%Without these simple checks, errors may only be detected at the back end of a compiler and thus comparatively difficult to be reported to the user.
%Besides, even a simple statically-checked type system can help users more easily debug and compose less error-prone programs. \todo{In fact, for this part, I do not know...}
%This is another future work to be fulfilled.
