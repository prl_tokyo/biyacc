% !TEX root = BiYacc.tex

\paragraph{Comparison with our earlier prototype.}
This paper is primarily based on and significantly extends our previous tool demonstration paper~\citep{zhu2015biyacc},
which conducts an early experiment with the idea of casting parsing and reflective printing in the framework of bidirectional transformations.
Compared to our previous prototype, the current system has the following improvements:
\begin{itemize}[leftmargin=*]
\item The well-behavedness of the underlying bidirectional language \bigul~(\autoref{sec:bigul}) we use in this version is formally verified~\citep{ko2016bigul}, so we can guarantee the well-behavedness of \biyacc\ programs much more confidently;
\item concrete parsers and printers between program text and CSTs are automatically derived (\autoref{sec:concrete-parser-and-printer}), so \biyacc\ synchronises program text and ASTs,
not just CSTs and ASTs as with the previous version;
\item we present many nontrivial applications such as resugaring and language evolution (\autoref{sec:tiger});
\item our current system tries to preserve layouts and comments in a transparent manner.
\end{itemize}

\paragraph{Unifying parsing and printing.}
Much research has been devoted to describing parsers and printers in a single program.
For example, both \citet{rendel2010invertible} and \citet{matsuda2013flippr} adopt a combinator-based approach,
where small components are glued together to yield more sophisticated behaviour,
and can guarantee inverse properties similar to (\ref{equ:ParsePrint})~and~(\ref{equ:PrintParse}) (with CST replaced by AST in the equations).
In \citet{rendel2010invertible}' system (called ``invertible syntax descriptions'', which we shorten to ISDs henceforth), both parsing and printing semantics are pre-defined in the combinators and the consistency is guaranteed by their partial isomorphisms,
whereas in \citet{matsuda2013flippr}'s system (called \flippr), the combinators describing pretty printing are lately removed by a semantic-preserving transformation to a syntax, which is further processed by their grammar-based inversion system to achieve the parsing semantics.
%
By specialising one of the syntax to be XML syntax, \citet{brabrand2008dual} present a tool XSugar that handles bijection between the XML syntax and any other syntax for a grammar, guaranteeing that the transformation is reversible.
However, the essential factor that distinguishes our system from others is that \biyacc\ is designed to handle synchronisation while others are all targeted at dealing with transformations.


%
Just like \biyacc, all of the systems described above handle unambiguous grammars without disambiguation declarations only.
When the user-defined grammar (or the derived grammar) is ambiguous, the behaviour of these systems is as follows:
Neither of ISDs and \flippr\ will notify the user that the (derived) grammar is ambiguous.
For ISDs, \hyperref[equ:PrintParse]{property (4)} will fail,
while for \flippr\ both properties \hyperref[equ:ParsePrint]{(3)}~and~\hyperref[equ:PrintParse]{(4)} will fail.
(Since the discussion on ambiguous grammars has not been presented in their papers, we test the examples provided by their libraries.)
In contrast, \citet{brabrand2008dual} give a detailed discussion about ambiguity detection, and XSugar can statically check that the transformations are reversible.
If any ambiguity in the program is detected, XSugar will notify the user of the precise location where ambiguity arises.
In \biyacc, the ambiguity analysis is performed by the parser generator employed in the system, and the result is reported at compile time.
If no warning is reported, the well-behavedness is always guaranteed, as explained in \autoref{sec:inverse-properties}.

Even though all these systems handle unambiguous grammars only, there are design differences between them.
An ISD is more like a parser, while \flippr\ lets the user describe a printer: To handle operator priorities, for example,
the user of ISDs will assign priorities to different operators, consume parentheses, and use combinators such as \lstinline{chainl} to handle left recursion in parsing, while the user of \flippr\ will produce necessary parentheses according to the operator priorities.
In \biyacc, the user defines the concrete syntax that has a hierarchical structure (\lstinline{Expr}, \lstinline{Term}, and \lstinline{Factor}) to express operator priority, and write printing strategies to produce (preserve) necessary parentheses.
The user of XSugar will also likely need to use such a hierarchical structure.

It is interesting to note that, the part producing parentheses in \flippr\ essentially corresponds to the hierarchical structure of grammars.
For example, to handle arithmetic expressions in \flippr, we can write:
\begin{lstlisting}
ppr' i (Minus x y) =
 parensIf (i >= 6) $ group $
   ppr 5 x <> nest 2
     (line' <> text "-" <> space' <> ppr 6 y);
\end{lstlisting}
\flippr\ will automatically expand the definition and derive a group of \lstinline{ppr_i} functions indexed by the priority integer~\lstinline{i}, corresponding to the hierarchical grammar structure.
In other words, there is no need to specify the concrete grammar, which is already implicitly embedded in the printer program.
This makes \flippr\ programs neat and concise.
Following this idea, \biyacc\ programs can also be made more concise:
In a \biyacc\ program, the user is allowed to omit the production rules in the concrete syntax part (or omit the whole concrete syntax part),
and they will be automatically generated by extracting the terminals and nonterminals in the right-hand sides of all actions.
However, if these production rules are supplied, \biyacc\ will perform some sanity checks:
It will make sure that, in an action group, the user has covered all of the production rules of the nonterminal appearing in the ``type declaration'', and never uses undefined production rules.


%Much research has been devoted to layout-preserving printing \cite{de2000pretty,van2000rewriting, van2001preserving,de2002pretty,kort2003parse}.
%
%In contrast to focusing on unifying parsing and printing, the problem of obtaining the fine-grained printing results from a CST
%%(or so-called parse tree)
%containing enough layout information is comprehensively discussed with proper benchmarks \todo{do not know how to express} by software engineering researchers.
%For instance,~\citet{van2001preserving} revealed some difficult parts of dealing with comments and raised many possible solutions via analysing documentary structure of the code.
%%
%In the work of~\citet{de2002pretty}, they proposed the notion of ``conservative pretty-printing'' stating that ``modifications made during software reengineering should be minimal'', which resembles our ``reflective printing''.
%They used a two-phases printing strategy by first translate the CST into a tree containing formatting instructions and then interpreted it to various output format based on users' demand.
%Other proposals such as make the printer language independent, extensible, and customisable can be found in the studies of~\citet{de2000pretty}.

% While the SE researchers\todo{too casually} solved the problem of printing with layouts from CST
% and the programming language scholars addressed the possibility of building a parser and a printer together,
% they have not worked out synchronising CST and AST with implicitly layouts kept, and there is not a running system that handles synchronisation and layouts preservation both.
% which turns out to be one of \todo{do we need one of?} our contributions.

%\todo{also xtext is somehow related}

%\todo{maybe we should explain get-based approaches here}
\paragraph{Bidirectional transformations (BXs).}
Our work is theoretically based on bidirectional transformations \cite{foster2007combinators,czarnecki2009bidirectional,hu2011dagstuhl},
particularly taking inspiration from the recent progress on putback-based bidirectional programming~\cite{pacheco2014monadic,pacheco2014biflux,hu2014validity,fischer2015essence,ko2016bigul}.
%which originated from the \emph{view-updating} problem in the database community~\cite{bancilhon1981update,dayal1982correct,gottlob1988properties}.
%Since the pioneering work of~\citet{} on a combinatorial language for bidirectional tree transformations, BXs have attracted a lot of attention from researchers in the communities of programming languages and software engineering~.
%As explained in \autoref{sec:bigul}, the purpose of bidirectional programming is to relieve the burden of thinking bidirectionally --- the programmer writes a program in only one direction, and a program in the other direction is derived automatically.
%We call a language \emph{get-based} when programs written in the language denote $\mathit{get}$ functions, and call a language \emph{putback-based} when its programs denote $\mathit{put}$ functions.
%In the context of parsing and reflecting printing, the get-based approach lets the programmer describe a parser, whereas the putback-based approach lets the programmer describe a printer.
%Below we compare \biyacc\ with a closely related, get-based system.
% Below we discuss in more depth how the putback-based methodology affects \biyacc's design by comparing \biyacc\ with a closely related, get-based system.
Also inspired by BXs, \citet{martins2014generating} introduces an attribute grammar--based system for defining transformations between two representations of languages (two grammars).
The utilisation is similar to \biyacc:
The programmer defines both grammars and a set of rules specifying a $\mathit{get}$ transformation, with a $\mathit{put}$ transformation being automatically generated.
%However, the well-behavedness of the two transformations is not mentioned in their paper, and hence might not be guaranteed.
%
One difference between \citeauthor{martins2014generating}'s system and ours is that their system performs synchronisation between two trees rather than program text and AST,
and thus the preservation of layouts and comments is not considered.
%
Another difference lies in the fact that, with their get-based system,
certain decisions on the backward transformation are, by design, permanently encoded in the bidirectionalisation system and cannot be controlled by the user,
whereas a putback-based system such as \biyacc\ can give the user fuller control.
%(See \citet{fischer2015essence} for detailed explanation.)


% \paragraph{Get-based vs putback-based parsing and reflective printing.}
% \citet{martins2014generating} introduces an attribute grammar--based BX system for defining transformations between two representations of languages (two grammars).
% The utilisation is similar to \biyacc:
% The programmer defines both grammars and a set of rules specifying a \emph{forward} transformation (i.e., $\mathit{get}$), with a backward transformation (i.e., $\mathit{put}$) being automatically generated.
% For example, the \biyacc\ actions in lines 32--34 of \autoref{fig:expr} can be expressed in \citeauthor{martins2014generating}'s system as
% \[ \setlength{\arraycolsep}{.2em}
% \begin{array}{lcl}
% \mathit{get}_A^{\,E}(\mathit{add}(l, \t{+}, r)) &\rightarrow& \mathrlap{\mathit{plus}}\phantom{\mathit{minus}}(\mathit{get}_A^{\,E}(l), \mathit{get}_A^{\,T}(r)) \\[.5ex]
% \mathit{get}_A^{\,E}(\mathrlap{\mathit{sub}}\phantom{\mathit{add}}(l, \t{-}, r)) &\rightarrow& \mathit{minus}(\mathit{get}_A^{\,E}(l), \mathit{get}_A^{\,T}(r)) \\[.5ex]
% \mathit{get}_A^{\,E}(\mathit{et}(t)) &\rightarrow& \mathit{get}_A^{\,T}(t)
% %getAE (sub(l, op, r)) → minus(getAE (l), getAT (r)) getAE (et(t)) → getAT (t)
% \end{array} \]
% which describes how to convert certain forms of CSTs to corresponding ASTs.
% The similarity is evident, and raises the question as to how get-based and putback-based approaches differ in the context of parsing and reflective printing.

% The difference lies in the fact that, with a get-based system, certain decisions on the backward transformation are, by design, permanently encoded in the bidirectionalisation system and cannot be controlled by the user, whereas a putback-based system can give the user fuller control.
% For example, when no source is given and more than one rules can be applied, \citeauthor{martins2014generating}'s system chooses, by design, the one that creates the most specialised version.
% This might or might not be ideal for the user of the system.
% For example:
% Suppose that we port to \citeauthor{martins2014generating}'s system the \biyacc\ action that relates \tiger's concrete `\lstinline{&}'~operator with a specialised abstract \lstinline{if} expression in \autoref{sec:synSugar}, coexisting with a more general rule that maps a concrete \lstinline{if} expression to an abstract \lstinline{if} expression.
% Then printing the AST \lstinline{TIf (TName "a") (TName "b") 0} from scratch will and can only produce \lstinline{a & b}, as dictated by the system's hard-wired printing logic.
% By contrast, the user of \biyacc\ can easily choose to print the AST from scratch as \lstinline{a & b} or \lstinline{if a then b else 0} by suitably ordering the actions.

% This difference is somewhat subtle, and one might argue that \citeauthor{martins2014generating}'s design simply went one step too far --- if their system had been designed to respect the rule ordering as specified by the user, as opposed to always choosing the most specialised rule, the system would have given its user the same flexibility as \biyacc.
% Interestingly, whether to let user-specified rule/action ordering affect the system's behaviour is, in this case, exactly the line between get-based and putback-based design.
% The user of \citeauthor{martins2014generating}'s system writes rules to specify a forward transformation, whose semantics is the same regardless of how the rules are ordered, and thus it would be unpleasantly surprising if the rule ordering turned out to affect the system's behaviour.
% We can view this from another angle:
% If the user is required to specify a forward transformation while customising the backward behaviour by carefully ordering the rules, then the purpose of a bidirectionalisation system --- which is to reduce the problem of writing bidirectional transformations to unidirectional programming --- is largely defeated.
% By contrast, the user of \biyacc\ only needs to think in one direction about the printing behaviour, for which it is natural to consider how the actions should be ordered when an AST has many corresponding CSTs; the parsing behaviour will then be automatically and uniquely determined.
% In short, relevance of action ordering is incompatible with get-based design, but is a natural consequence of putback-based thinking.
% Of course, it would be rather disappointing if all we can gain from adopting the putback-based approach was just the ability to customise the behaviour of printing from scratch.
% We will make another case for putback-based thinking by sketching one potential extension to \biyacc\ in \autoref{sec:globalMatching}.


