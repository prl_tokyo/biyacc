\documentclass[fleqn,10pt]{sigplanconf}

% The following \documentclass options may be useful:

% preprint      Remove this option only once the paper is in final form.
% 10pt          To set in 10-point type instead of 9-point.
% 11pt          To set in 11-point type instead of 9-point.
% numbers       To obtain numeric citation style instead of author/year.

\usepackage{ifthen}
\newcommand{\doubleblind}[2]{\ifthenelse{\boolean{doubleblind}}{#1}{#2}}

\usepackage{microtype}
\usepackage{amsmath}
\allowdisplaybreaks[1]
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{mathptmx}
\usepackage{mathtools}
\usepackage{fix-cm}
\usepackage{inconsolata}
\usepackage{listings}
\usepackage{enumitem}
\usepackage{upquote}

\usepackage{xcolor}
\usepackage{textcomp}
\usepackage{tikz}
\usepackage[hidelinks]{hyperref}
\usepackage{balance}
\usepackage[firstpage]{draftwatermark}
\SetWatermarkText{\hspace*{8in}\raisebox{6.9in}{\includegraphics[scale=0.1]{sle-aec-badge}}}
\SetWatermarkAngle{0}

% for adding comments
\usepackage{soul}
\setstcolor{red}

\tikzset{
  twolines/.style={execute at begin node=\setlength{\baselineskip}{10pt},align=center},
  codebox/.style={draw,rectangle,align=left,font=\tt\normalsize,inner sep=5pt}
}


\lstset{
  xleftmargin = 1em,
  numbers = none,
  % basicstyle=\footnotesize\ttfamily,
  basicstyle=\footnotesize\ttfamily,
  % tabsize=2, columns = space-flexible, basewidth = 0.5em,
  tabsize=2, columns = fixed, basewidth = 0.5em,
  emph={Abstract, Concrete, Actions},
  escapechar=~,
  escapeinside={(*@}{@*)},
  % aboveskip=\medskipamount,
  belowskip = 2pt,
  aboveskip = 2pt
}

% \newcommand{\cL}{{\cal L}}

\newcommand\todo[1]{\textcolor{red}{#1}}
\newcommand\bigul{\textsc{BiGUL}}
\newcommand\biyacc{\textsc{BiYacc}}
\newcommand {\yacc} {\textsc{Yacc}}
\newcommand{\happy}{\textsc{Happy}}
\newcommand{\haskell}{\textsc{Haskell}}
\newcommand{\flippr}{\textsf{FliPpr}}

\hyphenation{Bi-Yacc}

\def\listemphstyle{emphstyle={\color{blue}\bfseries}}

\newcommand {\BiYacc} {\textsc{BiYacc}}
\newcommand {\BiFluX} {\textsc{BiFluX}}
\newcommand {\tiger} {\textsc{Tiger}}

\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Section}
\renewcommand{\subsubsectionautorefname}{Section}
\renewcommand{\figureautorefname}{Figure}
\newtheorem*{theorem}{Theorem}

\newcommand{\branch}[2]{
  \draw(#1 - 0.22, #2 - 0.22) -- (#1 - 0.78, #2 - 0.78);
  \draw(#1 + 0.22, #2 - 0.22) -- (#1 + 0.78, #2 - 0.78);
}

\renewcommand{\t}[1]{\text{`{\tt #1}'}}
\newcommand{\nt}[1]{\mathit{#1}}
\newcommand{\var}[1]{\mathit{#1}}
\newcommand{\sem}[1]{[\kern-.16em[ #1 ]\kern-.16em]}
\newcommand{\fun}[1]{\textsc{\small #1}}
\newcommand{\iter}[3]{\bigl\langle#3 \bigm| #1 \in #2\bigr\rangle}



\begin{document}
\toappear{}
\setlength{\abovedisplayskip}{2pt}
\setlength{\belowdisplayskip}{2pt}

\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\conferenceinfo{SLE '16,}{October 31-November 01, 2016, Amsterdam, Netherlands}
\copyrightyear{2016}
\copyrightdata{978-1-4503-4447-0/16/10}
\copyrightdoi{2997364.2997369}

% Uncomment the publication rights you want to use.
%\publicationrights{transferred}
\publicationrights{licensed}     % this is the default
%\publicationrights{author-pays}

%\titlebanner{banner above paper title}        % These are ignored unless
%\preprintfooter{short description of paper}   % 'preprint' option specified.

\title{Parsing and Reflective Printing, Bidirectionally}
%\subtitle{no idea about the title}


\authorinfo{Zirun Zhu}
           {\fontsize{9}{11}\selectfont SOKENDAI (The Graduate University for Advanced Studies), Japan\\
            National Institute of Informatics, Japan}
           {\fontsize{9}{11}\selectfont zhu@nii.ac.jp}
\authorinfo{Yongzhe Zhang}
           {\fontsize{9}{11}\selectfont SOKENDAI (The Graduate University for Advanced Studies), Japan\\
            National Institute of Informatics, Japan}
           {\fontsize{9}{11}\selectfont zyz915@nii.ac.jp}
\authorinfo{Hsiang-Shang Ko}
           {\fontsize{9}{11}\selectfont National Institute of Informatics, Japan}
           {\fontsize{9}{11}\selectfont hsiang-shang@nii.ac.jp}
\authorinfo{Pedro Martins}
           {\fontsize{9}{11}\selectfont University of California, Irvine, USA}
           {\fontsize{9}{11}\selectfont pribeiro@uci.edu}
\authorinfo{Jo\~ao Saraiva}
           {\fontsize{9}{11}\selectfont University of Minho, Portugal}
           {\fontsize{9}{11}\selectfont jas@di.uminho.pt}
\authorinfo{Zhenjiang Hu}
           {\fontsize{9}{11}\selectfont SOKENDAI (The Graduate University for Advanced Studies), Japan\\
            National Institute of Informatics, Japan}
           {\fontsize{9}{11}\selectfont hu@nii.ac.jp}


% \authorinfo{Name2\and Name3}
           % {Affiliation2/3}
           % {Email2/3}


\maketitle

\begin{abstract}
Language designers usually need to implement parsers and printers.
Despite being two intimately related programs, in practice they are often designed separately, and then need to be revised and kept consistent as the language evolves.
It will be more convenient if the parser and printer can be unified and developed in one single program, with their consistency guaranteed automatically.

Furthermore, in certain scenarios (like showing compiler optimisation results to the programmer), it is desirable to have a more powerful \emph{reflective} printer that, when an abstract syntax tree corresponding to a piece of program text is modified, can reflect the modification to the program text while preserving layouts, comments, and syntactic sugar.

To address these needs, we propose a domain-specific language \textsc{BiYacc}, whose programs denote both a parser and a reflective printer for an unambiguous context-free grammar.
\textsc{BiYacc}\ is based on the theory of \emph{bidirectional transformations}, which helps to guarantee by construction that the pairs of parsers and reflective printers generated by \textsc{BiYacc}\ are consistent.
We show that \textsc{BiYacc}\ is capable of facilitating many tasks such as Pombrio and Krishnamurthi's ``resugaring'', simple refactoring, and language evolution.
\end{abstract}

% general terms are not compulsory anymore,
% you may leave them out
%\terms
%term1, term2

\category{D.3.2}{Programming Languages}{Language Classifications}[Specialized Application Languages]

\keywords
parsing, reflective printing, bidirectional transformations, domain-specific languages

% put each section into individual file
\section{Introduction}
\label{sec:introduction}
\input{introduction}

\section{A First Look at \biyacc}
\label{sec:highlevel_intro}
\input{overview}

\section{Foundation for \BiYacc: Putback-Based Bidirectional Transformations}
\label{sec:bigul}
\input{bigul}

\section{Implementation of \biyacc}
\label{sec:translation}
\input{translation}

\section{Case Study}
\label{sec:tiger}
\input{tiger}


\section{Related Work}
\label{sec:related_work}
\input{relatedwork}


\section{Conclusion}
\label{sec:conclusion}
\input{conclusion}


% \appendix
% \section{Appendix Title}
% This is the text of the appendix, if you need one.

\acks
We would like to thank reviewers for their valuable comments.
This work was partially supported by the Japan Society for the Promotion of Science (JSPS) Grant-in-Aid for Scientific Research~(A) No. 25240009.

% We recommend abbrvnat bibliography style.

%\newpage

\bibliographystyle{abbrvnat}

% The bibliography should be embedded for final submission.

% \begin{thebibliography}{}
% \softraggedright

% \bibitem[Smith et~al.(2009)Smith, Jones]{smith02}
% P. Q. Smith, and X. Y. Jones. ...reference text...

% \end{thebibliography}
\balance
\bibliography{biyacc}{}


\end{document}
