% !TEX root = biyacc.tex

%%%%%%%%%%%%%%%%
Whenever we come up with a new programming language, %general or domain-specific,
as the front-end part of the system we need to design and implement a parser and a printer to convert between program text and an internal representation.
A piece of program text, while conforming to a \emph{concrete syntax} specification, is a flat string that can be easily edited by the programmer.
The parser extracts the tree structure from such a string to a concrete syntax tree (CST), and converts it to an \emph{abstract syntax} tree (AST),
which is a more structured and simplified representation and is easier for the back-end to manipulate.
On the other hand, a printer converts an AST back to a piece of program text, which can be understood by the user of the system;
this is useful for debugging the system, or reporting internal information to the user.

Parsers and printers do conversions in opposite directions and are intimately related --- for example, the program text printed from an AST should be parsed to the same tree.
It is certainly far from being economical to write parsers and printers separately:
The parser and printer need to be revised from time to time as the language evolves,
and each time we must revise the parser and printer and also keep them consistent with each other, which is a time-consuming and error-prone task.
In response to this problem, many domain-specific languages
~\cite{Bout96,Viss97,rendel2010invertible,duregaard2011embedded,matsuda2013flippr}
have been proposed,
in which the user can describe both a parser and a printer in a single program.

Despite their advantages, these domain-specific languages cannot deal with synchronisation between program text and ASTs.
Let us look at a concrete example in \autoref{fig:exp_reflective_printing}:
%
\begin{figure}
%
Original program text:
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily]
-a /* a is the variable denoting(*@\,\textrm{\ldots}@*) */ * (1 + 1 + (a))
\end{lstlisting}
%
Abstract syntax tree:
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily]
Mul (Sub (Num 0) (Var "a"))
    (Add (Add (Num 1) (Num 1)) (Var "a"))
\end{lstlisting}
%
Optimised abstract syntax tree:
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily]
Mul (Sub (Num 0) (Var "a"))
    (Add (Num 2) (Var "a"))
\end{lstlisting}
%
Printed result from a \emph{conventional} printer:
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily]
(0 - a) * (2 + a)
\end{lstlisting}
%
Printed result from our \emph{reflective} printer:
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily]
-a /* a is the variable denoting(*@\,\textrm{\ldots}@*) */ * (2 + (a))
\end{lstlisting}
\caption{\small{Comparing conventional and reflective printing}}
\label{fig:exp_reflective_printing}
\end{figure}
%
The original program text is an arithmetic expression, containing negation, a comment, and (redundant) parentheses.
It is first parsed to an AST (supposing that addition is left-associative) where the negation is desugared to a subtraction, parentheses are implicitly represented by the tree structure, and the comment is thrown away.
Suppose the AST is optimised by replacing \lstinline{Add (Num 1) (Num 1)} with a constant \lstinline{Num 2}.
The user may want to observe the optimisation made by the compiler, but the AST is an internal representation not exposed to the user,
so a natural idea is to reflect the change on the AST back to the program text to make it easy for the user to check where the changes are.
With a conventional printer, however, the printed result will likely mislead the programmer into thinking that the negation is replaced by a subtraction by the compiler; also, since the comment is not preserved, it will be harder for the programmer to compare the updated and original versions of the text.
The problem illustrated here has also been investigated in many other practical scenarios where the parser and printer are used as a bridge between the system and the user, for example,
\begin{itemize}
  \item in program debugging where part of the original program in an error message is displayed much differently from its original form \cite{de2002pretty,kort2003parse}, and
  % \item in compiler optimisation where the comments and layout in the printed program text from an optimised AST get lost and unoptimised parts are changed, \todo{cite}
  \item in program refactoring where the comments and layouts in the original program are completely lost after the AST is transformed by the system \cite{de2011algorithm}.
  %\item in language-based editors, as introduced by Reps~\cite{reps1983incremental,reps1984synthesizer}, where the user needs to interact with different printed representations of the same underlying AST.
\end{itemize}
%Although many attempts have been made to enrich ASTs with more information to make the printed result better, the printing strategies are fixed, (\todo{see related work and discussion})
%which prevents the user from both controlling information to be preserved and describing specific strategies to be used in printing.

To address the problem, we propose a domain-specific language \biyacc, which lets the user describe both a parser and a \emph{reflective} printer for an unambiguous context-free grammar in a single program.
Different from a conventional printer, a reflective printer takes a piece of program text and an AST, which is usually slightly modified from the AST corresponding to the original program text, and reflects the modification back to the program text.
Meanwhile the comments (and layouts) in the unmodified parts of the program text are all preserved.
This can be seen clearly from the result of using our reflective printer on the above arithmetic expression example as shown in \autoref{fig:exp_reflective_printing}.
It is worth noting that reflective printing is a generalisation of the conventional notion of printing, because a reflective printer can accept an AST and an \emph{empty} piece of program text,
in which case it will behave just like a conventional printer, producing a new piece of program text depending on the AST only.
(\biyacc\ currently does not support pretty printing, which however can be achieved in the future by letting the user define their own ``adaptive functions'' (see \autoref{sec:translation}) to specify the layouts in the generated program text.)


From a \biyacc\ program we can generate a parser and a reflective printer; in addition, we want to guarantee that the two generated programs are \emph{consistent} with each other.
Specifically, we want to ensure two inverse-like properties:
Firstly, a piece of program text~$s$ printed from an abstract syntax tree~$t$ should be parsed to the same tree~$t$, i.e.,
\begin{equation}
\qquad\ \qquad\ \qquad\ \mathit{parse}\;(\mathit{print}\;s\;t) = t \label{equ:BXPrintParse}
\end{equation}
Secondly, updating a piece of program text~$s$ with an AST parsed from~$s$ should leave~$s$ unmodified (including formatting details like parentheses and spaces), i.e.,
\begin{equation}
\qquad\ \qquad\ \qquad\ \mathit{print}\;s\;(\mathit{parse}\;s) = s \label{equ:BXParsePrint}
\end{equation}
These two properties are inspired by the theory of \emph{bidirectional transformations} \cite{czarnecki2009bidirectional,hu2011dagstuhl}, and are guaranteed by construction for all \biyacc\ programs.

An online tool that implements the approach described in the paper can be accessed at {\small\url{http://www.prg.nii.ac.jp/project/biyacc.html}}.
The webpage also contains input examples with various test cases used in the paper.
%
The structure of the paper is as follows:
\begin{itemize}[leftmargin=*]
  \item We first give an overview of \biyacc\ in \autoref{sec:highlevel_intro}, explaining how to describe in a single program both a parser and a reflective printer for synchronising program text and its abstract syntax representation.
  \item After reviewing some background on bidirectional transformations in \autoref{sec:bigul}, in particular the bidirectional programming language \bigul~\cite{ko2016bigul}, we give the semantics of \biyacc\ by compiling it to \bigul\ in \autoref{sec:translation}, guaranteeing the properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}) by construction.
  \item We present a case study in \autoref{sec:tiger},
  showing that although \biyacc\ is currently restricted to handling unambiguous grammars, it is capable of describing \tiger\ \cite{Appel98}, which shares many similarities with full grown, widely used languages.
  We demonstrate that \biyacc\ can handle syntactic sugar, partially subsume \citet{pombrio2014resugaring}'s ``resugaring'', and facilitate language evolution.
  \item Related work including detailed comparison with other systems and conclusion are presented in \autoref{sec:related_work} and \autoref{sec:conclusion} respectively.
\end{itemize}

