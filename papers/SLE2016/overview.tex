% !TEX root = biyacc.tex

We first give an overview of \biyacc\ by going through the \biyacc\ program shown in \autoref{fig:expr}, which deals with the arithmetic expression example mentioned in \autoref{sec:introduction}.
For reference, we give the definition of \biyacc\ syntax in \autoref{fig:biyaccSyntax}, which will also be referred to in \autoref{sec:translation}.

\begin{figure}
\begin{center}
\begin{tabular}{c}
{\begin{lstlisting}[numbers=left, numberstyle=\tiny, emphstyle={\bfseries}, basicstyle=\footnotesize\ttfamily]
Abstract

data Arith = Num Int
           | Var String
           | Add Arith Arith
           | Sub Arith Arith
           | Mul Arith Arith
           | Div Arith Arith
  deriving (Show, Eq, Read)

Concrete

%commentLine  '//'      ;
%commentBlock '/*' '*/' ;

Expr   -> Expr '+' Term
        | Expr '-' Term
        | Term ;

Term   -> Term '*' Factor
        | Term '/' Factor
        | Factor ;

Factor -> '-' Factor
        | Int
        | Name
        | '(' Expr ')' ;

Actions

Arith +> Expr
Add x y  +>  (x +> Expr) '+' (y +> Term);
Sub x y  +>  (x +> Expr) '-' (y +> Term);
arith    +>  (arith +> Term);

Arith +> Term
Mul x y  +>  (x +> Term) '*' (y +> Factor);
Div x y  +>  (x +> Term) '/' (y +> Factor);
arith    +>  (arith +> Factor);

Arith +> Factor
Sub (Num 0) y  +>  '-' (y +> Factor);
Num i          +>  (i +> Int);
Var n          +>  (n +> Name);
arith          +>  '(' (arith +> Expr) ')';
\end{lstlisting}}
\end{tabular}
\end{center}
\caption{\small{A \biyacc\ program for the expression example}}
\label{fig:expr}
\end{figure}

\begin{figure}
\small
\setlength{\mathindent}{0em}
\begin{align*}
\nt{Program} &\Coloneqq \t{Abstract}\ \nt{HsDeclarations} \\[-1.25ex]
             &\phantom{\null\Coloneqq\null} \t{Concrete}\ \nt{ProductionGroup}^+ \\[-1.25ex]
             &\phantom{\null\Coloneqq\null} \rlap{\t{Actions}}\phantom{\t{Concrete}}\ \nt{ActionGroup}^+ \\
\nt{ProductionGroup} &\Coloneqq \nt{Nonterminal}\ \t{->}\ \nt{ProductionBody}^+\{\t{|}\}\ \t{;} \\
\nt{ProductionBody} &\Coloneqq Symbol^+ \\
\nt{Symbol} &\Coloneqq \nt{Primitive} \mid \nt{Terminal} \mid \nt{Nonterminal} \\
\nt{ActionGroup} &\Coloneqq \nt{HsType}\ \t{+>}\ \nt{Nonterminal} \\[-1.25ex]
                 &\phantom{\null\Coloneqq\null} \nt{Action}^+ \\
\nt{Action} &\Coloneqq \nt{HsPattern}\ \t{+>}\ \nt{Update}^+\ \t{;} \\
\nt{Update} &\Coloneqq \nt{Symbol} \\
            &\rlap{\hskip.75em$|$}\phantom{\null\Coloneqq\null} \t{(}\ \nt{HsVariable}\ \t{+>}\ \nt{UpdateCondition}\ \t{)} \\
            &\rlap{\hskip.75em$|$}\phantom{\null\Coloneqq\null} \t{(}\ \nt{Nonterminal}\ \t{->}\ \nt{Update}^+\ \t{)} \\
\nt{UpdateCondition} &\Coloneqq \nt{Symbol} \\
       &\rlap{\hskip.75em$|$}\phantom{\null\Coloneqq\null} \t{(}\ \nt{Nonterminal}\ \t{->}\ \nt{UpdateCondition}^+\ \t{)}
\end{align*}
\caption{\small{Syntax of \biyacc\ programs (Nonterminals with prefix $\mathit{Hs}$ denote Haskell entities and follow the Haskell syntax; the notation $\mathit{nt}^+\{\mathit{sep}\}$ denotes a nonempty sequence of the same nonterminal $\mathit{nt}$ separated by $\mathit{sep}$.)}
\label{fig:biyaccSyntax}}
\end{figure}

%
\subsection{Definitions of the Abstract and Concrete Syntax}

A \biyacc\ program consists of definitions of the abstract and concrete syntax and \emph{actions} for reflectively printing ASTs to CSTs.
The abstract syntax part --- which starts with the keyword \lstinline{Abstract} --- is just one or more definitions of Haskell datatypes.
In our example, the abstract syntax is defined in lines 1--9 by a single datatype \lstinline{Arith} whose elements are constructed from constants and arithmetic operators.

On the other hand, the concrete syntax --- which is defined in the second part beginning with the keyword \lstinline{Concrete} --- is defined by a context-free grammar.
For our expression example, in lines 16--27 we use a standard grammatical structure to encode operator precedence and order of association, which involves three nonterminal symbols \lstinline{Expr}, \lstinline{Term}, and \lstinline{Factor}:
An \lstinline{Expr} can produce a left-leaning tree of \lstinline{Term}s, each of which can in turn produce a left-leaning tree of \lstinline{Factor}s.
To produce right-leaning trees or operators of lower precedence under those with higher precedence, the only way is to reach for the last production rule \lstinline[breaklines=true]{Factor -> '(' Expr ')'}, resulting in parentheses in the produced program text.
(There is also a nonterminal $\mathit{Name}$, which produces identifiers.)

%
\paragraph{Syntax for comments.}
Syntax for comments can be declared at the beginning of this part before any production rules.
For example,
line~13 shows that the syntax for a single line comments is ``\lstinline!//!'', while line~14 states that ``\lstinline!/*!'' and ``\lstinline!*/!'' are respectively the beginning mark and ending mark for a block comment.
%If one (or both) of them is not defined, the default syntax for comments, in C style, will be adopted.
%(``\lstinline!//!'' for a single line comment, while ``\lstinline!/*!'' and ``\lstinline!*/!'' for a block comment.)



%
\subsection{Actions}

The last and main part of a \biyacc\ program starts with the keyword \lstinline{Actions}, and describes how to update a CST with an AST.
For our expression example, the actions are defined in lines 29--45 in \autoref{fig:expr}.
Before explaining the actions, we should first emphasise that we are identifying program text with CSTs:
Conceptually, whenever we write a piece of program text, we are actually describing a CST rather than just a sequence of characters.
We will expound on this identification of program text with CSTs in \autoref{sec:inverse-properties}.

%
The \lstinline{Actions} part consists of groups of actions, and each group begins with a ``type declaration'' of the form $\mathit{hsType}$ $\t{+>}$ $\mathit{nonterminal}$ stating that the actions in this group specify updates on CSTs generated from $\mathit{nonterminal}$ using ASTs of type $\mathit{hsType}$.
Informally, given an AST and a CST, the semantics of an action is to perform pattern matching simultaneously on both trees, and then use components of the AST to update corresponding parts of the CST, possibly recursively.
(The syntax~\t{+>} suggests that information from the left-hand side is embedded into the right-hand side.)
Usually the nonterminals in a right-hand side pattern are overlaid with update instructions, which are also denoted by \t{+>}.

%
Let us look at a specific action --- the first one for the expression example, at line~32 of \autoref{fig:expr}:
\begin{lstlisting}
Add x y +> (x +> Expr) '+' (y +> Term);
\end{lstlisting}
The AST pattern is just a Haskell pattern; as for the CST pattern, the main intention is to refer to the production rule \lstinline{Expr -> Expr '+' Term}
and use it to match those CSTs produced by this rule.
Since the action belongs to the group \lstinline[breaklines=true]{Arith +> Expr}, the part `\lstinline{Expr ->}' of the production rule can be inferred, and thus is not included in the CST pattern.
Finally we overlay `\lstinline{x +>}' and `\lstinline{y +>}' on the nonterminal symbols \lstinline{Expr} and \lstinline{Term} to indicate that, after the simultaneous pattern matching succeeds, the subtrees \lstinline{x}~and~\lstinline{y} of the AST are respectively used to update the left and right subtrees of the CST.

%
Having explained what an action means, we can now explain the semantics of the entire program.
Given an AST and a CST as input, first a group of actions is chosen according to the types of the trees.
Then the actions in the group are tried in order, from top to bottom, by performing simultaneous pattern matching on both trees.
If pattern matching for an action succeeds, the updating operations specified by the action is executed; otherwise the next action is tried.
Execution of the program ends when the matched action specifies either no updating operations or only updates to primitive datatypes such as \lstinline{Int}.
\biyacc's most interesting behaviour shows up when all actions in the chosen group fail to match --- in this case a suitable CST will be created.
The specific approach adopted by \biyacc\ is to perform pattern matching on the AST only and choose the first matched action.
A suitable CST conforming to the CST pattern is then created, and after that the whole group of actions is tried again.
This time the pattern matching should succeed at the action used to create the CST, and the program will be able to make further progress.

%\paragraph{Complex patterns.} % It is interesting to note that,
%By using more complex patterns, we can write actions that establish nontrivial relationships between CSTs and ASTs.
%For example, the action at line~39 of \autoref{fig:expr} associates abstract subtraction expressions whose left operand is zero with concrete negated expressions; this action is the key to preserving negated expressions in the CST.
%%(The symbol `\#' is used to distinguish the identifiers in \biyacc\ program and the exact data that is supposed to appear in the CST.)
%For an example of a more complex CST pattern:
%Suppose that we want to write a pattern that matches those CSTs produced by the rule \lstinline[breaklines=true]{Factor -> '-' Factor},
%where the inner nonterminal \lstinline{Factor} produces a further \lstinline{'-' Factor} using the same rule.
%This pattern is written by overlaying the production rule on the first nonterminal \lstinline{Factor} (an additional pair of parentheses is required for the expanded nonterminal):
%\begin{lstlisting}
%'-' (Factor -> '-' Factor)
%\end{lstlisting}
%More examples involving this kind of deep patterns will be presented in \autoref{sec:tiger}.


%
\paragraph{Layout and comment preservation.}
The reflective printer generated by \biyacc\ is capable of preserving layouts and comments, but, perhaps mysteriously, in \autoref{fig:expr} there is no clue as to how layouts and comments are preserved.
This is because we decide to hide layout preservation from the programmer, so that the more important logic of abstract and concrete syntax synchronisation is not cluttered with layout preserving instructions.
Our current approach is fairly simplistic:
We store layout information following each terminal in an additional field in the CST implicitly, and treat comments in the same way as layouts.\footnote{One might argue that layouts and comments should in fact be handled differently, since comments are usually attached to some entities which they describe. For example, when a function declaration is moved to somewhere else (e.g., by a refactoring tool), we will want the comment describing that function to be moved there as well. We leave the proper treatment of comments as future work.}
During the printing stage, if the pattern matching on an action succeeds, the layouts and comments after the terminals shown in the right-hand side of that action are preserved;
on the other hand, layouts and comments are dropped when a CST is created in the situation where pattern matching fails for all actions in a group.
The layouts and comments before the first terminal are always kept during the printing.
% More details about this treatment and some exceptions can be found in \autoref{sec:translation}.


%
% \paragraph{Usefulness of the concrete syntax.}
% Observant readers may have noticed that, the production rules in the concrete syntax part resemble the right-hand sides of the actions.
% In fact these production rules in the concrete syntax part can be omitted, and \biyacc\ will automatically generate them by extracting the right-hand sides of all actions.
% However, we recommend the users still write these production rules, so that \biyacc\ can perform some simple checks:
% It will make sure that in an action group, the users have covered all of the production rules of the nonterminal appearing in the ``type declaration'', and will never use undefined production rules.



%
% \subsection{Running the program on a specific input}
% \label{running_exp}

% To get a solid feeling of how \biyacc\ works, let us go through the execution of the program in \autoref{fig:expr} on the program text and optimised AST shown in \autoref{fig:exp_reflective_printing} to see how the updated program text is produced.

% Initially the types of the two trees are assumed to match those declared for the first group of actions, and hence we try the first action in the group, at line~29.
% However, since the AST is a \lstinline{Mul} and the first production rule used for the CST is \lstinline[breaklines=true]{Expr -> Term},
% % (followed by \lstinline[breaklines=true]{Term -> Term '*' Factor}),
% pattern matching fails on both the abstract and concrete sides.
% So, other actions in this group are tried in order, from top to bottom: We fail at line~30 but succeed at line~31.
% Now the update specified by this action is to proceed with updating the subtree produced from \lstinline{Term}, so we move on to the second group.
% This time the action at line~34 matches, meaning that we should update the subtrees \lstinline{-a}~and \lstinline{(1 + 1 + (a))} with \lstinline[breaklines=true]{Sub (Num 0) (Var "a")} and \lstinline{Add (Num 2) (Var "a")} respectively.
% Note that at this point the layout information after the terminal \lstinline{*} is preserved.

% For the update on the left-hand side of the multiplication, we again try the second group (according to its ``type'': a \lstinline{Term}), and the action at line~36 matches.
% Then we go through the actions at line~39 --- preserving the negation --- and line~41, eventually updating the variable \lstinline{a} in the CST with \lstinline{Var "a"} in the AST and preserving the comments after it.
% (The update is still performed even though the concrete and abstract sides are the same.)
% The remaining process of the synchronisation is much similar, and hence omitted here.
% As for the update on the right-hand side of the multiplication, the action at line~42 matches.
% Now the parentheses are kept and we are updating the subtree \lstinline{1 + 1 + (a)} produced from the \lstinline{Expr} inside the parentheses with the AST \lstinline{Add (Num 2) (Var "a")}.

%\todo{Revise.}
% Similarly, the action at line~29 matches, so \lstinline{1 + 1} is then to be updated by \lstinline{Num 2}, and \lstinline{(a)} by \lstinline{Var "a"}.
% As for \lstinline{1 + 1} and \lstinline{Num 2}, since none of the actions matches, a CST in the form of \lstinline{Expr -> Term} is created according to the AST pattern at line~31, and we go into the second group.
% The remaining execution sequences are as follows: Actions at lines 34, 35, and~36 fail to match, so the CST is reshaped to a \lstinline{Factor} at line~36.
% Actions at lines 39, 40, 41, and~42 fail, and the CST is adapted to an \lstinline{Int} at line~40.
% The newly created \lstinline{Int} is eventually replaced by the integer $2$ in the AST, and a default layout (a space) is inserted after it.
% This is why the updated result for this part is ``\lstinline{2 }''.
% For the update on \lstinline{(a)} with \lstinline{Var "a"} , we are going to update \lstinline{(a)} with \lstinline{Var "a"}.
% The program goes through actions at lines 36, 42, 31, 36, and~41.

%
\paragraph{Parsing semantics.}
So far we have been describing the reflective printing semantics of the \biyacc\ program, but we may also work out its parsing semantics intuitively by interpreting the actions from right to left, converting the production rules to the corresponding constructors.
(This might remind the reader of the usual \textsc{yacc} actions.)
In fact, this paper will not define the parsing semantics formally, because the parsing semantics is completely determined by the reflective printing semantics:
If the actions are written with the intention of establishing some relation between the CSTs and ASTs, then \biyacc\ will be able to derive the only well-behaved parser, which respects that relation.
We will explain how this is achieved in the next section.
