% !TEX root = biyacc.tex

The design of \biyacc\ may look simplistic and make the reader wonder how much it can describe.
However, in this section we demonstrate with a larger case study that, without any extension, \biyacc\ can already handle real-world language features.
For this case study, we choose the \tiger\ language, which is a statically typed imperative language first introduced in Appel's textbook on compiler construction~\cite{Appel98}.
Since \tiger's purpose of design is pedagogical,
it is not too complex and yet covers many important language features
including conditionals, loops, variable declarations and assignments, and function definitions and calls.
\tiger\ is therefore a good case study with which we can test the potential of our BX-based approach to constructing parsers and reflective printers.
Some of these features can be seen in this \tiger\ program:
%
\begin{lstlisting}
function foo() =
  (for i := 0 to 10
    do (print(if i < 5 then "smaller"
                       else "bigger");
        print("\n")))
\end{lstlisting}
%
To give a sense of \tiger's complexity, it takes a grammar with 81 production rules to specify \tiger's syntax, while for C89 and C99 it takes respectively 183 and 237 rules without any disambiguation declarations (based on \citet{kernighan1988c} and the draft version of 1999 ISO C standard, excluding the preprocessing part).
The difference is basically due to the fact that C has more primitive types and various kinds of assignment statements.
% \todo{Since the grammar of C are described in a LALR setting without any disambiguation rules (see the ISO C standard and Java specification), we omit the support for disambiguation rules in current \biyacc.}
% (A few problems remain such as the dangling-else problem)

Excerpts of the abstract and concrete syntax of \tiger\ are shown in \autoref{lst:tiger1}.
The abstract syntax is substantially the same as the original one defined in \citeauthor{Appel98}'s textbook (page~98); as for the concrete syntax, \citeauthor{Appel98} does not specify the whole grammar in detail, so we use a version slightly adapted from \citet{tigerSpec}'s lecture notes.
Some changes are made to handle features that are not supported by current \biyacc:
For example, to make the grammar become unambiguous without disambiguation rules,
the operators are divided into several groups, with the highest-precedence terms (like literals) placed in the last group,
just like what we did in the arithmetic expression example (\autoref{fig:expr});
the AST constructors \lstinline{TFunctionDec} or \lstinline{TTypeDec} take a single function or type declaration instead of a list of adjacent declarations (for representing mutual recursion) as in \citet{Appel98}, since we cannot handle the synchronisation between a list of lists (in ASTs) and a list (in CSTs) with \biyacc's current syntax;
%\todo{A few production rules are slightly adjusted to avoid shift/reduce conflicts caused by the indistinguishable identifiers: \lstinline{typeId} and \lstinline{id} --- because they are all represented by the same \lstinline{Name} token in this prototype implementation.}
finally, to circumvent the ``dangling else'' problem,
a terminal ``\lstinline{end}'' is added to mark the end of an \lstinline{if}-\lstinline{then} expression.


Even though the underlying \bigul\ programs have runtime checks as mentioned in the beginning of \autoref{sec:translation},
we have successfully tested our \biyacc\ program for \tiger\ on all the sample programs provided on the homepage of \citeauthor{Appel98}'s book\footnote{\url{https://www.cs.princeton.edu/~appel/modern/testcases/}},
including a merge sort implementation and an eight-queen solver, and there is no problem parsing and printing them.
% The complete \biyacc\ program for \tiger\ can be found at {\small\url{http://www.prg.nii.ac.jp/project/biyacc.html}}.
In the following subsections, we will present some printing strategies used in that \tiger\ program to demonstrate what \biyacc, in particular reflective printing, can achieve.

%
\begin{figure}[t]
\begin{lstlisting}[emphstyle={\bfseries}]
Abstract

data TExp = TString String | TInt Int | TNilExp
          | TSeq [TExp]    | TLet [TDec] TExp
          | TIf TExp TExp (Maybe TExp)
          | TOp TExp TOper TExp | (*@\,\textrm{\ldots}@*)
  deriving (Show, Eq, Read)
data TOper = TPlusOp | TMinusOp | (*@\,\textrm{\ldots}@*)
           | TEqOp | TNeqOp | (*@\,\textrm{\ldots}@*)
  deriving (Show, Eq, Read)

data TDec = TTypeDec TTyDec | TFunctionDec TFundec
  |TVarDec TSymbol (Maybe TSymbol) TExp
  deriving (Show, Eq, Read)
(*@\,\textrm{\ldots}@*)
type TSymbol = String

Concrete

Exp -> 'break'  | LetExp | ArrExp | Assignment
     |  ForExp  | RecExp | IfThen | IfThenElse
     | WhileExp | PrimitiveOpt ;

VarDec -> 'var' Name          ':=' Exp
        | 'var' Name ':' Name ':=' Exp ;

LValue      -> Name | OtherLValue ;
OtherLValue -> Name '[' Exp ']'
  | OtherLValue '[' Exp ']' | LValue '.' Name ;

SeqExp  -> '(' ')' | '(' ExpSeq ')' ;
ExpSeq  -> Exp ';' ExpSeq  | Exp ;

PrimitiveOpt  -> PrimitiveOpt '|' PrimitiveOpt1
               | PrimitiveOpt1 ;
(*@\,\textrm{\ldots}@*)
PrimitiveOpt3 -> PrimitiveOpt3 '+' PrimitiveOpt4
               | (*@\,\textrm{\ldots}@*) | PrimitiveOpt4 ;
(*@\,\textrm{\ldots}@*)
PrimitiveOpt5 -> 'nil'  | Int    | String
               | LValue | SeqExp | CallExp
               | '-' PrimitiveOpt5 ;

IfThen     -> 'if' Exp 'then' Exp 'end' ;
IfThenElse -> 'if' Exp 'then' Exp 'else' Exp ;
(*@\,\textrm{\ldots}@*)
\end{lstlisting}
\caption{\small{An excerpt of \tiger's abstract and concrete syntax}}
\label{lst:tiger1}
\end{figure}


%
\subsection{Syntactic Sugar}
\label{sec:synSugar}
We start with a simple example about syntactic sugar, which is pervasive in programming languages and lets the programmer use some features in an alternative (perhaps conceptually higher-level) syntax.
For instance, \tiger\ represents boolean values false and true respectively as zero and nonzero integers, and the logical operators \lstinline{&}~(``and'') and \lstinline{|}~(``or'') are converted to \lstinline{if} expressions in the abstract syntax:
\lstinline{e1 & e2} is desugared and parsed to \lstinline{TIf e1 e2 (TInt 0)} and \lstinline{e1 | e2} to \lstinline{TIf e1 (TInt 1) e2}.
The printing actions for them in \biyacc\ are:
%
\begin{lstlisting}[emphstyle={\color{blue}\bfseries}]
TExp +> PrimitiveOpt
TIf e1 (TInt 1) (Just e2)     +>
 (e1 +> PrimitiveOpt) '|' (e2 +> PrimitiveOpt1);

TExp +> PrimitiveOpt1
TIf e1 e2 (Just (TInt 0))  +>
 (e1 +> PrimitiveOpt1) '&' (e2 +> PrimitiveOpt2);
\end{lstlisting}
%
The $\mathit{parse}$ function for these syntactic sugar is not injective, since the alternative syntax and the features being desugared into are both mapped to the latter.
A conventional printer --- which takes only the AST as input --- cannot reliably determine whether an abstract expression should be resugared or not, whereas a reflective printer can make the decision by inspecting the CST.


\subsection{Resugaring}

We have seen that \biyacc\ can handle syntactic sugar. In this subsection we will present another application based on that.
The idea of \emph{resugaring}~\citep{pombrio2014resugaring} is to print evaluation sequences in a core language in terms of a surface syntax.
Here we show that, without any extension, \biyacc\ is already capable of reflecting some of the AST changes resulting from evaluation back to the concrete syntax, subsuming a part of \citeauthor{pombrio2014resugaring}'s work.
% Resugaring tools help users to know the semantic of the underlying

We borrow their example of resugaring evaluation sequences for the logical operators ``or'' and ``not'', but recast the example in \tiger.
The ``or'' operator has been defined as syntactic sugar in \autoref{sec:synSugar}.
For the ``not'' operator, which \tiger\ lacks, we introduce `\lstinline{~}', represented by \lstinline{TNot} in the abstract syntax.
Now consider the source expression
\begin{lstlisting}
~1 | ~0
\end{lstlisting}
which is parsed to
\begin{lstlisting}
TIf (TNot (TInt 1)) (TInt 1) (J (TNot (TInt 0)))
\end{lstlisting}
where \lstinline{J}~is a shorthand for \lstinline{Just}.
%
A typical call-by-value evaluator will produce the following evaluation sequence given the above AST:
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) TIf (TNot (TInt 1)) (TInt 1) (J (TNot (TInt 0)))
(*@$         \rightarrow $@*) TIf (TInt 0) (TInt 1) (J (TNot (TInt 0)))
(*@$         \rightarrow $@*) TNot (TInt 0)
(*@$         \rightarrow $@*) TInt 1
\end{lstlisting}
%
If we perform reflective printing after every evaluation step using \biyacc, we will get the following evaluation sequence on the source:
\begin{lstlisting}
~1 | ~0    (*@$\rightarrow$@*)    0 | ~0    (*@$\rightarrow$@*)    ~0    (*@$\rightarrow$@*)    1
\end{lstlisting}
%
Due to the \ref{equ:PutGet} property, parsing these concrete terms will yield the corresponding abstract terms in the first evaluation sequence, and this is exactly \citeauthor{pombrio2014resugaring}'s ``emulation'' property, which they have to prove for their system; for \biyacc, however, the emulation property holds by construction, since \biyacc's semantics is defined in terms of \bigul, whose programs are always well-behaved.
Also different from \citeauthor{pombrio2014resugaring}'s approach is that we do not need to insert any additional information into the ASTs for remembering the form of the original sources.
The advantage of our approach is that we can keep the abstract syntax pure, so that other tools --- the evaluator in particular --- can process the abstract syntax without being modified,
whereas in \citeauthor{pombrio2014resugaring}'s approach, the evaluator has to be adapted to work on the enriched abstract syntax.

Also note that the above resugaring for \tiger\ is achieved for free --- the programmer does not need to write additional, special actions to achieve that.
In general, \biyacc\ can easily and reliably reflect AST changes that involve only ``simplification'', i.e., replacing part of an AST with a simpler tree, so it should not be surprising that \biyacc\ can also reflect simplification-like optimisations such as constant propagation and dead code elimination, and some refactoring transformations such as variable renaming and adding or removing parameters.
All these can be achieved by one ``general-purpose'' \biyacc\ program, which does not need to be tailored for each application.


\subsection{Language Evolution}

We conclude this section by looking at a practical scenario in language evolution, incorporating all the features we introduced before in this section.
When a language evolves, some new features of the language (e.g., \lstinline{foreach} loops in Java~5) can be implemented by desugaring to some existing features (e.g., ordinary \lstinline{for} loops), so that the compiler does not need to be extended to handle the new features.
As a consequence, all the engineering work about refactoring or optimising transformations that has been developed for the abstract syntax remains valid.

Consider a kind of ``generalised-\lstinline{if}'' expression allowing more than two cases, resembling the alternative construct in \citet{dijkstra1975guarded}'s guarded command language.
We extend \tiger's concrete syntax with the following production rules:
\begin{lstlisting}
Exp    -> (*@\,\textrm{\ldots}@*) | Guard | (*@\,\textrm{\ldots}@*) ;
Guard  -> 'guard' CaseBs 'end';
CaseBs -> CaseB CaseBs | CaseB ;
CaseB  -> LValue '=' Int '->' Exp ;
\end{lstlisting}
For simplicity, we restrict the predicate produced by \lstinline{CaseB} to the form \lstinline{LValue '=' Int}, but in general this can be any expression computing an integer.
The reflective printing actions for this new construct can still be written within \biyacc, but require much deeper pattern matching:
%
\begin{lstlisting}
TExp +> Guard
TIf (TOp (TVar lv) TEqOp (TInt i)) e1 Nothing
  +>  'guard' (CaseBs -> (CaseB ->
          (lv +> LValue) '=' (i +> Int) '->' (e1 +> Exp))
              ) 'end';
TIf (TOp (TVar lv) TEqOp (TInt i)) e1 (J if2@(TIf _ _ _))
  +>  'guard' (CaseBs -> (CaseB ->
          (lv +> LValue) '=' (i +> Int) '->' (e1 +> Exp))
                             (if2 +> CaseBs)
              ) 'end';

TExp +> CaseBs
TIf (TOp (TVar lv) TEqOp (TInt i)) e1 Nothing
    +>  (CaseB ->
          (lv +> LValue) '=' (i +> Int) '->' (e1 +> Exp));
TIf (TOp (TVar lv) TEqOp (TInt i)) e1 (J if2@(TIf _ _ _))
    +>  (CaseB ->
          (lv +> LValue) '=' (i +> Int) '->' (e1 +> Exp))
          (if2 +> CaseBs);
\end{lstlisting}
%
Though complex, these printing actions are in fact fairly straightforward:
The first group of type \lstinline{Tiger +> Guard} handles the enclosing \lstinline{guard}--\lstinline{end} pairs, distinguishes between single- and multi-branch cases, and delegates the latter case to the second group,
which prints a list of branches recursively.

This is all we have to do --- the corresponding parser is automatically derived and guaranteed to be consistent.
Now \lstinline{guard} expressions are desugared to nested \lstinline{if} expressions in parsing and preserved in printing,
and we can also resugar evaluation sequences on the ASTs to program text.
For instance, the following \lstinline{guard} expression
%
\begin{lstlisting}
guard  choice = 1  ->  4
       choice = 2  ->  8
       choice = 3  ->  16  end
\end{lstlisting}
%
is parsed to
%
\begin{lstlisting}
TIf (TOp (TVar (TSV "c")) TEqOp (TInt 1)) (TInt 4) (J
  (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 2)) (TInt 8) (J
    (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 3)) (TInt 16)
      Nothing))))
\end{lstlisting}
where \lstinline{TSimpleVar} is shortened to \lstinline{TSV}, and \lstinline{choice} is shortened to \lstinline{c}.
Suppose that the value of the variable \lstinline{choice} is~$2$.
The evaluation sequence on the AST will then be:
%
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) TIf (TOp (TVar (TSV "c")) TEqOp (TInt 1)) (TInt 4) (J
(*@$\phantom{\rightarrow}$@*)  (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 2)) (TInt 8) (J
(*@$\phantom{\rightarrow}$@*)   (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)     Nothing))))
(*@$         \rightarrow $@*) TIf (TInt 0) (TInt 4) (J
(*@$\phantom{\rightarrow}$@*)  (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 2)) (TInt 8) (J
(*@$\phantom{\rightarrow}$@*)   (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)     Nothing))))
(*@$         \rightarrow $@*) TIf (TOp (TVar (TSV "c")) TEqOp (TInt 2)) (TInt 8) (J
(*@$\phantom{\rightarrow}$@*)  (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)    Nothing))
(*@$         \rightarrow $@*) TIf (TInt 1) (TInt 8) (J
(*@$\phantom{\rightarrow}$@*) (TIf (TOp (TVar (TSV "c")) TEqOp (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)   Nothing))
(*@$         \rightarrow $@*) TInt 8
\end{lstlisting}
%
And the reflected evaluation sequence on the concrete expression will be:
%
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) guard  choice = 1  ->  4
(*@$\phantom{\rightarrow}$@*)        choice = 2  ->  8
(*@$\phantom{\rightarrow}$@*)        choice = 3  ->  16  end
(*@$     \not\rightarrow $@*)
(*@$         \rightarrow $@*) guard  choice = 2  ->  8
(*@$\phantom{\rightarrow}$@*)        choice = 3 -> 16 end
(*@$     \not\rightarrow $@*)
(*@$         \rightarrow $@*) 8
\end{lstlisting}
%
Reflective printing fails for the first and third steps (the program text becomes an \lstinline{if-then-else} expression if we do printing at these steps), but this behaviour in fact conforms to \citeauthor{pombrio2014resugaring}'s ``abstraction'' property, which demands that core evaluation steps that make sense only in the core language must not be reflected to the surface.
In our example, the first and third steps in the \lstinline{TIf}-sequence evaluate the condition to a constant, but conditions in guard expressions are restricted to a specific form and cannot be a constant; evaluation of guard expressions thus has to proceed in bigger steps, throwing away or going into a branch in each step, which corresponds to two steps for \lstinline{TIf}.

The reader may have noticed that, after the \lstinline{guard} expression is reduced to two branches, the layout of the second branch is disrupted; this is because the second branch is in fact printed from scratch.
In current \biyacc, the printing from an AST to a CST is accomplished by recursively performing pattern matching on both tree structures.
This approach naturally comes with the disadvantage that the matching is mainly decided by the position of nodes in the AST and CST.
Consequently, a minor structural change on the AST may completely disrupt the matching between the AST and the CST.
We intend to handle this problem in the future.

% \subsubsection*{refactoring}
% \todo{Java 7 $\leftrightarrow$ Java 8 code may need to be put in the potential apps (our future work?) }
% This introduction is extremely useful to programmers of \tiger, but implies editing the complete ecosystem of the language.
