% !TEX root = biyacc.tex

We have improved \biyacc\ in several ways and will introduce them in this section.

\subsection{Kleene Operators}
Kleene operators greatly reduce the number of production rules the user need to write and it is a pity that we did not provide this feature in the conference submission\footnote{Note that neither the parser generator \happy\ we used nor the famous \yacc\ supports this feature.}.
%
To have a quick look, with the help of Kleene operators the productions of \tiger
\begin{lstlisting}
ExpSeq -> Exp ';' ExpSeq
        | Exp
\end{lstlisting}
now can be written as
\begin{lstlisting}
ExpSeq -> Exp {';' Exp}* (*@.@*)
\end{lstlisting}
Productions like these are widely used in programming languages to define syntax such as a list of expressions, statements, arguments of function definition and call, and elements used in array construction.
%
These are what we have already known; however, what makes the feature more interesting is its behaviour in the printing direction.

% \subsubsection{New Update Syntax and Strategy}
In \biyacc\ Kleene operators are equipped with a new update syntax \lstinline{~*>} and distinguished from normal update. For instance, the update strategy for \lstinline{ExpSeq} above is
\begin{lstlisting}
[TExp] +> ExpSeq
(e:es)  +>  (e +> Exp) {';' (es ~*> Exp)}* ; (*@,@*)
\end{lstlisting}
from which the generated \(\var{put}\) function will align the source and view lists (associated by \lstinline{~*>}) according to a predefined minimum edit distance function on trees~\cite{Bille:2005:STE:1085274.1085283} instead of using the default position-wise alignment.
This to some extend solves the problem discussed below.

In previous \biyacc, the \(\var{put}\) from an AST to a CST is accomplished by recursively performing pattern matching on both tree structures.
This approach naturally comes with the disadvantage that the matching is mainly decided by the position of nodes in the AST and CST.
Consequently, a minor structural change on the AST may completely disrupt the matching between the AST and the CST, and this may not be desirable.
%, causing the CST to be constructed from scratch and losing the layout information.


A typical scenario where the matching of a CST and an AST can be easily disrupted is the synchronisation of two expression sequences.
Consider the following example in \tiger:
\begin{lstlisting}
( k := 2;  /* dead code */
 inc(x);  /* increment on x */
 k := x + 5;
 if k then ...; ... )         (*@.@*)
\end{lstlisting}
Suppose that we optimise the AST by removing the dead expression \lstinline{k := 2}.
Then the CST and updated AST become:
\begin{center}
\begin{tikzpicture}[scale=0.7,font=\tt,line width=1.2pt,line cap=round,font=\small\ttfamily]
 \draw (0, 0) node [font=\bf\small\ttfamily] {ExpSeq0};
 \branch{0}{0}
 \draw (-1, -1) node [font=\small\ttfamily] {$\mathbf{k := 2}$};
 \draw (-1, -1.38) node [font=\bf\small\ttfamily] {(dead code)};
 \draw (1, -1) node {ExpSeq0};
 \branch{1}{-1}
 \draw (0, -2) node {inc($x$)};
 \draw (2, -2) node {ExpSeq0};
 \branch{2}{-2}
 \draw (1, -3) node {$k := x + 5$};
 \draw (3, -3) node {ExpSeq0};
 \branch{3}{-3}
 \draw (2, -4) node {IfThen0};
 \draw (4, -4) node {$\ldots$};

 \draw (5, 0)  node {TExpSeq};
 \branch{5}{0}
 \draw (4, -1) node {inc($x$)};
 \draw (6, -1) node {TExpSeq};
 \branch{6}{-1}
 \draw (5, -2) node {$k := x + 5$};
 \draw (7, -2) node {TExpSeq};
 \branch{7}{-2}
 \draw (6, -3) node {TIf};
 \draw (8, -3) node {TExpSeq};
 \branch{8}{-3}
 \draw (7, -4) node {$\ldots$};
 \draw (9, -4) node {$\ldots$};

 \draw[dotted, line width=.75pt] (5.72, -2.78) arc (70:110:5.8476);
 \draw[dotted, line width=.75pt] (4.72, -1.78) arc (70:110:5.8476);
 \draw[dotted, line width=.75pt] (3.72, -0.78) arc (70:110:5.8476);

 \draw (1, -4.6) node [font=\rmfamily\it\footnotesize,align=left] {CST (Simplified)};
 \draw (6.75, -4.6) node [font=\rmfamily\it\footnotesize,align=center] {Updated AST};
\end{tikzpicture}
\end{center}
What we want is removing the first concrete subtree, synchronising the second concrete subtree with the first abstract subtree, and so on, in order to retain as much information as possible (e.g.~the comment \lstinline[breaklines=true]{/* increment on x */}).
However, the conference version of \biyacc\ synchronises the concrete and abstract subtrees in order, and the subtrees are all mismatched, causing the concrete subtrees to be printed from scratch and the original whitespaces to be discarded.
% This scenario motivates us to extend \biyacc\ with a mechanism in which the user can write strategies that match subtrees of the same structure in a more global manner.

If the expression sequences are written using Kleene operators as shown above, this problem is solved silently without any additional effort.
We believe that our default measurement function using minimum edit distance between trees has a passable performance in many scenarios.


% \subsubsection{Illustration of Translation}
% We illustrate the translation process using the production and updating strategy
% \begin{lstlisting}
% ExpSeq -> Exp {';' Exp}*

% [TExp] +> ExpSeq
% (e:es)  +>  (e +> Exp) {';' (es +> Exp)}* ;
% \end{lstlisting}

% \paragraph{Generating the Concrete Parser}
% Since the underlying parser generator \happy\ does not support Kleene operators, from the production \lstinline!ExpSeq -> Exp {';' Exp}*! we generate the following semantically equivalent \happy\ program for building the CST\footnote{Here the word CST is defined in terms of the user. As for \happy, it is already the final AST produced.}:
% \begin{lstlisting}
% ExpSeq : Exp ZeroOrMore_i            { ExpSeq0 $1 $2 }

% ZeroOrMore_i : {- empty -}           { [] }
%              | ';' Exp ZeroOrMore_i  { ($1, $2) : $3 } (*@.@*)
% \end{lstlisting}
% The \lstinline{_i} part of \lstinline{ZeroOrMore} is a fresh integer guaranteed by a state monad and thus it makes the nonterminal fresh.
% %
% The terminals and nonterminals within a Kleene operator (\lstinline{';'} and \lstinline{Exp} in this example) are gathered in a tuple.
% %
% Thus the type of the part within a Kleene operator is a list of tuples of strings\footnote{We use \(\mathsf{String}\) to store all the data in CST since it is the most precise representation. For instance, if a language permits leading zeros to an integer such as 073, storing it as \(\mathsf{Integer}\) will cause the leading zero to be lost.}.
% The translation of one-or-more occurrence is quite similar, in which the \lstinline!{- empty -} { [] }! case is replaced by \lstinline!';' Exp { [($1,$2)] }! to guarantee it occurring at least once.


% \paragraph{Generating the Concrete Printer}
% Printing the CST built from Kleene operator back to program text is simply a fold over a list of tuples (of strings).
% The production \lstinline!ExpSeq -> Exp {';' Exp}*! generates
% \begin{lstlisting}
% ppExpSeq (ExpSeq0 s0 s1) = ppExp s0 ++
%   foldr (\(x1, x2) xs ->  ";" ++ x1 ++  ppExp x2 ++ xs) "" s1 (*@.@*)
% \end{lstlisting}
% Here \lstinline{s0} is a little confusing, since it contains no \lstinline{';'} but all the whitespaces after that \lstinline{';'}.
% Again, this is because from the constructor \lstinline{ExpSeq0} we already know all the literals and their exact positions in the production; so there is no need to store them in the CST.



% \paragraph{Generating the \bigul\ Program}
% Now we introduce how to generate the \bigul\ program for synchronising CSTs described by Kleene operators and their corresponding ASTs.
% From \lstinline!(e:es) +> (e +> Exp) {';' (es +> Exp)}* ;! the following \bigul\ program is generated\footnote{The outmost \(\mathsf{Case}\) construct and the entrance and exit conditions are almost the same as before and omitted here.}:
% \begin{lstlisting}
% $(rearrV [| \(e:es) -> (e, es) |] ) $
%   $(rearrS [| \(ExpSeq0 e by_kleene0) -> (e, by_kleene0) |] ) $
%     $(update [p| (e, es) |] [p| (e, es) |]
%       [d| e = bigulTExpExp;
%           es = minEditDistLens (" ", ExpNull) (bigulTExpTupleStringExp) |])  (*@.@*)
% \end{lstlisting}
% We can see that the source and view are rearranged into tuples and are updated position-wise.
% %
% If the source and view lists (\lstinline{es} here) are associated by Kleene operators, then they are updated using \lstinline{minEditDistLens}, which first aligns the elements of the lists (according to minimum edit distance) and then performs update on the aligned pair using the lens \lstinline{bigulTExpTupleStringExp} in its argument.
% %
% The name \lstinline{bigulTExpTupleStringExp} is generated from the type of the view \(\mathsf{TExp}\) and the type of the source \((\mathsf{String},\mathsf{Exp})\).
% %
% If the length of the source list is less than the view's, the default value \lstinline{(" ", ExpNull)} is used as placeholders to make the length match.
% %
% Both the default value and the lens for updating aligned pairs are automatically generated in accordance with the types of the source and view lists.
% %

% The code generation will be more complicated if there are more than one nonterminals within the Kleene operator.
% For example, consider the following (artificial) production and updating strategy
% \begin{lstlisting}
% ExprPlus -> {Expr '+' Expr}+

% Exprs +> ExprPlus
% Adds es1 es2  +>  { (es1 ~*> Expr) '+' (es2 ~*> Expr) }+
% \end{lstlisting}
% where the left and right operands of an addition are stored in two different lists in the view (\lstinline{es1} and \lstinline{es2}).
% %
% Here, the type corresponding to \lstinline!{Expr '+' Expr}+! in CST is \([(\mathsf{Expr},\mathsf{String},\mathsf{Expr})]\),
% whereas the type of the corresponding rearranged view \lstinline{(es1,es2)} is \((\mathsf{[Arith]}, \mathsf{[Arith]})\), which is not a list.
% %
% Thus before aligning the source and view, an unzip-like lens\footnote{It performs `unzip' in the \emph{putback} direction and `zip' in the \emph{get} direction.} should be invoked to converting data of type \((\mathsf{[Arith]}, \mathsf{[Arith]})\) into \([(\mathsf{Arith}, \mathsf{Arith})]\).
% This unzip-like lens is composed with \lstinline{minEditDistLens} to make the whole update run smoothly.
% \begin{lstlisting}
% $(rearrV [| \(Adds es1 es2) -> (es1, es2) |] ) $
% $(rearrS [| \(ExprPlus0 by_kleene0) -> by_kleene0 |] ) $
% $(update [p| es1es2 |] [p| es1es2 |]
%    [d| es1es2 = minEditDistLens (ExpNull, " ", ExpNull) bigulExprsTupleExprStringExpr
%                 `Compose` bigulUnZip2 |])
% \end{lstlisting}

% \paragraph{Limitations}
% \biyacc\ does not support nested Kleene operators because we have not devised proper syntax for it.
% Besides, we do not provide the optional operator \lstinline{?} denoting zero-or-one occurrence either.
% Although there is no technique barrier, the optional operator does not give rise to any benefit in the putback direction, unlike Kleene operators.


\subsection{User-Defined Adaptation Functions}
In the last paragraph of \autoref{sec:bigul} we have introduced adaptive functions for `printing an AST when the source program text is too different from the AST or even nonexistent at all';
these adaptive functions are automatically generated from a \biyacc\ program.
%
Despite being convenient, sometimes the user may want to write his own adaptive functions so that in the putback direction he has more control such as alignment between the source and view lists and adjustment to layouts.

\todo{example 1. alignment strategies other than minimum edit distantces}

\todo{example 2. layouts adjustment the program text is too different from the AST}


\subsection{View-Dependency}

\todo{In particular, Non-Linear Patterns. Examples such as simple `optimisation' in parsing. Add x x ~ Mul x 2}

\todo{In general syntax, Add x y | id x = y ~ Mul x 2}


% \todo{This matching problem is one that can be solved more effectively with the putback-based approach, since matching strategies inherently belong to the putback direction.
% Indeed, we have already hand-coded in \bigul\ one global matching strategy that finds the longest common subsequence of two expression sequences, and it is easy to change to other strategies such as one based on tree edit distance~\cite{Bille:2005:STE:1085274.1085283}.
% It is, however, still a challenge to design proper surface syntax in \biyacc\ for the user to write their own global matching strategies.}


\todo{rewriting the tiger example using kleene operators, ambiguous grammar, and filters?}

