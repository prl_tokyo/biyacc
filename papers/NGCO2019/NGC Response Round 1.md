Here is the complete response to the reviewers. We appreciate the effort the reviewers spent for reviewing our paper, a very long one. We have taken all the concerns into consideration and made necessary revisions as detailed in the response. 

(We are sorry that the [demo page](http://www.prg.nii.ac.jp/project/biyacc.html) was reverted to its one-year ago state, without the supporting for bidirectionalised filters, by mistake; we lost the data and are trying to rebuild the webpage.)

# For Reviewer 1

> My only misgiving is with how the case studies are organised. The
authors have coded up the Tiger language, which is good-size example
demonstrating the feasibility of the proposed system. However, the
subsections 6.1, 6.2, and 6.3 are all about syntactic sugar. What is
preventing your from drawing from a wider range of transformations?
After all, you have the full parser, not just a sugaring
transformation.

We chose the Tiger language for our case study because it is not too complex and yet covers many important language features; moreover, it is used in Appel's famous textbook on compiler implementation. 

We use the grammar and AST description from the textbook almost as is, without adding many auxiliary nodes to the AST for the ease of implementation. However, as you pointed out, the transformation between the CST (described by the grammar) and the AST turns out to be straightforward, without many non-trivial mappings. But we should also notice that the existence of the disambiguation module imposes more constraints on the transformation (for satisfying the bi-filter laws). We did not build parsers for real-world languages, but according to our survey of JDT's Java 8 parser, it even does not desugar any surface language's constructs through parsing...

Let us call the transformation between CSTs and ASTs ‘horizontal transformation’. Instead of showing non-trivial horizontal transformations, in the case study section, we showed that the horizontal transformation is easy to evolve and quite a few meaningful ‘vertical transformations’ from an AST to its modified AST' can be propagated back to the CST (program text) using the (given) horizontal transformation without additional effort:

- Subsection 6.1 shows that our tool can preserve syntactic sugar in the printing direction; subsection 6.2 further explains that the ability of preserving syntactic sugar gives rise to tools such as resugaring. We have merged them into a single subsection.

- Subsection 6.3, in fact, demonstrates that our tool facilitates language evolution in its own right: the horizontal transformation is easy to evolve. Instead of modifying both a parser and a (reflective) printer and verify the consistency of the updated pair, we simply updated the (reflective) printer and that is all we have to do. This is not directly relevant to syntactic sugar, however, if the new feature of an evolved language is just a syntactic sugar of an old one, our approach slightly outperforms others' mentioned in the related work of the paper.

- Just before the beginning of subsection 6.3, we mentioned that BiYacc also helps to propagate simplification-like optimisations and some code refactoring transformations. Perhaps they are not noticeable, so we added a new subsection presenting them.

In general, there seems to be a trade-off between the complexity of horizontal and vertical transformations: vertical transformations can quickly get difficult or restrictive as the complexity of horizontal transformations increases.  We happen to choose an example where the horizontal transformation is non-trivial and yet allows a good range of meaningful vertical transformations.

----

> The discussion of related work can be improved too. In Page 44 Line
36, there are references to properties (4) and (3). It is not clear
what they are. Particularly, reference (3) seems to refer to a program
skeleton, not a property.

The references to properties (4) and (3) were wrong. We have fixed them in the revised version. They should point to (the right-to-left direction and left-to-right direction of) Theorem 1.


>The discussion of ISDs and FliPpr is a bit misleading, though not incorrect. The two systems produce invertible programs, not bidirectional programs. So it should not be surprising that some of the bidirectional properties are not satisfied.

Since the reference is wrong, we gave you the wrong information regarding the comparison; we are very sorry for this. Here, we were not talking about their bidirectionality for unambiguous grammars; we were talking about the preservation of their inverse properties (e.g., `parse (print t) = t` and `print (parse s) = s`) when the grammar is ambiguous. They do not promise to handle ambiguous grammars, though, but other tools such as XSugar handles ambiguous ones. We have rewritten the related work regarding this.

>There are also subsequent work on FliPpr which may be worth mentioning given how closely related that line of work is.

The comparison is based on the journal version (2018) of FliPpr.

----

Minor points:

> Page 9, line 11: A forward reference to Section 3.2 inside Section
3.2.

> Page 9, line 38-49: I feel that a bit more explanation is needed
here. For example, there are multiple bindings of "x" in the example,
but it is not clear how they are related.

> Page 10, line 15-28: I am able to understand the description because I
am modarately educated with the idea of BiGUL. But I don't think an
average reader will be able to appreciate the point simply by being
told that the source is "too mismatched". An example is needed.

> Page 16, line 8: The is the second appearance of Happy. The first one
has no reference.

> Page 18, line 31: Again, adaptive branch is not easy to understand. More explanation here will be useful.

> Page 15, Theorem 1: The injectivity discussion confused me for some
time. I think it will be better clarify early in Page 14, Line 49 that
the isomorphism is for injective cprint.


All are addressed. (For example, more explanations and examples are added.)


# For Reviewer 2

## What We Addressed

> and [34] and [40] are later discussed and on p.43, l.33-38 "For example, both Rendel and Ostermann [40] and Matsuda and Wang [34] adopt a combinator-based approach, where small components are glued together to yield more sophisticated behaviour, and can guarantee inverse properties similar to the ones of our concrete parsing and printing isomorphism (with CST replaced by AST in the equations)." -how am I to interpret "cannot deal with synchronisation between program text and ASTs"? Either more explanation is needed, or, perhaps a more humble statement is needed. Your achievements have their right without such a statement, e.g., you are discussing ambiguous grammars.

We revised the sentence to "However, the essential factor that distinguishes our system from others is that the printer produced from a BiYacc program is reflective and can deal with synchronisation."
(Others are pretty printers that are not aimed for synchronisation; it is obvious from the type of the printers.)


> The only reference to the existing work in the introduction is in line 32-37, p. 2: "In response to this problem, many domain-specific languages [5, 13, 34, 40, 48] have been proposed, in which the user can describe both a parser and a printer in a single program. Despite their advantages, these domain-specific languages cannot deal with synchronisation between program text and ASTs." This confuses me since [6] is missing,

At first, we deliberately exclude [6] here because when we use [6] to write ‘parsers and printers’, one syntax of the two is fixed to be XML and we didn’t feel that it was general enough. We added it back.



> ### The article is not self-contained.

> I am desperately missing as (short) preliminary which introduces the notation you use -mark them clearly, so that people familiar with, e.g., Haskell may skip it.  
> There is no introduction to the Haskell-like notation that is used through the paper or even mentioning that it is Haskell -this is assumed and the reader can figure it out at page 4, from the sentence " Abstract Syntax. The abstract syntax part—which starts with the keyword #Abstract— is just one or more definitions of Haskell datatypes."  
> p.3.,  "Specifically, we want to ensure two inverse-like properties: Firstly, a piece of program text s printed from an abstract syntax tree t should be parsed to the same tree t, i.e.," This relation between s and t seems not to be reflected in (1)? does there need to be a certain relation between s and t? is it normal to read "print s t" as "a program text s is printed from an abstract syntax tree t"?
> p.3., (1) and (2): the formulas both miss "for all s", and "for all t"  
> p.8.,def 1. : missing forall s and forall v  
> p.26, Thm 2: insert missing "for all text and cst" in both (6) and (7) and,  again, this is not a correct sentence.

We forgot to say that we use Haskell notation. In the introduction of the revised paper, we added the explanation that we use Haskell notation throughout; in particular, type variables are implicitly universally quantified.

As for `print s t`, the function `print` has two arguments `s` and `t`. In Haskell notation, they are implicitly universally quantified.

Also added is the sentence: Additionally, we omit universal quantification for ‘free’ variables in an equation; for instance, ‘parse (print s t) = t’ is in fact ‘forall s, t. parse (print s t) = t’.

> P.11: is ">>=" infix ? please state so.

It is the standard Haskell operator *bind*.

> p.14, l.22:  "Just o print" what is o here?

It is the standard notation for function composition.





> ### The related work

> The level of details in the discussion of the closely related literature [6,34,40] is good. How does Lutterkort's article from 2008 [Lut08] relate? He presents a lens-based transformation, which aims at preserving comments and formatting in the textual configuration files.

> [Lut08] D. Lutterkort: Augeas-A Configuration API. In Proceedings of the Linux Symposium, Ottawa, ON, pages 47-56, July 2008.

Thanks. We added a paragraph comparing our approach with his.



> p.4.,l.16-18  "We demonstrate that BIYACC can handle syntactic sugar,
partially subsume Pombrio and Krishnamurthi's 'resugaring' [38, 39], and facilitate language evolution.": it is unclear to me what is meant by "partially subsume Pombrio and Krishnamurthi's 'resugaring'".

It means that our system can partly achieve Pombrio and Krishnamurthi's 'resugaring' work.


> p.3, l.22-23: " which enables the user describe" ->  which enables the user to describe

> Sometimes, terms are used that are not yet defined:
> p.3., (1),(2), please introduce the functions print and parse before using them, e.g., in lines 26-27.

> p.4.,l.47-49: help the reader to understand "constants", e.g.,  if you mean integers and variables, you could write "whose elements are constructed from constants, i.e., integers and variables, and arithmetic operators."

> p.3-4: Consider introducing (and consistently use) a more precise terminology for the elements in figure 2, e.g., use "groups", "rules", "declarations".

All the above are fixed.

> p.6,l.8: "hsType -> nonterminal" does not make sense until the syntax in Fig. 4, p.12 is introduced.

We added a short explanation. (And `hsType +> nonterminal` should be `HsType +> Nonterminal`.)

> This would also support the choice of names in formula (3), p.10, which only makes sense after seeing the syntax in Fig. 4. For instance, the "part" can be removed in "The #Directives part" and instead rephrased to "The directive declarations (occurring after #Directives)".

Now we give more explanations in the paper:  
(a) The abstract syntax part (decls for HASKELL data type declarations)...  
(b) The concrete syntax part (pgs for production groups)...   
(c) The directives part (drctvs for directives)   
(d) The printing actions part (ags for action groups) ...

> p.6., l.39. please provide a small example

Example provided.


> p.6., l.43: "This time the pattern matching should succeed at the action used to create the CST, and the program will be able to make further progress." Why have you used "should"? Can there be several repetitions (I believe this is the case)? Have you assumed termination or is termination implicit by construction?

We changed *should* to *will*; there are no repetitions. However, the termination is not guaranteed. For instance, put the parenthesis rules on top will lead to a printing strategy producing parentheses forever: `print t = "(" ++ print t ")"`, like ordinary Haskell functions.


> p.7, footnote: "We leave the proper treatment of comments as future work." please provide a small example of this in the discussion of the results, or if it already is there, refer to that example.

We removed the footnote. It is not easy to present our approach in a few limited sentences; and we cannot give a reference either, since our work on that was rejected this year.


> p.11, l.42-43: when you write " We omit the (standard) proof that the new lens is indeed well-behaved." insert a reference.

We added a reference [17].


> p.13, l. 29-30:  (pgs in (3)) ->  (pgs in formula (3), p. <insert pagref>)

We will check with the (copy) editor how this can be done.


> p.13, footnote: "String is the most precise representation" wouldn't it be "least" so that "String is the least precise representation".

String is the most precise representation regarding information preservation. There was a footnote stating the reason: “… Storing 073 as Integer will cause the loss of the leading zero.”

> p.15,l.29: refer to Lexer when describing "tokenizing"

Changed this to ‘lexing’.

> p.15, thm 1: where does injective come from? please support the requirement with an example.

Fixed. The injectivity of cprint amounts to saying that the grammar is unambiguous.

> p.16, l.49: the sentence is confusing, are all the branch-rules occurring first and the all adaptive branches?

Yes.

> p.22, l.7: "'in a good shape'"-> "of an allowed form".  
> p.22, l.49: "bring its input tree into a correct state with respect to judgeF" -> "transform its input tree into a state accepted by judgeF"  
> p.24, l. 19: consider changing the title from " The New BI YACC  System" to " A BI YACC  System for ambiguous grammars"

OK. Addressed.


> p.25, l.25+l.33: consider rephrasing
>
> " A generalised parser cgparse is correct" to  "A generalised parser cgparse is correct with respect to cprint, abbreviated cprint-correct,"
and
>
> " A bi-filter F is complete with respect to a printer cprint .. " to  "A bi-filter F is complete with respect to a printer cprint, abbreviated cprint-complete, .."
> 
This would make the upcoming Lemmas more elegant:
"In the definitions of cparseF and cprintF, if cgparse is correct and F is complete with respect to cprint, then"
can be rephrased to
"If a cgparse is cprint-correct and F is cprint-complete then"

Thanks. We rephrased it to "A generalised parser cgparse is correct *with respect to cprint*". But we did not give them abbreviations since they are used only once later.


> p.26, l.43: Just a comment that I lost my sense of direction here, so it might be a nice place to insert some meta-text on what we have done now and where we are going.

Meta-text Added.


> p.30, l.22: "(The bi-filter semantics of)" ->  "The bi-filter semantics of"

Fixed. (We deleted those words.)


> p.30, l.47-p.31, l.5: please explain the notation used here, it is not clear to me how to understand the forms in (ii).

We revised the explanation and added a concrete example.


> p.34,l.40: "match t1 p" -> "match t3 p"
> p.36,l.20-22: "Therefore the two bi-filter always repair different parts of a tree, and can repair the tree in any order without changing the final result." is called non-overlapping in term rewriting.

Thanks.

> p.5., fig. 2: comment on why // are written "//" and + are written '+'.
> p.5., l.39:  '// ' -> "//"
> p.5., l.39:  '/\* ' '\*/ '-> "/\*" "\*/"

Addressed. ‘While single quotation marks are for characters, double quotation marks are for strings. For simplicity, the user can always use double quotation marks.’


> p.41, l.38: "(e.g. foreach loops in Java 5)" please provide a reference

Oracle no longer provides Java 5 documentation (although various sources indicate that foreach loops were introduced in Java 5). We have included a reference to the documentation of Java SE 6: https://docs.oracle.com/javase/specs/




## Discussion

> Structure:
The article would benefit greatly from a restructure of the paper.
The handling of ambiguous grammars starts on page 19, and it feels like the paper repeats itself, rather than, as is intended, describing the extensions only. Since the generalised parsing and disambiguation should handle unambiguous grammars as a special case, it would reduce the paper significantly if you instead presented this system directly (after the easy introduction on p.2-3.).

The extension to deal with ambiguous grammars is itself quite complex, and we believe that separating it from the basic architecture will make the material more gentle and accessible. We are also not sure about how much of the content can be reduced if we directly introduce the full BiYacc: for example, it is important to know the basic iso + lens structure before talking about how to adapt that structure to handle ambiguity.


> Language:
The language is understandable, but it is in my opinion not up to article standards; there are examples of this on almost every page, and due to the length it is beyond my obligations as a reviewer to comment on this.The authors tend to use spoken language which is at times ok, but mainly is wordy and feels like a lack of effort. e.g. "the differences in a nutshell are as follows:" would typically be phrased as "the main differences are:" instead and "a bi-filter that solves the (in)famous dangling-else problem." as "a bi-filter that solves the classic dangling-else problem." Some sections are better, e.g., Section 7.1, but in general, a careful rephrasing is required and I would recommend a proof-reading.

The usage is indeed common in the computer science literature. For example, "the infamous dangling else problem" is used in books such as

- *Theoretical Studies in Computer Science* by Jeffrey D. Ullman

- *Java and Object Orientation: An Introduction* by John Hunt

- *Formal Methods in Computer Science* by Jiacun Wang


> The introduction:
> I am missing the contributions in the introduction.

The first paragraph on p4 outlines what we have done. And since this is an extended version of our conference paper, we additionally emphasised the new contributions and listed them in the last paragraph in the introduction.

----

> I'm missing an overview - a short introduction - of the related literature in the introduction somewhere, so the reader becomes familiar with the field and is confident that your work contributes to it.
> I would expect that you group the existing literature according to different themes and subthemes, e.g., bidirectional transformations, biparsers, reflective biparsers, unambiguous/ambiguous grammars, so we get to know the field (superficially) and so we easily see that your contributions in the paper are relevant (choose some of the themes according to your contributions). It might also be relevant to group them according to the methods they use.

Different people prefer different structure of an article. In our area, related work is presented almost before the conclusions and almost never after the introduction. See page 27--page 32 of the [slides](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/How-to-write-a-great-research-paper.pdf
) of the talk ‘How to write a great research paper’ by Simon Peyton Jones.

Additionally, there was very detailed related work in Section 7. If the reader would like to know the field first, they can first read Section 7 and then go back to Section 2. We mentioned this in the ‘structure of the paper’, the last but one paragraph in the introduction: ‘In Section 7, we present detailed related work including comparison with other systems.’


----

> There is no reference to Yacc and Lex in the introduction -one might guess from the name BiYacc that they are somehow related, but not how or whether it is an extension or a generalisation or it is inspired. Yacc is mentioned (with capital letters, instead of "Yacc") in the parsing at page 7 "(This might remind the reader of the usual YACC  actions.)", but Yacc is not cited until page 38 in the context of a case study "For this case study, we choose the TIGER  language, which is a statically typed imperative language first introduced in Appel's textbook on compiler construction [4]." Please note that Appel (et al) has written several books on Yacc, see, e.g., https://www.cs.princeton.edu/~appel/modern/. Another comment would be that Appel was not the first to describe Yacc, that was S. C. Johnson who published a report in 1975 describing Yacc.

We are sorry that we assume that researchers are familiar with Yacc ('s semantic actions) and did not give it a citation on page 7. We added a citation.

(By the way, Appel's book is not about Yacc, which is just a parser generator, whereas Appel explains the implementation of compilers.)

---

> When introducing/discussing the composition of filters, I would expect some comment on [XOW19].

> [XOW19] Li-Yao Xia, Dominic Orchard, Meng Wang:
Composing Bidirectional Programs Monadically. In Proceedings of ESOP 2019, pages 147-175, DOI: 10.1007/978-3-030-17184-1\_6
Extended version: http://arxiv.org/abs/1902.06950

(Our paper was submitted in January, before the above paper appeared.)

Overall, the work [XOW19] is very novel; however, the work is not directly related to ours for it is about a general framework for lenses and their compositions rather than for parsers and printers. Additionally, (bi-)filters are not lenses. As for producing lenses (representing parsers and printers), we used generator-based approaches instead of those combinator-based approaches.



----

> The Properties (1) and (2) are sometimes referred to as "inverse properties" and sometimes as "consistency properties". In literature, e.g., [17], they are referred to as GetPut and PutGet laws. Pick the name you want to use and remove the other and refer to examples of literature if there are two names.

(1) and (2) are PutGet and GetPut laws (see Section 3.1) rephrased in the setting of parsing and printing. They are also called consistency properties. However, they are NOT inverse properties; we call it inverse-like properties in the introduction due to its inverse-like form and the unwilling to introduce BX laws at the very beginning.


> A list of confusing typos and errors:  
> p.2, l.22 "concrete syntax tree" should be italic  
> p.2, l. 22 "abstract syntax tree (AST)": "tree" should be italic

Not really. What we emphasise here are concrete syntax and abstract syntax instead of concrete syntax trees and abstract syntax trees. We have emphasised concrete syntax two lines above. 

>  p.3, l. 30 "The parser and printer need to be revised from time to time as the language evolves, and each time we must revise the parser and printer and also keep them consistent with each other, which is a time-consuming and error-prone task." requires a rephrasing.

Could you please give a reason? We do not know how it goes wrong.


> p.8, l2, the reference is oddly stated and is not the only one on BX and even perhaps not the most typical one -consider referring to a textbook too,  "the readers are referred to the lecture notes for the 2016 Oxford Summer School on Bidirectional Transformations [18]." -> "see, e.g., [18]."

The Oxford lecture notes are the most up-to-date reference for understanding the current landscape of BX and are very much like a textbook.  “see, e.g., [18]” is, strictly speaking, not grammatical since “[18]” is parenthetical and not part of the sentence.

----

> p.9, l.1: The sentence "having rephrased parsing and printing in term of lenses" is misleading to me; I expected a definition containing " forall s, t : print s t = Just s' => parse s' = t" and "forall s,t: parse s = Just t => print s t = Just s".

Just one line above there is:
> The parse and print semantics of a BIYACC program will be the pair of functions get and put in a lens, required by definition to satisfy the two well-behavedness laws, which are exactly the consistency properties (1) and (2) reformulated in a partial setting.

Anyway, we added the new formulae *Parsing and Printing in Terms of Lenses*.

----

> p.9, l.27: bigul s s -> bigul s v

It is indeed `BiGUL s s`. As explained in the paper: ‘The simplest BIGUL operation we use is `Replace :: BiGUL s s` which discards the original source and returns the view — which has the same type as the source — as the updated source.


> p.9, l.28 " which discards the original source and returns the view" -> "which discards the original source s and returns the view v"

There are no (data) s and v in the context.


> p.9,l.34: why is update a different font than replace?

`Replace` is a data constructor while `update` is a function name.

> p.9,l.36: the notation used here is not explained and it is not possible to guess.

It is indeed Template Haskell syntax, which was mentioned and has a citation one line above.

> p.10, l.10: should "branch s v" and "bigul s v" switch places?

No, they should not. bigul s v are updates. `$(normalSV [p| spat |] [p| vpat |] [p| spat'|])` takes an update and makes it a branch.


----

> p.11, l.31: using isomorphism before the definition

p.11 l.31: ‘*After giving the general definitions of isomorphism and composition in Section 4.1*’. Seems no problem?


> p.11, l.48-p.12, l.38: this sounds like a lemma or a new definition and a lemma -consider referencing a paper containing proof of this.

One line below there is a sentence ‘*This is specialised from the standard definition of lens composition [17]*’.


> p. 11, l. 3: "Maybe a -> (a -> Maybe b) -> Maybe b" -> "Maybe A -> (A -> Maybe B) -> Maybe B"

`Maybe a -> (a -> Maybe b) -> Maybe b` is correct because `>>=` works for all types. Note that the get and put functions are between fixed types `A` and `B`. They are different.

> p.16, l.1-2: the sentence about whitespaces occurs somewhere before.

Because we would like to mention the important part again.


> p.16,l. 39: state that this is a repetition of formula (3), p. <insert pageref>

We put it there again for the convenience of the reader.


> p.17: figure 5 is not described anywhere. It is referred to in beginning of Section 4.4.

We already introduced the notation in the last paragraph in Section 4.2. The remaining of Section 4.4 described the figure.

----

> p.18: l. 41-46: why is this paragraph here? the paragraph structure of this section supports the idea that this is a part of branching -which it is not.

The section is called ‘Generating the BiGUL Program’; not all of them are about branching.



> p.19, l. 49: "power users" is not a word I have seen before, "superuser" is.

It is indeed a commonly used word. See the wikipedia page: https://en.wikipedia.org/wiki/Power_user


> p.20, l.20: "(in)famous" -> classic

Explained. It is common in text books and papers.


> p.20, l.21: explain how "%left '+'; %left '*';" specifies the precedence over addition.

We wrote *‘... use YACC-style disambiguation directives %left '+'; %left '\*'; to specify that multiplication has higher precedence over addition’*.


> p.20, l.26: a direction is not "broken", since you have proven the theorem.

But one line below shows the reason.


> p.21,l.7: why is Yacc not mentioned before?

We are not sure what is intended by this question.


> p.21,l.35: Is there a test for filters guaranteeing disambiguous grammars? If discussed later, provide a reference to that.

Here bi-filters haven’t even appeared.  The right place to talk about guaranteeing unambiguity is Definition 7 (bi-filter completeness), below which we explained how.

----

> p.21, footnote 5: could the reason be that we can avoid wrapping functions such as "SelectBy"?

Sorry, we couldn’t understand this question.  Could you clarify?


> p.22,l.15: refer to a definition (and page number) for "bidirectionalised filter" and include the abbreviation in that definition instead.

The definition follows p.22 l.15 immediately.


> p.23: The PassThrough property (Def 5, p. 23) seems to require that repair_F is an identity function, i.e., a function f: M -> M is an identity function if for all x in M, f(x) = x. Please comment on this aspect.

It is not necessarily an identity function. The explanation one line below explains it. Note that the filter F and function get are fixed.

> p.23, l.31: insert references to definitions

Everything relevant (e.g., judge_F and repair_F) is given in the preceding paragraph.

----

> p.25, l.10: " if there is no correct CST or more than one correct CST, Nothing is returned" -please provide a small example or refer to later examples where this occur.

We are merely restating the definition of selectByF in prose.


> p.25,l.47: this Lemma does not contain a correct sentence. Please rephrase, e.g., see above.

Please give a reason. We think it is a correct sentence.


> p.26, l.1-17, proof: refer to lemmas and definitions and their page numbers.
> p.27, l.39- p.28, l.14, proof: refer to lemmas and definitions and their page numbers.

It is common practice to refer to only the names of lemmas and definitions without their page numbers.

> p.27, l.22: why suddenly use "mx", "x" and "y" as variables?

Because the notation is commonly used.


> p. 29 , lemma 5: would it make sense to in some way be able to describe "jugde_j t or judge_i t" instead?

We do not understand the question...

----

> p.29, l.9: " will never break what may .. " would the following be correct instead " will never transform what may .. "?

Not necessarily. ‘Never transform ...’ is stronger than ‘never break...’.



> p.29, Lemma 5: This Lemma allow us to interpret the repair actions as nondeterministic term rewriting rules, if so, the respects-requirement in Theorem 4 may be replaced with the more general requirement "convergence", i.e., that they are terminating and confluent, known from Term Rewriting Systems, e.g., [BN98]. Then you obtain the property for the repair-rules that for any state x the rules will produce the same output state y (if applied until no rule can apply anymore), no matter how and in which order the rules apply. The requirements you have given seems to be special cases of convergence. An interesting thing is that, if the programmer can guarantee that the rules are terminating then there is an automatic check for confluence, see the Critical Pair Lemma and Critical Pair Theorem in, e.g., [BN98].

> [BN98] F. Baader, T. Nipkow. Term Rewriting and All That. Cambridge Univ. Press, 1998


Thanks. But for two bi-filters `i` and `j`, `i ◁ j` and `j ◁ i` have different behaviour, which is the reason that we use an asymmetric symbol ◁ for the composition; confluence or convergence is sufficient but not necessary.


> p.30, l.12-16: it is a hard for the reader to be convinced when the semantics of Yacc directives is not explained.

We believe it is reasonable to assume knowledge about Yacc since Yacc or similar tools (e.g., bison) are often introduced in undergraduate CS compiler courses.  Also there were references to Yacc earlier in the paper, and the readers should know what Yacc is by the time they reach here.

> p.30, footnote: It is not okay to be sloppy with your terminology, pick one term or explain in which context you use which one.

There’s no sloppiness here. The whole footnote reads: “The YACC-style approach adopts the word precedence [21] while the filter-based approaches tend to use the word priority [8, 24]. We follow the traditions and use either word depending on the context.”  We are being precise about when to use precedence and priority.

----


> p.31, l.21: insert spaces for readability
> 
Inserted.


> p.32, l. 1-2: what is NT_1->RHS_1 referring to?

It refers to any productions.


> p.34, l.49: what is "Paren"?

See p.34 l.47--l.49: *For example, for the grammar in Figure 6, we should write t +> '(' [t +> Expr] ')' as the only printing action mentioning parentheses*.  Also “the parenthesis production Paren” appears above on l41.



> p.36, l.36: " There are some other ambiguities that our directives cannot eliminate" please provide a small example

See the contents after this sentence.


> p.37, l.39: "Appel's textbook" please note that he has authored several.

Please note that the sentence is ‘... in Appel’s textbook on compiler construction [4]’.

> p.38, Tiger: is it higher-order? please provide a table with the programs, some interesting features of the programs and e.g., the runtime, size of produced programs.

Do not understand the (intention of the) question. We are not building a compiler or a runtime system, just manipulating its syntax...

> Section 6: what are the limits of your reflective bi-parser?

Are there specific kinds of limit that you think are worth discussing here?
