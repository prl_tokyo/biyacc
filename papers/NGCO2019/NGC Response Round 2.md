We would like thank the reviewers for their carefully reviewing, even in the second round. With your help, the readability of the paper is much improved, especially regarding how several case studies are structured, how theorems for the basic BIYACC and theorems for the extended version handling ambiguous grammars are related, and how look-alike notions are ‘disambiguated’.

The changes in the revision (this time) are *marked in red*, which we ought to do but forgot in the last revision.

# Reviewer #1

>  When I mentioned subsequent work on FliPpr, I meant this one https://dl.acm.org/citation.cfm?id=3242758.

Thanks. We have added the missing reference and a footnote: ‘Although they use different implementation techniques, we will not dive into them in our related work. See Matsuda and Wang’s [36] related work for a comparison.’

# Reviewer #3

> I appreciate your changes -they are nice improvements and I like your change of the title. Thank you also for your response -the comments and questions are much appreciated.

> A general comment: I would like the authors to clearly state how Theorem 1, 2 and 3, and Definition 1 or 2 are related. Please provide this description as meta-text around the Theorems.

We have added a new theorem stating the well-behavedness of the lenses (for synchronising CSTs and ASTs) in the basic BiYacc system. Now we have

- (a new) lemma 1 clearly stating that any composite lens from an isomorphism and a well-behaved lens is well-behaved
- theorem 1 *Inverse Properties*, for the partial isomorphism between program text and CSTs
- theorem 2 *Well-behavedness*, for the BiGUL lens between CSTs and ASTs
- theorem 3 *Inverse Properties with Bi-Filters*, for the partial isomorphism extended with bi-filters dealing with ambiguous grammars
- theorem 4 *Well-behavedness with Bi-Filters*, for the BiGUL lens extended with bi-filters dealing with ambiguous grammars

Theorems 1 and 2 are for the basic BiYacc system while theorems 3 and 4 are for the extended system handling ambiguous grammars. When introducing theorems 3 and 4, we also mention that they are the generalised versions of theorems 1 and 2 respectively.

We also restructured Section 4 *The Basic BiYacc* to keep it more consistent with the structure of Section 5, the extended version.

>p.26, Theorem 2: a similar comment as for Lemma 2.

> p.26, Theorem 2: which lemma/theorem of the unambiguous case is this a generalization of? or is there something at all?
> The title of the section (Section 4.3.1) where Theorem 1 occurs, states "Inverse Properties" and the name of Theorem 2 is "(Inverse Properties with Bi-Filters)" -so from the naming they seem related. But this is perhaps misleading?
>
> p.28, theorem 3
> Is it correct that Theorem 3 is for ambiguous grammars what Theorem 1 is for unambiguous grammars? Or how exactly does Theorem 1, 2, and 3 relate?
>
> p.28, l.16: please comment on how Theorem 3 relates to previous definitions and theorems. Is it so that Definition 2 describes "well-behavedness" and Theorem 3 proves that for ambiguous grammars? If so, please create a clearer link, perhaps by a comment and maybe reconsider the name of Definition 2, so it is clear. This link only became clear to me after I searched through the paper for "well-behavedness" and found that p.8 states something about well-behavedness.

See the response above. Theorem 2 is the generalisation of Theorem 1 (with bi-filters), as they apply to the transformation between program text and CSTs; we have added the name *inverse properties* now.

Theorem 3 is the generalisation of well-behavedness (in the setting of parsing and printing) (with bi-filters), as it applies to the transformation between CSTs and ASTs.

--------



> p.3, l.40: concerning the name "consistency properties"
> \>(1) and (2) are PutGet and GetPut laws (see Section 3.1) rephrased in the setting of parsing and printing. They are also called consistency properties.

> If you later use both "inverse-like" and "consistency properties", please introduce "consistency properties" here as well, so that the reader does not have to guess.

> p.14, l.40 concerning "inverse properties"
> \> However, (1) and (2) are NOT inverse properties; we call it inverse-like properties in the introduction due to its inverse-like form

> I agree that (1) and (2) are not "inverse properties" and your term "inverse-like" is a better and more precise name. In the following places, you use the term "inverse":
> p.14, l.40; p.15, l.1; ; p.25, l.1; Theorem 2, p.44, l.17; p45, l.14 & 16.
> In the light of not confusing the reader, please consider whether "inverse" is the correct term or perhaps can be exchanged with "isomorphism" or "inverse-like" (when/if proper) for clarification?

Thanks a lot. As for *inverse properties* and *inverse-like properties*, we used the term *inverse properties* to mean the (partial) isomorphism between program text and their CSTs and the term *inverse-like properties* to mean the well-behavedness (or consistency properties) between program text and their ASTs.

The two terms are indeed kind of confusing. Actually we made mistakes ourselves when describing them (see pp. 25 l.38: *'we are eager to give the (revised version of) inverse-like properties'*, where the inverse-like properties should be *inverse properties*.)

Now we have removed the terms *inverse-like properties* as many as possible from the main text and use *consistency properties* instead; the term *inverse properties* is only used to mean the partial isomorphism between program text and CSTs, which is also the name of theorem 1 now.

All the occurrences are revised.
