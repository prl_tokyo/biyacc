% !TEX root = biyacc.tex

The design of \biyacc\ may look simplistic and make the reader wonder how much it can describe.
However, in this section we demonstrate with a larger case study that, without any extension, \biyacc\ can already handle real-world language features.
For this case study, we choose the \tiger\ language, which is a simple, statically typed imperative language first introduced in Appel's textbook on compiler construction~\cite{Appel98}.
% as a running example to teach the various steps in compiler construction.
Since \tiger's purpose of design is pedagogical, it is small yet covers many important language features; \tiger\ is therefore a good case study with which we can test the potential of our BX-based approach to constructing parsers and reflective printers.

\tiger's features include the well-known ones like conditionals, loops, variable declarations and assignments, and function definitions and calls.
Some of these features can be seen in this \tiger\ program:
%
%\todo{deleted if the page exceeds limit}
\begin{lstlisting}
function foo() =
  (for i := 0 to 10
    do (print(if i < 5 then "smaller"
                       else "bigger");
        print("\n")))
\end{lstlisting}
%
Excerpts of the abstract and concrete syntax of \tiger\ are shown in \autoref{lst:tiger1}.
%, where the usage of constructors can be guessed from its name easily. (The character `\lstinline{T}' appearing in each constructor in the abstract syntax means \lstinline{Tiger}.)
Different from the original description of \tiger, we make some changes to the concrete syntax so that the grammar becomes unambiguous: In order to encode operator precedence in an unambiguous manner, the operators are divided into several groups, with the highest-precedence terms (like literals) placed in the last group, just like what we did in the arithmetic expression example (\autoref{fig:expr}); also, to avoid the ``dangling else'' problem, an additional ``\lstinline{end}'' is needed to mark the end of an \lstinline{if}-\lstinline{then} expression.
%One remark is that the language shown in \autoref{fig:arithExpr} is a subset of \tiger\ and is reused for building this example, except for some changes on names of constructors and production rules, which means that the \biyacc\ program can be developed in an incremental way.

In the following subsections, we will present some nontrivial printing strategies to demonstrate what \biyacc\ is capable of expressing.
% The full \tiger\ example can be obtained \todo{SOMEWHERE}, and an interactive website is \todo{SOMEWHERE}.
\doubleblind{}{The full \biyacc\ program for \tiger\ can be found at \url{http://www.prg.nii.ac.jp/project/biyacc.html}.}


\begin{figure}[t]
\begin{lstlisting}[emphstyle={\bfseries}]
Abstract

data Tiger =
    TInt Int | TStr String | TName String
  | TTypeNil | TSeqNil
  | TExpSeq Tiger Tiger |  TIf     Tiger Tiger Tiger
  | TEq     Tiger Tiger |  TVarDec Tiger Tiger Tiger
  | TFunDec Tiger Tiger Tiger Tiger
(*@\ldots@*)
  deriving (Show, Eq, Read)

Concrete

Exp -> PrimitiveOpt | CallExp | ForExp
     | IfThenElse   | IfThen  | LetExp |(*@\ldots@*) ;

PrimitiveOpt  -> PrimitiveOpt '|'  PrimitiveOpt1
              |  PrimitiveOpt1 ;
(*@\ldots@*)
PrimitiveOpt3 -> PrimitiveOpt3 '+'  PrimitiveOpt4
              |  PrimitiveOpt3 '-'  PrimitiveOpt4
              |  PrimitiveOpt4 ;
(*@\ldots@*)
PrimitiveOpt5 -> '-' PrimitiveOpt5 | Int
              | String | LValue | SeqExp ;

SeqExp -> '(' ')' | '(' ExpSeq ')' ;
ExpSeq -> Exp ';' ExpSeq  | Exp ;

LetExp -> 'let' Decs 'in' ExpSeq 'end' ;
Decs   -> Dec Decs | Dec ;
Dec    -> TyDec | VarDec | FunDec ;

VarDec -> 'var' Name          ':=' Exp
        | 'var' Name ':' Name ':=' Exp ;

IfThenElse -> 'if' Exp 'then' Exp 'else' Exp;
IfThen     -> 'if' Exp 'then' Exp 'end';

(*@\ldots@*)
\end{lstlisting}
\caption{An excerpt of \tiger's abstract syntax and concrete syntax}
\label{lst:tiger1}
\end{figure}
  % | TArrCreate    Tiger Tiger Tiger
  % | TLValueName   Tiger

% \begin{figure}[t]
% \begin{lstlisting}[emphstyle={\color{blue}\bfseries}]
% Concrete

% Exp -> PrimitiveOpt | CallExp | ForExp
%      | IfThen | IfThenElse | (*@\ldots@*) ;

% SeqExp -> '(' ')' | '(' ExpSeq ')' ;
% ExpSeq -> Exp ';' ExpSeq  | Exp ;

% Decs  -> Dec Decs | Dec ;
% Dec   -> TyDec | VarDec | FunDec ;

% VarDec -> 'var' Name          ':=' Exp
%         | 'var' Name ':' Name ':=' Exp ;

% IfThenElse -> 'if' Exp 'then' Exp 'else' Exp;
% IfThen     -> 'if' Exp 'then' Exp 'end';

% PrimitiveOpt  -> PrimitiveOpt '|'  PrimitiveOpt1
%               |  PrimitiveOpt1 ;
% (*@\ldots@*)
% PrimitiveOpt3 -> PrimitiveOpt3 '+'  PrimitiveOpt4
%               |  PrimitiveOpt3 '-'  PrimitiveOpt4
%               |  PrimitiveOpt4 ;
% (*@\ldots@*)
% PrimitiveOpt5 -> '-' PrimitiveOpt5 | Int
%               | String | LValue | SeqExp ;
% (*@\ldots@*)
% \end{lstlisting}
% \caption{An excerpt of \tiger's concrete syntax}
% \label{lst:tiger2}
% \end{figure}

% FunDec ->
%     'function' Name '(' FieldDecs ')' ':' Name '=' Exp
%   | 'function' Name '(' FieldDecs ')'        '=' Exp
%   | 'function' Name '('           ')' ':' Name '=' Exp
%   | 'function' Name '('           ')'        '=' Exp ;




\subsection{Simplified abstract syntax}
\label{sec:sim_ast}
An abstract syntax --- being the internal representation manipulated by the compiler --- should be more concise than a concrete syntax, so that optimisations can be programmed more easily.
In \tiger, for example, variable declarations (\lstinline{VarDec}) in the concrete syntax shown in \autoref{lst:tiger1} come in two variants: one with type declarations and the other without.
In the abstract syntax, on the other hand, we use the same constructor \lstinline{TVarDec} to represent both cases:
If the type declaration is missing from the program text, the second field of \lstinline{TVarDec} in the corresponding abstract syntax tree will be \lstinline{TTypeNil}, indicating that there is no type information in the source.
The printing actions for dealing with this correspondence are straightforward:
%
\begin{lstlisting}[emphstyle={\color{blue}\bfseries}]
Tiger +> VarDec
TVarDec (TName n)  TTypeNil  e +>
  'var' (n +> Name)                  ':=' (e +> Exp);
TVarDec (TName n) (TName ty) e +>
  'var' (n +> Name) ':' (ty +> Name) ':=' (e +> Exp);
\end{lstlisting}
%

The same situation arises for function declarations and conditional expressions as well:
Four forms of function declarations (with/without arguments, and with/without return type) are represented abstractly by a single \lstinline{TFunDec} constructor, and both \lstinline{if-then} and \lstinline{if-then-else} expressions are parsed to \lstinline{TIf}.
The printing actions for them are similar to those for variable declarations, and are omitted here.
%The missing type information is denoted by \lstinline{TTypeNil} and missing expression sequences by \lstinline{TSeqNil}.


\subsection{Syntactic sugar and language evolution}
\label{sec:synSugar}
Syntactic sugar, which lets the programmer use some features in an alternative (perhaps conceptually higher-level) syntax, is pervasive in programming languages.
For instance, \tiger\ represents boolean values false and true respectively as zero and nonzero integers, and the logical operators \lstinline{&}~(``and'') and \lstinline{|}~(``or'') are converted to \lstinline{if} expressions in the abstract syntax:
\lstinline{e1 & e2} is desugared and parsed to \lstinline{TIf e1 e2 0} and \lstinline{e1 | e2} to \lstinline{TIf e1 1 e2}.
The printing actions for them in \biyacc\ are:
%
\begin{lstlisting}[emphstyle={\color{blue}\bfseries}]
Tiger +> InfixExp
TIf e1 e2 (TigerInt 0)  +> (e1 +> Exp) '&' (e2 +> Exp);
TIf e1 (TigerInt 1) e2  +> (e1 +> Exp) '|' (e2 +> Exp);
\end{lstlisting}
%
This kind of syntactic sugar should be distinguished from ``syntactic abstraction'' discussed in \autoref{sec:sim_ast}:
The essential difference between them is that the $\mathit{get}$ transformation for syntactic sugar is not injective, since the alternative syntax and the features being desugared into are both mapped to the latter.
A conventional printer --- which take only the AST as input --- cannot reliably determine whether an abstract expression should be ensugared or not, whereas a reflective printer can make the decision by inspecting the CST.

% \subsection{Constant propagation}
% \todo{not so interesting... I write it but now I want to ignore it and just say a few sentences}
% \subsection{Constant propagation}
% Constant propagation is a straightforward but important compiler optimisation that substitutes the values of known constants in expressions at compile time rather than at runtime.
% We use the example below of solving the 8-Queens problem to show that by using \biyacc\ we can observe changes in the program text when constant propagation is performed on AST.
% This required nothing but print the AST back to the program text.
% \begin{lstlisting}[numbers=left, numberstyle=\tiny]
% let var N := 8
%     type intArray = array of int
%     var row   := intArray [ N ] of 0
%     (*@\ldots @*)
%     function try(c:int)   = ((*@\ldots @*))
% in  try(0)   end
% \end{lstlisting}
% The code fragment is one of the testcases of the \tiger\ language from its official website\footnote{https://www.cs.princeton.edu/~appel/modern/testcases/}.
% Note that the size of the problem is defined by the variable \lstinline{N}, and the lengths of the arrays all depend on the variable \lstinline{N} and thus can be evaluated at compiler time.
% For instance, the right hand side of the variable declaration in the third line will be parsed to:
% \begin{lstlisting}
% TArrCreate (TName "intArray")
%   (TLValueName (TName "N")) (TInt 0)
% \end{lstlisting}
% After constant propagation, it is optimised to:
% \begin{lstlisting}
% TArrCreate (TName "intArray") (TInt 8)
% \end{lstlisting}
% If we reflect the changes on the AST back to program text using \biyacc, the result will be:
% \begin{lstlisting}[numbers=left, numberstyle=\tiny]
% let var N := 8
%     type intArray = array of int
%     var row   := intArray [ 8 ] of 0
%     (*@\ldots @*)
%     function try(c:int)   = ((*@\ldots @*))
% in  try(0)   end
% \end{lstlisting}
% Since the change is precisely located in \todo{use to?} the right hand side of an assignment, other parts of the program including layout information remain intact.


\subsection{Resugaring}

The idea of \emph{resugaring}~\citep{pombrio2014resugaring} is to print evaluation sequences in a core language in terms of a surface syntax.
Here we show that, without any extension, \biyacc\ is already capable of reflecting to the concrete syntax some of the AST changes resulting from evaluation, subsuming a part of \citeauthor{pombrio2014resugaring}'s work.
% Resugaring tools help users to know the semantic of the underlying

We borrow \citeauthor{pombrio2014resugaring}'s example of resugaring evaluation sequences for the logical operators ``or'' and ``not'', but recast the example in \tiger.
The ``or'' operator has been defined as syntactic sugar in \autoref{sec:synSugar}.
For the ``not'' operator, which \tiger\ lacks, we introduce `\lstinline{~}', represented by \lstinline{TNot} in the abstract syntax.
%instead, since in \tiger\ ``false'' is represented by \lstinline{0} and ``true'' is represented by nonzero integers.
% and the lack of \lstinline{not} operation on the boolean value in \tiger,
%For clarity, we introduce \lstinline{"True"} and \lstinline{"False"} to denote boolean values.
Now consider the source expression
\begin{lstlisting}
~1 | ~0
\end{lstlisting}
which is parsed to
\begin{lstlisting}
TIf (TNot (TInt 1)) (TInt 1) (TNot (TInt 0))
\end{lstlisting}
%
A typical call-by-value evaluator will produce the following evaluation sequence given the above AST:
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) TIf (TNot (TInt 1)) (TInt 1) (TNot (TInt 0))
(*@$         \rightarrow $@*) TIf (TInt 0) (TInt 1) (TNot (TInt 0))
(*@$         \rightarrow $@*) TNot (TInt 0)
(*@$         \rightarrow $@*) TInt 1
\end{lstlisting}
%
If we perform reflective printing after every evaluation step using \biyacc, we will get the following evaluation sequence on the source:
\begin{lstlisting}
~1 | ~0    (*@$\rightarrow$@*)    0 | ~0    (*@$\rightarrow$@*)    ~0    (*@$\rightarrow$@*)    1
\end{lstlisting}
%
Due to the \ref{equ:PutGet} property, parsing these concrete terms will yield the corresponding abstract terms in the first evaluation sequence, and this is exactly \citeauthor{pombrio2014resugaring}'s ``emulation'' property, which they have to prove for their system; for \biyacc, however, the emulation property holds by construction, since \biyacc's semantics is defined in terms of \bigul, whose programs are always well-behaved.
Also different from \citeauthor{pombrio2014resugaring}'s approach is that we do not need to insert any additional information into the ASTs for remembering the form of the original sources.
The advantage of our approach is that we can keep the abstract syntax pure, so that other tools --- the evaluator in particular --- can process the abstract syntax without being modified,
whereas in \citeauthor{pombrio2014resugaring}'s approach, the evaluator has to be adapted to work on the enriched abstract syntax.

Also note that the above resugaring for \tiger\ is achieved for free --- the programmer does not need to write additional, special actions to achieve that.
%only basic reflective printing strategies are needed.
In general, \biyacc\ can easily and reliably reflect AST changes that involve only ``simplification'', i.e., replacing part of an AST with a simpler tree, so it should not be surprising that \biyacc\ can also reflect simplification-like optimisations such as constant propagation and dead code elimination, and some refactoring transformations such as variable renaming.
All these can be achieved by one ``general-purpose'' \biyacc\ program, which does not need to be tailored for each application.


\subsection{Language evolution}

We conclude this section by looking at a practical scenario in language evolution, incorporating all the applications we introduced in this section.
When a language evolves, some new features of the language (e.g., \lstinline{foreach} loops in Java~5) can be implemented by desugaring to some existing features (e.g., ordinary \lstinline{for} loops), so that the compiler does not need to be extended to handle the new features.
As a consequence, all the engineering work about refactoring or optimising transformations that has been developed for the abstract syntax remains valid.
%
%Having introduced resugaring, next we present another non-trivial example about language evolution that makes use of all the features we introduced before in this section and show that how \biyacc\ facilitates this work.

Consider a kind of ``generalised-\lstinline{if}'' expression allowing more than two cases, resembling the alternative construct in \citet{dijkstra1975guarded}'s guarded command language.
% in which, say, a program assigning to~\lstinline{m} the maximum value of \lstinline{x}~and~\lstinline{y} can be written as:
%\begin{lstlisting}[emphstyle={\bfseries}, emph={if,fi}]
%if  x >= y -> m := x
%    y >= x -> m := y
%fi
%\end{lstlisting}
%Now we introduce the variant of the syntax to the \tiger\ language, where the concrete syntax is defined as:
We extend \tiger's concrete syntax with the following production rules:
\begin{lstlisting}
Exp    -> (*@\ldots @*) | Guard | (*@\ldots @*) ;
Guard  -> 'guard' CaseBs 'end' ;
CaseBs -> CaseB CaseBs | CaseB ;
CaseB  -> Name '=' Int '->' Exp ;
\end{lstlisting}
For simplicity, we restrict the predicate produced by \lstinline{CaseB} to the form \lstinline{Name '=' Int}, but in general this can be any expression computing an integer.
The reflective printing actions for this new construct can still be written within \biyacc, but require much deeper pattern matching:
%
\begin{lstlisting}
Tiger +> Guard
TIf (TEq (TName n) (TInt i)) e1 TSeqNil
  +>  'guard' (CaseBs -> (CaseB ->
          (n +> Name) '=' (i +> Int) '->' (e1 +> Exp))
      ) 'end';
TIf (TEq (TName n) (TInt i) ) e1 if2@(TIf _ _ _)
  +>  'guard' (CaseBs ->  (CaseB ->
          (n +> Name) '=' (i +> Int) '->' (e1 +> Exp))
                          (if2 +> CaseBs)
      ) 'end';

Tiger +> CaseBs
TIf (TEq (TName n) (TInt i) ) e1 TSeqNil
    +>  (CaseB ->
          (n +> Name) '=' (i +> Int) '->' (e1 +> Exp));
TIf (TEq (TName n) (TInt i) ) e1 if2@(TIf _ _ _)
    +>  (CaseB ->
          (n +> Name) '=' (i +> Int) '->' (e1 +> Exp))
          (if2 +> CaseBs);
\end{lstlisting}
%
Though complex, these printing actions are in fact fairly straightforward:
The first group of type \lstinline{Tiger +> Guard} handles the enclosing \lstinline{guard}--\lstinline{end} pairs, distinguishes between single- and multi-branch cases, and delegates the latter case to the second group,
%Lines 2--5 deals with the case where there is only one branch, while
%state that we want to produce a piece of program text of the form \lstinline{'guard' CaseB 'end'} if the AST is \lstinline{TIf _ _ TSeqNil}; this is because there is only one branch in the AST (the second field of the \lstinline{TIf}), and so should the newly printed code.
%lines 6--10 claims that if the second field (the ``else-part'') in the \lstinline{TIf} in AST is not \lstinline{TSeqNil} but another \lstinline{TIf}, we should produce a source of multiple \lstinline{CaseBs}.
%This reflective printing is achieved by using information from the condition part (\lstinline{TEq ...}) and the ``\lstinline{then}'' part (\lstinline{e1}) in the AST to update the inner parts of the \lstinline{CaseB} branch expanded from the \lstinline{CaseBs},
%and using the ``\lstinline{else}'' part (\lstinline{if2}) in the AST to update the \lstinline{CaseBs} part that is the second nonterminal expanded from the initial \lstinline{CaseBs}.
%(Remember that the production rule for \lstinline{CaseBs} is \lstinline{CaseBs -> CaseB CaseBs | CaseB}).
which prints a list of branches recursively.
%Then how the \lstinline{CaseBs} is updated by \lstinline{if2} is described by the second group of the above program, which is similar to but simpler than the first group and therefore the explanation is omitted here.

This is all we have to do --- the corresponding parser is automatically derived and guaranteed to be consistent.
Now \lstinline{guard} expressions are desugared to nested \lstinline{if} expressions in parsing and preserved in printing,
and we can also resugar evaluation sequences on the ASTs to program text.
For instance, the following \lstinline{guard} expression
%
\begin{lstlisting}
guard  choice = 1  ->  4
       choice = 2  ->  8
       choice = 3  ->  16  end
\end{lstlisting}
%
is parsed to
%
\begin{lstlisting}
TIf (TEq (TName "choice") (TInt 1)) (TInt 4)
  (TIf (TEq (TName "choice") (TInt 2)) (TInt 8)
    (TIf (TEq (TName "choice") (TInt 3)) (TInt 16)
      TSeqNil))
\end{lstlisting}
Suppose that the value of the variable \lstinline{choice} is~$2$.
The evaluation sequence on the the AST will then be:
%\todo{there is a problem, since we do not write printing rules for the intermediate results, reflecting intermediate results such as \lstinline{TIf "True" (TInt 4) (...)} back to program text will fail the put}
%
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) TIf (TEq (TName "choice") (TInt 1)) (TInt 4)
(*@$\phantom{\rightarrow}$@*)   (TIf (TEq (TName "choice") (TInt 2)) (TInt 8)
(*@$\phantom{\rightarrow}$@*)     (TIf (TEq (TName "choice") (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)       TSeqNil))
(*@$         \rightarrow $@*) TIf (TInt 0) (TInt 4)
(*@$\phantom{\rightarrow}$@*)   (TIf (TEq (TName "choice") (TInt 2)) (TInt 8)
(*@$\phantom{\rightarrow}$@*)     (TIf (TEq (TName "choice") (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)       TSeqNil))
(*@$         \rightarrow $@*) TIf (TEq (TName "choice") (TInt 2)) (TInt 8)
(*@$\phantom{\rightarrow}$@*)   (TIf (TEq (TName "choice") (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)     TSeqNil)
(*@$         \rightarrow $@*) TIf (TInt 1) (TInt 8)
(*@$\phantom{\rightarrow}$@*)   (TIf (TEq (TName "choice") (TInt 3)) (TInt 16)
(*@$\phantom{\rightarrow}$@*)     TSeqNil)
(*@$         \rightarrow $@*) TInt 8
\end{lstlisting}
%
And the reflected evaluation sequence on the concrete expression will be:
%
\begin{lstlisting}
(*@$\phantom{\rightarrow}$@*) guard  choice = 1  ->  4
(*@$\phantom{\rightarrow}$@*)        choice = 2  ->  8
(*@$\phantom{\rightarrow}$@*)        choice = 3  ->  16  end
(*@$     \not\rightarrow $@*)
(*@$         \rightarrow $@*) guard  choice = 2  ->  8
(*@$\phantom{\rightarrow}$@*)        choice = 3 -> 16 end
(*@$     \not\rightarrow $@*)
(*@$         \rightarrow $@*) 8
\end{lstlisting}
%
Reflective printing fails for the first and third steps, but this behaviour in fact conforms to \citeauthor{pombrio2014resugaring}'s ``abstraction'' property, which demands that core evaluation steps that make sense only in the core language must not be reflected to the surface.
In our example, the first and third steps in the \lstinline{TIf}-sequence evaluate the condition to a constant, but conditions in guard expressions are restricted to a specific form and cannot be a constant; evaluation of guard expressions thus has to proceed in bigger steps, throwing away or going into a branch in each step, which corresponds to two steps for \lstinline{TIf}.

%\todo{the paragraph is in draft version}
The reader may have noticed that, after the \lstinline{guard} expression is reduced to two branches, the layout of the second branch is disrupted; this is because the second branch is in fact printed from scratch.
This problem is discussed in \autoref{sec:globalMatching}.
%

%\todo{this paragraph is not necessarily needed}
%One remark is that by adding boolean expressions to \tiger\ and refining the hard-coded production rule \lstinline[breaklines=true]{CaseB  -> Name '=' Int '->' Exp} to \lstinline[breaklines=true]{CaseB -> BooleanExp '->' Exp},
%we can observe more detailed evaluation sequences in the program text in the sense that the evaluation process of the condition part of a if-expression (which is evaluated to \lstinline{True} or \lstinline{False}) is then able to be reflected back to the source code.


% \subsection{some other things...}
% \todo{may be merged into other subsections}
% % Another scenario where the expressive power of our approach is helpful is in language evolution. Suppose we want to evolve \tiger\ so that a new specific type of variable, \emph{pointer}, is available.
% % This new type represents an address that does not have to be initialised in memory, and will be helpful for programmers to optimise their code in the new target hardware \tiger\ will be used in.

% % This introduction is extremely useful to programmers of \tiger, but implies editing the complete ecosystem of the language.
% % The parser, the pretty printer, the transformation, all these artifacts have to evolve to support the new extension.
% % With \BiYacc\ however, evolving \tiger\ is simplified.
% % We would have to necessarily introduce information on the concrete representation of the language, and a new transformation rule of the form:
% %%
% % \begin{center}
% % \begin{tabular}{c}
% % {\begin{lstlisting}[numbers=left, numberstyle=\tiny, emphstyle={\color{blue}\bfseries}]
% % ArithTypeField lhs ArithNil
% %                  -> 'pointer' (lhs => String)
% % \end{lstlisting}}
% % \end{tabular}
% % \end{center}

% % After introducing the information in the concrete representation and this instruction in the \lstinline{Action}, \biyacc\ will automatically generate a pretty printer, a parser, and a transformation system that guarantees a generation of good concrete representations.

% % The biggest advantage however, is that the data type \lstinline{ArithTypeField} already existed in the abstract representation.
% % This means all the engineering efforts regarding refactorings or optimisations on the language, which were developed in its abstract form for convenience, maintain their validity.
% % biyacc provides us the expressivity to evolve a language and, together with the bidirectional, putback-based engine, an ecosystem where this evolution is highly facilitated.

% % There will be scenarios where the evolution of the source language will enforce an evolution in the abstract representation as well, but in this cases the usage of \biyacc\ will not be more complex that it is in a more traditional approach.
% % We believe however that will be instances where this evolution can be guaranteed not to touch the abstract representation, thanks to the ability of \biyacc\ on mapping between different, heterogeneous productions both in the concrete and in the abstract representations.

% In scenarios as the ones represented here with the \tiger\ language, \emph{putback-based} bidirectional transformation allow an easy definition of transformation where redundancy is automatically solved.
% Moreover, these features and examples on \tiger\ can be easily extrapolated to work on other programming languages.

% \subsubsection*{refactoring}
% \todo{Java 7 $\leftrightarrow$ Java 8 code may need to be put in the potential apps (our future work?) }
% This introduction is extremely useful to programmers of \tiger, but implies editing the complete ecosystem of the language.
