% !TEX root = biyacc.tex

%\todo{Josh's section: intro to putback-based BX (will refer to an example of different puts for the same get in the previous section) and BiGUL; structure of BiYacc will be deferred to the next section}

%We move on to explain how \biyacc\ is realised.
The behaviour of \biyacc\ is totally nontrivial:
Not only do we need to generate two different programs from one, but we also need to guarantee that the two generated programs are consistent with each other, i.e., satisfy the properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}) stated in \autoref{sec:introduction}.
It is possible to separately implement the $\mathit{print}$ and $\mathit{parse}$ semantics in an ad hoc way, but verifying the two consistency properties takes extra effort.
The implementation we present, however, is systematic and guarantees consistency by construction, thanks to the well-developed theory of \emph{bidirectional transformations} (BXs for short).
(See \citet{czarnecki2009bidirectional} and \citet{hu2011dagstuhl} for a comprehensive introduction.)

\subsection{Parsing and printing as bidirectional transformations}
%\todo{background. simple introduction to bidirectional transformations getput,putget,putget,getput \ldots well-behavedness, they behave well. getput, putget \ldots}

The $\mathit{parse}$ and $\mathit{print}$ semantics of \biyacc\ programs are potentially \emph{partial} --- for example, if the actions in a \biyacc\ program do not cover all possible forms of program text and abstract syntax trees, $\mathit{parse}$ and $\mathit{print}$ will fail for those uncovered inputs.
Thus we should take partiality into account when choosing a BX framework in which to model $\mathit{parse}$ and $\mathit{print}$. The framework we use in this paper is an explicitly partial version of asymmetric lenses~\cite{foster2007combinators}:
A (well-behaved) \emph{lens} between a \emph{source} type~$S$ and a \emph{view} type~$V$ is a pair of functions $\mathit{get}$ and $\mathit{put}$, where
\begin{itemize}
\item the function $\mathit{get} :: S \to \mathsf{Maybe}\;V$ extracts a part of a source of interest to the user as a view, and
\item the function $\mathit{put} :: S \to V \to \mathsf{Maybe}\;S$ takes a source and a view and produces an updated source incorporating information from the view.
\end{itemize}
Partiality is explicitly represented by wrapping the result types in the $\mathsf{Maybe}$ monad.
The pair of functions should satisfy the \emph{well-behavedness} laws:
\begin{align*}
\mathit{put}\;s\;v = \mathsf{Just}\;\mathrlap{s'}\phantom{v} &\quad\Rightarrow\quad \phantom{\mathit{put}\;s\;v}\mathllap{\mathit{get}\;s'} = \mathsf{Just}\;v \tag{\textsc{PutGet}} \label{equ:PutGet} \\
\mathit{get}\;s = \mathsf{Just}\;v &\quad\Rightarrow\quad \mathit{put}\;s\;v = \mathsf{Just}\;s \tag{\textsc{GetPut}} \label{equ:GetPut}
\end{align*}
Informally, the \ref{equ:PutGet} law enforces that $\mathit{put}$ must embed all information of the view into the updated source, so the view can be recovered from the source by $\mathit{get}$, while the \ref{equ:GetPut} law prohibits $\mathit{put}$ from performing unnecessary updates by requiring that putting back a view directly extracted from a source by $\mathit{get}$ must produce the same, unmodified source.
The $\mathit{parse}$ and $\mathit{print}$ semantics of a \BiYacc\ program will be the pair of functions $\mathit{get}$ and $\mathit{put}$ in a BX, satisfying the \ref{equ:PutGet} and \ref{equ:GetPut} laws by definition.
The well-behavedness laws are then exactly the consistency properties (\ref{equ:BXPrintParse})~and~(\ref{equ:BXParsePrint}) reformulated for a partial setting.

\subsection{Putback-based bidirectional programming}
%\todo{in order to understand the details about the implementation of \biyacc, we first introduce BX then \bigul.}

Having rephrased parsing and printing in terms of BXs, we can now easily construct consistent pairs of parsers and printers using \emph{bidirectional programming} techniques, in which the programmer writes a single program to denote the two directions of a well-behaved BX.
Specifically, \biyacc\ programs are compiled to the \emph{putback-based} bidirectional programming language \bigul~\cite{ko2016bigul}.
It has been formally verified in \textsc{Agda} \cite{norell2007towards} that \bigul\ programs always denote well-behaved BXs, and \bigul\ has been ported to \textsc{Haskell} as an embedded DSL library, which will be introduced in more detail in \autoref{sec:programming-in-BiGUL}.
\bigul\ is putback-based, meaning that a \bigul\ program describes a $\mathit{put}$ function, but --- since \bigul\ is bidirectional --- can also be executed as the corresponding $\mathit{get}$ function.
The advantage of putback-based bidirectional programming lies in the following theorem~\cite{foster2010bidirectional}:
\begin{theorem}
Given a $\mathit{put}$ function, there is at most one $\mathit{get}$ function that forms a (well-behaved) BX with this $\mathit{put}$ function.
\end{theorem}
\noindent That is, once we describe a $\mathit{put}$ function in \bigul, not only can we immediately obtain a $\mathit{get}$ function satisfying \ref{equ:PutGet} and \ref{equ:GetPut} with the $\mathit{put}$ function, but also guarantee that the $\mathit{get}$ function is unique, i.e., completely determined by the $\mathit{put}$ function.
Due to this theorem, in this paper we can focus solely on the printing ($\mathit{put}$) behaviour, leaving the parsing ($\mathit{get}$) behaviour only implicitly (but unambiguously) specified.

%This lets the programmer gain full control of the behaviour of their bidirectional programs, in particular the $\mathit{put}$ behaviour.
%For \biyacc, this full control of $\mathit{put}$ behaviour is essential, since \biyacc\ allows the programmer to specify different reflective printing strategies that correspond to the same parsing behaviour, \todo{as shown in \autoref{sec:highlevel_intro}.}
%\todo{so do I need to recover that example again? or see more examples in \autoref{sec:tiger}?}

\subsection{Bidirectional programming in \bigul}
\label{sec:programming-in-BiGUL}

Compilation of \biyacc\ to \bigul~(\autoref{sec:translation}) only uses three \bigul\ operations, which we explain here.
A \bigul\ program has type \lstinline{BiGUL s v}, where \lstinline{s} and \lstinline{v} are respectively the source and view types; its $\mathit{put}$ interpreter can then be given the type
\begin{lstlisting}
put :: BiGUL s v -> s -> v -> Maybe s
\end{lstlisting}
The simplest \bigul\ operation we use is
\begin{lstlisting}
Replace :: BiGUL s s
\end{lstlisting}
which discards the original source and returns the view --- which has the same type as the source --- as the updated source.
That is,
\begin{lstlisting}
put Replace _ v = v
\end{lstlisting}

The next operation \lstinline{update} is more complex, and is implemented with the help of Template Haskell \cite{sheard2002template}.
The general form of the operation is
\begin{lstlisting}
$(update [p| (*@$\mathit{vpat}$@*) |] [p| (*@$\mathit{spat}$@*) |] [d| (*@$\mathit{bs}$@*) |]) :: BiGUL s v
\end{lstlisting}
This operation decomposes the view and source by pattern matching with the patterns $\mathit{vpat}$ and $\mathit{spat}$ respectively, pairs the view and source components as specified by the patterns (see below), and performs further \bigul\ operations listed in $\mathit{bs}$ on the view--source pairs.
The way to determine which view and source components are paired and which operation is performed on a pair is by looking for the same names in the three arguments --- for example, this \lstinline{update} operation
\begin{lstlisting}
$(update [p| x |] [p| (x, _) |] [d| x = Replace |])
\end{lstlisting}
pairs the view with the first component of the source, since both are matched with~\lstinline{x}; the \lstinline{Replace} operation is then performed on this pair, since it is the operation associated with~\lstinline{x} in the (singleton) list of operations.\footnote{This representation of lists of named \bigul\ operations is admittedly an abuse of syntax, but simplifies prototyping this system with Template Haskell.}
%\todo{the underscore is not explained}

The most complex operation we use is \lstinline{Case} for doing case analysis on the source and view:
\begin{lstlisting}
Case :: [Branch s v] -> BiGUL s v
\end{lstlisting}
\lstinline{Case} takes a list of branches.
For this paper we only need two particular kinds of branch, namely \emph{normal} branches whose entry condition is stated as two patterns on the source and view respectively:
\begin{lstlisting}
$(normalSV [p| (*@$\mathit{spat}$@*) |] [p| (*@$\mathit{vpat}$@*) |]) :: BiGUL  s v ->
               (*@$\phantom{\mathit{spat}}$@*)        (*@$\phantom{\mathit{vpat}}$@*)        Branch s v
\end{lstlisting}
and \emph{adaptive} branches whose entry condition is a pattern on the view:
\begin{lstlisting}
$(adaptiveV [p| (*@$\mathit{vpat}$@*) |]) :: (s -> v -> s) -> Branch s v
\end{lstlisting}
There is no surprise about how the condition part works: A normal branch is applicable when the source and view matches $\mathit{spat}$ and $\mathit{vpat}$ respectively, while an adaptive branch is applicable when the view matches $\mathit{vpat}$.
The $\mathit{put}$ behaviour of \lstinline{Case} chooses the first applicable branch from the list of branches, and continues with that branch.
When the chosen branch is normal, the associated \bigul\ operation is performed; more unconventionally, when the chosen branch is adaptive, the associated function is applied to the source and view to compute an adapted source, and the whole \lstinline{Case} is rerun on the adapted source and the view, and is expected to go into a normal branch this time.
Think of an adaptive branch as bringing a source that is too mismatched with the view to a suitable shape so that a normal branch --- which deals with sources and views in some sort of correspondence --- can take over.
This adaptation mechanism is used by \biyacc\ to print an AST when the source program text is too different from the AST or even nonexistent at all.