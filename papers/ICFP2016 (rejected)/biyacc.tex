\documentclass[preprint,fleqn]{sigplanconf}

% The following \documentclass options may be useful:

% preprint      Remove this option only once the paper is in final form.
% 10pt          To set in 10-point type instead of 9-point.
% 11pt          To set in 11-point type instead of 9-point.
% numbers       To obtain numeric citation style instead of author/year.

\usepackage{ifthen}
\newboolean{doubleblind}
\setboolean{doubleblind}{true}
\newcommand{\doubleblind}[2]{\ifthenelse{\boolean{doubleblind}}{#1}{#2}}

\usepackage{amsmath}
\allowdisplaybreaks[1]
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{mathptmx}
\usepackage{mathtools}
\usepackage{fix-cm}
\usepackage{upquote}
%\usepackage{courier}
\usepackage{listings}
\usepackage{enumitem}

\usepackage{xcolor}
\usepackage{textcomp}
\usepackage{tikz}
\usepackage[hidelinks]{hyperref}

% for adding comments
\usepackage{soul}
\setstcolor{red}

\tikzset{
  twolines/.style={execute at begin node=\setlength{\baselineskip}{10pt},align=center},
  codebox/.style={draw,rectangle,align=left,font=\tt\normalsize,inner sep=5pt}
}


\lstset{
  xleftmargin = 1em,
  numbers = none,
  basicstyle=\small\ttfamily,
  % tabsize=2, columns = space-flexible, basewidth = 0.5em,
  tabsize=2, columns = fixed, basewidth = 0.5em,
  emph={Abstract, Concrete, Actions, Program},
  escapechar=~,
  escapeinside={(*@}{@*)},
  % aboveskip=\medskipamount,
  belowskip = 2pt,
  aboveskip = 2pt
}

% \newcommand{\cL}{{\cal L}}

\newcommand\todo[1]{\textcolor{red}{#1}}
\newcommand\bigul{\textsc{BiGUL}}
\newcommand\biyacc{\textsc{BiYacc}}
\newcommand {\yacc} {\textsc{Yacc}}
\newcommand{\happy}{\textsc{Happy}}
\newcommand{\haskell}{\textsc{Haskell}}
\hyphenation{Bi-Yacc}

\def\listemphstyle{emphstyle={\color{blue}\bfseries}}

\newcommand {\BiYacc} {\textsc{BiYacc}}
\newcommand {\BiFluX} {\textsc{BiFluX}}
\newcommand {\tiger} {\textsc{Tiger}}

\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Section}
\renewcommand{\subsubsectionautorefname}{Section}
\renewcommand{\figureautorefname}{Figure}
\newtheorem*{theorem}{Theorem}

\newcommand{\branch}[2]{
  \draw(#1 - 0.22, #2 - 0.22) -- (#1 - 0.78, #2 - 0.78);
  \draw(#1 + 0.22, #2 - 0.22) -- (#1 + 0.78, #2 - 0.78);
}

\renewcommand{\t}[1]{\text{`{\tt #1}'}}
\newcommand{\nt}[1]{\mathit{#1}}
\newcommand{\var}[1]{\mathit{#1}}
\newcommand{\sem}[1]{[\kern-.16em[ #1 ]\kern-.16em]}
\newcommand{\fun}[1]{\textsc{\small #1}}
\newcommand{\iter}[3]{\bigl\langle#3 \bigm| #1 \in #2\bigr\rangle}

\begin{document}

\setlength{\abovedisplayskip}{2pt}
\setlength{\belowdisplayskip}{2pt}

\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\conferenceinfo{CONF 'yy}{Month d--d, 20yy, City, ST, Country}
\copyrightyear{20yy}
\copyrightdata{978-1-nnnn-nnnn-n/yy/mm}
\copyrightdoi{nnnnnnn.nnnnnnn}

% Uncomment the publication rights you want to use.
%\publicationrights{transferred}
%\publicationrights{licensed}     % this is the default
%\publicationrights{author-pays}

%\titlebanner{banner above paper title}        % These are ignored unless
%\preprintfooter{short description of paper}   % 'preprint' option specified.

\title{Parsing and Reflective Printing, Bidirectionally}
%\subtitle{no idea about the title}

\doubleblind{%
\authorinfo{author information omitted for submission}
           {}
           {}
}{%
\authorinfo{Zirun Zhu \and Yongzhe Zhang}
           {SOKENDAI (The Graduate University for Advanced Studies), Japan\\
            National Institute of Informatics, Japan}
           {\{zhu,zyz915\}@nii.ac.jp}
\authorinfo{Hsiang-Shang Ko}
           {National Institute of Informatics, Japan}
           {hsiang-shang@nii.ac.jp}
\authorinfo{Pedro Martins}
           {University of California, Irvine, USA}
           {pedromartins4@gmail.com}
\authorinfo{Jo\~ao Saraiva}
           {University of Minho, Portugal}
           {jas@di.uminho.pt}
\authorinfo{Zhenjiang Hu}
           {SOKENDAI (The Graduate University for Advanced Studies), Japan\\
            National Institute of Informatics, Japan}
           {hu@nii.ac.jp}
}
% \authorinfo{Name2\and Name3}
           % {Affiliation2/3}
           % {Email2/3}

\maketitle

\begin{abstract}
Language designers usually need to implement parsers and printers.
Despite being two intimately related programs, in practice they are often designed separately, and then need to be revised and kept consistent as the language evolves.
It will be more convenient if the parser and printer can be unified and developed in one single program, with their consistency guaranteed automatically.

Furthermore, in certain scenarios (like showing compiler optimisation results to the programmer), it is desirable to have a more powerful \emph{reflective} printer that, when an abstract syntax tree corresponding to a piece of program text is modified, can reflect the modification to the program text while preserving layouts, comments, and syntactic sugar.

To address these needs, we propose a domain-specific language \biyacc, whose programs denote both a parser and a reflective printer.
\biyacc\ is based on the theory of \emph{bidirectional transformations}, which helps to guarantee by construction that the pairs of parsers and reflective printers generated by \biyacc\ are consistent.
We show that \biyacc\ is capable of facilitating many tasks such as \citeauthor{pombrio2014resugaring}'s ``resugaring'', language evolution, and refactoring.
\end{abstract}

\category{D.3.2}{Programming Languages}{Language Classifications}[Specialized Application Languages]

% general terms are not compulsory anymore,
% you may leave them out
%\terms
%term1, term2

\keywords
parsing, reflective printing, bidirectional transformations, domain-specific languages

% put each section into individual file
\section{Introduction}
\label{sec:introduction}
\input{introduction}

\section{A first look at \biyacc}
\label{sec:highlevel_intro}
\input{overview}

\section{Foundation for \BiYacc: putback-based bidirectional transformations}
\label{sec:bigul}
\input{bigul}

\section{Implementation of \biyacc}
\label{sec:translation}
\input{translation}

\section{Case study}
\label{sec:tiger}
\input{tiger}


\section{Related work}
\label{sec:related_work}
\input{relatedwork}


\section{Conclusion and future work}
\label{sec:discussion}
\input{discussion}


% \appendix
% \section{Appendix Title}
% This is the text of the appendix, if you need one.

\doubleblind{}{\acks
This work was partially supported by the Japan Society for the Promotion of Science (JSPS) Grant-in-Aid for Scientific Research~(A) No. 25240009.}

% We recommend abbrvnat bibliography style.

%\newpage

\bibliographystyle{abbrvnat}

% The bibliography should be embedded for final submission.

% \begin{thebibliography}{}
% \softraggedright

% \bibitem[Smith et~al.(2009)Smith, Jones]{smith02}
% P. Q. Smith, and X. Y. Jones. ...reference text...

% \end{thebibliography}
\bibliography{biyacc}{}


\end{document}
