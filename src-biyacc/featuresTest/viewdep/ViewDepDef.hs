{-# Language TemplateHaskell, TypeFamilies #-}

module ViewDepDef where

import Generics.BiGUL
import Generics.BiGUL.TH
import GHC.Generics

data Arith = Add Arith Arith
           | Sub Arith Arith
           | Mul Arith Arith
           | Mul2 Arith Arith Arith Arith
           | Div Arith Arith
           | Num Int
           | Var String
  deriving (Show, Eq, Read)

data Expon = Expon String BiInt String BiInt String
           | ExNull
  deriving (Show, Eq, Read)
type BiInt = Int

deriveBiGULGeneric ''Arith

deriveBiGULGeneric ''Expon
