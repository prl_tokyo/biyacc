{-# Language TemplateHaskell, TypeFamilies #-}

import Generics.BiGUL
import Generics.BiGUL.TH
import Generics.BiGUL.Interpreter
import GHC.Generics
import ViewDepDef


-- Mul2 x y a b   |   id x == y, id a == b
--   +> '((' (x +> Expon) '^2' ';'  (y +> Expon) '^2' '))' ;

testDep :: BiGUL Expon Arith
testDep = Case
  [$(normal [|  \(Expon _ _ _ _ _) (Mul2 x y a b) -> and [x == y, a == b] |]
            [p| Expon _ _ _ _ _ |] ) $
      $(rearrV [| \(Mul2 x y a b) -> (((),(x,a)), (y,b)) |]) $
        Dep (applyT2 id id) $
          $(update  [p| Expon _ x _ a _  |]
                    [p| ((), (x,a)) |]
                    [d| x = arithInt; a = arithInt |])
  ,$(adaptiveSV [p| _ |] [p| Mul2 _ _ _ _ |]) (\ _ _ -> Expon " " 0 ";" 0 " " )
  ]

arithInt :: BiGUL Int Arith
arithInt = $(update [p| i |] [p| Num i |] [d| i = Replace |])

applyT2 f g ((),(a,b)) = (f a , g b)

applyT3 f g h ((),(a,b,c)) = (f a , g b, h c)

testDep2 :: BiGUL (Maybe Int) (Maybe Int)
testDep2 = Case
  [$(normal [|  \(Just _) (Just _) -> True |] [p| Just _  |]) $
     $(update [p| Just x |] [p| Just x |] [d| x = Replace |])
  ]
