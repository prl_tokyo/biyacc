{-# Language TemplateHaskell, QuasiQuotes, FlexibleContexts #-}

import Text.Peggy

[peggy|

-- top :: Double = expr !.

expr :: CExpr
  = expr "+" term { CAdd $1 $2 }
  / term {ETerm $1}
  / "if" expr "then" expr "else" expr {Cond0 $1 $2 $3}
  / "if" expr "then" expr {Cond1 $1 $2}

term :: Term
  = term "*" fact { CMul $1 $2 }
  / fact {TFactor $1}

fact :: Factor
  = "(" expr ")" {FExpr $1 }
  / number {CInt $1}

number ::: Int
  = [1-9] [0-9]* { read ($1 : $2) }

|]


data CExpr = CAdd CExpr Term
      | ETerm Term
      | Cond0 CExpr CExpr CExpr
      | Cond1 CExpr CExpr
  deriving Show

data Term = CMul Term Factor
      | TFactor Factor
  deriving Show

data Factor = CInt Int
            | FExpr CExpr
  deriving Show


-- main :: IO ()
test1 = either (error . show) (putStrLn . show) (parseString expr "<stdin>" "1+2*3")

test2 = either (error . show) (putStrLn . show) (parseString expr "" "if 1 then 2")

test3 = either (error . show) (putStrLn . show) (parseString expr "" "if 1 then 2 else 3")

test4 = either (error . show) (putStrLn . show) (parseString expr "" "if 1 then if 22 then 33 else 4")

-- Cond1 (ETerm (TFactor (CInt 1)))
--       (Cond0 (ETerm (TFactor (CInt 22)))
--              (ETerm (TFactor (CInt 33)))
--              (ETerm (TFactor (CInt 4))))