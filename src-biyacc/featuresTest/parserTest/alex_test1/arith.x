{
module ArithLexter (arithLexer, testArithLexer) where
import Text.Show.Pretty
}

%wrapper "posn"

$digit = 0-9
$upper = A-Z
$lower = a-z
$alpha = [$lower $upper]
$alphaNum = [$alpha $digit]
@var = $lower [$upper $lower $digit]*
@intLit = $digit
-- @wideNames = @var | [\( \) \$]

tokens :-
  $white+   {\p s -> ATokenSpaces p s}
  "--"[.]*  {\p s -> ATokenCommentSingle p s}
  "{-" (([.\n] # '-') | (\- ([.\n] #  \})+) )* ("--}" | "-}") {\p s -> ATokenComment p s}
  "(" / $white+     {\p s -> (ATokenLParen  p "")}
  ")" / $white+     {\p s -> (ATokenRParen  p "")}
  "+"     {\p s -> (ATokenAdd  p "")}
  "-"     {\p s -> (ATokenSub  p "")}
  "*"     {\p s -> (ATokenMul  p "")}
  "/"     {\p s -> (ATokenDiv  p "")}

  @intLit {\p s -> ATokenIntLit p ((read s :: Int),"") }
  @var    {\p s -> ATokenVar p (s, "")}


{

data ArithToken =
    ATokenStrLit  AlexPosn (String, String)
  | ATokenIntLit  AlexPosn (Int, String)
  | ATokenVar     AlexPosn (String, String)
  | ATokenLParen  AlexPosn String
  | ATokenRParen  AlexPosn String
  | ATokenAdd  AlexPosn String
  | ATokenSub  AlexPosn String
  | ATokenMul  AlexPosn String
  | ATokenDiv  AlexPosn String
  | ATokenSpaces  AlexPosn String
  | ATokenComment AlexPosn String
  | ATokenCommentSingle AlexPosn String
  | ATokenWideNames AlexPosn (String, String)
  deriving (Show, Eq)

testArithLexer = do
  s <- readFile "input.txt"
  pPrint (mergeSpaces $ alexScanTokens s)

arithLexer = alexScanTokens

mergeSpaces :: [ArithToken] -> [ArithToken]
mergeSpaces (a:b:tokens) =
  if isSpace b
    then (addSpace a b) : mergeSpaces tokens
    else a : mergeSpaces (b:tokens)
mergeSpaces [] = []
mergeSpaces [x] = [x]

isSpace :: ArithToken -> Bool
isSpace ATokenSpaces{} = True
isSpace ATokenComment{} = True
isSpace ATokenCommentSingle{} = True
isSpace _ = False

addSpace :: ArithToken -> ArithToken -> ArithToken
addSpace (ATokenStrLit p (str, ws0)) tws = ATokenStrLit p (str, ws0 ++ extractWs tws)
addSpace (ATokenIntLit p (i, ws0))   tws = ATokenIntLit p (i, ws0 ++ extractWs tws)
addSpace (ATokenVar p    (var, ws0)) tws = ATokenVar p    (var, ws0 ++ extractWs tws)
addSpace (ATokenLParen  p ws0) tws = ATokenLParen  p (ws0 ++ extractWs tws)
addSpace (ATokenRParen  p ws0) tws = ATokenRParen  p (ws0 ++ extractWs tws)
addSpace (ATokenAdd  p ws0) tws = ATokenAdd  p (ws0 ++ extractWs tws)
addSpace (ATokenSub  p ws0) tws = ATokenSub  p (ws0 ++ extractWs tws)
addSpace (ATokenMul  p ws0) tws = ATokenMul  p (ws0 ++ extractWs tws)
addSpace (ATokenDiv  p ws0) tws = ATokenDiv  p (ws0 ++ extractWs tws)
addSpace (ATokenSpaces p ws0) tws   = ATokenSpaces p (ws0 ++ extractWs tws)
addSpace (ATokenComment p ws0) tws  = ATokenComment p (ws0 ++ extractWs tws)
addSpace (ATokenCommentSingle p ws0) tws = ATokenCommentSingle p (ws0 ++ extractWs tws)
addSpace (ATokenWideNames p (str, ws0)) tws = ATokenWideNames p (str, ws0 ++ extractWs tws)


extractWs :: ArithToken -> String
extractWs (ATokenSpaces _ s) = s
extractWs (ATokenComment _ s) = s
extractWs (ATokenCommentSingle _ s) = s
extractWs _ = error "not a whitespace token."

}


