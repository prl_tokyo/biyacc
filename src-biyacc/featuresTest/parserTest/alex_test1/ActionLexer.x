{
module ActionLexer (actionLexer) where
}

%wrapper "posn"

$digit = 0-9
$upper = A-Z
$lower = a-z
$alpha = [$lower $upper]
$alphaNum = [$alpha $digit]
@identifier = ($lower){1}  [$alphaNum \_]*
@constructor = ($upper){1} (@identifier)*
@strLit = \" ([.\n] # \")* \" | \' ([.\n] # \')* \'
@intLit = $digit
@floatLit = $digit+ \. $digit+
@boolLit = "True" | "False"

tokens :-


  
  $white+ ;
  "--"[.]*  {\p s -> ATokenCommentSingle p s}

  "{-" (([.\n] # '-') | (\- ([.\n] #  \})+) )* ("--}" | "-}") {\p s -> ATokenComment p s}
  @strLit {\p s -> ATokenStrLit p s}
  @intLit {\p s -> ATokenIntLit p (read s :: Int) }
  @floatLit {\p s -> ATokenFloatLit p (read s :: Double)}
  @boolLit {\p s -> ATokenBoolLit p (read s :: Bool)}
  @constructor     {\p s -> ATokenConstructor p s}
  @identifier      {\p s -> ATokenIdentifier p s}
  "("     {\p s -> ATokenLParen  p}
  ")"     {\p s -> ATokenRParen  p}
  "["     {\p s -> ATokenLBrack  p}
  "]"     {\p s -> ATokenRBrack  p}
  "_"     {\p s -> ATokenWildCard  p}
  ":"     {\p s -> ATokenListCons  p}
  "+>"    {\p s -> ATokenEmbed  p}
  ";"     {\p s -> ATokenSemi  p}
  "@"     {\p s -> ATokenAs  p}
  "->"    {\p s -> ATokenTo  p}
  ","     {\p s -> ATokenComma  p}
  "|"     {\p s -> ATokenOr  p}



{

data ActionToken =
    ATokenStrLit AlexPosn String
  | ATokenIntLit AlexPosn Int
  | ATokenFloatLit AlexPosn Double
  | ATokenBoolLit AlexPosn Bool
  | ATokenConstructor AlexPosn String
  | ATokenIdentifier AlexPosn String
  | ATokenLParen  AlexPosn
  | ATokenRParen  AlexPosn
  | ATokenLBrack  AlexPosn
  | ATokenRBrack  AlexPosn
  | ATokenWildCard  AlexPosn
  | ATokenListCons  AlexPosn
  | ATokenEmbed  AlexPosn
  | ATokenSemi  AlexPosn
  | ATokenAs  AlexPosn
  | ATokenTo  AlexPosn
  | ATokenComma  AlexPosn
  | ATokenOr  AlexPosn
  | ATokenComment AlexPosn String
  | ATokenCommentSingle AlexPosn String
  deriving (Show, Eq)

-- token_posn (TokenAdd p) = p
-- token_posn (TokenSub p) = p
-- token_posn (TokenMul p) = p
-- token_posn (TokenDiv p) = p
-- token_posn (Num p _) = p
-- token_posn (Var p _) = p
-- token_posn (Spaces p _) = p
-- token_posn (Comment p _) = p
-- token_posn (TokenLP p) = p
-- token_posn (TokenRP p) = p


actionLexer = do
  s <- readFile "input.txt"
  print (alexScanTokens s)
}

