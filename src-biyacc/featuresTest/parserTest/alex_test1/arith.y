{
module ActionPaser where
}

%name actionParser
%tokentype {ArithToken}
%error {parseError}

%token
  yStrLit         {ATokenStrLit _ $$}
  yIntLit         {ATokenIntLit _ $$}
  yVar            {ATokenVar _ $$}
  yLParen         {ATokenLParen  _}
  yRParen         {ATokenRParen  _}
  yAdd            {ATokenAdd  _}
  ySub            {ATokenSub  _}
  yMul            {ATokenMul  _}
  yDiv            {ATokenDiv  _}
  ySpaces         {ATokenSpaces _ $$}
  yComment        {ATokenComment _ $$}
  yCommentSingle  {ATokenCommentSingle _ $$}

%%

Expr : Expr yAdd Term {EAdd $1 $3}
     | Expr ySub Term {ESub $1 $3}
     | Term {ET $1}

Term : Term yMul Factor {EMul $1 $3}
     | Term yDiv Factor {EDiv $1 $3}
     | Factor {TF $1}

Factor : ySub Factor {Neg $2}
       | yIntLit {Num $1}
       | yStrLit {Var $1}
       | yLParen Expr yRParen {FE $2}


{
parseError :: [ArithToken] -> a
parseError x = error $ "parse error " + show x

data Expr = EAdd Expr Term
          | ESub Expr Term
          | ET Term
  deriving (Show, Eq)

data Term = EMul Term Factor
          | EDiv Term Factor
          | TF Factor
  deriving (Show, Eq)

data Factor = Neg Factor
            | Num Int
            | Var String
            | FE Expr
  deriving (Show, Eq)
}