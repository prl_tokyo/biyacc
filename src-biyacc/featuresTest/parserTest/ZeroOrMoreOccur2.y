{
module Parser where

}

%name parser
%tokentype { Token }
%error { parseError }

%token
    '+'     { TAdd $$ }
    int     { TInt $$ }
    '*'     { TMul $$ }
    ')'     { TLP $$  }
    '('     { TRP $$  }
    str     { TStr $$ }
    ';'     { TSemi $$ }
    ','     { TSeqDivider $$ }


-- test case: parser [TInt 1, TAdd, TInt 3, TSeqDivider, TInt 2, TMul , TInt 4]

%%
-- orininal: Exprs -> Expr {',' ',' Expr}
-- translated to:

Exprs : Expr ZeroOrMoreExpr0 { $1 : $2 }

ZeroOrMoreExpr0
  : {- empty -}              { [] }
  | ',' ',' Expr ZeroOrMoreExpr0 { ($1,$2,$3) : $4 }


Expr : Expr '+' Term  { CAdd $1 $2 $3 }
     | Term  { ETerm $1 }

Term : Term '*' Factor  { CMul $1 $2 $3 }
     | Factor  { TFactor $1 }

Factor : int  { CInt $1 }
       | '(' Expr ')'  { FExpr $1 $2 $3 }


{

parseError :: [Token] -> a
parseError _ = error "Parse error"

data Token =
    TAdd String
  | TInt (Int, String
  | TMul String
  | TLP  String
  | TRP  String
  | TStr String
  | TSemi String
  | TSeqDivider String
  deriving (Show, Eq)

data ExprSeq = ExprSeq [(String, String, Expr)]

data Expr = CAdd Expr String Term
      | ETerm String Term
      deriving Show

data Term = CMul Term String Factor
      | TFactor Factor
      deriving Show

data Factor = CInt (Int, String)
            | FExpr Expr
  deriving Show

data Prog = Prog [String]
  deriving Show

}
