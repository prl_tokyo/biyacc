-- '-' and '+' has different associativities
-- solved by different precendences

{
module Parser where

}

%name parser
%tokentype { Token }
%error { parseError }

%token
    '+'     { TAdd }
    '-'     { TSub }
    int     { TInt $$ }
    '*'     { TMul }
    ')'     { TLP }
    '('     { TRP }

%right '+'
%left  '-'
%right '*'

%%

Expr : Expr '+' Expr  { CAdd $1 $3 }
     | Expr '-' Expr  { CSub $1 $3 }
     | Expr '*' Expr  { CMul $1 $3 }
     | Factor  { TFactor $1 }

Factor : int  { CInt $1 }
       | '(' Expr ')'  { FExpr $2 }

{

parseError :: [Token] -> a
parseError _ = error "Parse error"

data Token = TAdd
  | TSub
  | TMul
  | TLP
  | TRP
  | TStr String
  | TInt Int
  | TSemi
  deriving (Show, Eq)

data Expr = CAdd Expr Expr
  | CSub Expr Expr
  | CMul Expr Expr
  | TFactor Factor
      deriving Show

data Factor = CInt Int
            | FExpr Expr
  deriving Show

}
