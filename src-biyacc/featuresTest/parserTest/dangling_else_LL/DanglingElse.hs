import Text.Parsec
import Text.Parsec

data Expr = Cond0 Expr Expr Expr
          | Cond1 Expr Expr
          | IntExpr Int
  deriving (Show, Eq)

renderExpr :: Expr -> String
renderExpr (Cond0 e1 e2 e3) = "(if " ++ renderExpr e1 ++ "\n  then " ++ renderExpr e2 ++ "\n  else " ++ renderExpr e3 ++ ")"
renderExpr (Cond1 e1 e2) = "(if " ++ renderExpr e1 ++ "\n  then " ++ renderExpr e2 ++ ")"
renderExpr (IntExpr i) = show i

pExp' = pExp >>= \e -> eof >> return e

pExp :: Parsec String () Expr
pExp =
  try (pIfThen) <|>
  try (pIfThenElse) <|>
  (whiteSpaces >> pInt >>= return . IntExpr)


pIfThenElse = do
  whiteSpaces
  string "if"
  whiteSpaces
  c <- pExp
  whiteSpaces
  string "then"
  whiteSpaces
  t <- pExp
  whiteSpaces
  string "else"
  whiteSpaces
  e <- pExp
  whiteSpaces
  return $ Cond0 c t e


pIfThen = do
  whiteSpaces
  string "if"
  whiteSpaces
  c <- pExp
  whiteSpaces
  string "then"
  whiteSpaces
  t <- pExp
  whiteSpaces
  return $ Cond1 c t


whiteSpaces :: Parsec String () ()
whiteSpaces = (many . choice . map (char $) $ [' ', '\t', '\n', '\r']) >> return ()

pInt :: Parsec String () Int
pInt = (many1 . choice . map (char $) $ ['0','1','2','3','4','5','6','7','8','9']) >>= return . read


-- test1 = either (error . show) (putStrLn . renderExpr) (parse pExp "" "if 1 then if 11 then 22 else 2")
-- test2 = either (error . show) (putStrLn . renderExpr) (parse pExp "" "if 1 then 2")
-- test3 = either (error . show) (putStrLn . renderExpr) (parse pExp "" "if 1 then 2 else 3")

test1 = either (error . show) (putStrLn . show) (parse pExp' "" "if 1 then if 11 then 22 else 2")
-- test2 = either (error . show) (putStrLn . show) (parse pExp "" "if 1 then 2")
-- test3 = either (error . show) (putStrLn . show) (parse pExp "" "if 1 then 2 else 3")