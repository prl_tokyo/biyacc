{
module Parser where

}

%name parser
%tokentype { Token }
%error { parseError }

%token
    '+'     { TAdd }
    int     { TInt $$ }
    '*'     { TMul }
    ')'     { TLP }
    '('     { TRP }
    str     { TStr $$}
    ';'     { TSemi }
    ','     { TSeqDivider }


%%
Exprs : Expr { [$1] }
      | Expr ZeroOrMore0 { $1 : $2 }

ZeroOrMore0 : ',' Expr { [$2] }
            | ',' Expr ZeroOrMore0 { $2 : $3 }


Expr : Expr '+' Term  { CAdd $1 $3 }
     | Term  { ETerm $1 }

Term : Term '*' Factor  { CMul $1 $3 }
     | Factor  { TFactor $1 }

Factor : int  { CInt $1 }
       | '(' Expr ')'  { FExpr $2 }


prog : stmts {Prog $1}

stmts : stmts ';' stmt          { $3 : $1 }
      | stmts ';'               { $1 }
      | stmt            { [$1] }
      | {- empty -}     { [] }

stmt : str {$1}

{

parseError :: [Token] -> a
parseError _ = error "Parse error"

data Token = TAdd
  | TInt Int
  | TMul
  | TLP
  | TRP
  | TStr String
  | TSemi
  | TSeqDivider
  deriving (Show, Eq)

data ExprSeq = ExprSeq [Expr]

data Expr = CAdd Expr Term
      | ETerm Term
      deriving Show

data Term = CMul Term Factor
      | TFactor Factor
      deriving Show

data Factor = CInt Int
            | FExpr Expr
  deriving Show

data Prog = Prog [String]
  deriving Show

}
