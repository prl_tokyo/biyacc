import Data.Map as Map
import Control.Monad.Reader
import Control.Monad.State
import Debug.Trace

data Arith =
    Add Arith Arith
  | Mul Arith Arith
  | Num Int
  | ATrue
  | AFalse
  | Cond Arith Arith Arith
  deriving (Show, Eq)

class ConsName a where
  consName :: a -> String

instance ConsName Arith where
  consName (Add _ _) = "Add"
  consName (Mul _ _) = "Mul"
  consName (Num _) = "Num"
  consName (Cond _ _ _) = "Cond"
  consName (ATrue) = "ATrue"
  consName (AFalse) = "AFalse"

type Priority = Int
type PriorityEnv = Map String Priority

env = fromList [("Add", 5), ("Mul", 6)]

-- version one
lookupPri :: String -> Priority -> PriorityEnv -> Priority
lookupPri name parentPri env =
  case Map.lookup name env of
    Nothing -> parentPri
    Just pri -> pri

parensIf thisPri parentPri str =
  if thisPri < parentPri then "(" ++ str ++ ")" else str


pprint2 :: Arith -> ReaderT PriorityEnv (State Priority) String
pprint2 a@(Add l r) = do
  parentPri <- get
  thisPri <- asks (lookupPri (consName a) parentPri)
  put thisPri
  llog <- pprint2 l
  put thisPri
  rlog <- pprint2 r
  let logs = llog ++ " + " ++ rlog
  return $ parensIf thisPri parentPri logs

pprint2 a@(Mul l r) = do
  parentPri <- get
  thisPri <- asks (lookupPri (consName a) parentPri)
  put thisPri
  llog <- pprint2 l
  put thisPri
  rlog <- pprint2 r
  let logs = llog ++ " * " ++ rlog
  return $ parensIf thisPri parentPri logs

pprint2 a@(Num i) = do
  parentPri <- get
  thisPri <- asks (lookupPri (consName a) parentPri)
  let logs = show i
  return $ parensIf thisPri parentPri logs

pprint2 a@(Cond b0 b1 b2) = do
  parentPri <- get
  thisPri <- asks (lookupPri (consName a) parentPri)
  log0 <- pprint2 b0
  log1 <- pprint2 b1
  log2 <- pprint2 b2
  let logs = "if " ++ log0 ++ " then " ++ log1 ++ " else " ++ log2
  return $ parensIf thisPri parentPri logs

pprint2 a@(ATrue) = do
  parentPri <- get
  thisPri <- asks (lookupPri (consName a) parentPri)
  let logs = "True"
  return $ parensIf thisPri parentPri logs

pprint2 a@(AFalse) = do
  parentPri <- get
  thisPri <- asks (lookupPri (consName a) parentPri)
  let logs = "False"
  return $ parensIf thisPri parentPri logs



printer2 t = evalState (runReaderT (pprint2 t) env) 0

data1 = Add (Mul (Add (Num 1) (Num 2))
                 (Add (Num 3) (Mul (Num 4) (Num 5))))
            (Num 0)

data2 = Mul (Num 0) (Cond (ATrue) (Mul (Num 1) (Num 2)) (Add (Num 3) (Num 4)) )

