{-# Language TypeFamilies #-}

import Data.Map as Map (Map , lookup, fromList)
import Debug.Trace

import Generics.BiGUL hiding (Expr, Pat, Var, Direction)
import Generics.BiGUL.Interpreter
import Generics.BiGUL.TH
import GHC.Generics

import Data.Maybe (fromMaybe)

data CST = CAdd CST CST
         | CMul CST CST
         | CParen CST
         | CNum Int
         | CToCond CCond
         | CSTNull
  deriving (Show, Eq)

data CCond = CCondIf String CST CST
           | CCondNull
  deriving (Show, Eq)

data Arith =
    Add Arith Arith
  | Mul Arith Arith
  | Num Int
  | Cond String Arith Arith
  deriving (Show, Eq)


deriveBiGULGeneric ''CST
deriveBiGULGeneric ''CCond
deriveBiGULGeneric ''Arith

class ConsName a where
  consName :: a -> String

instance ConsName Arith where
  consName (Add _ _) = "Add"
  consName (Mul _ _) = "Mul"
  consName (Num _) = "Num"
  consName (Cond _ _ _) = "Cond"

instance ConsName CST where
  consName (CAdd _ _) = "CAdd"
  consName (CMul _ _) = "CMul"
  consName (CNum _) = "CNum"
  consName (CParen _) = "CParen"
  consName (CToCond _ ) = "CToCond"

instance ConsName CCond where
  consName (CCondIf _ _ _) = "CCondIf"


type ProdRuleCons = String
type Priority = Int
data Assoc = LeftAssoc | RightAssoc | NonAssoc deriving (Show, Eq)
data Branch = LBranch | RBranch | NoBranch deriving (Show, Eq)

type DisAmb = (Maybe Priority, Maybe Assoc)

type DisAmbEnv = Map String DisAmb

-- type Paths = [String]
-- type PathsViaBracket = Map String Paths

type RuntimeEnv = (DisAmbEnv, DisAmb, Branch) -- (unchanged env, parent's attribute)

disAmbEnv = fromList [("CAdd", (Just (5 :: Int), Just LeftAssoc))
                     ,("CMul", (Just 6, Just LeftAssoc))]

lookupDisAmb :: DisAmb -> String -> DisAmbEnv -> DisAmb
lookupDisAmb parentDisAmb@(Just pPri, pAssoc) name env =
  case Map.lookup name env of
    Nothing -> parentDisAmb
    Just (ePri, eAssoc) ->
      let pri'  = fromMaybe pPri ePri
          assoc' = eAssoc
      in  (Just pri', assoc')


needParen :: DisAmb -> DisAmb -> Branch -> Bool
needParen (pPri,pAssoc) (myPri,myAssoc) myBranch =
  if myPri < pPri then True
    else if myAssoc /= Nothing && pAssoc /= Nothing
         then let Just myAssoc' = myAssoc
                  Just pAssoc'  = pAssoc
              in  if myPri == pPri && myBranch /= NoBranch && pAssoc' == myAssoc' && not (isSameBranch myBranch myAssoc') -- not exhausted?
                    then True
                    else False
          else False
  where
    isSameBranch :: Branch -> Assoc -> Bool
    isSameBranch LBranch LeftAssoc = True
    isSameBranch RBranch RightAssoc = True
    isSameBranch _ _ = False


bigulCSTArith :: RuntimeEnv -> BiGUL CST Arith
bigulCSTArith (disAmbEnv, parentDisAmb, myBranch) =

  Case [$(normalSV [p| CAdd _ _ |] [p| Add _ _ |]
                   [p| CAdd _ _ |])
          $(update [p| CAdd l r |] [p| Add l r |]
                   [d|  l = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CAdd" disAmbEnv, LBranch) ;
                        r = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CAdd" disAmbEnv, RBranch) |])
       ,$(normalSV [p| CMul _ _ |] [p| Mul _ _ |]
                   [p| CMul _ _ |])
          $(update [p| CMul l r |] [p| Mul l r |]
                   [d|  l = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CMul" disAmbEnv, LBranch) ;
                        r = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CMul" disAmbEnv, RBranch) |])
       ,$(normalSV [p| CNum _ |] [p| Num _|]
                   [p| CNum _ |])
          $(update [p| CNum i |] [p| Num i |]
                   [d| i = Replace |])

       ,$(normalSV [p| CToCond _ |] [p| Cond _ _ _ |]
                   [p| CToCond _ |])
          $(update [p| CToCond c |] [p| c |]
                   [d| c = bigulCCondArith (disAmbEnv, lookupDisAmb parentDisAmb "CToCond" disAmbEnv, NoBranch) |])

       ,$(normalSV [p| CParen _ |] [p| _ |]
                   [p| CParen _ |])
          $(update [p| CParen p |] [p| p |]
                   [d| p = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CParen" disAmbEnv, NoBranch)  |])


       ,$(adaptiveSV [p| _ |] [p| Add _ _ |])
          (\ s v -> if needParen parentDisAmb (lookupDisAmb parentDisAmb "CAdd" disAmbEnv) myBranch
                    then CParen (CAdd CSTNull CSTNull)
                    else CAdd CSTNull CSTNull)
       ,$(adaptiveSV [p| _ |] [p| Mul _ _ |])
          (\ s v -> if needParen parentDisAmb (lookupDisAmb parentDisAmb "CMul" disAmbEnv) myBranch
                    then CParen (CMul CSTNull CSTNull)
                    else CMul CSTNull CSTNull)

       ,$(adaptiveSV [p| _ |] [p| Num _ |])
          (\ _ _ ->  CNum 0 )

       ,$(adaptiveSV [p| _ |] [p| Cond _ _ _ |])
          (\ _ _ ->  CToCond CCondNull )

          ]

bigulCCondArith :: RuntimeEnv -> BiGUL CCond Arith
bigulCCondArith (disAmbEnv, parentDisAmb, myBranch) =
  Case [$(normalSV [p| CCondIf _ _ _ |] [p| Cond _ _ _ |]
                   [p| CCondIf _ _ _ |])
          $(update [p| CCondIf b0 b1 b2 |] [p| Cond b0 b1 b2 |]
                   [d| b0 = Replace ;
                       b1 = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CCondIf" disAmbEnv, NoBranch) ;
                       b2 = bigulCSTArith (disAmbEnv, lookupDisAmb parentDisAmb "CCondIf" disAmbEnv, NoBranch) |])
       ,$(adaptiveSV [p| _ |] [p| Cond _ _ _ |])
          (\ _ _ ->  CCondIf undefined CSTNull CSTNull)]


ctree1 = CAdd (CNum 0) (CNum 1)
ctree2 = CMul (CParen (CAdd (CNum 0) (CNum 2))) (CNum 9)

atree1 = Add (Num 0) (Num 1)
atree2 = Mul (Add (Num 0) (Num 2)) (Num 9)

atree3 = Add (Mul (Add (Num 1) (Num 2))
                  (Add (Num 3) (Mul (Num 4) (Num 5))))
                  (Num 0)
atree4 = Mul (Num 0) (Cond ("True") (Mul (Num 1) (Num 2)) (Add (Num 3) (Num 4)) )

atree5 = Add (Num 0) (Add (Num 10) (Num 11))

test1 = get (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc), NoBranch)) ctree1

test2 = get (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc),NoBranch)) ctree2

test3  = put (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc),NoBranch)) CSTNull atree2
test3' = putTrace (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc),NoBranch)) CSTNull atree2


test4 = put (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc),NoBranch)) CSTNull atree3

test5 = put (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc),NoBranch)) CSTNull atree4

test6 = put (bigulCSTArith (disAmbEnv,(Just 0, Just NonAssoc),NoBranch)) CSTNull atree5



