import Data.Map as Map
import Control.Monad.Reader
import Control.Monad.State
import Debug.Trace

data Arith =
    Add Arith Arith
  | Mul Arith Arith
  | Num Int
  deriving (Show, Eq)

class ConsName a where
  consName :: a -> String

instance ConsName Arith where
  consName (Add _ _) = "Add"
  consName (Mul _ _) = "Mul"
  consName (Num _) = "Num"

type Priority = Int

-- the second argument is the priority of the parent
type PriorityEnv = (Map String Priority, Priority)

env = fromList [("Add", 5), ("Mul", 6)]

-- version one
lookupPri :: String -> Priority -> Map String Priority -> Priority
lookupPri name parentPri env =
  case Map.lookup name env of
    Nothing -> parentPri
    Just pri -> pri

parensIf thisPri parentPri str =
  if thisPri < parentPri then "(" ++ str ++ ")" else str

setPriority :: Priority -> PriorityEnv -> PriorityEnv
setPriority newPri (env, pri) = (env, newPri)

askMe :: String -> Reader PriorityEnv (Priority, Priority)
askMe name = do
  parentPri <- asks snd
  thisPri   <- asks (lookupPri name parentPri . fst)
  return (thisPri, parentPri)

pprint :: Arith -> Reader PriorityEnv String
pprint a@(Add l r) = do
  (thisPri, parentPri) <- askMe (consName a)
  llog <- local (setPriority thisPri) (pprint l)
  rlog <- local (setPriority thisPri) (pprint r)
  let logs = llog ++ " + " ++ rlog
  return $ parensIf thisPri parentPri logs

pprint a@(Mul l r) = do
  (thisPri, parentPri) <- askMe (consName a)
  llog <- local (setPriority thisPri) (pprint l)
  rlog <- local (setPriority thisPri) (pprint r)
  let logs = llog ++ " * " ++ rlog
  return $ parensIf thisPri parentPri logs

pprint a@(Num i) = do
  (thisPri, parentPri) <- askMe (consName a)
  let logs = show i
  return $ parensIf thisPri parentPri logs


printer t = runReader (pprint t) (env,0)

data1 = Add (Mul (Add (Num 1) (Num 2))
                 (Add (Num 3) (Mul (Num 4) (Num 5))))
            (Num 0)
------------