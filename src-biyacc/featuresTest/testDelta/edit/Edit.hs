{-# Language TemplateHaskell, DeriveDataTypeable, TypeFamilies, GADTs #-}
{-# Language ScopedTypeVariables, CPP #-}

module Edit where

import GHC.Generics
import Data.Generics
import Data.Dynamic
import Data.Maybe (fromJust)
import Generics.BiGUL
import Generics.BiGUL.TH
import Generics.BiGUL.Interpreter

import Text.Show.Pretty
import Debug.Trace

-- data One where
--   One :: One
--   InjOne :: forall a. a -> One

-- deriveBiGULGeneric ''One

emb :: Eq v => (s -> v) -> (s -> v -> s) -> BiGUL s v
emb g p = Case
  [ $(normal [| \s v -> g s == v |] [p| _ |]) (Skip g)
  , $(adaptive [| \s v -> True |]) p]


idLens :: Show ds => BiGUL (ds, ()) (ds, ())
idLens =
  Case  [ $(normal [| \(_, ()) (_, ()) -> True |] [| \(_, ()) -> True |] ) $
            $(update [p| (dx, ()) |] [p| (dx, ()) |]
                     [d| dx = Replace; |])
        ]
-- K = {(x,(),x) | x elemOf X}.
-- C = ()

infixr 5 `compLens`

compLens :: (Show dx, Show dy, Show dz, Eq dx, Eq dy, Eq dz, Eq ck, Eq cl) =>
  BiGUL (dx, ck) (dy, ck) ->
  BiGUL (dy, cl) (dz, cl) ->
  BiGUL (dx, (ck,cl)) (dz, (ck,cl))
compLens k l = emb g p
  where
    g (dx, (ck,cl)) = case get k (dx, ck) of
      Just (dy, ck')   ->
        case get l (dy, cl) of
          Just (dz, cl')   -> (dz, (ck',cl'))

    p (dx0, (ck0,cl0)) (dz, (ck,cl)) = case put l (getDY0, cl0) (dz, cl) of
      Just (dy, cl')   ->
        case put k (dx0, ck0) (dy, ck) of
          Just (dx, ck')   -> (dx, (ck',cl'))

      where getDY0 = fst $ fromJust $ get k (dx0,ck0)
-- C = k.C * l.c
-- K = ...


-- termLens :: BiGUL (One dx, ()) (One, ())
-- termLens =
--   Case  [ $(normal [| \(InjOne x, ()) (One, ()) -> True |] [| \(InjOne x, ()) -> True |] ) $
--             $(update [p| (InjOne x, ()) |] [p| (x, ()) |]
--                      [d| x = Skip (const One) |])

--         , $(normal [| \(One, ()) (One, ()) -> True |] [| \(One, ()) -> True |] ) $
--             $(update [p| (One, ()) |] [p| (One, ()) |]
--                      [d| |])
--         ]


-- ttt :: (Show dx, Eq dx) => BiGUL (dx, ((),())) (dx, ((),()))
-- ttt = idLens `compLens` idLens

isoLens :: (Eq dx, Eq dy) => (dx -> dy) -> (dy -> dx) -> BiGUL (dx, ()) (dy, ())
isoLens h hinv = emb (h *** id) (\_ v -> (hinv *** id) v)

(***) f g (a,b) = (f a , g b)


{------ false
data CExpr e t where
  CAdd  :: e -> t -> CExpr e t
  TTerm :: t -> CExpr e t

data CTerm t f where
  CMul    :: t -> f -> CTerm t f
  TFactor :: f -> CTerm t f

data CFactor e where
  CNum  :: Int -> CFactor Int
  Paren :: e   -> CFactor e


ttt1 = CAdd (TTerm (TFactor (CNum 1))) (TFactor (CNum 2))
ttt2 = CAdd (CNum 1) (TFactor (CNum 2))

--------}

{--------- no use
data CExpr e t f where
  CAdd  :: CExpr e t f -> CTerm t f e -> CExpr e t f
  TTerm :: CTerm t f e -> CExpr e t f

data CTerm t f e where
  CMul    :: CTerm t f e   -> CFactor e t f-> CTerm t f e
  TFactor :: CFactor e t f -> CTerm t f e

data CFactor e t f where
  CNum  :: Int         -> CFactor e t Int
  Paren :: CExpr e t f -> CFactor e t f

ttt1 = CAdd (TTerm (TFactor (CNum 1))) (TFactor (CNum 2))
-- ttt2 = CAdd (CNum 1) (TFactor (CNum 2))
ttt3 = TTerm (TFactor (Paren (CAdd (TTerm (TFactor (CNum 1))) (TFactor (CNum 2)))))

-}


