-- classification of code refactor actions
-- substitution + insert (new things)
-- substitution + delete
-- substitution + move   (should keep original things)

{-# Language TemplateHaskell, DeriveDataTypeable, TypeFamilies, StandaloneDeriving #-}
module Trace where

import Helper

import Data.List as List
import Data.Map as Map (Map, empty, delete)
import GHC.Generics
import Data.Generics
import Data.Dynamic
import Data.Foldable.Deriving
import Generics.BiGUL
import Generics.BiGUL.TH
import Generics.BiGUL.Interpreter

import Text.Show.Pretty

-- get s => Just v
-- collect leaves (perhaps wrapped in a container type Primitive) of v in pre-order [vi]
-- collect leaves (perhaps wrapped in a container type Primitive) of v' in pre-order [vi']
-- give the relation between [vi] and [vi'], we get [(vi', vi)] in pre-order of vi'.
-- pre-order means the order they are going to be used in put.
-- e.g. (1 + 1) + (1 + 1) has many similar (identical if does not consider position) leaves,
-- and it needs an order...

-- then we have relations between s and v', [(vi',si)] in pre-order.


---------------------
-- bigul

addInt :: (Show s, Show v) => BiGUL s v -> BiGUL (Int, s) (Int, v)
addInt b = Skip id `Prod` b


bigulArithExpr :: (Path, Path) -> RecoverEnv -> BiGUL (DeltaEnv, CExpr) Arith
bigulArithExpr (sPath, vPath) rcvEnv =
  Case [$(adaptive [| \ (dtEnv,_) _ -> needReShape (sPath, vPath) dtEnv |])
          (\(dtEnv, _) _ -> (Map.delete (vPath,sPath) dtEnv, reShape ExprNull vPath rcvEnv))

       ,$(normal [| \(_ , (CAdd _ _ _)) (AAdd _ _) -> True |]
                 [p| (_ , (CAdd _ _ _)) |]) $
           $(rearrS [| \(dtEnv, CAdd x _ y) -> ((dtEnv, x), (dtEnv, y)) |]) $
              $(update   [p| (xEnv, yEnv) |] [p| (AAdd xEnv yEnv) |]
                         [d| xEnv = bigulArithExpr (sPath ++ [("CAdd", 0)], vPath ++ [("AAdd", 0)]) rcvEnv ;
                             yEnv = bigulArithTerm (sPath ++ [("CAdd", 2)], vPath ++ [("AAdd", 1)]) rcvEnv ; |])

       -- ,$(normal [| \(ToTerm _) (ASub (ANum 0) _) -> True |]
       --           [p| (ToTerm _) |]) $
       --    $(update [p| ToTerm e |] [p| e |]
       --               [d| e = bigulArithTerm (sPath ++ [("ToTerm",0)], vPath) dtEnv rcvEnv ; |])

       -- ,$(normal [| \(CSub _ _ _) (ASub x y) -> True |]
       --           [p| (CSub _ _ _) |]) $
       --    $(update [p| CSub x _ y |] [p| (ASub x y) |]
       --               [d| x = bigulArithExpr (sPath ++ [("CSub" ,0)], vPath ++ [("ASub", 0)]) dtEnv rcvEnv ;
       --                   y = bigulArithTerm (sPath ++ [("CSub" ,2)], vPath ++ [("ASub", 1)]) dtEnv rcvEnv ; |])

       ,$(normal [| \(_, (ToTerm _)) _ -> True |]
                 [p| (_, ToTerm _) |]) $
          $(rearrS [| \(dtEnv, ToTerm e) -> (dtEnv, e) |]) $
            $(update   [p| eEnv |] [p| eEnv |]
                       [d| eEnv = bigulArithTerm (sPath ++ [("ToTerm" , 0)], vPath) rcvEnv ; |])

       ,$(adaptive [| \_ (AAdd _ _) -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv, CAdd ExprNull " + " TermNull))
       -- ,$(adaptive [| \_ (ASub (ANum 0) _) -> True |])
       --    (\ _ _ ->  ToTerm TermNull)
       -- ,$(adaptive [| \_ (ASub x y) -> True |])
       --    (\ _ _ ->  CSub ExprNull " - " TermNull)
       ,$(adaptive [| \_ _ -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv , ToTerm TermNull))]


bigulArithTerm :: (Path, Path) -> RecoverEnv -> BiGUL (DeltaEnv, CTerm) Arith
bigulArithTerm (sPath, vPath) rcvEnv =
  Case [$(adaptive [| \ (dtEnv, _) _ -> needReShape (sPath, vPath) dtEnv |])
          (\(dtEnv , _) _ -> (Map.delete (vPath,sPath) dtEnv, reShape TermNull vPath rcvEnv))

       -- ,$(normal [| \(CMul _ _ _) (AMul x y) -> True |]
       --           [p| (CMul _ _ _) |]) $
       --    $(update [p| CMul x _ y |] [p| (AMul x y) |]
       --               [d| x = bigulArithTerm   (sPath ++ [("CMul", 0)], vPath ++ [("AMul", 0)]) rcvEnv;
       --                   y = bigulArithFactor (sPath ++ [("CMul", 2)], vPath ++ [("AMul", 1)]) rcvEnv; |])

       -- ,$(normal [| \(CDiv _ _ _) (ADiv x y) -> True |]
       --           [p| (CDiv _ _ _) |]) $
       --    $(update [p| CDiv x _ y |] [p| (ADiv x y) |]
       --               [d| x = bigulArithTerm   (sPath ++ [("CDiv", 0)], vPath ++ [("ADiv", 0)]) rcvEnv;
       --                   y = bigulArithFactor (sPath ++ [("CDiv", 2)], vPath ++ [("ADiv", 1)]) rcvEnv ; |])

       ,$(normal [| \(_ , ToFactor _) _ -> True |]
                 [p| (_ , ToFactor _) |]) $
          $(rearrS [| \(dtEnv, ToTerm e) -> (dtEnv, e) |]) $
            $(update   [p| eEnv |] [p| eEnv |]
                       [d| eEnv = bigulArithFactor (sPath ++ [("ToFactor" , 0)], vPath) rcvEnv ; |])
       -- ,$(adaptive [| \_ (AMul x y) -> True |])
       --    (\ _ _ ->  CMul TermNull " * " FactorNull)
       -- ,$(adaptive [| \_ (ADiv x y) -> True |])
       --    (\ _ _ ->  CDiv TermNull " / " FactorNull)
       ,$(adaptive [| \_ _ -> True |])
          (\ (dtEnv , _) _ -> (dtEnv , ToFactor FactorNull))]




bigulArithFactor :: (Path, Path) -> RecoverEnv -> BiGUL (DeltaEnv, CFactor) Arith
bigulArithFactor (sPath, vPath) rcvEnv =
  Case [$(adaptive [| \ (dtEnv, _) _ -> needReShape (sPath, vPath) dtEnv |])
          (\(dtEnv, _) _ -> (Map.delete (vPath,sPath) dtEnv, reShape FactorNull vPath rcvEnv))
          -- the value looked up from rcvEnv should be changed along with the function execution.
          -- previously we have AAdd .. ---> ToFactor (CParen " ( " ts00 " ) " ) of type Term.
          -- but now, we go into another function, which gives a default term FactorNull, which
          -- if of type Factor... type diffs, conversion fails.
          -- cause: condition s /= reShape FactorNull vPath rcvEnv   is not correct.

       -- ,$(normal [| \(CNeg _ _) (ASub (ANum 0) y) -> True |]
       --           [p| (CNeg _ _) |]) $
       --    $(update [p| CNeg _ y |] [p| (ASub (ANum 0) y) |]
       --               [d| y = bigulArithFactor (sPath ++ [("CNeg", 1)], vPath ++ [("ASub", 1)]) rcvEnv ; |])

       ,$(normal [| \(_ , CNum _) (ANum _) -> True |]
                 [p| (_ , CNum _) |]) $
          $(update   [p| (_ , CNum (n, _)) |] [p| (ANum n) |]
                     [d| n = replaceStrInt ; |])

       ,$(normal [| \(_ , CParen _ _ _) _ -> True |]
                 [p| (_ , CParen _ _ _) |]) $
          $(rearrS [| \(dtEnv , CParen _ e _) -> (dtEnv , e)  |]) $
            $(update   [p| eEnv |] [p| eEnv |]
                       [d| eEnv = bigulArithExpr (sPath ++ [("CParen", 1)], vPath) rcvEnv ; |])

       -- ,$(adaptive [| \_ (ASub (ANum 0) y) -> True |])
       --    (\ _ _ ->  CNeg "-" FactorNull)
       ,$(adaptive [| \_ (ANum _) -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv , CNum ("0", " ")) )
       ,$(adaptive [| \_ _ -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv , CParen "(" ExprNull ")"))
  ]


---------------------
tg1 = get (bigulArithExpr ([] :: [(String, Int)], [] :: [(String, Int)]) recoverEnv1) (Map.empty, ts0)
tg1T = getTrace (bigulArithExpr ([] :: [(String, Int)], [] :: [(String, Int)]) recoverEnv1) (Map.empty, ts0)


tp1  = put      (bigulArithExpr ([] :: [(String, Int)], [] :: [(String, Int)]) recoverEnv1) (deltaEnv1, ts0) td1'
tp1T = putTrace (bigulArithExpr ([] :: [(String, Int)], [] :: [(String, Int)]) recoverEnv1) (deltaEnv1, ts0) td1'

