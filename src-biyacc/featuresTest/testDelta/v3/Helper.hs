{-# Language TemplateHaskell, DeriveDataTypeable, TypeFamilies, StandaloneDeriving #-}
{-# Language ScopedTypeVariables, CPP #-}

module Helper where

import GHC.Generics
import Data.Generics
import Data.Dynamic
import Data.Map as Map
import Generics.BiGUL
import Generics.BiGUL.TH
import Generics.BiGUL.Interpreter

import Text.Show.Pretty
import Debug.Trace

---------------------------------
data Arith =
    AAdd Arith Arith
  | ASub Arith Arith
  | AMul Arith Arith
  | ADiv Arith Arith
  | ANum Int
  deriving (Data, Typeable, Show, Eq)


data CExpr =
    CAdd    CExpr String CTerm
  | CSub    CExpr String CTerm
  | ToTerm  CTerm
  | ExprNull
  deriving (Data, Typeable, Show, Eq)

data CTerm =
    CMul      CTerm String CFactor
  | CDiv      CTerm String CFactor
  | ToFactor  CFactor
  | TermNull
  deriving (Data, Typeable, Show, Eq)

data CFactor =
    CNeg   String  CFactor
  | CNum   (String, String)
  | CParen String CExpr String
  | FactorNull
  deriving (Data, Typeable, Show, Eq)

deriveBiGULGeneric ''Arith
deriveBiGULGeneric ''CExpr
deriveBiGULGeneric ''CTerm
deriveBiGULGeneric ''CFactor

----------------------------------
replaceStrInt :: BiGUL String Int
replaceStrInt = emb (safeReadInt) (\_ v -> show v)

emb :: Eq v => (s -> v) -> (s -> v -> s) -> BiGUL s v
emb g p = Case
  [ $(normal [| \s v -> g s == v |] [p| _ |]) (Skip g)
  , $(adaptive [| \s v -> True |]) p]

safeReadInt :: String -> Int
safeReadInt "" = 0
safeReadInt s  = read s

---------------------------
flattenCE :: CExpr -> String
flattenCE (CAdd    ce p ct) = flattenCE ce ++ p ++ flattenCT ct
flattenCE (CSub    ce p ct) = flattenCE ce ++ p ++ flattenCT ct
flattenCE (ToTerm  ct)      = flattenCT ct
flattenCE ExprNull          = "ENULL"

flattenCT  :: CTerm -> String
flattenCT (CMul      ct p cf) = flattenCT ct ++ p ++ flattenCF cf
flattenCT (CDiv      ct p cf) = flattenCT ct ++ p ++ flattenCF cf
flattenCT (ToFactor  cf)      = flattenCF cf
flattenCT TermNull            = "FNULL"

flattenCF :: CFactor -> String
flattenCF (CNeg   p  cf) = p ++ flattenCF cf
flattenCF (CNum   (n1, p1)) = n1 ++ p1
flattenCF (CParen p1 p2 p3) = p1 ++ flattenCE p2 ++ p3
flattenCF FactorNull = "FNULL"
--------------



-- λ [toDyn 1, toDyn "1", toDyn 1.0]
-- [<<Integer>>,<<[Char]>>,<<Double>>]

-- listb = [toDyn . fromJust . fromDynamic . toDyn $ 1, toDyn . fromJust . fromDynamic . toDyn $ "1"]

-- remove the first appearance according to the key
removeKey :: Eq a => a -> [(a,b)] -> [(a,b)]
removeKey k [] = []
removeKey k ((k',v) :as)
  | k == k'   = as
  | otherwise = (k',v) : removeKey k as

addInt :: (Show s, Show v) => BiGUL s v -> BiGUL (Int, s) (Int, v)
addInt b = Skip id `Prod` b

splitMap :: Eq k => Map k a -> ((k,a), Map k a)
splitMap mka | not (Map.null mka) =
  let l = Map.toAscList mka
  in  (head l, Map.fromAscList (tail l))

fromJust (Just x) = x

type NPath = [Int]

type Path = [(String, Int)]

-- AAdd (AAdd (ANum 1) {(ANum 2)})  (AAdd (ANum 3) (ANum 4))
-- path : [("AAdd", 0), ("AAdd", 1), ("ANum", 0)]


-- view' path, source path
type DeltaEnv = Map (Path,Path) Path

-- view' path, source representation.
type RecoverEnv = Map Path Dynamic


