{-# LANGUAGE DeriveDataTypeable, TypeFamilies #-}
module LabelData where

import Helper
import TestData

import Control.Monad.State
import Data.Maybe (catMaybes)
import Data.Data
import Data.Typeable
import GHC.Generics

import Generics.BiGUL
import Generics.BiGUL.TH
import qualified Generics.BiGUL.Interpreter as Bi

import Debug.Trace

type Label = Int

-- extend the Arith data type to have a Label field everywhere
data ArithLabel =
    AAddLabel (ArithLabel, Label) (ArithLabel, Label)
  | ASubLabel (ArithLabel, Label) (ArithLabel, Label)
  | AMulLabel (ArithLabel, Label) (ArithLabel, Label)
  | ADivLabel (ArithLabel, Label) (ArithLabel, Label)
  | ANumLabel (Int, Label)
  deriving (Data, Typeable, Show, Eq)


data CExprLabel =
    CAddLabel    (CExprLabel, Label) (String, Label) (CTermLabel, Label)
  | CSubLabel    (CExprLabel, Label) (String, Label) (CTermLabel, Label)
  | ToTermLabel  (CTermLabel, Label)
  | ExprLabelNull
  deriving (Data, Typeable, Show, Eq)

data CTermLabel =
    CMulLabel      (CTermLabel, Label) (String, Label) (CFactorLabel, Label)
  | CDivLabel      (CTermLabel, Label) (String, Label) (CFactorLabel, Label)
  | ToFactorLabel  (CFactorLabel, Label)
  | TermLabelNull
  deriving (Data, Typeable, Show, Eq)

data CFactorLabel =
    CNegLabel   (String, Label)  CFactorLabel
  | CNumLabel   ((String, Label), (String, Label))
  | CParenLabel (String, Label) (CExprLabel, Label) (String, Label)
  | FactorLabelNull
  deriving (Data, Typeable, Show, Eq)

deriveBiGULGeneric ''ArithLabel
deriveBiGULGeneric ''CExprLabel
deriveBiGULGeneric ''CTermLabel
deriveBiGULGeneric ''CFactorLabel


-----------------------
-- give a piece of CExpr data, generate CExprLabel data: add Label everywhere including root.
tupCExprLabel :: CExpr -> State Int (CExprLabel, Label)
tupCExprLabel (CAdd x lay1 y) = do
  caddi <- get
  modify (+1)
  xi' <- tupCExprLabel x
  lay1i <- get
  modify (+1)
  yi' <- tupCTermLabel y
  return $ (CAddLabel xi' (lay1, lay1i) yi', caddi)

tupCExprLabel (ToTerm t) = do
  totermi <- get
  modify (+1)
  ti' <- tupCTermLabel t
  return $ (ToTermLabel ti', totermi)

tupCTermLabel :: CTerm -> State Int (CTermLabel, Label)
tupCTermLabel (CMul x lay1 y) = do
  cmuli <- get
  modify (+1)
  xi' <- tupCTermLabel x
  lay1i <- get
  modify (+1)
  yi' <- tupCFactorLabel y
  return $ (CMulLabel xi' (lay1, lay1i) yi' , cmuli)

tupCTermLabel (ToFactor f) = do
  tofaci <- get
  modify (+1)
  fi' <- tupCFactorLabel f
  return $ (ToFactorLabel fi' , tofaci)

tupCFactorLabel :: CFactor -> State Int (CFactorLabel, Label)
tupCFactorLabel  (CNum   (i, lay1)) = do
  cnumi <- get
  let inti  = cnumi + 1
  let layi  = inti + 1
  modify (+3)
  return $ ( CNumLabel ((i,inti) , (lay1,layi)) , cnumi)

tupCFactorLabel (CParen lay1 e lay2) = do
  cpareni <- get
  let lay1i = cpareni + 1
  modify (+2)
  ei' <- tupCExprLabel e
  lay2i <- get
  modify (+1)
  return $ (CParenLabel (lay1, lay1i) ei' (lay2, lay2i) , cpareni)

-------------------------------


-------------------
-- bx for CExpr with Labels. Should be automatically generated.
bigulArithExprLabel :: BiGUL (CExprLabel, Label) (ArithLabel, Label)
bigulArithExprLabel =
  Case [$(normal [| \(CAddLabel _ _ _ , _) (AAddLabel _ _ , _) -> True |]
                 [p| (CAddLabel _ _ _ , _) |]) $
          $(update   [p| (CAddLabel x _ y , label) |] [p| (AAddLabel x y , label) |]
                     [d| x = bigulArithExprLabel ;
                         y = bigulArithTermLabel ;
                         label = Replace ; |])

       ,$(normal [| \(ToTermLabel _ , label) _ -> True |]
                 [p| (ToTermLabel _ , label) |]) $
          $(update   [p| (ToTermLabel e , _) |] [p| e |]
                     [d| e = bigulArithTermLabel ; |])

        ]


bigulArithTermLabel :: BiGUL (CTermLabel, Label) (ArithLabel, Label)
bigulArithTermLabel = Case
  [
    $(normal [| \(CMulLabel _ _ _ , _) (AMulLabel x y , _) -> True |]
             [p| (CMulLabel _ _ _ , _) |]) $
      $(update [p| (CMul x _ y , label) |] [p| (AMul x y , label) |]
               [d| x = bigulArithTermLabel ;
                   y = bigulArithFactorLabel ;
                   label = Replace ; |])

   ,$(normal [| \(ToFactorLabel _ , _) _ -> True |]
             [p| (ToFactorLabel _ , _) |]) $
      $(update   [p| (ToFactorLabel e , _) |] [p| e |]
                 [d| e = bigulArithFactorLabel ; |])
    ]

bigulArithFactorLabel :: BiGUL (CFactorLabel, Label) (ArithLabel , Label)
bigulArithFactorLabel = Case
  [
    $(normal [| \(CNumLabel _ , _) (ANumLabel _ , _) -> True |]
             [p| (CNumLabel _ , _) |]) $
      $(update   [p| (CNumLabel ( (n,label2) , (_,_) ) , label1) |] [p| (ANumLabel (n, label2) , label1) |]
                 [d| n = replaceStrInt ;
                     label1 = Replace ;
                     label2 = Replace ; |])

   ,$(normal [| \(CParenLabel _ _ _ , _) _ -> True |]
             [p| (CParenLabel _ _ _ , _) |]) $
      $(update   [p| (CParenLabel _ e _ , _) |] [p| e |]
                 [d| e = bigulArithExprLabel ; |])
  ]
-------------------

-- find the label for the node indicated by the path
vPath2Label :: (ArithLabel, Label) -> NPath -> Label
vPath2Label (_, lb) [] = lb
vPath2Label (view, _) (hd:tl) = case (view, hd) of
  (AAddLabel x _ , 0) -> vPath2Label x tl
  (AAddLabel _ y , 1) -> vPath2Label y tl
  (AMulLabel x _ , 0) -> vPath2Label x tl
  (AMulLabel _ y , 1) -> vPath2Label y tl
  -- (ADivLabel x _ , 0) -> vPath2Label x tl
  -- (ADivLabel _ y , 0) -> vPath2Label y tl
  (ANumLabel (_, lb) , 0) -> lb

-- convert path-based correspondence to label-based correspondence.
vvPath2VLabel :: (ArithLabel, Label) -> [(NPath,NPath)] -> [(Label,Label)]
vvPath2VLabel view = map (\(x,y) -> (vPath2Label view x, vPath2Label view y))

-- convert label-based delta to path based-delta .
ssLabelPair2Path :: (CExprLabel, Label) -> [(Label, Label)] -> [(NPath,NPath)]
ssLabelPair2Path src = map (\(l1,l2) -> (ssLabel2Path src l1, ssLabel2Path src l2))


-- find the path for the node annotated by the label (for source)
ssLabel2Path :: (CExprLabel, Label) -> Label -> NPath
ssLabel2Path = ((.) . (.)) (reverse . fromJust)  (ssLabel2PathCE [])


ssLabel2PathCE :: NPath -> (CExprLabel, Label) -> Label -> Maybe NPath
ssLabel2PathCE path (_, lb') lb | lb == lb'  = Just path
ssLabel2PathCE path (ce, lb') lb | lb /= lb' =
  case ce of
    CAddLabel x lay1 z ->
      catMaybesHead $ [ ssLabel2PathCE  (0:path) x lb
                                , ssLabel2PathStr (1:path) lay1 lb
                                , ssLabel2PathCT  (2:path) z lb]
    ToTermLabel t ->
      catMaybesHead $ [ssLabel2PathCT (0:path) t lb]

ssLabel2PathCT :: NPath -> (CTermLabel, Label) -> Label -> Maybe NPath
ssLabel2PathCT path (_, lb')  lb | lb == lb' = Just path
ssLabel2PathCT path (ct, lb') lb | lb /= lb' =
  case ct of
    CMulLabel x lay1 z ->
      catMaybesHead $ [ ssLabel2PathCT  (0:path) x lb
                                , ssLabel2PathStr (1:path) lay1 lb
                                , ssLabel2PathCF  (2:path) z lb]
    ToFactorLabel f ->
      catMaybesHead $ [ssLabel2PathCF (0:path) f lb]

ssLabel2PathCF :: NPath -> (CFactorLabel, Label) -> Label -> Maybe NPath
ssLabel2PathCF path (_, lb')  lb | lb == lb' = Just path
ssLabel2PathCF path (cf, lb') lb | lb /= lb' =
  case cf of
    (CNumLabel   (i, lay1))   ->
      catMaybesHead $ [ssLabel2PathStr (0:path) i lb
                                ,ssLabel2PathStr (0:path) lay1 lb]
    (CParenLabel lay1 e lay2) ->
      catMaybesHead $ [ssLabel2PathStr (0:path) lay1 lb
                                ,ssLabel2PathCE  (1:path) e lb
                                ,ssLabel2PathStr (2:path) lay2 lb]

ssLabel2PathStr :: NPath -> (String, Label) -> Label -> Maybe NPath
ssLabel2PathStr path (_, lb')  lb | lb == lb' = Just path
ssLabel2PathStr path (_, lb') lb | lb /= lb' = Nothing

catMaybesHead :: [Maybe a] -> Maybe a
catMaybesHead maya = case catMaybes maya of
  []   -> Nothing
  a:as -> Just a


-------------------------------
-- give an expr, to see if we can successfully generate labelled expr
tLabelled :: (CExprLabel, Label)
tLabelled = evalState (tupCExprLabel cexpr1) 0

-- to see if we can perfor get on the labelled data
tLabelledGet = Bi.get bigulArithExprLabel tLabelled

-- test converting path-based correspondence to label-based
tPath2Label :: [(Label, Label)]
tPath2Label = vvPath2VLabel (fromJust tLabelledGet) vvDelta

-- test converting label-based correspondence to path-based
tLabel2Path = ssLabelPair2Path tLabelled tPath2Label
------------------


