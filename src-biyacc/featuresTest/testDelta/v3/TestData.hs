module TestData where

import Helper

cexpr1 = CAdd ts00
           " + "
           cterm1

ts00 = CAdd (ToTerm (ToFactor (CNum ("1"," ")))) " {- some -} + {- comment -} " (ToFactor (CNum ("2"," ")))
cterm1 = ToFactor (CParen "(" ts03 ")")

ts03 = CAdd (ToTerm (ToFactor (CNum ("3"," ")))) "      +       " (ToFactor (CNum ("4"," ")))

aexpr1 = AAdd (AAdd (ANum 1) (ANum 2))  (AAdd (ANum 3) (ANum 4))

aexpr1' = AAdd  (AAdd (ANum 3) (ANum 4)) (AAdd (ANum 1) (ANum 2))

vvDelta :: [(NPath,NPath)]
vvDelta = [([0],[1]),([1],[0])]

svDelta :: [(NPath,NPath)]
svDelta =
  [([0], [1])
  ,([2,0,1], [0])]
