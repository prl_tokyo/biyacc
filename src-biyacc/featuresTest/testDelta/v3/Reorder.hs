{-# Language TemplateHaskell, TypeFamilies, StandaloneDeriving #-}
module Reorder where

import Helper
import LabelData

import Data.List as List
import Data.Map as Map (Map, empty, delete, fromAscList, toAscList, null)
import GHC.Generics
import Data.Generics
import Data.Dynamic


-- give a source and a path, wrap the (whole) tree at the position into a dynamic value
locToPath :: CExpr -> NPath -> Dynamic
locToPath = locToPathCExpr

locToPathCExpr :: CExpr -> NPath -> Dynamic
locToPathCExpr src []   = toDyn src
locToPathCExpr src path =
  case (src, head path) of
    (CAdd x _ _, 0)    -> locToPathCExpr x (tail path)
    -- (CAdd _ _ _, 1) -> locToPathCTerm x (tail path)
    (CAdd _ _ y, 2)    -> locToPathCTerm y (tail path)
    (ToTerm t, 0)      -> locToPathCTerm t (tail path)


locToPathCTerm :: CTerm -> NPath -> Dynamic
locToPathCTerm src []   = toDyn src
locToPathCTerm src path =
  case (src, head path) of
    (CMul x _ _, 0)    -> locToPathCTerm   x (tail path)
    (CMul _ _ y, 2)    -> locToPathCFactor y (tail path)
    (ToFactor f, 0)    -> locToPathCFactor f (tail path)

locToPathCFactor :: CFactor -> NPath -> Dynamic
locToPathCFactor src [] = toDyn src
locToPathCFactor src path =
  case (src, head path) of
    (CNum n , 0) -> toDyn n
    (CParen _ x _, 1) -> locToPathCExpr x (tail path)

-- give a source, a path, a new part of source (dynamic value), replace the (whole) tree
-- at the path with the new part of source.
updPath :: CExpr -> NPath -> Dynamic -> CExpr
updPath = updPathCExpr

updPathCExpr :: CExpr -> NPath -> Dynamic -> CExpr
updPathCExpr _   []   twig = fromDyn twig ExprNull
updPathCExpr src path twig =
  case (src, head path) of
    (CAdd x lay1 y, 0)    -> CAdd (updPathCExpr x (tail path) twig) lay1 y
    (CAdd x lay1 y, 2)    -> CAdd x lay1 (updPathCTerm y (tail path) twig)
    (ToTerm t, 0)         -> ToTerm (updPathCTerm t (tail path) twig)


updPathCTerm :: CTerm -> NPath -> Dynamic -> CTerm
updPathCTerm _   []   twig = fromDyn twig TermNull
updPathCTerm src path twig =
  case (src, head path) of
    (CMul x lay1 y, 0)    -> CMul (updPathCTerm x (tail path) twig) lay1 y
    (CMul x lay1 y, 2)    -> CMul x lay1 (updPathCFactor y (tail path) twig)
    (ToFactor f, 0)      -> ToFactor (updPathCFactor f (tail path) twig)

updPathCFactor :: CFactor -> NPath -> Dynamic -> CFactor
updPathCFactor _   []   twig = fromDyn twig FactorNull
updPathCFactor src path twig =
  case (src, head path) of
    (CNum (_, lay1) , 0) -> CNum (fromDyn twig "0", lay1)
    (CParen lay1 e lay2, 1) -> CParen lay1 (updPathCExpr e (tail path) twig) lay2



-- top down update the (CExpr) tree.
updPaths :: CExpr -> [(NPath, Dynamic)] -> CExpr
updPaths s [] = s
updPaths s (p:ps) =
  let (path, dyn) = p
      s' = updPath s path dyn
  in  updPaths s' ps


{-
reorder = swapN

-- this is not correct because in the put direction, it first swap the source, and
-- then apply the put which also swap the source again ... (aka, no swap at all)
swapN :: [(NPath,NPath)] -> BiGUL CExpr CExpr
swapN [] = exprReorder Nothing

swapN (dlt:dlts) = exprReorder (Just dlt) `Compose` swapN dlts


exprReorder :: Maybe (NPath, NPath) -> BiGUL CExpr CExpr
exprReorder Nothing = Replace
exprReorder (Just (sPath, sPath')) = emb ge pu
  where
    ge src =
      let a = locToPath src sPath
          b = locToPath src sPath'
          src' = updPath src sPath  b
      in  updPath src' sPath' a
    pu _ view =
      let a = locToPath view sPath
          b = locToPath view sPath'
          view' = updPath view sPath  b
      in  updPath view' sPath' a

-}