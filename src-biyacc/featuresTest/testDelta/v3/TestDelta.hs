{-# Language TemplateHaskell, TypeFamilies, StandaloneDeriving #-}

module Trace where

import Helper
import Reorder
import LabelData
import TestData

import Data.List as List
import Data.Map as Map (Map, empty, delete, fromList)
import Control.Monad.State (evalState)
import GHC.Generics
import Data.Generics
import Data.Dynamic
import Data.Foldable.Deriving
import Generics.BiGUL
import Generics.BiGUL.TH
import Generics.BiGUL.Interpreter

import Text.Show.Pretty


bigulArithExpr :: BiGUL CExpr Arith
bigulArithExpr =
  Case [$(normal [| \(CAdd _ _ _) (AAdd _ _) -> True |]
                 [p| (CAdd _ _ _) |]) $
          $(update   [p| CAdd x _ y |] [p| (AAdd x y) |]
                     [d| x = bigulArithExpr ;
                         y = bigulArithTerm ; |])

       -- ,$(normal [| \(ToTerm _) (ASub (ANum 0) _) -> True |]
       --           [p| (ToTerm _) |]) $
       --    $(update [p| ToTerm e |] [p| e |]
       --               [d| e = bigulArithTerm ; |])

       ,$(normal [| \(CSub _ _ _) (ASub x y) -> True |]
                 [p| (CSub _ _ _) |]) $
          $(update [p| CSub x _ y |] [p| (ASub x y) |]
                     [d| x = bigulArithExpr ;
                         y = bigulArithTerm ; |])

       ,$(normal [| \(ToTerm _) _ -> True |]
                 [p| (ToTerm _) |]) $
          $(update   [p| ToTerm e |] [p| e |]
                     [d| e = bigulArithTerm ; |])

       ,$(adaptive [| \_ (AAdd _ _) -> True |])
          (\ _ _ ->  CAdd ExprNull " + " TermNull)
       -- ,$(adaptive [| \_ (ASub (ANum 0) _) -> True |])
       --    (\ _ _ ->  ToTerm TermNull)
       ,$(adaptive [| \_ (ASub x y) -> True |])
          (\ _ _ ->  CSub ExprNull " - " TermNull)
       ,$(adaptive [| \_ _ -> True |])
          (\ _ _ ->  ToTerm TermNull)]


bigulArithTerm :: BiGUL CTerm Arith
bigulArithTerm =
  Case [
        $(normal [| \(CMul _ _ _) (AMul x y) -> True |]
                 [p| (CMul _ _ _) |]) $
          $(update [p| CMul x _ y |] [p| (AMul x y) |]
                   [d| x = bigulArithTerm ;
                       y = bigulArithFactor ; |])

       ,$(normal [| \(CDiv _ _ _) (ADiv x y) -> True |]
                 [p| (CDiv _ _ _) |]) $
          $(update [p| CDiv x _ y |] [p| (ADiv x y) |]
                     [d| x = bigulArithTerm   ;
                         y = bigulArithFactor ; |])

       ,$(normal [| \(ToFactor _) _ -> True |]
                 [p| (ToFactor _) |]) $
          $(update   [p| ToFactor e |] [p| e |]
                     [d| e = bigulArithFactor ; |])
       ,$(adaptive [| \_ (AMul x y) -> True |])
          (\ _ _ ->  CMul TermNull " * " FactorNull)
       ,$(adaptive [| \_ (ADiv x y) -> True |])
          (\ _ _ ->  CDiv TermNull " / " FactorNull)
       ,$(adaptive [| \_ _ -> True |])
          (\ _ _ -> ToFactor FactorNull)]


bigulArithFactor :: BiGUL CFactor Arith
bigulArithFactor =
  Case [ $(normal [| \(CNeg _ _) (ASub (ANum 0) y) -> True |]
                  [p| (CNeg _ _) |]) $
          $(update [p| CNeg _ y |] [p| (ASub (ANum 0) y) |]
                   [d| y = bigulArithFactor ; |])

       ,$(normal [| \(CNum _) (ANum _) -> True |]
                 [p| (CNum _) |]) $
          $(update   [p| (CNum (n, _)) |] [p| (ANum n) |]
                     [d| n = replaceStrInt ; |])

       ,$(normal [| \(CParen _ _ _) _ -> True |]
                 [p| (CParen _ _ _) |]) $
          $(update   [p| CParen _ e _ |] [p| e |]
                     [d| e = bigulArithExpr ; |])

       ,$(adaptive [| \_ (ASub (ANum 0) y) -> True |])
          (\ _ _ ->  CNeg "-" FactorNull)
       ,$(adaptive [| \_ (ANum _) -> True |])
          (\ _ _ ->  CNum ("0", " ") )
       ,$(adaptive [| \_ _ -> True |])
          (\ _ _ ->  CParen "(" ExprNull ")" )
  ]

-----------------------
-- new one
-----------------------------------------

putCorr :: BiGUL (CExpr, [(NPath,NPath)]) Arith
putCorr = putCorrCExpr

-- suppose delta env is (source path, view path) pair.
-- paths are in pre order of a tree.
putCorrCExpr :: BiGUL (CExpr, [(NPath,NPath)]) Arith
putCorrCExpr =
  Case  [ $(normal [| \(_, denv) _ -> List.null denv |] [| \(_, denv) -> List.null denv |] ) $
            $(update [p| (src, _) |] [p| src |] [d| src = bigulArithExpr |])

        , $(adaptive  [|\(_, denv) _ -> not (List.null denv)  |])
            ( \(s,denv) v -> (magicReshape s v denv, [])) -- set to empty correspondence links
        ]
  where
    -- a naive implementation. running process:
    -- first,  perform naive put to get a new source (skeleton)
    -- second, use correspondence links to recover necessary information from old source
    -- finally, perform naive put again, on the recovered source.
    magicReshape :: CExpr -> Arith -> [(NPath,NPath)] -> CExpr
    magicReshape s v (svPaths) =
      let sklton = maybe (error "put error") id (put bigulArithExpr s v)
          skltonWithLabel = evalState (tupCExprLabel sklton) 0
          viewWithLabel   = fromJust $ get bigulArithExprLabel skltonWithLabel

          skltSPaths  = map (\(sPath,vPath) -> (vPath2sPath skltonWithLabel viewWithLabel vPath, sPath)) svPaths
          updEnv = madeUpdEnv skltSPaths s
      in  updPaths sklton (sortByFst updEnv)

    vPath2sPath sL vL vPath =
      let vLabel = vPath2Label vL vPath
          sLabel = vLabel
      in  ssLabel2Path sL sLabel

-- return a env from paths in skeleton tree to the tree to be replaced
madeUpdEnv :: [(NPath,NPath)] -> CExpr -> [(NPath, Dynamic)]
madeUpdEnv [] _ = []
madeUpdEnv (x:xs) s =
  let (skltPath, sPath) = x
  in  (skltPath, (locToPath s sPath)) : (madeUpdEnv xs s)


sortByFst :: Ord a => [(a,b)] -> [(a,b)]
sortByFst [] = []
sortByFst ((x, notouch):l) =
  sortByFst (filter (\(alpha, _) -> alpha <= x) l) ++ [(x,notouch)] ++
  sortByFst (filter (\(alpha, _) -> alpha >  x) l)



{-

-- currently reshape and replace is fused into one traversal
-- deltaAlign deltas = (reorder deltas) `Compose` reshape `Compose` replace
-- deltaAlign deltas bx = (reorder deltas) `Compose` bx

-- deltaAlign2 deltas bx = emb
--  (\s    -> fromJust $ get bx s)
--  (\s v  -> fromJust $ get (reorder deltas) (fromJust $ put ((reorder deltas) `Compose` bx) s v))


--------------------- test swap add and add
tt1 = pPrint . flattenCE $ cexpr1
tt2 = pPrint . flattenCE . fromJust $ put (deltaAlign [] bigulArithExpr)    cexpr1 aexpr1'
tt3 = pPrint . flattenCE . fromJust $ put (deltaAlign path1 bigulArithExpr) cexpr1 aexpr1'


tt4 = flattenCE . fromJust $ get (reorder []) $
  fromJust $ put (deltaAlign [] bigulArithExpr)    cexpr1 aexpr1'


tt5 = pPrint $ fromJust $ get (deltaAlign2 path1 bigulArithExpr)   cexpr1
tt6 = pPrint $ flattenCE . fromJust $ put (deltaAlign2 path1 bigulArithExpr)    cexpr1 aexpr1'
tt7 = pPrint $ flattenCE . fromJust $ put (deltaAlign2 path1 bigulArithExpr)    cexpr1 aexpr1

path1 :: [(NPath,NPath)]
path1 = [ ( sp1 , sp1') ]
  where sp1  = [0]
        sp1' = [2, 0, 1]


---------------------- test swap add and mulplication
tt10 = pPrint $ fromJust $ get (deltaAlign2 path2 bigulArithExpr)   cexpr2
tt11 = pPrint $ flattenCE . fromJust $ put (deltaAlign2 path2 bigulArithExpr)    cexpr2 aexpr2'
tt12 = pPrint $ flattenCE . fromJust $ put (deltaAlign2 path2 bigulArithExpr)    cexpr2 aexpr2
tt12C = pPrint $ flattenCE . fromJust $ put (deltaAlign2 [] bigulArithExpr)    cexpr2 aexpr2


cexpr2 = CAdd ts000 "  +  " cterm2

ts000  = CAdd (ToTerm (ToFactor (CNum ("1"," ")))) " {- some -} + {- comment -} " (ToFactor (CNum ("2"," ")))
cterm2 = ToFactor (CParen "(" ts003 ")")

ts003  = ToTerm (CMul (ToFactor (CNum ("3"," "))) "      *       " (CNum ("4"," ")))

aexpr2  = AAdd  (AAdd (ANum 3) (ANum 4))  (AMul (ANum 1) (ANum 2))

aexpr2' = AAdd  (AMul (ANum 1) (ANum 2))  (AAdd (ANum 3) (ANum 4))

path2 :: [(NPath,NPath)]
path2 =
  [ ( sp1 , sp1')
  ]
  where
    sp1  = [0]
    sp1' = [2, 0, 1]

-------------
deltaEnv1 :: DeltaEnv
deltaEnv1 = Map.fromList
  [ ( ( vp1,sp1 ) , sp1')
  , ( ( vp2,sp2 ) , sp2')
  ]
  where
    vp1  = [("AAdd", 0)]
    sp1  = [("CAdd", 0)]
    sp1' = [("CAdd", 2), ("ToFactor", 0), ("CParen", 1)]

    vp2  = [("AAdd", 1)]
    sp2  = [("CAdd", 2), ("ToFactor", 0), ("CParen", 1)]
    sp2' = [("CAdd", 0)]

-- inferred
recoverEnv1 :: RecoverEnv
recoverEnv1 = Map.fromList
  [([("AAdd", 0)], toDyn ts03)
  ,([("AAdd", 1)], toDyn ts00 )]
  -- ,([("AAdd", 1)], toDyn (ToFactor (CParen " ( " ts00 " ) " )))]

-- I forgot what is this to test ...
bg1 :: BiGUL [Int] [Int]
bg1 =
  Case  [ $(normal [| \[x,y,z] [vx,vy,vz] -> y == x + 1 && z == x + 1 && vx == vy && vy == vz |]
                   [| \[x,y,z] -> y == x + 1 && z == x + 1 |] ) $
            $(update [p| [x,y,z] |] [p| [x,y,z] |]
                     [d| x = Replace; y = Skip (flip (-) 1); z = Skip (flip (-) 1) |])

        , $(normal [| \s [v] -> not (null s) && all (== head s) s  |]
                   [| \s -> all (== head s) s |] ) $
            $(rearrV [| \[v] -> (v,(v,(v,(v,v))))  |]) $
              $(update [p| [a,b,c,d,e] |] [p| (a,(b,(c,(d,e)))) |]
                       [d| a = Replace; b = Replace; c = Replace;
                           d = Replace; e = Replace; |])

        , $(adaptive [|\_ [vx,vy,vz] -> vx == vy && vy == vz |])
            (\_ [vx,vy,vz] -> [vx, vy-1, vz-1] )

        , $(adaptive [|\s [v] -> (null s || not (all (== head s) s)) |])
            (\_ [v] ->  [v,v,v,v,v] )
        ]


test1 :: BiGUL [Int] [Int]
test1 = $(rearrV [| \[v] -> (v,v) |]) $
        $(update [p| [a,b] |] [p| (a,b) |]
                 [d| a = Replace; b = Replace;  |])

reShape :: forall s. (Typeable s, Eq s, Show s) => s -> Path -> RecoverEnv -> s
reShape dVal vPath env =
  let dyn = fromJust . Map.lookup vPath $ env
      src = (fromDyn dyn dVal)
  in  trace (show dyn) $
      if src == dVal
        then error $ "impossible in reshape: "  ++ ppShow src
        else src

-}


