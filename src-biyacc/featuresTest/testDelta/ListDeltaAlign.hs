----------------------

import Data.List as List (delete,findIndex)
import Data.Maybe (isJust, fromJust, catMaybes)
import Generics.BiGUL
import Generics.BiGUL.Interpreter
import Generics.BiGUL.TH


emb :: Eq v => (s -> v) -> (s -> v -> s) -> BiGUL s v
emb g p = Case
  [ $(normal [| \s v -> g s == v |] [p| _ |]) (Skip g)
  , $(adaptive [| \s v -> True |]) p]


-- delta align on list
-- minimum setting

type DeltaPos = (Int, Int)

testDelta :: [(Int,Int)]
testDelta = [(2,0),(1,2),(0,1),(3,3),(5,4),(4,5)]

testData = ["12","34","56","78","910","1112"]

-- deltaAlign :: (Eq a, Show a, Eq b, Show b) => BiGUL a b -> BiGUL [(a,DeltaPos)] [b]
-- deltaAlign bx = (bigulUnZip `Compose` rearrangeD) `Compose` naiveMap bx


deltaAlign :: (Eq a, Show a, Eq b, Show b) => [DeltaPos] -> a -> BiGUL a b -> BiGUL [a] [b]
deltaAlign ds defaultSrc bx = deltaAlign' (deltaToSwapOp ds) `Compose` naiveMap defaultSrc bx
  where
    deltaAlign' :: (Eq a, Show a) => [DeltaPos] -> BiGUL [a] [a]
    deltaAlign' [] = Replace
    deltaAlign' (d:ds) =
        $(update [p| ss |] [p| ss |] [d| ss = uncurry swapIJ d `Compose` deltaAlign' ds |])


-- natural number does not have constructor. can not do induction in bigul.
-- swap i j xs | i < j    ===   take i xs ++ swap 0 (j - i) (drop (i+1) xs)
swapIJ :: (Eq s, Show s) => Int -> Int -> BiGUL [s] [s]
swapIJ 0 j = swapHead j
swapIJ i j = if i > j then swapIJ j i else
  Case  [ $(normalSV [p| _:_ |] [p| _:_ |] [p| _:_ |] ) $
             $(update [p| x:xs |] [p| x:xs |] [d| x = Replace; xs = swapIJ (i-1) (j-1) |])
        , $(normalSV [p| [] |] [p| _ |] [| const True |] ) $
             Fail "source index too large"
        , $(normalSV [p| _ |] [p| [] |] [| const True |] ) $
             Fail "view index too large"
        ]

-- both the get and put direction are to swap the head element with i's element.
swapHead :: (Eq s, Show s) => Int -> BiGUL [s] [s]
swapHead i = extToHead i `Compose` insertTo (i-1)

-- put direction is to insert the head element into i's position.
insertTo :: (Show s, Eq s) => Int -> BiGUL [s] [s]
insertTo 0 = Replace
insertTo i =
  Case  [ $(normal [| \(_:_:_) (_:_:_) -> True |] [p| _:_:_ |] ) $
            $(rearrV [| \(v1:v2:vs) -> (v2,(v1:vs)) |]) $
              $(update [p| v2:vs |] [p| (v2,vs) |]
                       [d|  v2 = Replace ; vs = insertTo (i-1) |])
        , $(normalSV [p| _:_ |] [p| _:_ |] [p| _:_ |] ) $
            Fail "index too large"
        , $(normalSV [p| [] |] [p| [] |] [p| [] |] ) $
            Fail "index too large"
        ]

-- put direction is to bring the i's element to the head.
extToHead :: (Show s, Eq s) => Int -> BiGUL [s] [s]
extToHead = magicInv . insertTo

-- generate a bigul program having the get and put semantics swapped.
-- the input bigul program should be an isomorphism.
magicInv :: (Eq s) => BiGUL s s -> BiGUL s s
magicInv bx = emb (\v -> fromJust $ put bx v v) (\_ s -> fromJust $ get bx s)

-------------
-- all rearrangements (permutation) can be obtained by swaps.
findCycles :: (Eq a) => [(a,a)] -> [[a]]
findCycles [] = []
findCycles (x:xs) =
  let (i,j) = x
  in  case findOneCycle i (i,j) xs of
        []  -> []
        cyc -> cyc : findCycles (filter (\(i,_) -> not (i `elem` cyc)) xs)

-- find one cycle. a cycle is in the form of [a,b,c,...,a]
findOneCycle :: (Eq a) => a -> (a,a) -> [(a,a)] -> [a]
findOneCycle _ (i,j) _ | i == j = [i,j]
findOneCycle _ (i,j) [] = error "invalid input 1"
findOneCycle init (i,j) ds =
  case (filter (\(i',_) -> j == i') ds) of
    [(i',j')] ->
      if j' == init
        then [i,j,j']
        else [i] ++ findOneCycle init (i',j') (List.delete (i',j') ds)
    _     -> error "invalid input 2"

-- convert cycles to swap operation sequences
cycToSwapOp :: [[a]] -> [(a,a)]
cycToSwapOp [] = []
cycToSwapOp (x:xs) =
  let x' = reverse (tail x) -- the cycle is [a,b,...,a], we drop the redundant 'a'
  in  zip x' (tail x') ++ cycToSwapOp xs

-- original delta (permutation) to swap operations
deltaToSwapOp :: (Eq a) => [(a,a)] -> [(a,a)]
deltaToSwapOp = cycToSwapOp . findCycles
----------------------

-------------------- not used helper.
modifyByPos :: (Show a) => Int -> BiGUL [a] a
modifyByPos 0 =
  Case  [ $(adaptiveSV [p| [] |] [p| a |] ) $
             (\_ a -> [a] )
        , $(normalSV [p| x:xs |] [p| a |] [p| x:xs |]) $
            $(update [p| a:_ |] [p| a |] [d| a = Replace |])
        ]

modifyByPos i =
  Case  [ $(normalSV [p| x:xs |] [p| a |] [p| x:xs |] ) $
             $(update [p| _:a |] [p| a |] [d| a = modifyByPos (i-1) |])

        , $(normalSV [p| [] |] [p| a |] [| const True |]) $
             Fail "index too large"
        ]


modifyByP :: (Show a) => (a -> Bool) -> BiGUL [a] a
modifyByP p =
  Case  [ $(normal [| \(s:_) _ -> p s |] [| \(s:_) -> p s |] ) $
             $(update [p| a:_ |] [p| a |] [d| a = Replace |])

        , $(normal [| \(s:_) _ -> not (p s) |] [| \(s:_) -> not (p s) |] ) $
             $(update [p| _:a |] [p| a |] [d| a = modifyByP p |])

        , $(normalSV [p| [] |] [p| _ |] [| const True |]) $
             Fail "no element in source can be matched by predicate p"
        ]

-- upd1BX :: (Show a) => (Int, Int) -> DeltaPos -> BiGUL [a] [a]
-- upd1BX (i,i') =
--   Case  [ $(normal [| \(x:xs) (y:ys) -> i == 0 && i' == 0 |]
--                    [p| x:xs |] ) $
--              $(update [p| x:xs |] [p| x:xs |] [d| x = Replace; xs = Skip |])

--         , $(normal [| \(x:xs) (x:xs) -> i > 0 && i' > 0 |] ) $
--              $(update [p| _:xs |] [p| _:xs |] [d| xs = upd1BX (i-1,i'-1) |])

--         , $(normal [| \(x:xs) (x:xs) -> i == 0 && i' > 0 |] ) $
--              $(update [p| _:xs |] [p| _:xs |] [d| xs = upd1BX (i,i'-1) |])

--         , $(normal [| \(x:xs) (x:xs) -> i > 0 && i' == 0 |] ) $
--              $(update [p| _:xs |] [p| _:xs |] [d| xs = upd1BX (i-1,i') |])

--         , $(normal [| \[] _ -> i > 0 |] ) $
--              Fail "source delta index too large"

--         , $(normal [| \_ [] -> i' > 0 |] ) $
--              Fail "view delta index too large"
--         ]


------------------
-- emb version
-- it is hard to write isomorphism ...
deltaAlignEmb :: (Eq a, Show a, Eq b, Show b) => a -> BiGUL a b -> BiGUL ([a],[DeltaPos]) [b]
deltaAlignEmb defaultSrc bx = rearrangeD `Compose` naiveMap defaultSrc bx

naiveMap :: (Show a, Show b) => a -> BiGUL a b -> BiGUL [a] [b]
naiveMap defaultSrc b =
  Case  [ $(normalSV [p| _:_ |] [p| _:_ |] [p| _:_ |])
          $ $(update [p| x:xs |] [p| x:xs |] [d| x = b; xs = naiveMap defaultSrc b |])

        , $(adaptiveSV [p| _:_ |] [p| [] |] )
            (\_ _ -> [])

        , $(adaptiveSV [p| [] |] [p| _:_ |])
            (\_ _ -> [defaultSrc])

        , $(normalSV [p| [] |] [p| [] |] [p| [] |]) $
            $(update [p| [] |] [p| [] |] [d| |])
        ]


rearrangeD :: (Eq a, Show a) => BiGUL ([a], [DeltaPos]) [a]
rearrangeD = emb g p
  where
    g :: ([a], [DeltaPos]) -> [a]
    g (ss, ds) = upd1 ss ss ds

    p :: ([a], [DeltaPos]) -> [a] -> ([a], [DeltaPos])
    p (ss, ds) v =
      let ds' = updateDelta (genIdentityDelta ss) ds
      in  (upd1 v ss (map swap ds'), ds')

    swap (a,b) = (b,a)

    genIdentityDelta :: [a] -> [DeltaPos]
    genIdentityDelta s = take (length s) . map (\x -> (x,x)) $ [0..]

    updateDelta :: [DeltaPos] -> [DeltaPos] -> [DeltaPos]
    updateDelta ds [] = ds
    updateDelta ds ((d1,d1'):d1s) = case List.findIndex ((== d1) . fst) ds of
      Nothing -> updateDelta ds d1s
      Just idx  -> updateDelta (modify idx (d1,d1') ds) d1s


upd1 :: [a] -> [a] -> [DeltaPos] -> [a]
upd1 ss ss' [] = ss'
upd1 ss ss' ((i,i'):ds) = upd1 ss (modify i' (ss !! i) ss') ds

modify :: Int -> a -> [a] -> [a]
modify i a ss = take i ss ++ [a] ++ drop (i+1) ss

bigulUnZip :: (Eq a, Show a, Eq b, Show b) => BiGUL [(a,b)] ([a],[b])
bigulUnZip = Case
  [ $(normalSV [p| [] |] [p| ([],[]) |] [p| [] |]) $ Skip (const ([],[]))

  , $(normalSV [p| _:_ |]  [p| (_:_, _:_) |] [p| _:_ |]) $
      $(rearrV [| \(a:as,b:bs) -> ((a,b), (as,bs))  |])
        $(update [p| (a,b):abss |] [p| ((a,b),abss) |]
                 [d| a = Replace;
                     b = Replace;
                     abss = bigulUnZip |])
  , $(adaptiveSV [p| [] |]  [p| (_:_, _:_) |])
      (\_ (a:_,b:_) -> [(a,b)])
  , $(adaptiveSV [p| _:_ |]  [p| ([],[]) |])
      (\_ _ -> [])
  ]


