-- classification of code refactor actions
-- substitution + insert (new things)
-- substitution + delete
-- substitution + move   (should keep original things)

{-# Language TemplateHaskell, DeriveDataTypeable, TypeFamilies, StandaloneDeriving #-}
module Reorder where

import Helper

import Data.List as List
import Data.Map as Map (Map, empty, delete, fromAscList, toAscList, null)
import GHC.Generics
import Data.Generics
import Data.Dynamic
import Data.Foldable.Deriving
import Generics.BiGUL
import Generics.BiGUL.TH
import Generics.BiGUL.Interpreter

import Text.Show.Pretty

-- get s => Just v
-- collect leaves (perhaps wrapped in a container type Primitive) of v in pre-order [vi]
-- collect leaves (perhaps wrapped in a container type Primitive) of v' in pre-order [vi']
-- give the relation between [vi] and [vi'], we get [(vi', vi)] in pre-order of vi'.
-- pre-order means the order they are going to be used in put.
-- e.g. (1 + 1) + (1 + 1) has many similar (identical if does not consider position) leaves,
-- and it needs an order...

-- then we have relations between s and v', [(vi',si)] in pre-order.


---------------------
-- bigul

addInt :: (Show s, Show v) => BiGUL s v -> BiGUL (Int, s) (Int, v)
addInt b = Skip id `Prod` b

-- swapN :: RecoverEnv -> BiGUL CExpr CExpr
-- swapN rcvEnv | (Map.null rcvEnv) =
--   exprReorder Nothing nullPath

-- swapN rcvEnv | not (Map.null rcvEnv) =
--   let (delta1, rcvEnv') = splitMap rcvEnv
--   in  exprReorder (Just delta1) nullPath `Compose` swapN rcvEnv'


splitMap :: Eq k => Map k a -> ((k,a), Map k a)
splitMap mka | not (Map.null mka) =
  let l = Map.toAscList mka
  in  (head l, Map.fromAscList (tail l))


nullPath = []

reorder = swapN

swapN :: [(NPath,NPath)] -> BiGUL CExpr CExpr
swapN [] = exprReorder Nothing

swapN (dlt:dlts) = exprReorder (Just dlt) `Compose` swapN dlts


exprReorder :: Maybe (NPath, NPath) -> BiGUL CExpr CExpr
exprReorder Nothing = Replace
exprReorder (Just (sPath, sPath')) = emb ge pu
  where
    ge src =
      let a = locToPath src sPath
          b = locToPath src sPath'
          src' = updPath src sPath  b
      in  updPath src' sPath' a
    pu _ view =
      let a = locToPath view sPath
          b = locToPath view sPath'
          view' = updPath view sPath  b
      in  updPath view' sPath' a


type NPath = [Int]

locToPath :: CExpr -> NPath -> Dynamic
locToPath = locToPathCExpr

locToPathCExpr :: CExpr -> NPath -> Dynamic
locToPathCExpr src []   = toDyn src
locToPathCExpr src path =
  case (src, head path) of
    (CAdd x _ _, 0)    -> locToPathCExpr x (tail path)
    -- (CAdd _ _ _, 1) -> locToPathCTerm x (tail path)
    (CAdd _ _ y, 2)    -> locToPathCTerm y (tail path)
    (ToTerm t, 0)      -> locToPathCTerm t (tail path)


locToPathCTerm :: CTerm -> NPath -> Dynamic
locToPathCTerm src []   = toDyn src
locToPathCTerm src path =
  case (src, head path) of
    (CMul x _ _, 0)    -> locToPathCTerm   x (tail path)
    (CMul _ _ y, 2)    -> locToPathCFactor y (tail path)
    (ToFactor f, 0)    -> locToPathCFactor f (tail path)

locToPathCFactor :: CFactor -> NPath -> Dynamic
locToPathCFactor src [] = toDyn src
locToPathCFactor src path =
  case (src, head path) of
    (CNum n , 0) -> toDyn n
    (CParen _ x _, 1) -> locToPathCExpr x (tail path)



updPath :: CExpr -> NPath -> Dynamic -> CExpr
updPath = updPathCExpr


updPathCExpr :: CExpr -> NPath -> Dynamic -> CExpr
updPathCExpr _   []   twig = fromDyn twig ExprNull
updPathCExpr src path twig =
  case (src, head path) of
    (CAdd x lay1 y, 0)    -> CAdd (updPathCExpr x (tail path) twig) lay1 y
    (CAdd x lay1 y, 2)    -> CAdd x lay1 (updPathCTerm y (tail path) twig)
    (ToTerm t, 0)         -> ToTerm (updPathCTerm t (tail path) twig)


updPathCTerm :: CTerm -> NPath -> Dynamic -> CTerm
updPathCTerm _   []   twig = fromDyn twig TermNull
updPathCTerm src path twig =
  case (src, head path) of
    (CMul x lay1 y, 0)    -> CMul (updPathCTerm x (tail path) twig) lay1 y
    (CMul x lay1 y, 2)    -> CMul x lay1 (updPathCFactor y (tail path) twig)
    (ToFactor f, 0)      -> ToFactor (updPathCFactor f (tail path) twig)

updPathCFactor :: CFactor -> NPath -> Dynamic -> CFactor
updPathCFactor _   []   twig = fromDyn twig FactorNull
updPathCFactor src path twig =
  case (src, head path) of
    (CNum (_, lay1) , 0) -> CNum (fromDyn twig "0", lay1)
    (CParen lay1 e lay2, 1) -> CParen lay1 (updPathCExpr e (tail path) twig) lay2









{-
exprReorder :: Maybe (Path, Dynamic) -> (Path, Path) -> BiGUL CExpr CExpr
exprReorder Nothing _ = Replace
exprReorder (Just rcvEnv) (sPath, vPath) =
  Case [$(adaptive [| \ _ _ -> needReShape (sPath, vPath) dtEnv |])
          (\_ _ -> (reShape ExprNull vPath rcvEnv))

       ,$(normal [p| CAdd x _ y |] [p| (AAdd x y) |]
                 [p| (CAdd _ _ _) |]) $
          $(update   [p| (x, y) |] [p| (AAdd x y) |]
                     [d| x = exprReorder (sPath ++ [("CAdd", 0)], vPath ++ [("AAdd", 0)]) rcvEnv ;
                         y = termReorder (sPath ++ [("CAdd", 2)], vPath ++ [("AAdd", 1)]) rcvEnv ; |])

       -- ,$(normal [| \(ToTerm _) (ASub (ANum 0) _) -> True |]
       --           [p| (ToTerm _) |]) $
       --    $(update [p| ToTerm e |] [p| e |]
       --               [d| e = termReorder (sPath ++ [("ToTerm",0)], vPath) dtEnv rcvEnv ; |])

       -- ,$(normal [| \(CSub _ _ _) (ASub x y) -> True |]
       --           [p| (CSub _ _ _) |]) $
       --    $(update [p| CSub x _ y |] [p| (ASub x y) |]
       --               [d| x = exprReorder (sPath ++ [("CSub" ,0)], vPath ++ [("ASub", 0)]) dtEnv rcvEnv ;
       --                   y = termReorder (sPath ++ [("CSub" ,2)], vPath ++ [("ASub", 1)]) dtEnv rcvEnv ; |])

       ,$(normal [| \(_, (ToTerm _)) _ -> True |]
                 [p| (_, ToTerm _) |]) $
          $(rearrS [| \(dtEnv, ToTerm e) -> (dtEnv, e) |]) $
            $(update   [p| eEnv |] [p| eEnv |]
                       [d| eEnv = termReorder (sPath ++ [("ToTerm" , 0)], vPath) rcvEnv ; |])

       ,$(adaptive [| \_ (AAdd _ _) -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv, CAdd ExprNull " + " TermNull))
       -- ,$(adaptive [| \_ (ASub (ANum 0) _) -> True |])
       --    (\ _ _ ->  ToTerm TermNull)
       -- ,$(adaptive [| \_ (ASub x y) -> True |])
       --    (\ _ _ ->  CSub ExprNull " - " TermNull)
       ,$(adaptive [| \_ _ -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv , ToTerm TermNull))]


termReorder :: RecoverEnv -> (Path, Path) -> BiGUL CTerm CTerm
termReorder rcvEnv (sPath, vPath) =
  Case [$(adaptive [| \ (dtEnv, _) _ -> needReShape (sPath, vPath) dtEnv |])
          (\(dtEnv , _) _ -> (Map.delete (vPath,sPath) dtEnv, reShape TermNull vPath rcvEnv))

       -- ,$(normal [| \(CMul _ _ _) (AMul x y) -> True |]
       --           [p| (CMul _ _ _) |]) $
       --    $(update [p| CMul x _ y |] [p| (AMul x y) |]
       --               [d| x = termReorder   (sPath ++ [("CMul", 0)], vPath ++ [("AMul", 0)]) rcvEnv;
       --                   y = factorReorder (sPath ++ [("CMul", 2)], vPath ++ [("AMul", 1)]) rcvEnv; |])

       -- ,$(normal [| \(CDiv _ _ _) (ADiv x y) -> True |]
       --           [p| (CDiv _ _ _) |]) $
       --    $(update [p| CDiv x _ y |] [p| (ADiv x y) |]
       --               [d| x = termReorder   (sPath ++ [("CDiv", 0)], vPath ++ [("ADiv", 0)]) rcvEnv;
       --                   y = factorReorder (sPath ++ [("CDiv", 2)], vPath ++ [("ADiv", 1)]) rcvEnv ; |])

       ,$(normal [| \(_ , ToFactor _) _ -> True |]
                 [p| (_ , ToFactor _) |]) $
          $(rearrS [| \(dtEnv, ToTerm e) -> (dtEnv, e) |]) $
            $(update   [p| eEnv |] [p| eEnv |]
                       [d| eEnv = factorReorder (sPath ++ [("ToFactor" , 0)], vPath) rcvEnv ; |])
       -- ,$(adaptive [| \_ (AMul x y) -> True |])
       --    (\ _ _ ->  CMul TermNull " * " FactorNull)
       -- ,$(adaptive [| \_ (ADiv x y) -> True |])
       --    (\ _ _ ->  CDiv TermNull " / " FactorNull)
       ,$(adaptive [| \_ _ -> True |])
          (\ (dtEnv , _) _ -> (dtEnv , ToFactor FactorNull))]




factorReorder :: RecoverEnv -> (Path, Path) -> BiGUL CFactor CFactor
factorReorder rcvEnv (sPath, vPath) =
  Case [$(adaptive [| \ (dtEnv, _) _ -> needReShape (sPath, vPath) dtEnv |])
          (\(dtEnv, _) _ -> (Map.delete (vPath,sPath) dtEnv, reShape FactorNull vPath rcvEnv))
          -- the value looked up from rcvEnv should be changed along with the function execution.
          -- previously we have AAdd .. ---> ToFactor (CParen " ( " ts00 " ) " ) of type Term.
          -- but now, we go into another function, which gives a default term FactorNull, which
          -- if of type Factor... type diffs, conversion fails.
          -- cause: condition s /= reShape FactorNull vPath rcvEnv   is not correct.

       -- ,$(normal [| \(CNeg _ _) (ASub (ANum 0) y) -> True |]
       --           [p| (CNeg _ _) |]) $
       --    $(update [p| CNeg _ y |] [p| (ASub (ANum 0) y) |]
       --               [d| y = factorReorder (sPath ++ [("CNeg", 1)], vPath ++ [("ASub", 1)]) rcvEnv ; |])

       ,$(normal [| \(_ , CNum _) (ANum _) -> True |]
                 [p| (_ , CNum _) |]) $
          $(update   [p| (_ , CNum (n, _)) |] [p| (ANum n) |]
                     [d| n = replaceStrInt ; |])

       ,$(normal [| \(_ , CParen _ _ _) _ -> True |]
                 [p| (_ , CParen _ _ _) |]) $
          $(rearrS [| \(dtEnv , CParen _ e _) -> (dtEnv , e)  |]) $
            $(update   [p| eEnv |] [p| eEnv |]
                       [d| eEnv = exprReorder (sPath ++ [("CParen", 1)], vPath) rcvEnv ; |])

       -- ,$(adaptive [| \_ (ASub (ANum 0) y) -> True |])
       --    (\ _ _ ->  CNeg "-" FactorNull)
       ,$(adaptive [| \_ (ANum _) -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv , CNum ("0", " ")) )
       ,$(adaptive [| \_ _ -> True |])
          (\ (dtEnv, _) _ ->  (dtEnv , CParen "(" ExprNull ")"))
  ]

-}