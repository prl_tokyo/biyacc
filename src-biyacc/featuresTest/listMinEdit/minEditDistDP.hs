{-# Language TemplateHaskell, TypeFamilies #-}

module BX.BXHelpers where

import Generics.BiGUL
import Data.List
import Generics.BiGUL.Interpreter
import Generics.BiGUL.TH
import Control.Arrow ((***))
import Data.Maybe
import Data.Array.ST
import Data.Array
import Control.Monad.ST
import Control.Monad
import Debug.Trace
import Text.Show.Pretty

(==>) = ($)

lensMap :: (Show a, Show b) => a -> BiGUL a b -> BiGUL [a] [b]
lensMap defSrc b =
  Case  [ $(normalSV [p| _:_ |] [p| _:_ |]
                     [p| _:_ |])
          ==> $(update [p| x:xs |] [p| x:xs |] [d| x = b; xs = lensMap defSrc b |])
        , $(adaptiveSV [p| _:_ |] [p| [] |] ) (\_ _ -> [])
        , $(adaptiveSV [p| [] |] [p| _:_ |])
          ==> \_ _ -> [ defSrc ]
        , $(normalSV [p| [] |] [p| [] |]
                     [p| [] |])
          ==> $(update [p| [] |] [p| [] |] [d| |])
        ]

minEditDistLens :: (Show s, Show v, Eq s, Eq v) => s -> BiGUL s v -> BiGUL [s] [v]
minEditDistLens defSrc bx = Case
    [ $(adaptive [| \ s v -> length s /= length v  || structureEdit defSrc bx s v /= s |])
      ==> structureEdit defSrc bx
    ,
     $(normalSV [p| _ |] [p| _ |] [p| _ |])
      ==> lensMap defSrc bx
    ]

data Operation = OpModify | OpInsert | OpDelete | OpNothing deriving (Show, Eq, Ord)

minEditDistDP :: (Eq a) => [a] -> [a] -> Array (Int, Int) (Int, Operation)
minEditDistDP s v = runST $ do
    let (lenS, lenV) = (length s, length v)
    arrS <- newSTListArray (1, lenS) s
    arrV <- newSTListArray (1, lenV) v
    f <- newArray ((0,0), (lenS, lenV)) (-1, OpInsert) :: ST s (STArray s (Int, Int) (Int, Operation))
    forM_ [0 .. lenV] (\i -> writeArray f (0, i) (i, OpInsert))
    forM_ [0 .. lenS] (\i -> writeArray f (i, 0) (i, OpDelete))
    writeArray f (0,0) (0, OpNothing)
    forM_ (range ((1,1), (lenS, lenV))) (\(i, j) -> do
        si <- readArray arrS i
        sj <- readArray arrV j
        if si == sj
           then readArray f (i-1, j-1) >>= \p -> writeArray f (i, j) (modifySnd OpNothing p)
           else do
               cModify <- readArray f (i-1, j-1)
               cInsert <- readArray f (i, j-1)
               cDelete <- readArray f (i-1, j)
               writeArray f (i, j) ((\(x, y) -> (x+1, y)) (minimum [ modifySnd OpModify cModify,
                                                                     modifySnd OpInsert cInsert,
                                                                     modifySnd OpDelete cDelete ]) ))
    freeze f
    where newSTListArray :: (Ix i) => (i,i) -> [a] -> ST s (STArray s i a)
          newSTListArray = newListArray
          modifySnd x (a, b) = (a, x)

structureEdit :: (Show s, Show v, Eq s, Eq v) => s -> BiGUL s v -> [s] -> [v] -> [s]
structureEdit defSrc bx s v =
  let sv = get (lensMap defSrc bx) s
  in if sv == Nothing
    then  s
    else  let f = minEditDistDP (fromJust sv) v
              reconstruct 0 0 [] = []
              reconstruct i j s = case snd (f ! (i,j)) of
                  OpNothing -> head s : reconstruct (i-1) (j-1) (tail s)
                  OpModify -> head s : reconstruct (i-1) (j-1) (tail s)
                  OpInsert -> defSrc : reconstruct i (j-1) s
                  OpDelete -> reconstruct (i-1) j (tail s)
          in reverse $ reconstruct (length s) (length v) (reverse s)

-- FIXME: the following maybe more safe
-- OpInsert -> fromJust (put bx (defSrc v) v) : reconstruct i (j-1) s
