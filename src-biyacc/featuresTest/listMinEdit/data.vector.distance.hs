import Data.List as List
import Data.Vector.Distance
import Data.Vector
import Data.Monoid

data RTree a = RTree a [RTree a] deriving (Show, Eq, Ord)

param1 :: (Eq v, Show v) => Params v (String, Int, String) (Sum Int)
param1 = Params eqv del ins sub cost posOff
  where
    eqv = (==)
    del i c = ("del", i, "elem: " List.++ show c)
    ins i c = ("ins", i, "elem: " List.++ show c)
    sub i c c' = ("replace", i, "elem: " List.++ show c')
    cost _ = 1
    posOff ("del", _ ,_ ) = -1
    posOff ("ins", _ ,_ ) = 1
    posOff ("replace", _ ,_ ) = 0

data Operation = Ins | Del | Subst deriving (Show, Eq)

-- test1 = leastChanges t1 t2

-- test2 = allChanges t1 t2

t1v = fromList [t1]

t2v = fromList [t2]

t1 :: RTree Int
t1 = RTree 0
  [RTree 1 [], RTree 2
                [RTree 20 [], RTree 30
                                [RTree 40 []]]  , RTree 3 []]

t2 :: RTree Int
t2 = RTree 0
  [RTree 2
                [RTree 20 [], RTree 30
                                [RTree 40 []]]  , RTree 1 [],  RTree 3 [], RTree 2 []]

lft1 = [0,1,2,20,30,40,3]

lft2 = [0,2,20,30,40,1,3,2]

subTrees :: RTree a -> [RTree a]
subTrees (RTree a sbs) = sbs

leaves :: RTree a -> [a]
leaves (RTree a sbs) = [a] List.++ List.concatMap leaves sbs

ts1 = fromList "abcd123"
ts2 = fromList "1bcda23"
