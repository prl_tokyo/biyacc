import Generics.BiGUL.TH
import Generics.BiGUL
import Generics.BiGUL.Interpreter

import Data.Maybe

lensMap :: (Show a, Show b, Eq b) => a -> BiGUL a b -> BiGUL [a] [b]
lensMap defSrcVal b = Case
  [ $(normalSV  [p| [] |]  [p| [] |] [p| [] |]) $ Skip (const [])

  , $(adaptiveSV [p| _:_ |]  [p| [] |])
            (\_ _ -> [])

  , $(adaptiveSV [p| [] |]  [p| _:_ |])
            (\_ v -> [defSrcVal])

  , $(normalSV  [p| _:_ |]  [p| _:_ |] [p| _:_ |])
      $(update  [p| x:xs |] [p| x:xs |]
                [d| x = b; xs = lensMap defSrcVal b |])
  ]

-- RuntimeEnv -> BiGUL (String, (Expr, (Dummy, Dummy))) (Arith, (Int, Int))
lensTuple :: (Show a, Show b, Show c, Show d) => BiGUL a c -> BiGUL b d -> BiGUL (a, b) (c,d)
lensTuple bf bg =
  $(update  [p| (ac, bd) |] [p| (ac, bd) |]
            [d| ac = bf;
                bd = bg |])

lensTrile :: (Show a, Show b, Show c, Show d, Show e, Show f) =>
  BiGUL a d -> BiGUL b e -> BiGUL c f -> BiGUL (a, (b, c)) (d, (e,f))
lensTrile bf bg bh =
  $(update  [p| (ad, (be, cf)) |] [p| (ad, (be, cf)) |]
            [d| ad = bf;
                be = bg;
                cf = bh |])



lensMap' :: (Show a, Show b, Eq b) => BiGUL a b -> BiGUL [a] [b]
lensMap' b = Case
  [ $(normalSV  [p| [] |]  [p| [] |] [p| [] |]) $ Skip (const [])

  , $(adaptiveSV [p| _:_ |]  [p| [] |])
            (\_ _ -> [])

  , $(adaptiveSV [p| [] |]  [p| _:_ |])
            (\_ v -> [undefined])

  , $(normalSV  [p| _:_ |]  [p| _:_ |] [p| _:_ |])
      $(update  [p| x:xs |] [p| x:xs |]
                [d| x = b; xs = lensMap' b |])
  ]

-- bigulUnZip :: (Eq a, Show a, Eq b, Show b) => BiGUL [(a,b)] ([a],[b])
-- bigulUnZip = emb (unzip) (\_ v -> uncurry zip v)

bigulUnZip :: (Eq a, Show a, Eq b, Show b) => BiGUL [(a,b)] ([a],[b])
bigulUnZip = Case
  [ $(normalSV [p| [] |] [p| ([],[]) |] [p| [] |]) $ Skip (const ([],[]))

  , $(normalSV [p| _:_ |]  [p| (_:_, _:_) |] [p| _:_ |]) $
      $(rearrV [| \(a:as,b:bs) -> ((a,b), (as,bs))  |])
        $(update [p| (a,b):abss |] [p| ((a,b),abss) |]
                 [d| a = Replace;
                     b = Replace;
                     abss = bigulUnZip |])
  , $(adaptiveSV [p| [] |]  [p| (_:_, _:_) |])
      (\_ (a:_,b:_) -> [(a,b)])
  , $(adaptiveSV [p| _:_ |]  [p| ([],[]) |])
      (\_ _ -> [])
  ]

-- bigulUnZip3 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c) => BiGUL [(a,b,c)] ([a],[b],[c])
-- bigulUnZip3 = Case
--   [ $(normalSV [p| [] |] [p| ([],[],[]) |] [p| [] |]) $ Skip (const ([],[],[]))

--   , $(normalSV [p| _:_ |]  [p| (_:_ , _:_ , _:_) |] [p| _:_ |]) $
--       $(rearrV [| \(a:as, b:bs, c:cs) -> ((a,b,c), (as,bs,cs)) |])
--         $(update [p| (a,b,c):abcss |] [p| ((a,b,c),abcss) |]
--                  [d| a = Replace;
--                      b = Replace;
--                      c = Replace;
--                      abcss = bigulUnZip3 |])
--   , $(adaptiveSV [p| [] |]  [p| (_:_, _:_, _:_) |])
--       (\_ (a:_ , b:_ , c:_) -> [(a,b,c)])
--   , $(adaptiveSV [p| _:_:_ |]  [p| ([],[],[]) |])
--       (\_ _ -> [])
--   ]

putRightNested3 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c) => BiGUL (a,(b,c)) (a,b,c)
putRightNested3 = emb (\(a,(b,c)) -> (a,b,c)) (\_ (a,b,c) -> (a,(b,c)))


bigulUnZip3 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c) => BiGUL [(a,(b,c))] ([a],([b],[c]))
bigulUnZip3 = Case
  [ $(normalSV [p| [] |] [p| ([],([],[])) |] [p| [] |]) $ Skip (const ([],([],[])))

  , $(normalSV [p| _:_ |]  [p| (_:_ , (_:_ , _:_)) |] [p| _:_ |]) $
      $(rearrV [| \(a:as, (b:bs, c:cs)) -> ((a,(b,c)), (as,(bs,cs))) |])
        $(update [p| (a,(b,c)):abcss |] [p| ((a,(b,c)),abcss) |]
                 [d| a = Replace;
                     b = Replace;
                     c = Replace;
                     abcss = bigulUnZip3 |])
  , $(adaptiveSV [p| [] |]  [p| (_:_, (_:_, _:_)) |])
      (\_ (a:_ , (b:_ , c:_)) -> [(a,(b,c))])
  , $(adaptiveSV [p| _:_:_ |]  [p| ([],([],[])) |])
      (\_ _ -> [])
  ]





bb = emb gg ff
  where
    gg :: Int -> Bool
    gg 0 = False
    gg 1 = True

    ff :: Int -> Bool -> Int
    ff _ True = 1
    ff _ False = 0


emb g p = Case
  [ $(normal [| \s v -> g s == v |] [p| _ |])
    $ Skip g
  , $(adaptive [| \s v -> True |])
    $ p
  ]