{-# Language TemplateHaskell, QuasiQuotes #-}
module MatchList where

import GHC.Generics
import Generics.BiGUL.AST
import Generics.BiGUL.Interpreter
import Generics.BiGUL.TH
import Generics.BiGUL.Error

getLevel :: Int -> Int -> (Int, Int)
getLevel x y = if (y <= x) then (x, y) else getLevel (x + 1) (y - x - 1)

genPair :: Int -> (Int, Int)
genPair n = let (level, index) = getLevel 0 n
    in (index, level - index)

plist :: [(Int, Int)]
plist = fmap genPair [0..]

calcDelta :: (a -> b -> Bool) -> [a] -> [b] -> [Int]
calcDelta isMatch ss vv = match ss vv (0, 0)
  where
    result = map2 (cost ss vv) plist
    get pr = lookup2 pr result
    lens = length ss
    lenv = length vv
    map2 f [] = []
    map2 f (c:cs) = (c, f c) : map2 f cs
    lookup2 pr (c:cs) =
      if (pr == fst c) then snd c else lookup2 pr cs
    cost ss vv (s, v)
      | (s >= lens) = 0
      | (v >= lenv) = 0
      | otherwise        =
        if (isMatch (ss !! s) (vv !! v))
          then (get (s + 1, v + 1)) + 1
          else max (get (s + 1, v)) (get (s, v + 1))
        where
          hs = head (drop s ss)
          hv = head (drop v vv)
    match ss vv (s, v)
      | (v >= lenv) = []
      | (s >= lens) = replicate (lenv - v) (-1)
      | otherwise   =
        if (isMatch (ss !! s) (vv !! v))
          then s : match ss vv (s + 1, v + 1)
          else if get (s + 1, v) > get (s, v + 1)
            then match ss vv (s + 1, v)
            else -1 : match ss vv (s, v + 1)

emb :: Eq v => (s -> v) -> (s -> v -> s) -> BiGUL s v
emb g p = Case
  [ $(normal [| \x y -> g x == y |])$
      $(rearrV [| \x -> ((), x) |])$
        Dep Skip (\x () -> g x)
  , $(adaptive [| \_ _ -> True |])
      p
  ]

getRight :: Either a b -> b
getRight (Right b) = b

put' :: BiGUL a b -> a -> (a -> b -> Bool) -> [a] -> [b] -> [a]
put' b create isMatch as bs = let delta = calcDelta isMatch as bs
             in fst (getRight (put (matchList b create) (as, delta) bs))

get' :: BiGUL a b -> a -> [a] -> [b]
get' b create as = getRight (get (matchList b create) (as, [0 .. (length as) - 1]))

cleverAlign :: Eq b => BiGUL a b -> a -> (a -> b -> Bool) -> BiGUL [a] [b]
cleverAlign b create isMatch = emb (get' b create) (put' b create isMatch)

isID :: Int -> [Int] -> Bool
isID n [] = True
isID n (c:cs) = n == c && isID (n + 1) cs

reOrder :: [a] -> [Int] -> a -> [a]
reOrder s [] create = []
reOrder s (c:cs) create =
  if (c == -1)
    then create : reOrder s cs create
    else (s !! c) : reOrder s cs create
    
listMap2 :: BiGUL a b -> BiGUL [a] [b]
listMap2 b = Case
  [ $(normalSV [p| [] |] [p| [] |]) $
      $(rearrV [| \[] -> ()|]) $ Skip
  ,
    $(normalSV [p| _:_ |] [p| _:_ |]) $
      $(update [p| x:xs |] [p| x:xs |] [d| x = b; xs = listMap2 b; |])
  ]

matchList :: BiGUL a b -> a -> BiGUL ([a], [Int]) [b]
matchList b create = Case
  [ $(normalS [| \s -> isID 0 (snd s) |]) $
      $(rearrS [| \(s, delta) -> s |]) $
        listMap2 b
  , $(adaptive [| \_ _ -> True |]) $
      \(s, delta) v -> (reOrder s delta create, [0 .. (length delta - 1)])
  ]

