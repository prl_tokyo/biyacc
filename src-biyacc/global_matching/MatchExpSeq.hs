{-# Language TemplateHaskell, QuasiQuotes #-}
module MatchExpSeq where

import GHC.Generics
import Generics.BiGUL.AST
import Generics.BiGUL.Interpreter
import Generics.BiGUL.TH
import Generics.BiGUL.Error
import YourLangDef
import MatchList
import Data.Typeable

flattenS :: ExpSeq -> [(Exp, String)]
flattenS (ExpSeq0 e c es) = (e, c) : flattenS es
flattenS (ExpSeq1 e) = [(e, "")]
flattenS (ExpSeqNull17) = []

unFlattenS :: [(Exp, String)] -> ExpSeq
unFlattenS [] = ExpSeqNull17
unFlattenS [(e, c)] = ExpSeq1 e
unFlattenS ((e, c):es) = ExpSeq0 e c (unFlattenS es)

flattenV :: Tiger -> [Tiger]
flattenV (TigerExpSeq e TigerSeqNil) = [e]
flattenV (TigerExpSeq e es) = e : flattenV es

unFlattenV :: [Tiger] -> Tiger
unFlattenV [] = TigerSeqNil
unFlattenV [e] = TigerExpSeq e TigerSeqNil
unFlattenV (e:es) = TigerExpSeq e (unFlattenV es)

matchExpSeq :: (Exp, String) -> Tiger -> Bool
matchExpSeq ((Exp1 s), _) (TigerTypeNil) = True
matchExpSeq ((Exp2 s), _) (TigerIntLit v) = (fst s == v)
matchExpSeq ((Exp3 s), _) (TigerStrLit v) = (fst s == v)
matchExpSeq ((Exp4 _), _) (TigerExpSeq _ _) = True 
matchExpSeq ((Exp4 _), _) (TigerEmptySeq) = True
matchExpSeq ((Exp5 _), _) (TigerNegation _) = True
matchExpSeq ((Exp11 _), _) (TigerWhile _ _) = True
matchExpSeq ((Exp12 _), _) (TigerFor _ _ _ _) = True
matchExpSeq ((Exp16 _), _) (TigerLet _ _) = True
matchExpSeq ((Exp0 _), _) (TigerLValueName _) = True
matchExpSeq ((Exp0 _), _) (TigerLValueOther _) = True
matchExpSeq ((Exp7 _), _) (TigerMul _ _) = True
matchExpSeq ((Exp7 _), _) (TigerDiv _ _) = True
matchExpSeq ((Exp7 _), _) (TigerAdd _ _) = True
matchExpSeq ((Exp7 _), _) (TigerSub _ _) = True
matchExpSeq ((Exp7 _), _) (TigerEq  _ _) = True
matchExpSeq ((Exp7 _), _) (TigerNEq _ _) = True
matchExpSeq ((Exp7 _), _) (TigerGT  _ _) = True
matchExpSeq ((Exp7 _), _) (TigerLT  _ _) = True
matchExpSeq ((Exp7 _), _) (TigerGTE _ _) = True
matchExpSeq ((Exp7 _), _) (TigerLTE _ _) = True
matchExpSeq ((Exp13 _), _) (TigerIfThenElse _ _ TigerSeqNil) = True
matchExpSeq ((Exp7 (InfixExp10 _ _ _)), _) (TigerIfThenElse _ _ (TigerIntLit 0)) = True
matchExpSeq ((Exp7 (InfixExp11 _ _ _)), _) (TigerIfThenElse _ (TigerIntLit 1) _) = True
matchExpSeq ((Exp14 _), _) (TigerIfThenElse _ _ _) = True
matchExpSeq _ _ = False

putExpSeq :: BiGUL Exp Tiger -> (Exp, String) -> ExpSeq -> Tiger -> ExpSeq
putExpSeq b create s v = 
  let delta = calcDelta matchExpSeq s' v'
  in unFlattenS (fst (getRight (put (matchList b' create) (s', delta) v')))
  where
    s' = flattenS s
    v' = flattenV v
    b' = $(rearrS [| \(e, c) -> e |]) b

getExpSeq :: BiGUL Exp Tiger -> (Exp, String) -> ExpSeq -> Tiger
getExpSeq b create s = unFlattenV (getRight (get (matchList b' create) (s', [0 .. (length s') - 1])))
  where
    s' = flattenS s
    b' = $(rearrS [| \(e, c) -> e |]) b

bigulTiger2 :: BiGUL Exp Tiger -> Exp -> BiGUL ExpSeq Tiger
bigulTiger2 b create = emb (getExpSeq b (create, "")) (putExpSeq b (create, ""))

-- Test Cases --
ts = [Exp2 (1, " "), Exp2 (2, "  "), Exp2 (3, "   "), Exp2 (4, "    "), Exp2 (5, "     ")]
tv = [TigerIntLit 1, TigerIntLit 2, TigerIntLit 4, TigerIntLit 5]
prog :: BiGUL Exp Tiger
prog = $(update [p| TigerIntLit i |] [p| Exp2 (i, _) |] [d| i = Replace; |])
