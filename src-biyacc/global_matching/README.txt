Support global sequence matching strategy between CST and AST. Due to the comlexity in implementation, The code is not automatically generated. Thus, you should modify the 's1.bigul.program.hs' toinclude the new strategy. Changes to 's1.bigul.program.hs' includes:

1. [Import an additonal module]
import MatchExpSeq

2. [Rewrite this function]
bigulTigerExpSeq = bigulTiger2 bigulTigerExp (Exp1 "")


Here's a simple testcase. The original source code contains 5 expressions. In AST, we delete the second one. When put-back transformation is performed, the second expression disappears, while the layout and comment of other expressions are preserved.

----- Original Source Code -----
( 1     /* 1 */ ; 
  "2"   /* 2 */ ;
  name3 /* 3 */ ;
  4     /* 4 */ ;
  if 1 < 5 then name3 := 3 /* 5 */ 
)
----- Updated AST --------------
TigerExpSeq (TigerIntLit 1) (TigerExpSeq (TigerLValueName (TigerName "name3")) (TigerExpSeq (TigerIntLit 4) (TigerExpSeq (TigerIfThenElse (TigerLT (TigerIntLit 1) (TigerIntLit 5)) (TigerAssign (TigerLValueName (TigerName "name3")) (TigerIntLit 3)) TigerSeqNil) TigerSeqNil)))
----- Updated Source Code -----
( 1     /* 1 */ ; 
  name3 /* 3 */ ;
  4     /* 4 */ ;
  if 1 < 5 then name3 := 3 /* 5 */ 
)--------------------------------
