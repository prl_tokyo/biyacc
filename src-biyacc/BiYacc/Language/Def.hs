{-# Language DeriveDataTypeable, ViewPatterns, GADTs #-}

module BiYacc.Language.Def where

import Text.Parsec
import Text.Parsec.Token

import Data.Map as Map hiding (map)
import Data.Generics

import Debug.Trace
import Control.Monad.State (MonadState)

import qualified Text.PrettyPrint as TPP

langDef :: Monad m => GenLanguageDef String u m
langDef = LanguageDef
  {
    commentStart = "{-"
   ,commentEnd = "-}"
   ,commentLine ="--"
   ,nestedComments = True
   ,identStart = upper
   ,identLetter = alphaNum <|> char '_'
   ,opStart = oneOf ":!#$%&*+./<=>?@\\^|-~"
   ,opLetter = oneOf ":!#$%&*+./<=>?@\\^|-~"
   ,reservedNames = ["#Abstract", "#Concrete", "#Directives","#Actions"
                    ,"#OtherFilters", "Adaptive" ]
   ,reservedOpNames = [""]
   ,caseSensitive = True
  }

lexer :: Monad m => GenTokenParser String u m
lexer = makeTokenParser langDef


primTypes :: [String]
primTypes =  ["Identifier", "String", "Numeric"]

biPrimTypes :: [String]
biPrimTypes = ["BiIdentifierTy", "BiStringTy", "BiNumericTy"]


-- apply lexeme to a parser to make the parser skip all the following white spaces
byLexeme :: Monad m => ParsecT String u m a -> ParsecT String u m a
byLexeme = lexeme lexer

byWhiteSpace :: Monad m => ParsecT String u m ()
byWhiteSpace = whiteSpace lexer

--will change char literal to string literal
terminal :: Monad m => ParsecT String u m String
terminal = byLexeme
  (try (charLiteral lexer >>= \c -> return [c]) <|>
  charLitParser' <|>
  (stringLiteral lexer)
  <?> "want a terminal. Use doublequotes for a string.")

charLitParser' :: Monad m => ParsecT String u m String
charLitParser' = do
  char '\''
  str <- manyTill anyChar (try (string "'"))
  return str


nonterminal :: Monad m => ParsecT String u m String
nonterminal = byLexeme (identifier lexer) <?> "want a nonterminal."

------- basic (primitive type) parser
literal :: Monad m => ParsecT String u m LitPat
literal = biStrLit <|> biNumericLit

-- string literal. will change char literal to string literal
biStrLit :: Monad m => ParsecT String u m LitPat
biStrLit = byLexeme $
  (charLiteral lexer >>= \c -> return (LitStr [c])) <|>
  (stringLiteral lexer  >>= \s -> return (LitStr s))

biNumericLit :: Monad m => ParsecT String u m LitPat
biNumericLit = byLexeme (do
  choice [ try (integer lexer) >>= \i -> return (LitInt i)
         ,     (float lexer)   >>= \f -> return (LitDecimal f)
         ])

-- parse biyacc primitive (predefined) nonterminals
pBiPrimNont :: Monad m => ParsecT String u m String
pBiPrimNont = byLexeme (choice (map string primTypes) >>= \r ->
                        lookAhead (many1 space) >> return r )


data BiTuple a b = BiTuple a b
  deriving (Show, Read, Eq)

---------- data types for abstrac syntax
type DataTypeDecs   = [DataTypeDec]

-- DataType type definitions.  DataType "Arith" [...]
data DataTypeDec    = DataTypeDec  DataTypeRep [DataTypeDef] deriving (Typeable, Data, Show, Eq, Read)

-- TypeDef constructor [types]
data DataTypeDef = DataTypeDef String [DataTypeRep] deriving (Typeable, Data, Show, Eq, Read)

data DataTypeRep =
    TyCon String [DataTypeRep]
  | TyVar String
  deriving (Typeable, Data, Show, Eq, Read, Ord)

----------- data types for concrete syntax
-- NontDef type definitions. NontDef "Expr" [...]
data NontDef = NontDef Nonterminal [Prod]
  deriving (Typeable, Data, Show, Eq, Ord)

type NontDefs = [NontDef]
type Nonterminal = String
type Terminal = String

-- Prod constructor [types]
data Prod = Prod ProdCons [ProdField] (Maybe BracketAttr)
  deriving (Typeable, Data, Show, Eq, Ord)

-- either it is a string terminal or datatypes.
-- Nonterminal is reserved when finishing parsing and used when building the env. It is deleted later.
-- Left: string Terminal and primitive types: Numeric, Identifier, String. Right: others
type ProdField = Either Terminal Nonterminal

-- disambiguation annotations. %left, %right, %nonassoc ...
data Cmd =
    CmdLAssoc   ProdCons
  | CmdRAssoc   ProdCons
  | CmdNonAssoc ProdCons
  | CmdPatMatch Pattern             -- this pattern is forbidden
  | CmdPrio     (ProdCons,ProdCons) -- L > R
  | CmdCommLine  String
  | CmdCommBlock String String
  deriving (Typeable, Data, Show, Eq)

-- (["Expr", "+", "Term"],"Expr") ---> "A0"
type Pat2ConsEnv   = Map ([ProdField], ProdRuleType) ProdCons

-- ["Expr", "+", "Term"] ---> "A0" ? return the answer for production rules with unique RHS
type PartialPat2ConsEnv  = Map [ProdField] ProdCons

-- "A0"    ---> (["Expr", "+", "Term"],"Expr")
type Cons2PatEnv   = Map ProdCons ([ProdField],ProdRuleType)

--  Add ---> ([Arith, Arith], Arith)
-- constructor ([subsequent fields' types], constructor's type)
type Cons2FieldsEnv = Map String ([DataTypeRep], DataTypeRep)

-- "Expr"  ---> ["A0","A1","A2"]     (i.e. [Constructor Names])
type TypeNameEnv   = Map ProdRuleType [ProdCons]

type SynonymMap = Map DataTypeRep DataTypeRep


-- in a grammar, only one production rule can be the “bracket attribute”.
data BracketAttr = BracketAttr deriving (Typeable, Data, Show, Eq, Ord)
type BracketAttrEnv = Maybe ProdCons

type Path = [ProdCons]
type PathViaBracket = Map ProdCons Path

--------datatype defs for Actions part---------------
-- here the (ViewType, SourceType) stores types to be used for entrance of the program
-- (the types of the first action group)
data Program  = Program (ViewType, SourceType) [Group]
  deriving (Typeable, Data, Show, Eq)
data Group    = Group (ViewType, SourceType) [Rule]
  deriving (Typeable, Data, Show, Eq)

type SourceType    = DataTypeRep
type ViewType      = DataTypeRep

type Rule =  (ViewSide,SrcSide)

-- the left hand side of a rule is just represented by a pattern (matching by patterns)
-- ViewSide (type of the pattern) pattern
type ViewSide = Pattern

data Pattern
  = ConP String DataTypeRep [Pattern]
    -- constructor pattern: constructor name, type, sub patterns
  | AsP String DataTypeRep Pattern
  -- AsP: @. as-what-name; type
  | VarP String DataTypeRep  -- variable name; variable type
  | LitP LitPat    -- literal; type
  | WildP DataTypeRep
  deriving (Typeable, Data, Show, Eq)

data LitPat = LitStr String
            | LitChr Char
            | LitInt Integer
            | LitDecimal Double
  deriving (Typeable, Data, Show, Eq)

-- the right hand side of a rule is merely a production rule plus some updates somewhere.
data SrcSide = SrcSide SourceType ProdCons [UpdateUnit]
  deriving (Typeable, Data, Show, Eq)

type UpdateUnit = Either Terminal Update

data Update = NUpdate Var SourceType
            | NUpdateWithLookAhead Var SourceType SrcSide
            | Expansion  SrcSide  -- deep update. deep pattern match
            | NoUpdate SourceType -- Nontemrinals in the Expansion.
  deriving (Typeable, Data, Show, Eq)

type ProdRuleType = String
type ProdCons = String
type Var     = String

type BiGULGenEnv = (Cons2PatEnv, Cons2FieldsEnv)

-----------------------
eatLParen :: Monad m => ParsecT String u m Char
eatLParen = byLexeme (char '(' )

eatRParen :: Monad m => ParsecT String u m Char
eatRParen = byLexeme (char ')' )

eatLBrack :: Monad m => ParsecT String u m Char
eatLBrack = byLexeme (char '[' )

eatRBrack :: Monad m => ParsecT String u m Char
eatRBrack = byLexeme (char ']' )

eatLBrace :: Monad m => ParsecT String u m Char
eatLBrace = byLexeme (char '{')

eatRBrace :: Monad m => ParsecT String u m Char
eatRBrace = byLexeme (char '}')

eatWildCard :: Monad m => ParsecT String u m Char
eatWildCard = byLexeme (char '_')

eatListCons :: Monad m => ParsecT String u m Char
eatListCons = byLexeme (char ':')

eatEmbed    :: Monad m => ParsecT String u m String
eatEmbed    = byLexeme (string "+>")

eatLineEnd  :: Monad m => ParsecT String u m Char
eatLineEnd  = byLexeme (char ';')

eatAs       :: Monad m => ParsecT String u m Char
eatAs       = byLexeme (char '@')

eatTo :: Monad m => ParsecT String u m String
eatTo = byLexeme (string "->")

eatComma :: Monad m => ParsecT String u m Char
eatComma = byLexeme (char ',')

eatOr :: Monad m => ParsecT String u m Char
eatOr = byLexeme (char '|')

eatSemi :: Monad m => ParsecT String u m Char
eatSemi = eatLineEnd

eatColon :: Monad m => ParsecT String u m Char
eatColon = eatListCons <?> "want a colon (\":\")"


-- variable. starting with lower character
pVariable :: Monad m => ParsecT String u m String
pVariable = byLexeme (do
  fst <- lower
  s   <- many (alphaNum <|> char '_' )
  return (fst:s))

-- type constructor. starting with capital character
pConstructor :: Monad m => ParsecT String u m String
pConstructor = byLexeme (do
  fst <- upper
  s <- many (alphaNum <|> char '_')
  return (fst:s))
