module BiYacc.Main where

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.Parser.ActionParser
import BiYacc.Parser.ParserInterface

import BiYacc.Translation.Check
import BiYacc.Translation.GenDataTypes
import BiYacc.Translation.BX.GenBiGUL
import BiYacc.Translation.BX.BiGULRuntimeLib
import BiYacc.Translation.ISO.GenLexer
import BiYacc.Translation.ISO.GenParser
import BiYacc.Translation.ISO.GenPrinter
import BiYacc.Translation.TypeInf

import BiYacc.Translation.Filters.GenAddPar
import BiYacc.Translation.Filters.GenFilters
import BiYacc.Translation.Filters.TravelThrough
import BiYacc.Translation.Filters.TransitiveClosure

import Text.Parsec hiding (getPosition, newline)

import Control.Monad.Reader (runReader)
import System.Environment
import System.Process


main :: IO ()
main  = do
  args <- getArgs
  if length args < 2
    then error "please give input. usage: biyacc biyaccFileName outputExecutableName"
    else do
      putStrLn "compiling process may take to half a minute for a comparative big language."
      let bFile     = args !! 0
          exeFile   = args !! 1
          exeFileHS =  exeFile ++ ".hs"
      stream <- readFile bFile
      let eParseRes = runParser preProcess () bFile stream

          (tyDefStr, concrete, directives, actions, others) =
            either (error . show) id eParseRes

          (synonymMap, rawAbsTyDecls, rawNtDefs, cmds, rawActions, otherFilters) =
            parseAll (tyDefStr, concrete, directives, actions, others)
          csEnv = buildCon2FieldsEnv rawAbsTyDecls

      putStrLn $ allCheck rawNtDefs rawActions csEnv


      -- after check, refine the rawNtDefs and rawActions (e.g. add more type annotations)
      let (ntTyDecs, ntDefs) = processForFrontend $ rawNtDefs
          (synonymMap', absTyDecls, actions)  =
            processForBackend (synonymMap, rawAbsTyDecls, ntTyDecs, ntDefs, rawActions)
          (pcEnv, cpEnv, tyNameEnv) = buildEnv ntDefs
          topLevelSrcTy = extractTopLevelSrcTy ntDefs

      let progStr  = runReader (genBiGUL actions) (cpEnv,csEnv)
          entranceStr     = genEntrance actions
          srcDeclsStr     = prtConcDTs ntDefs
          absDeclsStr     = prtAbsDTs' absTyDecls
          cstInstanceShowStr   = genPrinter ntDefs

          brkEnv = buildBracketAttrEnv ntDefs
          addParInstances = genAddParIns tyNameEnv cpEnv pcEnv brkEnv
          filterCode = runReader (genFilters cmds otherFilters) cpEnv

      writeFile "BiFilter.hs" (foldr1 newlineSS
        [filterModHead
        ,render filterCode])

      writeFile "LangDef.hs" (foldr1 newlineSS
        [defFileHead
        ,absDeclsStr
        ,srcDeclsStr
        ,cstInstanceShowStr
        ,render genAddParClass
        ,render addParInstances
        ,genInjProdsStr ntDefs
        ,genBiYaccGeneric ntDefs
        ,listFunStr
        ])

      writeFile exeFileHS (foldr1 newlineSS
        [importStr
        ,entranceStr
        ,progStr
        ,numTyBiGULDefs
        ,mainStr])

      let parserStr = genLexerAndParser (cmds, rawNtDefs)

      writeFile ("ProgParser.y") parserStr
      writeFile ("ParserWrapper.hs") (genParserWrapper topLevelSrcTy)

      readProcess "happy" ["-i", "--glr", "--ghc", "--decode", "ProgParser.y"] ""
      -- readProcess "ghc" ["Parser.hs"] ""
      -- readProcess "ghc" ["LangDef.hs"] ""

      readProcess "ghc" ["-Wno-missing-methods", exeFileHS, "-o", exeFile] ""

      -- some cleaning work
      putStrLn $ "Nothing serious if the final callCommand failed." ++
                   "It just made some cleaning work."
      putStrLn $ "Information about shift/reduce conflicts can be found in ProgParser.info file."
                 ++ " (If there is any.)"
      let removeStr = ["rm"
                      ,"LangDef.dyn_o","LangDef.dyn_hi","LangDef.o","LangDef.hi","LangDef.hs"
                      ,"ProgParser.dyn_o","ProgParser.dyn_hi","ProgParser.o","ProgParser.hi","ProgParser.y", "ProgParser.hs"
                      ,"ParserWrapper.dyn_o","ParserWrapper.dyn_hi","ParserWrapper.o","ParserWrapper.hi", "ParserWrapper.hs"
                      ,exeFile++".dyn_o",exeFile++".dyn_hi",exeFile++".o",exeFile++".hi",exeFileHS]
      callCommand $ foldl1 (\xs x -> xs ++ " " ++ x) removeStr
      putStrLn $ "Successfully generated " ++ exeFile
