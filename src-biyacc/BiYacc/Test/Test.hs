module BiYacc.Test.TestFunctions where

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.Parser.ActionParser
import BiYacc.Parser.ParserInterface

import BiYacc.Translation.Check
import BiYacc.Translation.GenDataTypes
import BiYacc.Translation.BX.GenBiGUL
import BiYacc.Translation.BX.BiGULRuntimeLib
import BiYacc.Translation.ISO.GenLexer
import BiYacc.Translation.ISO.GenParser
import BiYacc.Translation.ISO.GenPrinter
import BiYacc.Translation.TypeInf

import BiYacc.Translation.Filters.GenAddPar
import BiYacc.Translation.Filters.GenFilters
import BiYacc.Translation.Filters.TravelThrough
import BiYacc.Translation.Filters.TransitiveClosure

import Text.Parsec hiding (getPosition, newline)
import Text.PrettyPrint (($+$),text)

import Control.Monad.Reader (runReader)
import System.Process

data TestCase =
    Expr
  | ExprAdapt
  | ExprAmb
  | ExprKleene
  | ExprKleene2
  | ExprNonlinear
  | Tiger
  | TigerAmb
  | TigerKleene
  | TigerAmbKleene
  | CLang

runTest :: TestCase -> IO ()
runTest inst = do
  let (prefix, bFile_) = case inst of
        Expr          -> ("BiYacc/Test/expr/", "expr.txt")
        ExprAdapt     -> ("BiYacc/Test/exprAdapt/", "exprAdapt.txt")
        ExprAmb       -> ("BiYacc/Test/exprAmb/", "exprAmb.txt")
        ExprKleene    -> ("BiYacc/Test/exprKleene/", "exprKleene.txt")
        ExprKleene2   -> ("BiYacc/Test/exprKleene2/", "exprKleene2.txt")
        ExprNonlinear -> ("BiYacc/Test/exprNonlinear/", "exprNonlinear.txt")

        Tiger          -> ("BiYacc/Test/tiger/", "tiger.txt")
        TigerAmb       -> ("BiYacc/Test/tigerAmb/", "tigerAmb.txt")
        TigerKleene    -> ("BiYacc/Test/tigerKleene/", "tigerKleene.txt")
        TigerAmbKleene -> ("BiYacc/Test/tigerAmbKleene/", "tigerAmbKleene.txt")

        CLang          -> ("BiYacc/Test/C/", "C.txt")
  let bFile = prefix ++ bFile_
  stream <- readFile bFile
  let eParseRes = runParser preProcess () bFile stream

      (tyDefStr, concrete, directives, actions, others) =
        either (error . show) id eParseRes

      (synonymMap, rawAbsTyDecls, rawNtDefs, cmds, rawActions, otherFilters) =
        parseAll (tyDefStr, concrete, directives, actions, others)
      csEnv = buildCon2FieldsEnv rawAbsTyDecls

  putStrLn $ allCheck rawNtDefs rawActions csEnv

  -- after check, refine the rawNtDefs and rawActions (e.g. add more type annotations)
  let (ntTyDecs, ntDefs) = processForFrontend $ rawNtDefs
      (synonymMap', absTyDecls, actions)  =
        processForBackend (synonymMap, rawAbsTyDecls, ntTyDecs, ntDefs, rawActions)
      (pcEnv, cpEnv, tyNameEnv) = buildEnv ntDefs
      topLevelSrcTy = extractTopLevelSrcTy ntDefs

  let progStr  = runReader (genBiGUL actions) (cpEnv,csEnv)
      entranceStr     = genEntrance actions
      srcDeclsStr     = prtConcDTs ntDefs
      absDeclsStr     = prtAbsDTs' absTyDecls
      cstInstanceShowStr   = genPrinter ntDefs

      brkEnv = buildBracketAttrEnv ntDefs
      addParInstances = genAddParIns tyNameEnv cpEnv pcEnv brkEnv
      filterCode = runReader (genFilters cmds otherFilters) cpEnv

  let outputDir = prefix ++ "output/"

  readProcessWithExitCode "mkdir" [outputDir] ""
  writeFile (outputDir ++ "BiFilter.hs") (foldr1 newlineSS
    [filterModHead
    ,render filterCode])

  writeFile (outputDir ++ "LangDef.hs") (foldr1 newlineSS
    [defFileHead
    ,absDeclsStr
    ,srcDeclsStr
    ,cstInstanceShowStr
    ,render genAddParClass
    ,render addParInstances
    ,genInjProdsStr ntDefs
    ,genBiYaccGeneric ntDefs
    ,listFunStr
    ])

  writeFile (outputDir ++ "BigulProg.hs") (foldr1 newlineSS
    [importStr
    ,entranceStr
    ,progStr
    ,numTyBiGULDefs
    ,mainStr])

  let parserStr = genLexerAndParser (cmds, rawNtDefs)

  writeFile (outputDir ++ "ProgParser.y") parserStr
  writeFile (outputDir ++ "ParserWrapper.hs") (genParserWrapper topLevelSrcTy)

  readProcess "happy" ["-i", "--glr", "--ghc", "--decode"
                      ,outputDir ++ "ProgParser.y"] ""

  readProcess "ghc" ["-Wno-missing-methods", outputDir ++ "BigulProg.hs", "-i" ++ outputDir] ""
  putStrLn $ "Nothing serious if the final callCommand failed." ++
               "It just made some cleaning work."
  putStrLn $ "Information about shift/reduce conflicts can be found in ProgParser.info file."
             ++ " (If there is any.)"

  let cleanCmd = "rm " ++ (outputDir ++ "*.hi ") ++ (outputDir ++ "*.o ")
                    ++ (outputDir ++ "*.dyn_o ") ++ (outputDir ++ "*.dyn_hi")
  -- callCommand cleanCmd
  return ()
