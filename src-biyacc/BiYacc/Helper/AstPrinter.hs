module BiYacc.Helper.AstPrinter (printAst) where

import Data.Char

data Arith = Add Arith Arith
           | Sub Arith Arith
           | Mul Arith Arith
           | Div Arith Arith
           | Num Int
  deriving (Show, Eq, Read)

data AToken =
    AStr String
  | ALPa
  | ARPa
  deriving Show

lexerAst :: String -> [AToken]
lexerAst [] = []
lexerAst (' ':cs) = lexerAst cs
lexerAst ('\n':cs) = lexerAst cs
lexerAst ('(':cs) = ALPa : lexerAst cs
lexerAst ( ')':cs) = ARPa : lexerAst cs
lexerAst ('\"':cs) = lexerString "\"" cs
lexerAst (c:cs) =
  let (var, rest) = span isAlphaNum (c:cs)
  in  AStr var : lexerAst rest

lexerString :: String -> String -> [AToken]
lexerString res [] = [AStr (reverse res)]
lexerString res ('\"':cs) = AStr (reverse ('\"':res)) : lexerAst cs
lexerString res (c:cs) = lexerString (c:res) cs

data Tree = Leaf String | Forest String [Tree] deriving Show

makeForest :: [Tree] -> Tree
makeForest ts = Forest cons args
  where
    (Leaf cons:args) = reverse ts

parseAst :: [AToken] -> Tree
parseAst toks = parse0 [[]] toks
  where
    parse0 :: [[Tree]] -> [AToken] -> Tree
    parse0 st (ALPa:toks) = parse0 ([]:st) toks
    parse0 (hd:st) ((AStr x):toks) = parse1 ((Leaf x:hd):st) toks
    parse1 :: [[Tree]] -> [AToken] -> Tree
    parse1 [r] [] = makeForest r
    parse1 st (ALPa:toks) = parse0 ([]:st) toks
    parse1 (hd:nxt:st) (ARPa:toks) = parse1 ((makeForest hd:nxt):st) toks
    parse1 (hd:st) ((AStr x):toks) = parse1 ((Leaf x:hd):st) toks


a = Mul (Sub (Num 0) (Num 2)) (Div (Add (Num 2) (Num 3)) (Sub (Num 0) (Num 4)))
b = Mul (Num 1) (Num 2)

prettyAst :: Tree -> String
prettyAst t = prettyPrt 0 t
  where
    inLine :: [Tree] -> Bool
    inLine [] = True
    inLine (Leaf x:ts) = inLine ts
    inLine (Forest con x:ts) = False
    showLeaf :: Tree -> String
    showLeaf (Leaf x) = " " ++ x
    prettyPrt :: Int -> Tree -> String
    prettyPrt n (Leaf x) = (replicate n ' ') ++ x ++ "\n"
    prettyPrt n (Forest con ts)
      | inLine ts = (replicate n ' ') ++ "(" ++ con ++ concat (fmap showLeaf ts) ++ ")\n"
      | otherwise = (replicate n ' ') ++ "(" ++ con ++ "\n" ++ concat (fmap (prettyPrt (n+2)) ts) ++ (replicate n ' ') ++ ")\n"


printAst :: String -> String
printAst = prettyAst . parseAst . lexerAst
