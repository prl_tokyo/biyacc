{-# Language OverloadedStrings, ViewPatterns #-}

module BiYacc.Helper.Utils
  ( module BiYacc.Helper.Utils,
    module BiYacc.Language.Def,
    module TPP,

    toLower,
    Map,elems, keys, foldrWithKey,
    filterWithKey, toList, adjust, mergeWithKey, fromList, merge, preserveMissing,
    zipWithMatched, unionsWith, mapWithKey, mapKeys,

    forM_, liftM, liftM2, lift,
    MonadState, State, StateT, get, put, evalState, evalStateT, runState, modify, execState,
    Reader, ReaderT, runReader, ask, asks,

    fromMaybe, catMaybes, rights,
    foldl1, foldl', (\\), intersperse, sort, nub, delete, partition, intersect,

    ppShow, pPrint,
    trace, traceM, traceIO
    ) where

import Prelude hiding (lookup, (<>))
import BiYacc.Language.Def

import Control.Monad -- (forM_, liftM, liftM2)
import Control.Monad.State -- (MonadState, State, StateT, get, put, evalState, modify, execState)
import Control.Monad.Reader -- (Reader, ReaderT, runReader, ask, asks)

import Data.Char (toLower)
import Data.Map as Map (Map, singleton, union, unions, insert, empty, elems, keys, lookup, foldrWithKey,
  filterWithKey, toList, adjust, mergeWithKey, unionsWith, mapWithKey, mapKeys, fromList)
import Data.Map.Merge.Lazy (merge, preserveMissing, zipWithMatched)
import Data.Maybe (fromMaybe, catMaybes)
import Data.List as List (foldl1, (\\), foldl', intersperse, sort, nub, elem, delete, partition, intersect, null)
import Data.Either (rights)
import Text.PrettyPrint as TPP hiding (char)
import Text.Show.Pretty (ppShow, pPrint)

import Debug.Trace

-- fill each filed with a (fresh) variable
fillVars :: [a] -> [String]
fillVars = fillVarsFrom 1
-- fillVars (PFKleene Occurence [ProdField])

fillVarsFrom :: Int -> [a] -> [String]
fillVarsFrom i fields = zipWith (\_ i' -> "t" ++ show i') fields [i,i+1..]

fillWilds :: [a] -> [String]
fillWilds = map (const "_")

fillUndefineds ::  [a] -> [String]
fillUndefineds = map (const "undefined")


extractTopLevelSrcTy :: NontDefs -> String
extractTopLevelSrcTy ((NontDef nt _):_) = nt

tshow :: Show a => a -> Doc
tshow = text . show

nest2 :: Doc -> Doc
nest2 = nest 2

nest3 = nest 3

nestn6 = nest (-6)

nestn2 = nest (-2)

nestn1 = nest (-1)


wildcardDoc = text "_"

-- put something in  [p|]: [p| ...something... ]
wrapPatQ :: Doc -> Doc
wrapPatQ p = text "[p|" <+> p <+> text "|]"

wrapDecQ :: Doc -> Doc
wrapDecQ d = text "[d|" <+> d <+> text "|]"

wrapExpQ :: Doc -> Doc
wrapExpQ e = text "[|" <+> e <+> text "|]"

vcat2 = vcat3 . punctuate (text "\n")
  where vcat3 [] = TPP.empty
        vcat3 a@(x:xs) = foldr1 ($+$) a

punctuate1 :: Doc -> [Doc] -> [Doc]
punctuate1 d = map (\e -> e <> d)

infixr 5 `newlineSS`
newlineSS :: String -> String -> String
newlineSS s1 s2 = s1 ++ "\n\n" ++ s2

infixr 5 `newlineS`
newlineS :: String -> String -> String
newlineS s1 s2 = s1 ++ "\n" ++ s2

foldr1c :: (a -> b) -> (a -> b -> b) -> [a] -> b
foldr1c c _ [e] = c e
foldr1c c h (e:es) = h e (foldr1c c h es)
foldr1c _ _ [] = error "empty list fed to foldr1c"

foldl1c :: (a -> b) -> (b -> a -> b) -> [a] -> b
foldl1c c _ [e] = c e
foldl1c c h (e:es) = gg h (c e) es
  where gg h c1 [] = c1
        gg h c1 (e:es) = gg h (h c1 e) es
foldl1c _ _ [] = error "empty list fed to foldl1c"

isRight :: Either a b -> Bool
isRight (Left _)  = False
isRight (Right _) = True

--
isVarPat :: Pattern -> Bool
isVarPat VarP{} = True
isVarPat AsP{} = True
isVarPat _ = False


tyAsFunType :: DataTypeRep -> String
tyAsFunType = printTypeRep


concatSpace :: [String] -> String
concatSpace [] = []
concatSpace (x:xs) = x ++ " " ++ concatSpace xs

infixr 5 +^+
(+^+) :: String -> String -> String
x +^+ y = x ++ " " ++ y


-- change primitive types such as Int, Float to String type to hold precisely all the information
-- the "Name" is pervasive in the libraries of Haskell.
-- so I changed it to another name to avoid confilict...
toBiYaccTy :: String -> String
toBiYaccTy "String"     = "BiStringTy"
toBiYaccTy "Identifier" = "BiIdentifierTy"
toBiYaccTy "Numeric"    = "BiNumericTy"
toBiYaccTy a = a


repPrimTy :: String -> String
repPrimTy ("BiStringTy")     = "replaceBiString"
repPrimTy ("BiIdentifierTy") = "replaceBiIdentifier"
repPrimTy ("BiNumericTy")    = "replaceBiNumeric"
repPrimTy st = error $ "source type:" +^+ st +^+ "not implemented"

fst3 (a,_,_) = a
snd3 (_,b,_) = b
third3 (_,_,c) = c


trimSpaces :: String -> String
trimSpaces = reverse . dropWhile (flip List.elem ("\t\n\r " :: String)) .
  reverse . dropWhile (flip List.elem ("\t\n\r " :: String))

wrapParen :: String -> String
wrapParen str = "(" ++ str ++ ")"


-- unsafe
fromJ :: String -> Maybe a -> a
fromJ errMsg ma = case ma of
  Nothing -> error errMsg
  Just a  -> a


-- find elements occur more than once in a list.
findDup :: Eq a => [a] -> [a]
findDup xs = nub [x | x <-xs, x `elem` delete x xs]


notNull :: [a] -> Bool
notNull = not . List.null

mkTyRep :: String -> [DataTypeRep] -> DataTypeRep
mkTyRep str fields = TyCon str fields

mkSimpTyRep :: String -> DataTypeRep
mkSimpTyRep str = TyCon str []


--
isPrimTypes :: String -> Bool
isPrimTypes t = elem t primTypes || elem t biPrimTypes

isPrimTypes' :: DataTypeRep -> Bool
isPrimTypes' (printTypeRep -> t) = elem t primTypes || elem t biPrimTypes

-- print a type representation to its string representation.
-- there is no parenthesis in the outmost layer
printTypeRep :: DataTypeRep -> String
printTypeRep = printTypeRepOuter

printTypeRepOuter t =
  case t of
    TyCon conName []    -> conName
    TyCon conName ts    -> conName ++ " " ++ concatMapWith ' ' printTypeRepInner ts
    TyVar x -> x

-- print a type representation to its string representation.
printTypeRepInner :: DataTypeRep -> String
printTypeRepInner t =
  case t of
    TyCon conName []    -> conName
    TyCon conName ts    -> "(" +^+ conName +^+ concatMapWith ' ' printTypeRepInner ts +^+ ")"
    TyVar x -> x

printTypeRepWithParen = printTypeRepInner

-- how to handle parameterised datatypes ?
printTyAsName :: DataTypeRep -> String
printTyAsName (TyCon n ts) = n ++ foldr (++) "" (map printTyAsName ts)
printTyAsName (TyVar x) = x

printTyTag :: DataTypeRep -> String
printTyTag (printTyAsName -> s) = s ++ "Tag"

-- generating text (doc, string ...) for tuple types
genTuple :: (a -> Doc) -> [a] -> Doc
genTuple f [e] = f e
genTuple f (e:es) = parens $ f e <> comma <+> genTuple f es
genTuple _ [] = "()" -- error "error in internal function genTuple. empty list detected."

-- generate right-dense nested 2-tuple
toDenseTupleDoc :: (a -> Doc) -> [a] -> Doc
toDenseTupleDoc f [e] = f e
toDenseTupleDoc f (e:es) = parens $ f e <> comma <+> toDenseTupleDoc f es
toDenseTupleDoc _ [] = "()" -- error "error in internal function genTuple. empty list detected."


-- initialise serial number
genSerialTuple :: Int -> (a -> Doc) -> [a] -> Doc
genSerialTuple n f [e] = f e <> text (show n)
genSerialTuple n f (e:es) = parens $ f e <> text (show n) <> comma <+> genSerialTuple (n+1) f es
genSerialTuple n _ [] = error "in internal function genSerialTuple. empty list detected."

-- initialise serial number
genSerialTuple' :: Int -> (a -> String) -> [a] -> String
genSerialTuple' n f [e] = f e ++ show n
genSerialTuple' n f (e:es) = wrapParen $ f e ++ show n ++ "," +^+ genSerialTuple' (n+1) f es
genSerialTuple' n _ [] = error "in internal function genSerialTuple'. empty list detected."


concatMapWith :: b -> (a -> [b]) -> [a] -> [b]
concatMapWith sep f [] = []
concatMapWith sep f [x] = f x
concatMapWith sep f (x:xs) = f x ++ [sep] ++ concatMapWith sep f xs

-- make default values
mkDftVal :: String -> String
mkDftVal st =
  if isPrimTypes st
    then case st of
      --  the default value will be overridden at run time later
      "BiIdentifierTy" -> "DEF_IDF"
      "BiNumericTy"    -> "0"
      "BiStringTy"     -> "DEF_STR"
      _                -> error $ "panic. should not reach here in function mkDftVal"
    else st ++ "Null"
