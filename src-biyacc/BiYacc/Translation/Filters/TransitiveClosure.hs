module BiYacc.Translation.Filters.TransitiveClosure where

{-
For associativity filters, definining both Plus and Minus to be left-assoc
(or right-assoc) implies that the pattern
Plus _ _ (Minus _ _ _) and
Minus _ _ (Plus _ _ _)
are also forbidden
if there is no priority declaration between Plus and Minus
-}

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.DataQuery

-- prio-filter  assoc-filters  closure-defs
-- Two LAssocs Con1 and Con2 that do not have relative priority forbid patterns
-- Con1 ... (Con1 ...), Con1 ... (Con2 ...), Con2 ... (Con1 ...), and Con2 ... (Con2 ...)
genLAssocTC :: [Cmd] -> [Cmd] -> [(ProdCons,ProdCons)]
genLAssocTC prioFs lAssocFs =
  let lAssocTC0 = cartPrd $ map (\(CmdLAssoc con) -> con) lAssocFs
      prioPairs = map (\(CmdPrio (conH,conL)) -> (conH, conL)) prioFs
      lAssocTC  = filter (\(con1,con2) -> not ((con1,con2) `elem` prioPairs) &&
                              not ((con2,con1) `elem` prioPairs)) lAssocTC0
  in  lAssocTC


-- Two RAssocs Con1 and Con2 forbid patterns Con1 (Con2 ...) ...  and
-- Con2 (Con1 ...) ...
genRAssocTC :: [Cmd] -> [Cmd] -> [(ProdCons,ProdCons)]
genRAssocTC prioFs rAssocFs =
  let rAssocTC0 = cartPrd $ map (\(CmdRAssoc con) -> con) rAssocFs
      prioPairs = map (\(CmdPrio (conH,conL)) -> (conH, conL)) prioFs
      rAssocTC  = filter (\(con1,con2) -> not ((con1,con2) `elem` prioPairs) &&
                              not ((con2,con1) `elem` prioPairs)) rAssocTC0
  in  rAssocTC

-- cartesian product
cartPrd :: Eq a => [a] -> [(a,a)]
cartPrd ls = [ (x,y) | x <- ls, y <-ls ]

------------------

-- prio-filter  ->  prio-closure-defs
genPrioTC :: [Cmd] -> [Cmd]
genPrioTC prioFs =
  let prioTC0 = mkTransitive $ map (\(CmdPrio (h,l)) -> (h,l)) prioFs
  in  map (\(h,l) -> CmdPrio (h,l)) prioTC0

-- (a,b) and (b,c) imply (a,c)
mkTransitive :: Eq a => [(a, a)] -> [(a, a)]
mkTransitive l = toFixed oneStep l
  where toFixed :: (Eq a) => (a -> a) -> a -> a
        toFixed f s = let s' = f s
                      in  if s' == s then s else toFixed f s'
        oneStep l = nub $ l ++ [(a,c) | (a,b) <- l, (b',c) <- l, b == b' ]


-- minOfPOSet :: Eq a => [(a, a)] -> [a]
-- minOfPOSet pairs =
--   let (potentials, impossibles) = unzip pairs
--   in  nub $ [ p | p <- potentials, not (p `elem` impossibles)]
--
-- maxOfPOSET :: Eq a => [(a, a)] -> [a]
-- maxOfPOSET pairs =
--   let (impossibles, potentials) = unzip pairs
--   in  nub $ [ p | p <- potentials, not (p `elem` impossibles)]
--
-- sortPOSet :: Eq a => [(a, a)] -> [[a]]
-- sortPOSet pairs =
--   let maxs = maxOfPOSET pairs
--   in  sortPOSet' pairs ++ [maxs]
--   where
--     sortPOSet' [] = []
--     sortPOSet' pairs =
--       let mins = minOfPOSet pairs
--           newSet = filter (\(a,b) -> not (a `elem` mins)) pairs
--       in  mins : sortPOSet' newSet
