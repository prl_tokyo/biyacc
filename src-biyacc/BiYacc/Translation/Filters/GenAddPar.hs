{-# LANGUAGE OverloadedStrings #-}

module BiYacc.Translation.Filters.GenAddPar where

import BiYacc.Language.Def
import BiYacc.Helper.Utils

import Data.Map (Map, union, unions, singleton)
import qualified Data.Map as Map
import Text.PrettyPrint as TPP hiding (empty)
import qualified Text.PrettyPrint as TPP

genAddParClass :: Doc
genAddParClass = text $ foldr1 newlineS
  ["class AddParen t where"
  ,"  canAddPar :: t -> Bool"
  ,"  addPar :: t -> t"]

-- pre-defined instance for BiTuple String String
biTupleAddParIns :: Doc
biTupleAddParIns = text . foldr1 newlineS $
  ["instance AddParen (BiTuple String String) where"
  ,"  canAddPar _ = False"
  ,"  addPar x    = x"]

-- generate instances
genAddParIns :: TypeNameEnv -> Cons2PatEnv -> Pat2ConsEnv -> BracketAttrEnv -> Doc
genAddParIns tnEnv cpEnv pcEnv Nothing = TPP.empty
genAddParIns tnEnv cpEnv pcEnv (Just brktCon) =
  let conPathPairs = Map.toList $ buildBrkPath tnEnv cpEnv pcEnv (Just brktCon)
      uniquePairs = selUniquePair cpEnv conPathPairs
      cannotAddPar = extractCannotAddPar (map fst conPathPairs) cpEnv
  in  vcat2 $ biTupleAddParIns : map (genAddParInsCan cpEnv) uniquePairs ++
              map genAddParInsCannot cannotAddPar

-- The old code generate the path for each constructor (or production). But we
-- only need to generate instances for each type, not for each constructor.
selUniquePair :: Cons2PatEnv -> [(ProdCons, Path)] -> [(ProdCons, Path)]
selUniquePair cpEnv conPathPairs =
  let types = map (\(con, _) -> (snd . fromJ "in selUniquePair")
                    (Map.lookup con cpEnv)) conPathPairs
      uniLocs = snd $ execState (mapM uniIndex (zip types [0..])) ([],[])
  in  map (\i -> conPathPairs !! i) uniLocs

-- return indexes of unique elements
uniIndex :: (String,Int) -> State ([String],[Int]) ()
uniIndex (s,pos) = do
  (repeated, poss) <- get
  if not (s `elem` repeated)
    then do
      put ((s:repeated), (pos:poss))
    else
      return ()

-- (constructor, paths for the constructor reaching brackets and back)
genAddParInsCan :: Cons2PatEnv -> (ProdCons, Path) -> Doc
genAddParInsCan cpEnv (con,path) =
  let (fields, ty) = fromJ "in genAddParInsCan" (Map.lookup con cpEnv)
      path_ = init path
      addParStr = "addPar x = " ++ foldr (oneStep cpEnv) "x" path_
  in  text . foldr1 newlineS $
        ["instance AddParen " ++ ty ++ " where"
        ,"  canAddPar _ = True"
        ,"  " ++ addParStr]

-- env ProdCons accumulated-results
oneStep :: Cons2PatEnv -> ProdCons -> String -> String
oneStep cpEnv con accRes =
  let (fields, _) = fromJ "in genAddParInsCan" (Map.lookup con cpEnv)
      (nt, pos) = selOnlyNT fields
      defVals = map mkDefValForTyField fields
  in  wrapParen $ (con +^+ concatSpace (replaceAt pos accRes defVals))


extractCannotAddPar :: [ProdCons] -> Cons2PatEnv -> [String]
extractCannotAddPar cons cpEnv =
  let typesCan = map (\con -> (snd . fromJ "in extractCannotAddPar")
                      (Map.lookup con cpEnv)) cons
      allTypes = nub . map snd $ elems cpEnv
  in  allTypes \\ typesCan

genAddParInsCannot :: String -> Doc
genAddParInsCannot ty = text . foldr1 newlineS $
  ["instance AddParen" +^+ ty +^+ "where"
  ,"  canAddPar _ = False"
  ,"  addPar x    = x"]



-- replace the string at pos position with a new string
replaceAt :: Int -> String -> [String] -> [String]
replaceAt pos str xs =
  let (h,t) = splitAt pos xs
  in  h ++ [str] ++ (if null t then [] else tail t)

-- select the only nonterminal and otherwise report an error
selOnlyNT :: [ProdField] -> (String, Int)
selOnlyNT fds =
  case catMaybes' $ zip (map selOnlyNT' fds) [0..] of
    [(nt,i)] -> (nt,i)
    _    -> error "more than one nonterminals detected. In selOnlyNT."
  where
    catMaybes' :: [(Maybe a, b)] -> [(a,b)]
    catMaybes' ((Nothing , _):xs) = catMaybes' xs
    catMaybes' ((Just x, i):xs) = (x, i) : catMaybes' xs
    catMaybes' [] = []

    selOnlyNT' :: ProdField -> Maybe String
    selOnlyNT' (Left _) = Nothing
    selOnlyNT' (Right nt) = Just nt



------------ buildEnvironment for adding parentheses
-- e.g.: given "Add _ _", find the route ["ETerm", "TFactor", "FExpr" (Paren), ..., "Add"]
-- then we have the map "Add" ------> ["ETerm", "TFactor", "FExpr", ..., "Add"]

-- old code. THE ALGORITHM IS WRONG
buildBrkPath :: TypeNameEnv -> Cons2PatEnv -> Pat2ConsEnv -> BracketAttrEnv -> PathViaBracket
buildBrkPath tyNameEnv cpEnv pcEnv brkAttrEnv =
  if  brkAttrEnv == Nothing then Map.empty else
      let Just brkCon = brkAttrEnv
          (_ , brkConsType) = maybe (error "impossible. buildBrkPath 1") id
                                (Map.lookup brkCon cpEnv)
          allNonts = keys tyNameEnv
          prePath  = expand $ execState (pathToCertainProd allNonts brkCon) Map.empty
          consNameHasRoutesToBrkRules = keys prePath
          postPath = pathFromCertainProd brkCon consNameHasRoutesToBrkRules
          completePath = mergeWithKey (\_ a b -> Just (a ++ tail b)) (const Map.empty) (const Map.empty) prePath postPath
      in  completePath

  where
  addToState :: ProdRuleType -> Path -> State (Map ProdRuleType Path) ()
  addToState rTy newRoute = modify (union (singleton rTy newRoute))

  pathToCertainProd :: [ProdRuleType] -> ProdCons -> State (Map ProdRuleType Path) ()
  pathToCertainProd [] certainProd = return ()
  pathToCertainProd rTys certainProd = do
    -- for each Nonterminal, check if there is a way to the “bracket attribute” production rule.
    routesMap <- get
    let lastState = keys routesMap
    forM_ rTys
      (\srcNont -> do
        let maybeConsForNextRoute = canReachCertainRule srcNont certainProd routesMap
        case maybeConsForNextRoute of
          Nothing -> return ()
          -- if there is, add the route to the state (a Map environment)
          Just consName -> do
            -- two conditions. 1. the nonterminal rTy contains the “bracket attribute” production rule.
            -- 2. the nonterminal rTy need other nonterminals as bridge to the “bracket attribute” production rule.
            -- e.g. fst . fromJust . lookup consName $ cpEnv will produce
            -- for condition 1) [Left "(",Right "Expr",Left ")"]  .    for condition 2) [Right "Factor"]
            -- WARNING. the procedure concatMap extractTypeField is likely to be wrong. But we do not hope to meet the wrong case at run time. fix it later.
            let [nextNont] = rights . fst .
                             fromJ "impossible. maybeConsForNextRoute. 0x0E." .
                             Map.lookup consName $ cpEnv
                newRoute = case Map.lookup nextNont $ routesMap of
                    Nothing -> [consName]                           -- condition 1.
                    Just furtherRoutes -> consName : furtherRoutes  -- condition 2.
            addToState srcNont newRoute)

    -- get the current routes, excluding the Nonterminals in the current routes (already
    -- has the way to the “bracket attribute” production rule), enter the next iteration.
    routesMap' <- get
    let newState = keys routesMap'
    if sort lastState == sort newState
      then return ()
      else pathToCertainProd (rTys \\ newState) certainProd

  -- extractTypeField :: ProdField -> [ProdField]
  -- extractTypeField etnt = [etnt]

  -- two kinds of Nonterminals can reach the “bracket attribute” production rule.
  -- 1. the Nonterminal can produce the “bracket attribute” production rule (inCertainGroup)
  -- 2. the Nonterminal has a production rule which produce only a Nonterminal', and Nonterminal' can
  --    reach the “bracket attribute” production rule using the environment. (oneStepToOtherRoute)
  canReachCertainRule :: ProdRuleType -> ProdCons -> Map ProdRuleType Path -> Maybe ProdCons
  canReachCertainRule srcNont dstConsName routesMap = case (inCertainGroup srcNont dstConsName) of
    Nothing -> oneStepToOtherRoute srcNont routesMap
    justName -> justName
    where
      inCertainGroup :: ProdRuleType -> ProdCons -> Maybe ProdCons
      inCertainGroup srcNont dstConsName =
        case Map.lookup srcNont tyNameEnv of
          Nothing -> error "impossible. inCertainGroup. 0x01"
          Just conNames ->
            if elem dstConsName conNames
              then Just dstConsName
              else Nothing

      oneStepToOtherRoute :: ProdRuleType -> Map ProdRuleType Path -> Maybe ProdCons
      oneStepToOtherRoute srcNont routesMap =
        case Map.lookup srcNont tyNameEnv of
          Nothing -> error "impossible. oneStepToOtherRoute. 0x02"
          Just consNames ->
            -- consFieldss e.g.: [[Right "Expr", Left "+", Right "Term"], [...], [Right "Term"]]
            let consFieldss = map (fst . fromJ "impossible. oneStepToOtherRoute. 0x03" . flip Map.lookup cpEnv) consNames
            in  case catMaybes (map selPossibleRoute consFieldss) of
                  nextProdRules@(_:_) ->
                    -- I do not think the grammar of a well-defined programming language can have more than one ways
                    -- reaching the bracket attribute production rule. But if it is, arbitrary choose one.
                    case catMaybes (map forEachElem nextProdRules) of
                      x:_ -> Just x
                      _   -> Nothing
                  _ -> Nothing
        where
          forEachElem :: [ProdField] -> (Maybe ProdCons)
          forEachElem [Right possibleNextNont] =
            case Map.lookup possibleNextNont routesMap of
              Nothing -> Nothing
              Just _  -> Just . fromJ "impossible. forEachElem. 0x04." $
                          Map.lookup ([Right possibleNextNont], srcNont) pcEnv -- should not fail
          forEachElem _ = Nothing

          selPossibleRoute :: [ProdField] -> Maybe [ProdField]
          selPossibleRoute prdFds =
            if length prdFds == 1 && length (rights prdFds) == 1
            then Just prdFds
            else Nothing

  -- previous it is a map from “a nonterminal” to the “routes to the bracket attribute”
  -- now build a new map expanded from “each production rule of that nonterminal” to the “routes to the bracket attribute”
  expand :: Map ProdRuleType Path -> PathViaBracket
  expand = foldrWithKey ff Map.empty
    where
      ff :: ProdRuleType -> Path -> PathViaBracket -> PathViaBracket
      ff rTy rRoutes done =
        case Map.lookup rTy tyNameEnv of
          Nothing -> error "impossible . expand. 0x05."
          Just consNames ->
            union done $ unions $ [singleton consName rRoutes | consName <- consNames]


  -- single source shortest path is almost reasonable fast ...
  pathFromCertainProd :: ProdCons -> Path -> PathViaBracket
  pathFromCertainProd certainProd destConss =
    -- WARNING. possbily the same error as above warning.
    let [nextNont] = rights . fst .
                     fromJ "impossible. pathFromCertainProd. 0x06." .
                     Map.lookup certainProd $ cpEnv
        allNonts   = keys tyNameEnv
        results    = catMaybes $ map (forEachElem allNonts) destConss -- results [(destCons, paths), ...]
        envs       = map (\(destCons, paths) -> singleton destCons (certainProd:paths)) results
    in  unions $ envs
    where
      forEachElem :: [ProdRuleType] -> ProdCons -> Maybe (ProdCons, Path)
      forEachElem allNonts destCons =
        let pathsFromCertainProd = evalState (findPath certainProd destCons allNonts) Map.empty
        in  liftM ((,) destCons) pathsFromCertainProd

  -- the return value “paths” does not include the first path “orgnConsName” itself
  findPath :: ProdCons -> ProdCons -> [ProdRuleType] -> State (PathViaBracket) (Maybe Path)
  findPath orgnConsName dstConsName allNonts = do
    previousState <- get
    let pathss = catMaybes $ map (\srcNont -> findPathWithEnv srcNont previousState) allNonts
    -- there are always some nonterminals which can reach dstConsName
    case filter (\(consName, _) -> consName == orgnConsName) pathss of
      -- the last step (base case). There is a direct path from orgnConsName
      (_, paths):_ -> return (Just paths)
      []  -> do mapM_ (addToState) pathss -- need more iterations
                newState <- get
                if previousState == newState
                  then
                       -- from a certain nonterminal, sometimes there is not way to go back to itself (not cyclic).
                       return Nothing
                  else findPath orgnConsName dstConsName allNonts
    where
      addToState :: (ProdCons, Path) -> State (PathViaBracket) ()
      addToState (nextConsName, paths) = modify (union (singleton nextConsName paths))

      findPathWithEnv :: ProdRuleType -> PathViaBracket -> Maybe (ProdCons, Path)
      findPathWithEnv srcNont currentState =
        -- 1. one step finish.
        let dstNont   = snd . fromJ "impossible. findPathWithEnv. 0x08" $ Map.lookup dstConsName cpEnv
            consNames = fromJ "impossible. findPathWithEnv. 0x09." (Map.lookup srcNont tyNameEnv) -- all constructor names of a nonterminal
            -- all fileds of a consName
            tntss     = map (\consName -> (consName, fst . fromJ "impossible. findPathWithEnv. 0x0A" $ Map.lookup consName cpEnv)) consNames
            -- possible conditions： 1) fields has and only has a (Right Nonterminal)
            -- 2) constructor name == originalConstructorName
            possCond1 (condName, xs) = 1 == length xs && 1 == length (rights xs)
            possCond2 (condName, xs) = orgnConsName == condName
            adjustedFields = case filter possCond2 tntss of
              -- delete (Left Terminals) fields if the consName is the orgnConsName
              [onlyOne] -> [(fst onlyOne, filter isRight . snd $ onlyOne)]
              [] -> map (\e -> (fst e, filter isRight $ snd e)) . filter possCond1 $ tntss

        in  case filter ( \(_, [Right nont]) -> nont == dstNont) adjustedFields of
              -- there is only one element in the field and the element is dstNont
              [(nextConsName, _)] -> Just (nextConsName, [dstConsName])

              -- The element is not dstNont. Cannot reach the dstNont directly. Try building the path using previous environment (state)
              [] -> case catMaybes (map searchTheWay adjustedFields) of
                      [] -> Nothing
                      [(nextConsName, paths)] -> Just (nextConsName, paths) -- WARNING: are there more than one ways?
        where
          searchTheWay :: (ProdCons, [ProdField]) -> Maybe (ProdCons, Path)
          searchTheWay (consName,[Right nont]) = liftM ((,) consName ) (searchWayFromNextNont nont currentState)

          searchWayFromNextNont :: ProdRuleType -> PathViaBracket -> Maybe Path
          searchWayFromNextNont nont currentState =
            case Map.lookup nont tyNameEnv of
              Nothing -> error "impossible. searchWayFromNextNont. 0x0B"
              Just consNames ->
                case catMaybes $ map (\consName -> liftM ((,) consName)
                                                   (Map.lookup consName currentState)) consNames of
                  [] -> Nothing
                  [(nextnextConsName, route)] -> Just (nextnextConsName : route) -- remember modify here

-------------------------
-- mkDefValForUnit :: UpdateUnit -> String
-- mkDefValForUnit (UpdateUnitSingle u) = mkDefValForEach u
-- mkDefValForUnit (UpdateUnitKleene _ us) =
--   let = aa map mkDefValForUnit us
--   in  render $ brackets (genTuple id aa)
--
-- mkDefValForEach :: Either Unchanged Update -> Reader BiGULGenEnv Doc
-- mkDefValForEach x = case x of
--   Left  (Terminal _)     -> return "\" \""   -- default layout
--   Left  (Nonterminal st) -> mkDefVal (typeRep2Str st)
--   Right (NUpdate _ st)   -> mkDefVal (typeRep2Str st)
--   Right (Expansion deep)   -> liftM parens (genDefaultSrc deep)
--   Right (NUpdateWithLookAhead _ _ deep)  -> liftM parens (genDefaultSrc deep)
--
mkDefValForTyField :: ProdField -> String
mkDefValForTyField (Left t)  = wrapParen $ show (BiTuple t " ")
mkDefValForTyField (Right t) = mkDefVal t

-- produce default value
mkDefVal :: String -> String
mkDefVal ty = case ty of
    "BiIdentifyTy" -> show (BiTuple "DEF_AME" " ")
    "BiNumericTy"  -> show (BiTuple "0" " ")
    "BiStringTy"   -> show (BiTuple "DEF_STR" " ")
    t              -> t ++ "Null"
