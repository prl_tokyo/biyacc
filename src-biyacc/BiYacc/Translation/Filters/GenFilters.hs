module BiYacc.Translation.Filters.GenFilters where

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.DataQuery
import BiYacc.Translation.Filters.TransitiveClosure

import qualified Data.Map as Map
import Data.List
import Text.PrettyPrint as TPP hiding (empty)
import qualified Text.PrettyPrint as TPP

import Control.Monad.State

--
genFilters :: [Cmd] -> Maybe (Doc,[(String, String)]) -> Reader Cons2PatEnv Doc
genFilters cmds otherFlts = do
  cpEnv <- ask
  let patMatchCmds = selPatMatchCmds cmds
      priCmds0     = selPrioCmds cmds
      priCmds      = genPrioTC priCmds0 -- find transitive closures for priority filters
      assocCmds    = selAssocCmds cmds

      -- (patMatchDoc, patMatchTyNa) = zipDoc . unzip3 $
      --                                 map (genPatMatchFilters cpEnv) patMatchCmds
      (priDoc, priTyNa) = zipDoc . unzip3 $ map (genPrioFilters cpEnv) priCmds
      (assocDoc, assocTyNa) = zipDoc . unzip3 $ genAssocFilters cpEnv cmds
      -- (assocTCDoc, assocTCTyNa) = zipDoc . unzip3 $ genAssocTCF cpEnv cmds
      (otherDoc, otherTyNa) = maybe (TPP.empty, []) id otherFlts

      -- tyNamesList :: [(String, [String])]
      tyNamesList = map normalise . groupByType $ (priTyNa ++ assocTyNa ++ otherTyNa)
      compFDoc = vcat $ map (text . genComposedFilters) tyNamesList
      allFilterJStr = genAllFiltersJudge $ map fst tyNamesList
      allFilterRStr = genAllFiltersRepair $ map fst tyNamesList
  return $ vcat2 [priDoc, assocDoc, otherDoc, compFDoc
                 ,allFilterJStr, allFilterRStr]

  where
    zipDoc (doc, ts, ns) = (vcat2 doc, zip ts ns)
    groupByType = groupBy (\(ty1,_) (ty2,_) -> ty1 == ty2)
    normalise tyNaList = let (tys, names) = unzip tyNaList
                         in  (head tys, names)

-- return (filter-definition, fitler-name)
genPrioFilters :: Cons2PatEnv -> Cmd -> (Doc, String, String)
genPrioFilters cpEnv (CmdPrio (conG,conL)) =
  let (fieldsG, typeG) = maybe (error "impossible. genPrioFilters G") id
                        (Map.lookup conG cpEnv)
      (_, _) = maybe (error "impossible. genPrioFilters L") id
                            (Map.lookup conL cpEnv)

      fName = "f" ++ conG ++ conL ++ "Prio"
      fPat = wrapParen$ conG +^+ concatSpace subtrees
      subtrees = fillVars fieldsG
      caseCond = text $ "case or [" ++ concat [isMatch pj ++ ", " | pj <- subtrees] ++ "False] of"
      falseC = text "False -> Nothing"
      trueC  = text $ "True -> Just ( " ++ conG +^+ concat [repair pj | pj <- subtrees] ++ ")"
      caseDoc = caseCond $+$ nest2 (falseC $+$ trueC)

      whereStr = text $ "where p = " ++ conL +^+ concatSpace (fillUndefineds fieldsG)
  in  (((text (fName +^+ fPat ++ " =") $+$ nest2 caseDoc) $+$ nest2 whereStr) $+$
       (text (fName ++ " _ " ++ " = " ++ "Nothing")) , typeG , fName)


isMatch  :: String -> String
isMatch t = "gMatch " ++ t ++ " p"

repair :: String -> String
repair t = "(if gMatch " ++ t ++ " p " ++ "then addPar " ++ t ++ " else " ++ t ++ ") "


-------
genAssocFilters :: Cons2PatEnv -> [Cmd] -> [(Doc, String, String)]
genAssocFilters cpEnv cmds =
  let lAssocTC = genLAssocTC prioFs $ selLAssocCmds cmds
      rAssocTC = genRAssocTC prioFs $ selRAssocCmds cmds
      prioFs   = selPrioCmds cmds
  in  map (genLAssocF cpEnv) lAssocTC ++ map (genRAssocF cpEnv) rAssocTC


-- env -> (constructor-of-parent,constructor-of-child) -> (definition, type, name)
genLAssocF :: Cons2PatEnv -> (ProdCons,ProdCons) -> (Doc, String, String)
genLAssocF cpEnv (pCon,cCon) =
  let (pFields,pTy) = maybe (error $ "Constructor " ++ pCon ++ " not defined") id
                     (Map.lookup pCon cpEnv)
      (cFields,_) = maybe (error $ "Constructor " ++ cCon ++ " not defined") id
                     (Map.lookup cCon cpEnv)
      fName = "f" ++ pCon ++ cCon ++ "LAssoc"
      (alpha,nt) = (init pFields, last pFields)
      expandedNT = wrapParen $ cCon +^+ concatSpace (fillVarsFrom (length alpha + 1) cFields)
      fPat1 = wrapParen $ pCon +^+ concatSpace (fillVars alpha) +^+ expandedNT
      fBody1 = text $ "Just (" ++ pCon +^+ concatSpace (fillVars alpha) ++
                        (wrapParen $ "addPar" +^+ expandedNT) ++ ")"
      fCase1 = text (fName +^+ fPat1 ++ " =") $+$ nest2 (fBody1)
      fCase2 = text $ fName ++ " _ = Nothing"
  in  (fCase1 $+$ fCase2 , pTy , fName)

-- env -> (constructor-of-parent,constructor-of-child) -> (definition, type, name)
genRAssocF :: Cons2PatEnv -> (ProdCons,ProdCons) -> (Doc, String, String)
genRAssocF cpEnv (pCon,cCon) =
  let (pFields,pTy) = maybe (error $ "Constructor " ++ pCon ++ " not defined") id
                     (Map.lookup pCon cpEnv)
      (cFields,_) = maybe (error $ "Constructor " ++ cCon ++ " not defined") id
                     (Map.lookup cCon cpEnv)
      fName = "f" ++ pCon ++ cCon ++ "RAssoc"
      (nt, alpha) = (head pFields, tail pFields)

      expandedNT = wrapParen $ cCon +^+ concatSpace (fillVars cFields)
      fPat1 = wrapParen $ pCon +^+ expandedNT +^+
                concatSpace (fillVarsFrom (length cFields + 1) alpha)
      fBody1 = text $ "Just (" ++ pCon +^+ (wrapParen $ "addPar" +^+ expandedNT)
                +^+ concatSpace (fillVarsFrom (length cFields + 1) alpha) ++ ")"
      fCase1 = text (fName +^+ fPat1 ++ " =") $+$ nest2 fBody1
      fCase2 = text $ fName ++ " _ = Nothing"
  in  (fCase1 $+$ fCase2 , pTy , fName)


-- return (filter-definition, fitler-name)
-- genAssocNonAssocOld :: Cons2PatEnv -> Cmd -> (Doc, String, String)
-- genAssocNonAssocOld cpEnv (CmdNonAssoc con) =
--   let (fields, ty) = maybe (error "impossible. genPrioFilters G") id
--                         (Map.lookup con cpEnv)
--       fName = "f" ++ con ++ "NonAssoc"
--       fPat = wrapParen $ con +^+ concatSpace subtrees
--       subtrees = fillVars fields
--       caseCond = text $ "case or [" ++ concat [isMatch pj ++ ", " | pj <- subtrees] ++ "False] of"
--       falseC = text "False -> Nothing"
--       trueC  = text $ "True -> Just ( " ++ con +^+ concat [repair pj | pj <- subtrees] ++ ")"
--       caseDoc = caseCond $+$ nest2 (falseC $+$ trueC)
--
--       whereStr = text $ "where p = " ++ con +^+ concatSpace (fillUndefineds fields)
--   in  (((text (fName +^+ fPat ++ " =") $+$ nest2 caseDoc) $+$ nest2 whereStr) $+$
--        (text (fName ++ " _ " ++ " = " ++ "Nothing")) , ty , fName)



--                    (type, names) -> code
genComposedFilters :: (String, [String]) -> String
genComposedFilters (_ , []) =
  "allFilters = const Nothing\n"

genComposedFilters (ty , [filterName]) =
  "composed" ++ ty ++ "Filter = " ++ filterName ++ "\n"

genComposedFilters (ty , (fn:fns)) =
  (foldr1 newlineS $
    ("composed" ++ ty ++ "Filter = foldr1 fCompose [\n  " ++ fn) :
    (map (\fn0 -> "  ," ++ fn0) fns))
  ++ "]\n"


-- filterType1, filterType2 ... filterTypeN
-- allFiltersJudge :: (Data a, Typeable a) => a -> Bool
-- allFiltersJudge =
--  everything (&&) (mkQ True ((judge composedFilterType1) `extQ`
--                             (judge composedFilterType2))) ...
genAllFiltersJudge :: [String] -> Doc
genAllFiltersJudge []  = text $
  "allFiltersJudge :: (Data a, Typeable a) => a -> Bool" `newlineS`
  "allFiltersJudge = const True"
genAllFiltersJudge fTys =
  let funTy = "allFiltersJudge :: (Data a, Typeable a) => a -> Bool"

      qCases = foldr1c (\ty -> wrapParen $ "judge " ++ "composed" ++ ty ++ "Filter")
                (\ty str -> (wrapParen $ "judge " ++ "composed" ++ ty ++ "Filter")
                            ++ " `extQ` \n    " ++ str) fTys
      funBd = "allFiltersJudge = everything (&&) " ++ wrapParen ("mkQ True " ++ qCases)
  in  text $ funTy `newlineS` funBd

-- unfortunately, types of elements of composedFilterList mismatches, so
-- mkQ True (foldr (\f compf -> judge f `extQ` compf) judge composedFilterList) and
-- mkT (foldr (\f compf -> repair f `extT` compf) mkT composedFilterList)
-- do not work
genAllFiltersRepair :: [String] -> Doc
genAllFiltersRepair []   = text $
  "allFiltersRepair :: (Data a, Typeable a) => a -> a" `newlineS`
  "allFiltersRepair = id"
genAllFiltersRepair fTys =
  let funTy = "allFiltersRepair :: (Data a, Typeable a) => a -> a"

      tCases = foldr1c (\ty -> wrapParen $ "repair " ++ "composed" ++ ty ++ "Filter")
                (\ty str -> (wrapParen $ "repair " ++ "composed" ++ ty ++ "Filter")
                            ++ " `extT` \n    " ++ str) fTys
      funBd = "allFiltersRepair = everywhere " ++ wrapParen ("mkT " ++ tCases)
  in  text $ funTy `newlineS` funBd


--
filterModHead :: String
filterModHead = foldr1 newlineS
  ["module BiFilter where"
  ,""
  ,"import Data.Data"
  ,"import Data.Typeable"
  ,"import Data.Generics"
  ,"import Debug.Trace"
  ,"import LangDef"
  ,""
  ,"type BiFilter t = (t -> Maybe t)"
  ,""
  ,"judge :: BiFilter t -> (t -> Bool)"
  ,"judge f = maybe True (const False) . f"
  ,""
  ,"repair :: BiFilter t -> (t -> t)"
  ,"repair f = \\t -> (maybe t id . f) t"
  ,""
  ,"fCompose :: (BiFilter t) -> (BiFilter t) -> (BiFilter t)"
  ,"fCompose j i = \\t -> maybe (j t) (\\t' -> maybe (Just t') Just (j t')) (i t)"
  ,""
  -- ,"judgeEverywhere :: Data a => a -> Bool"
  -- ,"judgeEverywhere f = everything (&&) (mkQ True (judge f))"
  -- ,""
  ,"repairEverywhere :: Data a => a -> a"
  ,"repairEverywhere = allFiltersRepair"
  ,""
  ,"judgeAllCSTs :: Data cst =>[cst] -> [cst]"
  ,"judgeAllCSTs = filter allFiltersJudge"
  ,""
  ,"-- there is no need to define repairAllCSTs"
  ,""
  ,"gMatch :: (BiYaccGeneric a, BiYaccGeneric b) => a -> b -> Bool"
  ,"gMatch p c | showConStr p `elem` injectiveProds  =  showConStrInj p  == showConStr c"
  ,"gMatch p c | otherwise                           =  showConStr p     == showConStr c"
  ,""
  ]
