module BiYacc.Translation.Filters.TravelThrough where

{- generate type class
class BiYaccGeneric t where
  showConStr    :: t -> String
  showConStrInj :: t -> String

and instances.
The two functions are for showing constructors of a piece of data (source data only)
In particular, showConStrInj will go through the chains (injective productions).
Once it reaches the first not-injective production, show the constructor.
-}

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import Text.PrettyPrint as TPP hiding (char)

genInjProdsStr :: NontDefs -> String
genInjProdsStr ntDefs = "injectiveProds = " ++ show (extractChains ntDefs)

-- extract constructors of injective productions (chains) such as
-- NT1 -> ... | NT2,   NT2 -> ... | NT3 ...
extractChains :: NontDefs -> [ProdCons]
extractChains ntDefs = concatMap extractChains2 ntDefs

extractChains2 :: NontDef -> [ProdCons]
extractChains2 (NontDef _ prods) = catMaybes (map extractChains3 prods)

extractChains3 :: Prod -> Maybe ProdCons
extractChains3 (Prod con [Right _] _) = Just con
extractChains3 _ = Nothing

genBiYaccGeneric ::  NontDefs -> String
genBiYaccGeneric ntDefs =
  let injProds = extractChains ntDefs
      classDec = foldr1 newlineS ["class BiYaccGeneric t where"
                                 ,"  showConStr :: t -> String"
                                 ,"  showConStrInj :: t -> String"]
  in  classDec `newlineSS` predefinedBiYaccGeneric `newlineS`
      (foldr newlineSS "\n" $ map (genBiYaccGenericIns injProds) ntDefs)

-- generate instances of class BiYaccGeneric
genBiYaccGenericIns :: [ProdCons] -> NontDef -> String
genBiYaccGenericIns injProds (NontDef nt prods) =
  let insDeclHd = "instance BiYaccGeneric " ++ nt ++ " where\n"
      showCondefs = ["  showConStr " ++ wrapParen (con +^+ concatSpace (fillWilds fields))
                      ++ " = " ++ show con   | Prod con fields _ <- prods]
      showConInjdefs = ["  showConStrInj " ++ wrapParen (con ++ " i")
                        ++ " = " ++ "if showConStr i `elem` injectiveProds " ++
                          "then showConStrInj i else showConStr i"
                       | Prod con fields _ <- prods, con `elem` injProds]

  in  insDeclHd ++ (foldr1 newlineS (showCondefs ++ showConInjdefs))


-- for type BiTuple String String
predefinedBiYaccGeneric :: String
predefinedBiYaccGeneric = foldr newlineS "\n"
  ["instance BiYaccGeneric (BiTuple a b) where"
  ,"  showConStr (BiTuple _ _) = \"BiTuple\""]
