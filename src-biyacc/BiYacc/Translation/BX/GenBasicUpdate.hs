{-# LANGUAGE OverloadedStrings #-}

module BiYacc.Translation.BX.GenBasicUpdate where

import Prelude hiding ((<>))
import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.DataQuery
import BiYacc.Translation.TypeInf

import Control.Monad
import Control.Monad.Reader
import Data.Generics hiding (empty)
import Text.PrettyPrint as TPP hiding (empty)
import qualified Data.Map as Map
import qualified Text.PrettyPrint as TPP


-- render SrcSide to productions in string representation
sSide2ProdStr :: SrcSide -> String
sSide2ProdStr (SrcSide ty _ units) =
  printTypeRep ty ++ " -> " ++ prtPrdFd (getProdPat units)

-- print ProdField to string
prtPrdFd :: [ProdField] -> String
prtPrdFd fields = foldr (+^+) "" (map (either show id) fields)

------------------------------------------------------
sSide2HsCode :: SrcSide -> Doc
sSide2HsCode (SrcSide _ con units) =
  text con <+> hsep (map unit2HsCode units)
  where
    unit2HsCode :: UpdateUnit -> Doc
    unit2HsCode (Left  t)     = wildcardDoc
    unit2HsCode (Right (NUpdate uv st) ) = text uv
    unit2HsCode (Right (NUpdateWithLookAhead _ _ sside) ) = sSide2HsCode sside
    unit2HsCode (Right (Expansion sside)) = sSide2HsCode sside


genUpdSPat :: SrcSide -> Doc
genUpdSPat (SrcSide _ con units) =
  parens $ text con <+> hsep (map unit2HsCode units)
  where
    unit2HsCode :: UpdateUnit -> Doc
    unit2HsCode (Left  _) = wildcardDoc
    unit2HsCode (Right (NUpdate uv st) ) = text uv
    unit2HsCode (Right (NUpdateWithLookAhead _ _ sside) ) = genUpdSPat sside
    unit2HsCode (Right (Expansion sside)) = genUpdSPat sside

genRearrSPat :: SrcSide -> Doc
genRearrSPat = genUpdSPat


-- convert view-pattern in biyacc AST to haskell pattern
vSide2HsCode :: ViewSide -> Doc
vSide2HsCode pat =
  case pat of
    (ConP vc _ lhspats1) -> parens $ text vc <+> hsep (map vSide2HsCode lhspats1)
    (VarP uv _)   -> text uv
    (WildP _)     -> wildcardDoc
    (LitP l)      -> text (extractLitToString l)
    (AsP n _ vsp) -> text n <> "@" <> (parens $ vSide2HsCode vsp) -- problematic
