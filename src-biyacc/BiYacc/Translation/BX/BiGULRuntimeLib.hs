module BiYacc.Translation.BX.BiGULRuntimeLib
  (numTyBiGULDefs, importStr, mainStr, listFunStr, defFileHead) where

import BiYacc.Helper.Utils
-----------------------

---------------

defFileHead :: String
defFileHead = foldr newlineS ""
  ["{-# Language TemplateHaskell, TypeFamilies, DeriveDataTypeable, FlexibleInstances, MultiParamTypeClasses #-}"
  ,"module LangDef where"
  ,"import GHC.Generics"
  ,"import Data.Maybe (fromMaybe, fromJust)"
  ,"import Data.Array.ST"
  ,"import Data.Array"
  ,"import Control.Monad.ST"
  ,"import Control.Monad"
  ,"import Generics.BiGUL hiding (Expr, Pat, Var, Direction)"
  ,"import Generics.BiGUL.Interpreter"
  ,"import Generics.BiGUL.TH"
  ,"import Data.Natural"
  ,"import Data.Data"
  ,""
  ,"type BiNumeric    = (String, String)"
  ,"type BiIdentifier = (String, String)"
  ,"type BiString     = (String, String)"
  ,""
  ,"fstBiTuple (BiTuple a b) = a"
  ,"sndBiTuple (BiTuple a b) = b"
  ,""
  ]

importStr :: String
importStr = foldr newlineS ""
  ["{-# Language TemplateHaskell, QuasiQuotes #-}"
  ,"import GHC.Generics"
  ,"import Data.Typeable"
  ,"import Data.Map as Map (Map, lookup, fromList)"
  ,"import Data.Natural"
  ,"import Generics.BiGUL hiding (Expr, Pat, Var, Direction)"
  ,"import Generics.BiGUL.Interpreter"
  ,"import Generics.BiGUL.TH"
  ,"import ParserWrapper"
  ,"import BiFilter"
  ,"import Text.PrettyPrint (render)"
  ,"import LangDef"
  ,"import System.Process"
  ,"import System.Environment (getArgs)"
  ,"import System.IO.Strict as IOS"
  ,"import Debug.Trace"
  ,"import Data.Char"
  ]

mainStr :: String
mainStr = foldr newlineS ""
  ["main :: IO ()"
  ,"main = do"
  ,"  args <- getArgs"
  ,"  if args == [\"clean\"]"
  ,"  then do"
  ,"    putStrLn \"Cleaning... It does not matter if some files do not exist. \\n\\n\""
  ,"    callCommand \"rm LangDef.hs LangDef.dyn_o LangDef.dyn_hi LangDef.o LangDef.hi Parser.hs Parser.dyn_hi Parser.dyn_o Parser.hi Parser.o Parser.y bigulProg.hs bigulProg.hi bigulProg.dyn_hi bigulProg.dyn_o bigulProg.o\""
  ,"  else do"
  ,"    if length args < 3"
  ,"      then error $ usageInfoGet ++ usageInfoPut"
  ,"      else do"
  ,"        let getOrPut      = args !! 0"
  ,"            srcCodeFName  = args !! 1"
  ,"            astFName      = args !! 2"
  ,"        srcCode <- IOS.readFile srcCodeFName"
  ,"        let (comm, csts) = parserWrapper srcCode"
  ,"        putStrLn $ \"(Info:) Number of all CSTs before applying filters: \" ++ show (length csts)"
  ,"        case judgeAllCSTs csts of"
  ,"          [] -> error \"No valid CST remains after running filters.\""
  ,"          [cst] -> case getOrPut of"
  ,"            \"get\" ->"
  ,"              case get entrance cst of"
  ,"                Nothing  -> error $ \"get error. \" ++ getErrTip"
  ,"                Just ast -> writeFile astFName (show ast)"
  ,"            \"getTrace\" -> do"
  ,"               let tr = (getTrace entrance cst)"
  ,"               putStrLn (show tr)"
  ,"            \"put\" -> do"
  ,"              let newSrcCodeName = if length args == 4 then args !! 3 else srcCodeFName"
  ,"              srcCode <- IOS.readFile srcCodeFName"
  ,"              ast <- IOS.readFile astFName"
  ,"              case put entrance cst (read ast) of"
  ,"                Nothing   -> (error $ \"put error. \" ++ putErrTip)"
  ,"                Just cst' -> writeFile newSrcCodeName (comm ++ topPP (repairEverywhere cst'))"
  ,"            \"putTrace\" -> do"
  ,"              let newSrcCodeName = if length args == 4 then args !! 3 else srcCodeFName"
  ,"              srcCode <- IOS.readFile srcCodeFName"
  ,"              ast <- IOS.readFile astFName"
  ,"              let tr = (putTrace entrance cst (read ast))"
  ,"              putStrLn (show tr)"
  ,"          x@(cst:_)  -> do"
  ,"            putStrLn $ \"More than one CST remains after running filters. Select the first one.\\nNumber of remaining CSTs: \" ++ show (length x) ++ \".\""
  ,"            case getOrPut of"
  ,"              \"get\" ->"
  ,"                case get entrance cst of"
  ,"                  Nothing  -> error $ \"get error. \" ++ getErrTip"
  ,"                  Just ast -> writeFile astFName (show ast)"
  ,"              \"getTrace\" -> do"
  ,"                 let tr = (getTrace entrance cst)"
  ,"                 putStrLn (show tr)"
  ,"              \"put\" -> do"
  ,"                let newSrcCodeName = if length args == 4 then args !! 3 else srcCodeFName"
  ,"                srcCode <- IOS.readFile srcCodeFName"
  ,"                ast <- IOS.readFile astFName"
  ,"                case put entrance cst (read ast) of"
  ,"                  Nothing   -> (error $ \"put error. \" ++ putErrTip)"
  ,"                  Just cst' -> writeFile newSrcCodeName (comm ++ topPP (repairEverywhere cst'))"
  ,"              \"putTrace\" -> do"
  ,"                let newSrcCodeName = if length args == 4 then args !! 3 else srcCodeFName"
  ,"                srcCode <- IOS.readFile srcCodeFName"
  ,"                ast <- IOS.readFile astFName"
  ,"                let tr = (putTrace entrance cst (read ast))"
  ,"                putStrLn (show tr)"
  ,"  where"
  ,"    usageInfoGet = \"usage - get: YourExecutable get CodeFileName ASTFileName\\n\""
  ,"    usageInfoPut = \"usage - put: YourExecutable put OldCodeFileName ASTFileName NewCodeFileName\\n\""
  ,"    usageInfoClean = \"usage - clean: YourExecutable clean\\n\""
  ,"    getErrTip    = \"use getTrace instead of get to see tracing information.\""
  ,"    putErrTip    = \"use putTrace instead of put to see tracing information.\""
  ]

-- showOrdStr,
listFunStr = []
  -- foldr1 newlineSS [lensMapStr, operationStr, minEditDistLensStr, minEditDistDPStr
  -- ,structureEditStr, bigulUnzip2Str, bigulUnZip3Str, bigulUnZip4Str, bigulUnZip5Str, bigulUnZip6Str
  -- ,bigulUnZip7Str,bigulUnZip8Str,bigulUnZip9Str,bigulUnZip10Str,bigulUnZip11Str
  -- ,syntaxBeautify]


-- functions for mapping over lists
lensMapStr = foldr1 newlineS
  ["lensMap :: (Show a, Show b, Eq b) => a -> BiGUL a b -> BiGUL [a] [b]"
  ,"lensMap defSrcVal b = Case"
  ,"  [ $(normalSV  [p| [] |]  [p| [] |] [p| [] |]) $ Skip (const [])"
  ,""
  ,"  , $(adaptiveSV [p| _:_ |]  [p| [] |])"
  ,"            (\\_ _ -> [])"
  ,""
  ,"  , $(adaptiveSV [p| [] |]  [p| _:_ |])"
  ,"            (\\_ v -> [defSrcVal])"
  ,""
  ,"  , $(normalSV  [p| _:_ |]  [p| _:_ |] [p| _:_ |])"
  ,"      $(update  [p| x:xs |] [p| x:xs |]"
  ,"                [d| x = b; xs = lensMap defSrcVal b |])"
  ,"  ]"
  ]

minEditDistLensStr = foldr1 newlineS
  ["minEditDistLens :: (Show s, Show v, Eq s, Eq v) => s -> BiGUL s v -> BiGUL [s] [v]"
  ,"minEditDistLens defSrc bx = Case"
  ,"  [ $(adaptive [| \\ s v -> structureEdit defSrc bx s v /= s |])"
  ,"      ==> structureEdit defSrc bx"
  ,"    ,"
  ,"     $(normalSV [p| _ |] [p| _ |] [p| _ |])"
  ,"      ==> lensMap defSrc bx"
  ,"    ]"]


-- showOrdStr =
--   "showOrd :: Show a => a -> String" `newlineS`
--   "showOrd = show . map ord . show"

operationStr = "data Operation = OpModify | OpInsert | OpDelete | OpNothing deriving (Show, Eq, Ord)"

syntaxBeautify = "(==>) = ($)"

minEditDistDPStr = foldr1 newlineS
  ["minEditDistDP :: (Eq a) => [a] -> [a] -> Array (Int, Int) (Int, Operation)"
  ,"minEditDistDP s v = runST $ do"
  ,"    let (lenS, lenV) = (length s, length v)"
  ,"    arrS <- newSTListArray (1, lenS) s"
  ,"    arrV <- newSTListArray (1, lenV) v"
  ,"    f <- newArray ((0,0), (lenS, lenV)) (-1, OpInsert) :: ST s (STArray s (Int, Int) (Int, Operation))"
  ,"    forM_ [0 .. lenV] (\\i -> writeArray f (0, i) (i, OpInsert))"
  ,"    forM_ [0 .. lenS] (\\i -> writeArray f (i, 0) (i, OpDelete))"
  ,"    writeArray f (0,0) (0, OpNothing)"
  ,"    forM_ (range ((1,1), (lenS, lenV))) (\\(i, j) -> do"
  ,"        si <- readArray arrS i"
  ,"        sj <- readArray arrV j"
  ,"        if si == sj"
  ,"           then readArray f (i-1, j-1) >>= \\p -> writeArray f (i, j) (modifySnd OpNothing p)"
  ,"           else do"
  ,"               cModify <- readArray f (i-1, j-1)"
  ,"               cInsert <- readArray f (i, j-1)"
  ,"               cDelete <- readArray f (i-1, j)"
  ,"               writeArray f (i, j) ((\\(x, y) -> (x+1, y)) (minimum [ modifySnd OpModify cModify,"
  ,"                                                                     modifySnd OpInsert cInsert,"
  ,"                                                                     modifySnd OpDelete cDelete ]) ))"
  ,"    freeze f"
  ,"    where newSTListArray :: (Ix i) => (i,i) -> [a] -> ST s (STArray s i a)"
  ,"          newSTListArray = newListArray"
  ,"          modifySnd x (a, b) = (a, x)"]

structureEditStr = foldr1 newlineS
  ["structureEdit :: (Show s, Show v, Eq s, Eq v) => s -> BiGUL s v -> [s] -> [v] -> [s]"
  ,"structureEdit defSrc bx s v ="
  ,"  let sv = get (lensMap defSrc bx) s"
  ,"  in if sv == Nothing"
  ,"    then  s"
  ,"    else  let f = minEditDistDP (fromJust sv) v"
  ,"              reconstruct 0 0 [] = []"
  ,"              reconstruct i j s = case snd (f ! (i,j)) of"
  ,"                  OpNothing -> head s : reconstruct (i-1) (j-1) (tail s)"
  ,"                  OpModify -> head s : reconstruct (i-1) (j-1) (tail s)"
  ,"                  OpInsert -> fromJust (put bx defSrc (head v)) : reconstruct i (j-1) s"
  ,"                  OpDelete -> reconstruct (i-1) j (tail s)"
  ,"          in reverse $ reconstruct (length s) (length v) (reverse s)"]


bigulUnzip2Str = foldr1 newlineS
  ["bigulUnZip2 :: (Eq a, Show a, Eq b, Show b) => BiGUL [(a,b)] ([a],[b])"
  ,"bigulUnZip2 = Case"
  ,"  [ $(normalSV [p| [] |] [p| ([],[]) |] [p| [] |]) $ Skip (const ([],[]))"
  ,""
  ,"  , $(normalSV [p| _:_ |]  [p| (_:_, _:_) |] [p| _:_ |]) $"
  ,"      $(rearrV [| \\(a:as,b:bs) -> ((a,b), (as,bs))  |])"
  ,"        $(update [p| (a,b):abss |] [p| ((a,b),abss) |]"
  ,"                 [d| a = Replace;"
  ,"                     b = Replace;"
  ,"                     abss = bigulUnZip2 |])"
  ,"  , $(adaptiveSV [p| [] |]  [p| (_:_, _:_) |])"
  ,"      (\\_ (a:_,b:_) -> [(a,b)])"
  ,"  , $(adaptiveSV [p| _:_ |]  [p| ([],[]) |])"
  ,"      (\\_ _ -> [])"
  ,"  ]"
  ]

bigulUnZip3Str = foldr1 newlineS
  ["bigulUnZip3 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c) => BiGUL [(a,(b,c))] ([a],([b],[c]))"
  ,"bigulUnZip3 = Case"
  ,"  [ $(normalSV [p| [] |] [p| ([],([],[])) |] [p| [] |]) $ Skip (const ([],([],[])))"
  ,""
  ,"  , $(normalSV [p| _:_ |]  [p| (_:_ , (_:_ , _:_)) |] [p| _:_ |]) $"
  ,"      $(rearrV [| \\(a:as, (b:bs, c:cs)) -> ((a,(b,c)), (as,(bs,cs))) |])"
  ,"        $(update [p| (a,(b,c)):abcss |] [p| ((a,(b,c)),abcss) |]"
  ,"                 [d| a = Replace;"
  ,"                     b = Replace;"
  ,"                     c = Replace;"
  ,"                     abcss = bigulUnZip3 |])"
  ,"  , $(adaptiveSV [p| [] |]  [p| (_:_, (_:_, _:_)) |])"
  ,"      (\\_ (a:_ , (b:_ , c:_)) -> [(a,(b,c))])"
  ,"  , $(adaptiveSV [p| _:_ |]  [p| ([],([],[])) |])"
  ,"      (\\_ _ -> [])"
  ,"  ]"
  ]

bigulUnZip4Str = foldr1 newlineS
  ["bigulUnZip4 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c, Eq d, Show d) =>"
  ,"  BiGUL [(a,(b,(c,d)))] ([a],([b],([c],[d])))"
  ,"bigulUnZip4 = Case"
  ,"  [ $(normalSV [p| [] |] [p| ([],([],([],[]))) |] [p| [] |]) $ Skip (const ([],([],([],[]))))"
  ,""
  ,"  , $(normalSV [p| _:_ |]  [p| (_:_ , (_:_ , (_:_ , _:_))) |] [p| _:_ |]) $"
  ,"      $(rearrV [| \\(a:as, (b:bs, (c:cs, d:ds))) -> ((a,(b,(c,d))), (as,(bs,(cs,ds)))) |])"
  ,"        $(update [p| (a,(b,(c,d))):abcdss |] [p| ((a,(b,(c,d))),abcdss) |]"
  ,"                 [d| a = Replace;"
  ,"                     b = Replace;"
  ,"                     c = Replace;"
  ,"                     d = Replace;"
  ,"                     abcdss = bigulUnZip4 |])"
  ,"  , $(adaptiveSV [p| [] |]  [p| (_:_, (_:_, (_:_, _:_))) |])"
  ,"      (\\_ (a:_ , (b:_ , (c:_, d:_))) -> [(a,(b,(c,d)))])"
  ,"  , $(adaptiveSV [p| _:_ |]  [p| ([],([],([],[]))) |])"
  ,"      (\\_ _ -> [])"
  ,"  ]"
  ]

bigulUnZip5Str = foldr1 newlineS
  ["bigulUnZip5 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c, Eq d, Show d, Eq e, Show e) =>"
  ,"  BiGUL [(a,(b,(c,(d,e))))] ([a],([b],([c],([d],[e]))))"
  ,"bigulUnZip5 = Case"
  ,"  [ $(normalSV [p| [] |] [p| ([],([],([],([],[])))) |] [p| [] |]) $ Skip (const ([],([],([],([],[])))))"
  ,""
  ,"  , $(normalSV [p| _:_ |]  [p| (_:_ , (_:_ , (_:_ , (_:_, _:_)))) |] [p| _:_ |]) $"
  ,"      $(rearrV [| \\(a:as, (b:bs, (c:cs, (d:ds, e:es)))) -> ((a,(b,(c,(d,e)))), (as,(bs,(cs,(ds,es))))) |])"
  ,"        $(update [p| (a,(b,(c,(d,e)))):abcdess |] [p| ((a,(b,(c,(d,e)))),abcdess) |]"
  ,"                 [d| a = Replace;"
  ,"                     b = Replace;"
  ,"                     c = Replace;"
  ,"                     d = Replace;"
  ,"                     e = Replace;"
  ,"                     abcdess = bigulUnZip5 |])"
  ,"  , $(adaptiveSV [p| [] |]  [p| (_:_, (_:_, (_:_, (_:_, _:_)))) |])"
  ,"      (\\_ (a:_ , (b:_ , (c:_, (d:_, e:_)))) -> [(a,(b,(c,(d,e))))])"
  ,"  , $(adaptiveSV [p| _:_ |]  [p| ([],([],([],([],[])))) |])"
  ,"      (\\_ _ -> [])"
  ,"  ]"
  ]

bigulUnZip6Str = foldr1 newlineS
  ["bigulUnZip6 :: (Eq a, Show a, Eq b, Show b, Eq c, Show c, Eq d, Show d, Eq e, Show e, Eq f, Show f) =>"
  ,"  BiGUL [(a,(b,(c,(d,(e,f)))))] ([a],([b],([c],([d],([e],[f])))))"
  ,"bigulUnZip6 = Case"
  ,"  [ $(normalSV [p| [] |] [p| ([],([],([],([],([],[]))))) |] [p| [] |]) $ Skip (const ([],([],([],([],([],[]))))))"
  ,""
  ,"  , $(normalSV [p| _:_ |]  [p| (_:_ , (_:_ , (_:_ , (_:_, (_:_, _:_))))) |] [p| _:_ |]) $"
  ,"      $(rearrV [| \\(a:as, (b:bs, (c:cs, (d:ds, (e:es, f:fs))))) -> ((a,(b,(c,(d,(e,f))))), (as,(bs,(cs,(ds,(es,fs)))))) |])"
  ,"        $(update [p| (a,(b,(c,(d,(e,f))))):abcdefss |] [p| ((a,(b,(c,(d,(e,f))))),abcdefss) |]"
  ,"                 [d| a = Replace;"
  ,"                     b = Replace;"
  ,"                     c = Replace;"
  ,"                     d = Replace;"
  ,"                     e = Replace;"
  ,"                     f = Replace;"
  ,"                     abcdefss = bigulUnZip6 |])"
  ,"  , $(adaptiveSV [p| [] |]  [p| (_:_, (_:_, (_:_, (_:_, (_:_, _:_))))) |])"
  ,"      (\\_ (a:_ , (b:_ , (c:_, (d:_, (e:_, f:_))))) -> [(a,(b,(c,(d,(e,f)))))])"
  ,"  , $(adaptiveSV [p| _:_ |]  [p| ([],([],([],([],([],[]))))) |])"
  ,"      (\\_ _ -> [])"
  ,"  ]"
  ]

bigulUnZip7Str = "bigulUnZip7 = error \"bigulUnZip7Str not (mechanically) generated yet. contact the author\""
bigulUnZip8Str = "bigulUnZip8 = error \"bigulUnZip8Str not (mechanically) generated yet. contact the author\""
bigulUnZip9Str = "bigulUnZip9 = error \"bigulUnZip9Str not (mechanically) generated yet. contact the author\""
bigulUnZip10Str = "bigulUnZip10 = error \"bigulUnZip10Str not (mechanically) generated yet. contact the author\""
bigulUnZip11Str = "bigulUnZip11 = error \"bigulUnZi11Str not (mechanically) generated yet. contact the author\""

--------------------

numTyBiGULDefs :: String
numTyBiGULDefs = foldr1 newlineSS
  [replaceBiStringStr
  ,replaceBiIdentStr
  ,replaceBiNumericStr, safeReadBiNumericStr
  ,replaceBiNatStr, safeReadBiNatStr
  ,embStr]

replaceBiStringStr =
  "replaceBiString :: BiGUL (BiTuple String String) String" `newlineS`
  "replaceBiString = $(update [p| BiTuple s _ |] [p| s |] [d| s = Replace |])"

replaceBiIdentStr = "replaceBiIdentifier = replaceBiString"

replaceBiNumericStr =
  "replaceBiNumeric :: (Num a, Show a, Read a, Eq a) => BiGUL (BiTuple String String) a" `newlineS`
  "replaceBiNumeric = emb (\\(BiTuple n _) -> safeReadBiNumric n) (\\(BiTuple _ lay) v -> (BiTuple (show v) lay))"

safeReadBiNumericStr =
  "safeReadBiNumric :: (Num a, Show a, Read a) => String -> a" `newlineS`
  "safeReadBiNumric \"\" = 0" `newlineS`
  "safeReadBiNumric s  = read s"

replaceBiNatStr =
  "replaceStrNat :: BiGUL (BiTuple String String) Natural" `newlineS`
  "replaceStrNat = emb (\\(BiTuple n _) -> safeReadNat n) (\\(BiTuple _ lay) v -> (BiTuple (show v) lay))"

safeReadBiNatStr =
  "safeReadNat :: String -> Natural" `newlineS`
  "safeReadNat \"\" = 0" `newlineS`
  "safeReadNat s  = read s :: Natural"

embStr = foldr newlineS ""
  ["emb :: Eq v => (s -> v) -> (s -> v -> s) -> BiGUL s v"
  ,"emb g p = Case"
  ,"  [ $(normal [| \\s v -> g s == v |] [p| _ |]) (Skip g)"
  ,"  , $(adaptive [| \\s v -> True |]) p]"
  ]
