{-# LANGUAGE OverloadedStrings #-}

module BiYacc.Translation.BX.GenCaseCond where

import Prelude hiding (lookup, (<>))
import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.DataQuery
import BiYacc.Translation.BX.GenBasicUpdate

import Control.Monad
import Data.Generics hiding (empty)
import Text.PrettyPrint as TPP hiding (empty)
import qualified Text.PrettyPrint as TPP

{-
The file handles entrance and exit condition parts of a Case sentence.
Case $(normal [|\spat vpat -> entrance predicate |] [|\s -> exit predicate|] ) ...
-}

genCaseCond :: SrcSide -> ViewSide -> (Doc, Doc)
genCaseCond sside vside =
  let (sPat , vPat, exitPat) = genCaseCond1 sside vside
      entCond = "\\" <> sPat <+> vPat <+> "->" <+> "True"
  in  (entCond, exitPat) -- currently no exit predicate (on source). so pat = cond.

-- [|\spat vpat -> predicate (constraint) |] [|\s -> exit predicate|]
-- return (sPat , vPat, exitPat)
genCaseCond1 :: SrcSide -> ViewSide -> (Doc, Doc, Doc)
genCaseCond1 sside vside =
  (genEntSPat sside, genEntVPat vside, genExitSPat sside)


-- generate entrance patterns consisting of only a constructor wild cards, and variables.
-- used for pattern matching on source and decomposing source
genEntSPat :: SrcSide -> Doc
genEntSPat ss@(SrcSide _ con maybeUpdates) =
  parens $ text con <+> hsep (map genEachUnit maybeUpdates)
  where
    genEachUnit :: UpdateUnit -> Doc
    -- WARNING. ERROR. better to EXPAND the pattern precisely
    genEachUnit (Left _) = wildcardDoc
    genEachUnit (Right (NUpdate{}))  = wildcardDoc
    genEachUnit (Right (NoUpdate{})) = wildcardDoc
    genEachUnit (Right (NUpdateWithLookAhead _ _ rhs))  = genEntSPat rhs
    genEachUnit (Right (Expansion rhs))  = genEntSPat rhs


-- generate entrance patterns consisting of only Constructor, wild cards, and variables.
-- used for pattern matching on view and decomposing view.
genEntVPat = vSide2HsCode

genExitSPat = genEntSPat
