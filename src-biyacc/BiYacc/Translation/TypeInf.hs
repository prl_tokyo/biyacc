module BiYacc.Translation.TypeInf where

import Prelude hiding (lookup, (<>))
import BiYacc.Language.Def
import BiYacc.Helper.Utils
-- import BiYacc.Parser.ParserInterface

import Data.Map as Map hiding (map, filter)
import Data.Maybe (catMaybes)
import Control.Monad.Reader
import Data.Generics

import Text.Show.Pretty
import Debug.Trace

-----------------------------------------------
-- build env from constructors to their fields.
-- Add ---> ([Arith, Arith], Arith)
buildCon2FieldsEnv :: DataTypeDecs -> Cons2FieldsEnv
buildCon2FieldsEnv datatypes = unions $ map goInto1 datatypes
  where
    goInto1 :: DataTypeDec -> Cons2FieldsEnv
    goInto1 (DataTypeDec ty typedefs) = unions $ map (goInto2 ty) typedefs
    goInto2 :: DataTypeRep -> DataTypeDef -> Cons2FieldsEnv
    goInto2 ty (DataTypeDef cons tyDefs) = singleton cons (tyDefs,ty)


applyTyAnno :: Data a => DataTypeRep -> DataTypeDecs -> a -> a
applyTyAnno grpVTy tyDecs =
  everywhere' (mkT (addTypeAnno' grpVTy tyDecs))

-- well, we only do it for viwe-side. since type-annotation of source-side is
-- already included in the parsing phase.
addTypeAnno' :: DataTypeRep ->     -- (group) view type
                DataTypeDecs ->    -- Type declarations
                Rule ->            -- data to be processed
                Rule
addTypeAnno' vTy@(TyCon vTyName _) tyDecs (vPat , sSide) =
  let substL = produceSubst vTy tyDecs
      vPat'   = if isVarPat vPat
                  then handleVarP vPat vTy
                  else addTypeAnno tyDecs (vPat, vTy)
                  -- else addTypeAnno tyDecL (vPat, vTy)
  in (vPat' , sSide)


-- substitutions of type variables to concrete data types
-- the first argument is the concrete data type appeared in consistency relations (e.g. source type)
-- the second argument is the data type declarations user defined before
produceSubst :: DataTypeRep -> DataTypeDecs -> [(String,DataTypeRep)]
produceSubst (TyCon tyName []) _ = []
produceSubst (TyCon tyName ts) decs =
  let vars = findTyVars tyName decs
  in  zip vars ts

findTyVars :: String -> DataTypeDecs -> [String]
findTyVars tyName [] = []
findTyVars tyName (aa@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    -- then map (\(TyVar v) -> v) vars
    then map g vars
    else findTyVars tyName decs
  where
    g (TyVar v) = v
    g a = error $ ppShow aa

-- instantiate parameterised type variables to concrete types and update the DataTypeDec
-- the first argument is a list of (Parameterised-TyVar, Concrete-Types) pair
applyTyVarSubsts :: [(String, DataTypeRep)] -> DataTypeDec -> DataTypeDec
applyTyVarSubsts [] dec = dec
applyTyVarSubsts (subst:substs) dec@(DataTypeDec (TyCon tyName _) _) =
  applyTyVarSubsts substs $ everywhere' (mkT (applyOneSubst tyName subst)) dec
  where
    applyOneSubst :: String -> (String, DataTypeRep) -> DataTypeRep -> DataTypeRep
    applyOneSubst _ (v, new) tv@(TyVar v') | v == v' = new
    applyOneSubst _ _ tyRep = tyRep


-- the substitution for one data type, replace type variables with concrete data types
-- e.g. user may define : data List a = ... ; data Tree a = ...
-- we only want to replace "a" with "Int" in the data type "List" but not "Tree"
applyTyVarSubsts' :: String -> [(String, DataTypeRep)] -> [DataTypeDec] -> [DataTypeDec]
applyTyVarSubsts' _ _ [] = []
applyTyVarSubsts' tyName substs (dec@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    then applyTyVarSubsts substs dec : decs
    else dec : applyTyVarSubsts' tyName substs decs


applyTyVarSubstsAndSelect :: String -> [(String, DataTypeRep)] -> [DataTypeDec] -> DataTypeDec
applyTyVarSubstsAndSelect tyName substs [] = error $
  "Impossible. Type variable substitutions failed.\n" ++
  "Type variable name: " ++ tyName ++ "\n" ++
  "Subst: " ++ show substs ++ "\n" ++
  "check if there are typos"
applyTyVarSubstsAndSelect tyName substs (dec@(DataTypeDec (TyCon tyName' vars) _) : decs) =
  if tyName == tyName'
    then applyTyVarSubsts substs dec
    else applyTyVarSubstsAndSelect tyName substs decs


addTypeAnno :: DataTypeDecs ->
               (Pattern, DataTypeRep) ->  -- data to be processed
               Pattern
addTypeAnno tyDecs (ConP consName _ pats , ty@(TyCon tyName _)) =
  -- produce and apply type variable substitutions and build new env
  let subst   = produceSubst ty tyDecs
      tyDecs' = applyTyVarSubsts' tyName subst tyDecs
      env'    = buildCon2FieldsEnv tyDecs'
  -- lookup pats' types in the new env and recursively invoking addTypeAnno
      subTys' = case Map.lookup consName env' of
                  Nothing -> error $ "Cannot lookup a pattern's type. \n" ++
                             "Pattern's constructor name: " ++ show consName ++ "\n"
                  Just a -> fst a
      pats'   = map (addTypeAnno tyDecs') (zip pats subTys')
  in  ConP consName ty pats'

addTypeAnno tyDecs (VarP var _ , ty) = VarP var ty
addTypeAnno tyDecs (AsP var _ inner, ty) =
  AsP var ty (addTypeAnno tyDecs (inner, ty))
addTypeAnno tyDecs (WildP _ , ty) = WildP ty
addTypeAnno _ (litP, _) = litP


-- the case when the view pattern is merely a variable and whose type should be
-- referred to the group view type
handleVarP :: Pattern     ->   -- data to be processed
              DataTypeRep ->   -- the type of the view (type of the group of actions)
              Pattern

handleVarP (VarP uv' _) vt = VarP uv' vt
handleVarP (AsP uv' _ inner) vt = AsP uv' vt inner
handleVarP pat _  = pat



-- accept variable name version
findVarType :: Var -> Pattern -> Maybe DataTypeRep
findVarType var p@(ConP _ ty subpats) =
  case catMaybes (map (findVarType var) subpats) of
    [ty] -> Just ty
    t:ts  -> case all (== t) ts of
    -- handle non-linear patterns
      True  -> Just t
      False -> error $ "variable [" ++ (init var) ++
                 "] occurres more than once in the pattern:\n" ++ show p ++ "\n" ++
                 "with different types"
    []   -> Nothing
findVarType var (VarP var' ty) =
  if var /= var' then Nothing else Just ty
findVarType var (AsP var' ty innerPat) =
  if var == var' then Just ty else findVarType var innerPat
findVarType _ _ = Nothing
