{-# LANGUAGE OverloadedStrings #-}
module BiYacc.Translation.GenDataTypes where

import Prelude hiding (lookup, (<>))
import BiYacc.Language.Def
import BiYacc.Helper.Utils

import qualified Data.Map as Map (lookup)
import Data.Maybe (fromJust)
import Data.Generics hiding (empty)
import Text.PrettyPrint as TPP hiding (empty)
import qualified Text.PrettyPrint as TPP


-- generate abstract syntax data types and DeriveBiGulGenerics
prtAbsDTs' :: DataTypeDecs -> String
prtAbsDTs' decls = render (prtAbsDTs decls) `newlineSS` genDrvBiGULGenericAST decls

prtAbsDTs :: DataTypeDecs -> Doc
prtAbsDTs = vcat2 . map prtAbsDT

prtAbsDT :: DataTypeDec -> Doc
prtAbsDT (DataTypeDec ty defs) =
  "data" <+> text (printTypeRep ty) <+> "=" $+$
  nest2 (foldl1c (\e -> "  " <> prtDef e) (\es e -> es $$ "|" <+> prtDef e) defs) $+$
  nest2 "deriving (Show, Read, Eq, Ord, Data, Typeable)"
  where prtDef :: DataTypeDef -> Doc
        prtDef (DataTypeDef con fields) =
          hsep $ [text con] ++ map (text . wrapParen . printTypeRep) fields
        -- if there is any space, it means that the type has parameter.
        wrapParen :: String -> String
        wrapParen s | elem ' ' s = "(" ++ s ++ ")"
        wrapParen s = s


genDrvBiGULGenericAST :: DataTypeDecs -> String
genDrvBiGULGenericAST tydecs = foldr newlineS "" $
  map (\(DataTypeDec t _) -> "deriveBiGULGeneric ''" ++ (\(TyCon n _) -> n) t) tydecs

----------------------------------------

--- print the AST of #Concrete to Haskell datatypes-------
-- haskell data types do not include disambiguation filters annotation
-- print concrete syntax data types
prtConcDTs :: NontDefs -> String
prtConcDTs defs =
  render $ foldr ($+$) TPP.empty (punctuate1 "\n" (map prtSrcDatatype defs))
         $+$ text (genDrvBiGULGenericCST defs)

prtSrcDatatype :: NontDef -> Doc
prtSrcDatatype (NontDef dName defs) =
  "data" <+> text dName <+> equals $+$
    nest2 (foldl1c (\x -> "  " <> x) (\xs x -> xs $$ ("|" <+> x) ) (map prtSrcTypeDef defs))
    $$ nest2 data_drv
  where
    data_drv  = "deriving (Show, Read, Eq, Data, Typeable)"

prtSrcTypeDef :: Prod -> Doc
prtSrcTypeDef (Prod consName typefields _) = hsep (text consName : map go typefields)
  -- represent terminals as strings, with surrounding comments and layouts
  where
    go :: ProdField -> Doc
    go = parens . text . printTypeRep . either
          (\_ -> mkTyRep "BiTuple" [mkSimpTyRep "String", mkSimpTyRep "String"])
          (\t -> mkTyRep t [])


genDrvBiGULGenericCST :: NontDefs -> String
genDrvBiGULGenericCST ntDefs =
  foldr newlineS "" (map (\(NontDef ty _) -> "deriveBiGULGeneric ''" +^+  ty) ntDefs)
