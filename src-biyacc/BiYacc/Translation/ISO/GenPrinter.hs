module BiYacc.Translation.ISO.GenPrinter (genPrinter) where

import BiYacc.Language.Def
import BiYacc.Helper.Utils

import Control.Monad.State
import Debug.Trace


-- the printer code for "from CST back to program text"
genPrinter :: NontDefs -> String
genPrinter ntDefs =
  let st = extractTopLevelSrcTy ntDefs
      topPrinterStr = "topPP :: " ++ st ++ " -> String\n" ++
                    "topPP = " ++ "pp" ++ st ++ "\n\n"
  in  topPrinterStr ++ "\n\n" ++ concatMap genEachPrinter ntDefs

genEachPrinter :: NontDef -> String
genEachPrinter (NontDef nontTy prods) =
  let funName = "pp" ++ nontTy
  in  foldr (\x xs -> genPrts funName x ++ xs) "" prods ++ "\n"

genPrts :: String -> Prod -> String
genPrts funName (Prod pn fields _) =
  let strs  = evalState (mapM genPrtRes fields) 0
      strs' = if length strs /= 0
                then foldr1 (\x xs -> x ++ " ++ " ++ xs) strs
                else show pn -- usually the default null node
  in  funName ++ " (" ++ pn ++ genPrtPatt 0 fields ++ ") =" ++ strs' ++ "\n"

-- the integer is for counting string cointaining whitespaces.
genPrtPatt :: Int -> [a] -> String
genPrtPatt n (x:xs) = " s" ++ show n ++ genPrtPatt (n + 1) xs
genPrtPatt n [] = ""


-- foldr (\(te0,te1,...) -> te0 ++ te1) ""
-- [(String, [(String, Expr)])]
-- foldr (\(te0, te1) -> te0 ++ foldr (\(te0, te1) xs -> te0 ++ show te1 ++ xs) te1 ) xe
genPrtRes :: ProdField -> State Int String
genPrtRes (Left  x) = do
  n <- get
  modify(+1)
  return $ " fstBiTuple s" ++ show n ++ " ++ sndBiTuple s" ++ show n
genPrtRes (Right x) = do
  n <- get
  modify(+1)
  return $ " " ++ ("pp" ++ x) ++ " s" ++ show n
