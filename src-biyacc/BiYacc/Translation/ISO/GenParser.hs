module BiYacc.Translation.ISO.GenParser (genLexerAndParser, genParserWrapper) where

import Prelude hiding (lookup)

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.Translation.ISO.GenLexer

import Data.List as List hiding (lookup)
import Data.Char (isAlpha)
import qualified Data.Map as Map
import Data.Map hiding (foldl, foldr, map, drop, filter, (\\))
import Data.Generics hiding (GT, LT, EQ)

import Control.Monad.State
import Debug.Trace


genLexerAndParser :: ([Cmd], NontDefs) -> String
genLexerAndParser (cmds, ntDefs) =
  let st = extractTopLevelSrcTy ntDefs
      primFlags = markPrim ntDefs
      (termToTokIdEnv, tokIdToTermEnv) = evalState (genTerminalEnv ntDefs) 0
      TerminalToTokenIdEnv nakedtermToTokIdEnv = termToTokIdEnv
      -- sort by longerFirst is important. because the pattern ":=" should come before ":" to avoid the overlap
      tokens = map fst $ sortBy longerFirst $ toList nakedtermToTokIdEnv
  in  foldr1 newlineS
      [genHeader primFlags
      ,genTokenPart tokIdToTermEnv
      ,genHappyRules st termToTokIdEnv ntDefs
      ,""
      ,""
      ,"{"
      ,parseErrStr
      ,""
      ,genTokenDef primFlags termToTokIdEnv tokens
      ,genTokPrinter primFlags termToTokIdEnv tokens
      ,""
      ,genLexer  termToTokIdEnv tokens (comment cmds)
      ,genWrapWS termToTokIdEnv tokens
      ,""
      ,"forest_lookup f i = fromJust $ Map.lookup f i"
      ,"}"
      ]

-- (identFlag, strFlag, numericFlag)
markPrim :: (Data a) => a -> (Bool, Bool, Bool)
markPrim prodDefs =
  let res = nub $ everything (++) (mkQ [] collect) prodDefs
      [x,y,z] = map (\pt -> pt `elem` res) primTypes
  in  (x,y,z)
  where
    collect :: ProdField -> [String]
    collect (Left nt) | isPrimTypes nt = [nt]
    collect _ = []

-- Basically fixed strings.
-- Only generate definitions of primitive tokens if they are used by the user
genHeader :: (Bool, Bool, Bool) -> String
genHeader (identFlag, strFlag, numericFlag) =
  foldr newlineS ""
  ["{"
  ,"module ProgParser where\n"
  ,""
  ,"import Data.Char"
  ,"import Data.Map as Map (lookup)"
  ,"import Data.Maybe (fromJust)"
  ,"import Data.List (any)"
  ,"import LangDef"
  ,"import Text.Show.Pretty (ppShow)"
  ,"}"
  ,""
  ,"%tokentype { Token }"
  ,"%name parser"
  ,"%lexer { lexer } { TokenEOF }"
  ,"%error { parseError }"
  ,""
  ,"%token"
  ,(if identFlag   then   "  Identifier { TokenIdentifier $$ }\n" else "")
  ,(if strFlag     then   "  String     { TokenString   $$ }\n" else "")
  ,(if numericFlag then   "  Numeric    { TokenNumeric  $$ }" else "")
  ]

-- generate %token part
genTokenPart :: (TokenIdToTerminalEnv) -> String
genTokenPart (TokenIdToTerminalEnv tokEnv) =
  let tokLists = toList tokEnv
  -- in  foldr (\(tokId, terminal) accStr -> "  " ++ wrapQuote terminal ++
  --               "  { " ++ tokId ++ " ("++ wrapDQuote terminal ++ "," ++ "\"\") }\n"
  --               ++ accStr) "" tokLists
  in  foldr (\(tokId, terminal) accStr -> "  " ++ wrapQuote terminal ++
                "  { " ++ tokId ++ " $$ }\n"
                ++ accStr) "" tokLists

wrapQuote :: String -> String
wrapQuote s = "'" ++ s ++ "'"

wrapDQuote :: String -> String
wrapDQuote s = "\"" ++ s ++ "\""


-- generate Happy program
genHappyRules :: String -> TerminalToTokenIdEnv -> [NontDef] -> String
genHappyRules st (TerminalToTokenIdEnv tokEnv) cdatatypes =
  let (NontDef nont _) = head cdatatypes
      defs = map prtEachRule cdatatypes
  in  foldr1 (++)
    ["%%\n\n"
    ,"ProgStart :: { " ++ st ++ " }\n"
    ,"   : " ++ nont ++ " { $1 }\n"
    ,"   | { " ++ nont ++ "Null }\n\n"
    ,foldr1 newlineSS defs]
  where
    -- 每行可能都会产生数个newSyntaxProdRule
    prtEachRule :: NontDef -> String
    prtEachRule (NontDef nont defs) =
      let eachLineStrs = map prtDefs2 defs
          prods = foldl1 (\xs x -> xs ++ "\n  | " ++ x) eachLineStrs
      in  nont ++ " :: { " ++ nont ++ " }" ++ "\n  : " ++ prods

    prtDefs2 :: Prod -> String
    prtDefs2 (Prod con fields _ ) =
      let symbols = map prtEachDef2 fields
          str1 = foldl1 (\xs x -> xs ++ " " ++ x) symbols
          str2 = str1 ++ "    { " ++ con ++ " " ++ prtEachDef3 1 fields ++ "}"
      in  str2

    prtEachDef2 :: ProdField -> String
    prtEachDef2 (Left  x) | x `elem` primTypes = x
    prtEachDef2 (Left  x) | otherwise          = wrapQuote x
    prtEachDef2 (Right x) = x ++ ""

    prtEachDef3 :: Int -> [a] -> String
    prtEachDef3 n (x : xs) = "$" ++ show n ++ " " ++ prtEachDef3 (n + 1) xs
    prtEachDef3 n [] = ""


genEachCmd :: TerminalToTokenIdEnv -> [ProdField] -> String
genEachCmd (TerminalToTokenIdEnv tokEnv) [] = ""
genEachCmd (TerminalToTokenIdEnv tokEnv) (Left x : cs) =
  let Just tok = lookup x tokEnv
  in  " " ++ lowerTok tok ++ genEachCmd (TerminalToTokenIdEnv tokEnv) cs
  where
    lowerTok :: String -> String
    lowerTok (c:cs) = 't' : cs

-- other definitions included in the Happy file
parseErrStr :: String
parseErrStr =
  "parseError :: [Token] -> a" `newlineS`
  "parseError toks = error $ \"Parse error.\" ++"  `newlineS`
  "  \"\\nFirst 20 unconsumed tokens:\\n\" ++" +^+ "ppShow (take 20 toks) ++" `newlineS`
  "  \"\\nConverted to steram:\\n\" ++" +^+ "(concatMap pToken $ take 20 toks)"


--------------- a wrapper for the generated glr parser
genParserWrapper :: String -> String
genParserWrapper st = foldr newlineS ""
  ["module ParserWrapper where"
  ,"import LangDef"
  ,"import ProgParserData"
  ,"import ProgParser as ProgParser"
  ,"import Data.Maybe (fromJust)"
  ,"import qualified Data.Map as Map"
  ,"import Text.Show.Pretty (ppShow)"
  ,""
  ,"parserWrapper prog ="
  ,"  let toks = map (: []) . mergeWS $ TokenWS \"\" : lexer prog"
  ,"  in  case parser (tail toks) of"
  ,"        ProgParser.ParseEOF f  ->"
  ,"          error $ \"Premature end of input:\\n\" ++ unlines (map show $ Map.toList f)"
  ,""
  ,"        ProgParser.ParseError ts f ->"
  ,"          let unconsumedts = take 20 $ map (snd . head) ts"
  ,"          in  error $ \"Parse error. \" ++ \"number of results: \" ++ show (length ts) ++"
  ,"                \"\\nFirst 20 unconsumed tokens:\\n\" ++ ppShow unconsumedts ++"
  ,"                \"\\nConverted to steram:\\n\" ++ concatMap showToks unconsumedts"
  ,""
  ,"        ProgParser.ParseOK r f ->"
  ,"          let csts = (ProgParser.decode (flip forest_lookup f) r ::[" ++ st ++ "])"
  ,"          in  (pToken (head .head $ toks), csts)"
  ,""
  ,"-- showToks :: GSymbol -> String"
  ,"showToks (HappyTok tok) = pToken tok"
  ,"showToks other = \"\""
  ]
