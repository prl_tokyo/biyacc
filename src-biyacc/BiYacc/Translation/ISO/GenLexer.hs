module BiYacc.Translation.ISO.GenLexer where

import Prelude hiding (lookup)

import BiYacc.Language.Def
import BiYacc.Helper.Utils

import Data.List as List hiding (lookup)
import Data.Char (isAlpha)
import qualified Data.Map as Map
import Data.Map hiding (foldl, foldr, map, drop, filter, (\\))
import Data.Generics hiding (GT, LT, EQ)

import Control.Monad.State
import Debug.Trace


--          terminals env -> [token] -> (lineComm, (blockStartComm, blockEndComm))
genLexer :: TerminalToTokenIdEnv -> [String] -> (String, (String, String)) -> String
genLexer (TerminalToTokenIdEnv tokEnv) cs comntSym@(lComm, (bCommS, bCommE)) =
  foldr newlineS ""
  ["isWhiteSpace :: Char -> Bool"
  ,"isWhiteSpace c = elem c \" \\n\\t\\r\\v\""
  ,""
  ,"isValidIdent :: Char -> Bool"
  ,"isValidIdent c = isAlphaNum c || c == '_'"
  ,""
  ,"isWildRangeIdent :: Char -> Bool"
  ,"isWildRangeIdent c = isAlphaNum c || any ((==) c) \"-_~.:;#&@{}[]()'\""
  ,""
  ,"adhockSeparatable :: Char -> String -> Bool"
  ,"adhockSeparatable c [] = True"
  ,"adhockSeparatable c (sep:_) = isWhiteSpace sep"
  ,"  || (isValidIdent c && not (isValidIdent sep))"
  ,"  || (not (isValidIdent c) && isValidIdent sep)"
  ,"  || (isSymbol c || isPunctuation c) && (isSymbol sep || isPunctuation sep)"
  ,""
  ,"lexer :: String -> [Token]"
  ,"lexer [] = []"
  ,if List.null lComm
    then ""
    else "lexer (" ++ expand lComm ++ ":cs) = lexerComment1 \"" ++
          mkEscape (reverse lComm) ++ "\" cs"
  ,if List.null bCommS
    then ""
    else "lexer (" ++ expand bCommS ++ ":cs) = lexerComment2 \"" ++
          mkEscape (reverse bCommS) ++ "\" cs"
  ,concat (map prtLexRule cs)
  ,"lexer (c:cs) | isDigit c ="
  ,"  let (var, rest) = span isDigit (c:cs)"
  ,"  in case rest of"
  ,"        '.':rs -> let (var2, rest2) = span isDigit rs"
  ,"                 in  if not (null var2)"
  ,"                     then TokenNumeric (toBiTuple (var ++ \".\" ++ var2, \"\")) : lexer rest2"
  ,"                     else TokenNumeric (toBiTuple (var, \"\")) : lexer rest"
  ,"        _     -> TokenNumeric (toBiTuple (var, \"\")) : lexer rest"
  ,"  | isWhiteSpace c = let (ws, rest) = span isWhiteSpace (c:cs)"
  ,"      in TokenWS  ws  : lexer rest"
  ,"  | isValidIdent c = let (var, rest) = span isValidIdent (c:cs)"
  ,"      in case var of"
  ,"          _ -> TokenIdentifier (toBiTuple (var, \"\")) : lexer rest"
  ,"lexer ('\"':cs) = lexerString " ++ show "\"" ++ " cs"
  ,""
  ,"lexer x = error $ \"Non-exhaustive patterns in function lexer. First unconsumed 100 characters:\\n\" ++ take 100 x"
  ,""
  ,"lexerComment1 :: String -> String -> [Token]"
  ,"lexerComment1 res [] = [TokenWS (reverse res)]"
  ,"lexerComment1 res ('\\n':cs) = TokenWS (reverse ('\\n':res)) : lexer cs"
  ,"lexerComment1 res (c:cs) = lexerComment1 (c:res) cs"
  ,""
  ,"lexerComment2 :: String -> String -> [Token]"
  ,"lexerComment2 res [] = [TokenWS (reverse res)]"
  ,"lexerComment2 res (" ++ expand (bCommE) ++ ":cs) = TokenWS (reverse (" ++
      expand (reverse bCommE) ++ ":res)) : lexer cs"
  ,"lexerComment2 res (c:cs) = lexerComment2 (c:res) cs"
  ,""
  ,"lexerString :: String -> String -> [Token]"
  ,"lexerString res ('\\\\':'\\\\':cs) = lexerString ('\\\\':'\\\\':res) cs"
  ,"lexerString res ('\\\\':'\"':cs) = lexerString ('\"':'\\\\':res) cs"
  ,"lexerString res ('\"':cs) = TokenString (toBiTuple (reverse ('\"':res), \"\")) : lexer cs"
  ,"lexerString res (c:cs) = lexerString (c:res) cs"
  ,"lexerString res [] = [TokenString (toBiTuple (reverse res, \"\"))]"
  ,""
  ,"toBiTuple :: (a,b) -> BiTuple a b"
  ,"toBiTuple (x,y) = BiTuple x y"
  ,""
  ]
  where
    prtLexRule :: String -> String
    prtLexRule s =
      let Just tok = lookup s tokEnv
      in  "lexer (" ++ expandWithMark s ++ ":cs"  ++ ") | adhockSeparatable c1 cs" +^+ "="
          ++ "\n  " ++ tok +^+ "(BiTuple" +^+ show s +^+ "\"\")" +^+ ":" +^+ "lexer cs\n"

    expand :: String -> String
    expand []  = show ""
    expand [c] = show c
    expand (c:cs) = '\'':(mkEscape [c]) ++ ('\'':':':(expand cs))

    expandWithMark :: String -> String
    expandWithMark [c] = "c1@" ++ show c
    expandWithMark (c:cs) = '\'':(mkEscape [c]) ++ ('\'':':':(expandWithMark cs))


mkEscape :: String -> String
mkEscape = foldr (\e es -> f e ++ es) []
  where f '\"' = "\\\""
        f '\\' = "\\\\"
        f c    = [c]

--------------------
-- generate token definitions: data Token = TokenEOF | ...
genTokenDef :: (Bool, Bool, Bool) -> TerminalToTokenIdEnv -> [String] -> String
genTokenDef (strFlag, identFlag, numFlag) (TerminalToTokenIdEnv tokEnv) s =
  foldr newlineS ""
  ["data Token ="
  ,"    TokenEOF"
  ,"  | TokenWS       String"
  ,"  | TokenString     (BiTuple String String)"
  ,"  | TokenIdentifier (BiTuple String String)"
  ,"  | TokenNumeric    (BiTuple String String)"
  ,prtEachTok s
  ,"  deriving (Show, Eq, Ord)"]

  where
    prtEachTok :: [String] -> String
    prtEachTok [] = ""
    prtEachTok (c:cs) =
      let Just tok = lookup c tokEnv
      in  "  | " ++ tok ++ " (BiTuple String String)\n" ++ prtEachTok cs


-- the function converting tokens back to program text is used to show parse errors
genTokPrinter :: (Bool, Bool, Bool) -> TerminalToTokenIdEnv -> [String] -> String
genTokPrinter (strFlag, identFlag, numFlag) (TerminalToTokenIdEnv tokEnv) s =
  (if strFlag    then "pToken (TokenString   (BiTuple s ws))   = s ++ ws\n" else "") ++
  (if identFlag  then "pToken (TokenIdentifier (BiTuple s ws)) = s ++ ws\n" else "") ++
  (if numFlag    then "pToken (TokenNumeric  (BiTuple s ws))   = s ++ ws\n" else "") ++
  "pToken (TokenWS s) = s\n" ++
  prtEachShow s
  where
    prtEachShow :: [String] -> String
    prtEachShow [] = ""
    prtEachShow (c:cs) = let Just tok = lookup c tokEnv in
      "pToken (" ++ tok ++ " (BiTuple tok ws)) = tok ++ ws" ++ "\n" ++ prtEachShow cs



---------------------
newtype TerminalToTokenIdEnv = TerminalToTokenIdEnv (Map String String) deriving (Show, Eq)
newtype TokenIdToTerminalEnv = TokenIdToTerminalEnv (Map String String) deriving (Show, Eq)

-- generate Map from terminal to its Token
genTerminalEnv :: [NontDef] -> State Int (TerminalToTokenIdEnv, TokenIdToTerminalEnv)
genTerminalEnv datatypes = do
  env1 <- liftM unions' $ mapM gen2 datatypes
  let TerminalToTokenIdEnv nakedEnv1 = env1
      env2 = TokenIdToTerminalEnv . fromList . map (\(k,a) -> (a,k)) . toList $ nakedEnv1
  return (env1, env2)
  where
    gen2 :: NontDef -> State Int TerminalToTokenIdEnv
    gen2 (NontDef _ ctypedefs) = liftM unions' $ mapM gen3 ctypedefs

    gen3 :: Prod -> State Int TerminalToTokenIdEnv
    gen3 (Prod _ fields _ ) = liftM unions' $ mapM gen4 fields

    gen4 :: ProdField -> State Int TerminalToTokenIdEnv
    gen4 (Left terminal) | terminal `elem` primTypes =
      return (TerminalToTokenIdEnv Map.empty)
    gen4 (Left terminal) | otherwise = do
      i <- get
      modify (+1)
      return $ TerminalToTokenIdEnv $ singleton terminal ("Token" ++ show i)
    gen4 (Right _) = return (TerminalToTokenIdEnv Map.empty)

    unions' :: [TerminalToTokenIdEnv] -> TerminalToTokenIdEnv
    unions' list = TerminalToTokenIdEnv . unions $
      map (\(TerminalToTokenIdEnv env) -> env) list


-- wrap whitespaces behind tokens
genWrapWS :: (TerminalToTokenIdEnv) -> [String] -> String
genWrapWS (TerminalToTokenIdEnv tokEnv) cs =
  (foldr newlineS ""
      ["wrapWS :: Token -> String -> Token"
      ,"wrapWS (TokenString (BiTuple s c))     ws = TokenString (BiTuple s (c ++ ws))"
      ,"wrapWS (TokenIdentifier (BiTuple s c)) ws = TokenIdentifier (BiTuple s (c ++ ws))"
      ,"wrapWS (TokenNumeric (BiTuple s c))  ws   = TokenNumeric (BiTuple s (c ++ ws))"
      ,"wrapWS (TokenWS c) ws = TokenWS (c ++ ws)"
      ])
  ++ genWrapEach cs `newlineS`
  (foldr newlineS ""
      ["mergeWS :: [Token] -> [Token]"
      ,"mergeWS [] = []"
      ,"mergeWS (tok : TokenWS ws : toks) = mergeWS (wrapWS tok ws : toks)"
      ,"mergeWS (tok : toks) = tok : mergeWS toks"
      ])
  where
    genWrapEach :: [String] -> String
    genWrapEach [] = ""
    genWrapEach (c:cs) = let Just tok = lookup c tokEnv in
      "wrapWS (" ++ tok ++ " (BiTuple tok ws0)) ws = " ++
      tok ++ " (BiTuple tok (ws0 ++ ws))\n" ++
      genWrapEach cs


comment :: [Cmd] -> (String, (String, String))
comment [] = ("//", ("/*", "*/"))
comment ((CmdCommLine s):xs) = (s, snd (comment xs))
comment ((CmdCommBlock sl sr):xs) = (fst (comment xs), (sl, sr))
comment (x:xs) = comment xs


longerFirst :: (String,String) -> (String,String) -> Ordering
longerFirst (a,_) (b,_) =
  if length a == length b
    then EQ
    else if length a > length b
      then LT
      else GT
