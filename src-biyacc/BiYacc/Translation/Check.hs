{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts #-}

module BiYacc.Translation.Check where

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.DataQuery
import BiYacc.Parser.ParserInterface
import BiYacc.Translation.BX.GenBasicUpdate
import BiYacc.Translation.TypeInf

import qualified Data.Map as Map (lookup, elems, keys)
import Data.Maybe (fromJust)
import Control.Monad.Writer
import Control.Monad.Reader
import Data.Generics hiding (empty)

-- the input is tempNontDefs, which does not contain "NullConstructor", and "LayoutField"
allCheck :: NontDefs -> Program -> Cons2FieldsEnv -> String
allCheck tmpNontDefs tmpProgram csEnv =
  let grps = getGroups tmpProgram
      pcEnv = buildPat2ConsEnv tmpNontDefs
      cpEnv = buildCons2PatEnv tmpNontDefs
      tyNameEnv = buildTyNameEnv tmpNontDefs
      (flag1, errLog) = runWriter $ mapM (allCheckG tyNameEnv pcEnv csEnv) grps
      warnLog = maybe "" id (simpleExhaustCheck tmpProgram tyNameEnv cpEnv pcEnv)
  in  if and flag1
      then warnLog
      else error $ foldr1c id newlineS errLog


-- check all for a group of actions
allCheckG :: TypeNameEnv -> Pat2ConsEnv -> Cons2FieldsEnv ->
  Group -> Writer [String] Bool
allCheckG tenv pcEnv csEnv (Group ty@(_ , st_) rules) = do
  let st = printTypeRep st_
  flag1 <- case Map.lookup st tenv of
              Nothing -> do tell $ ["the non-terminal" +^+ ppShow st +^+ "in the type declaration of an action group"
                                   +^+ "is not defined in concrete syntax."
                                   +^+ "maybe a wrong type is written?"]
                            return False
              Just _  -> return True
  flag2s <- mapM (allCheckR pcEnv csEnv ty) rules
  return $ and (flag1:flag2s)

-- check all for a single rule
allCheckR :: Pat2ConsEnv -> Cons2FieldsEnv -> (ViewType,SourceType) ->
  Rule -> Writer [String] Bool
allCheckR p2nEnv csEnv (groupVT , groupST) rule@(vSide , sSide) = do
  let SrcSide  stype consName units = sSide
      varsInS = getVarFromS sSide
      varsInV = getVarFromV vSide

  flags1 <- mapM (isVarDefined vSide) varsInS
  flag3 <- isProdDefined p2nEnv groupST sSide
  flag4 <- noWildcard vSide
  flag5 <- noVarPatInAsP vSide
  flag6 <- isConsDefined csEnv groupVT vSide
  flag7 <- vVarTyCheck vSide groupVT csEnv
  flag8 <- isNTDefined p2nEnv sSide
  return (and (flags1 ++ [flag3, flag4, flag5, flag6, flag7, flag8]))

-- check if a upd-var in SrcSide is defined in ViewSide
isVarDefined :: ViewSide -> Var -> Writer [String] Bool
isVarDefined vsp uv = case findVarType uv vsp of
  Nothing -> do tell $ ["cannot find the update variable" +^+ uv
                        +^+ "in the view pattern (ViewSide)" `newlineS`
                        "ViewSide pattern:" +^+ ppShow (vSide2HsCode vsp)]
                return False
  Just _  -> return True

-- check if a variable defined in ViewSide is used in SrcSide
isVarUsed :: SrcSide -> String -> Writer [String] Bool
isVarUsed sSide var = do
  let vars = getVarFromS sSide
  case var `elem` vars of
    False -> do tell $ ["variable" +^+ var +^+ "is not used in source side." `newlineS`
                        "Source side:" +^+ (sSide2ProdStr sSide)]
                return False
    True  -> return True


-- check if a production rule in #Actions part is defined in #Concrete
isProdDefined :: Pat2ConsEnv -> SourceType -> SrcSide -> Writer [String] Bool
isProdDefined pcEnv st_ sSide@(SrcSide _ _ units) = do
  let st = printTypeRep st_
      prodStr = sSide2ProdStr sSide
  case Map.lookup (getProdPat units , st) pcEnv of
    Nothing -> do tell ["\nIn #Actions, find an undefined production rule " ++
                        "(deep pattern is also checked)" `newlineS` prodStr]
                  return False
    Just _  -> liftM and (mapM chkDeepPat units) -- go on checking deep pattern
  where
    chkDeepPat :: UpdateUnit -> Writer [String] Bool
    chkDeepPat (Right (Expansion s@(SrcSide ty _ units))) =
      isProdDefined pcEnv ty s
    chkDeepPat (Right (NUpdateWithLookAhead _ st0 s@(SrcSide ty _ units))) =
      isProdDefined pcEnv ty s
    chkDeepPat _ = return True


-- is view-side valid?  (no wildcard pat.)
noWildcard :: ViewSide -> Writer [String] Bool
noWildcard vSide = do
  let res = everythingBut (++) (mkQ ([],False) wild) vSide
  if null res
    then return True
    else do tell $ ["invalid wildcard (_) pattern found in view side."
                   +^+ render (vSide2HsCode vSide)]
            return False
  where
    wild :: Pattern -> ([Pattern],Bool)
    wild (WildP t) = ([WildP t], False)
    wild (AsP{})   = ([], True)
    wild _         = ([], False)

-- patterns in AsP must not be varaibles
noVarPatInAsP :: ViewSide -> Writer [String] Bool
noVarPatInAsP vSide = do
  let asp  = listify findAsP vSide
      varp = listify findVarP asp
  case null varp of
    True  -> return True
    False -> do tell ["cannot use variable pattern within an as-pattern."
                      +^+ render (vSide2HsCode vSide)]
                return False
  where findAsP :: Pattern -> Bool
        findAsP (AsP{}) = True
        findAsP _ = False
        findVarP :: Pattern -> Bool
        findVarP (VarP{}) = True
        findVarP _ = False

-- is constructors in view-side defined (and with correct arities)
isConsDefined :: Cons2FieldsEnv -> ViewType ->
  ViewSide -> Writer [String] Bool
isConsDefined csEnv groupVTDecl vSide = do
  -- let csEnv' = extendEnv groupVTDecl csEnv
  let csEnv' = csEnv
      conps = everything (++) (mkQ [] (getConP)) vSide
  flags <- mapM (isConsDefined1 csEnv') conps
  return (and flags)
  where
    getConP :: Pattern -> [Pattern]
    getConP c@(ConP {}) = [c]
    getConP _ = []

    isConsDefined1 :: Cons2FieldsEnv -> Pattern -> Writer [String] Bool
    isConsDefined1 env (ConP cName ty vsps) = case Map.lookup cName env of
      Nothing -> if isBuildIn cName
          then return True
          else do tell ["In #Actions part, the constructor  " +^+ cName +^+ "  is not defined."
                       ,"Pre-defined types are only String, Int, and Integer."+^+
                         "Currently it does not recognize other Haskell's builtin types."]
                  return False
      Just _  -> return True
    isBuildIn :: String -> Bool
    isBuildIn = flip elem ["TupleC", "ConsList", "Singleton", "EmptyList"
                          , "Just", "Nothing", "Left", "Right"]

-- is a nonterminal defined?
-- after isProdDefined checking, we can make sure that nonterminals used in
-- #Actions are already defined. However, currently BiYacc only supports 4 kinds of
-- primitive types. so we check them.
isNTDefined :: Pat2ConsEnv -> SrcSide -> Writer [String] Bool
isNTDefined p2nEnv sSide =
  let definedNTs = nub $ map snd $ Map.keys p2nEnv
      allNTs     = nub $ getNTNames sSide
      suspicious = allNTs \\ (definedNTs ++ primTypes)
  in  case null suspicious of
        True  -> return True
        False -> do  flags <- mapM errMsg suspicious
                     return False
  where errMsg ty = do
          tell ["\nUnsupported primitive type (pre-defined type)  " +^+ ty +^+
               "  in concrete syntax (or #Actions part)." `newlineS`
               "Currently only support Numeric, String, and Identifier."]
          return False


vVarTyCheck :: ViewSide->        -- data to be processed
               ViewType ->       -- the type of the view (type of the group of actions)
               Cons2FieldsEnv -> -- env from constructor to ([subsequent field], type)
               Writer [String] Bool
vVarTyCheck vsp groupVT csEnv =
  let vsp'   = vVarTyCheckB vsp groupVT
      csEnv' = csEnv
      maw    = runWriterT (vVarTyCheck2 vsp' csEnv' Nothing)
  in  writer (runReader maw 0)

vVarTyCheck2 :: Pattern -> -- data to be processed
                Cons2FieldsEnv ->  -- env from constructor to ([subsequent field], type)
                Maybe String ->    -- constructor of a data type. E.g.: the “Add” in “Add lhs rhs”.
                                   -- to be used for variable pattern.
                                   -- set to Nothing if it is boaderline cases.
                WriterT [String] (Reader Int) Bool
vVarTyCheck2 (ConP consName _ vsps) csEnv maybeCon  = do
  oldLoc <- lift ask
  case Map.lookup consName csEnv of
    Just (_, ty)  -> do
      -- when encounter a constructor. Its following field should (re)counter position from 0.
      flags <- mapM (\(p, iLoc) -> local (const iLoc) (go oldLoc p)) (zip vsps [0..])
      return $ and flags
    Nothing -> vVarTyCheckErrMsg oldLoc consName maybeCon csEnv
  where
    go :: Int -> Pattern -> WriterT [String] (Reader Int) Bool
    go oldLoc p = do
      iLoc <- lift ask
      case Map.lookup consName csEnv of
        Just (tys , _) -> do
          let ty = tys !! iLoc
              -- csEnv' = extendEnv ty csEnv
              csEnv' = csEnv
          local (const iLoc) (vVarTyCheck2 p csEnv' (Just consName))
        Nothing -> vVarTyCheckErrMsg oldLoc consName maybeCon csEnv


-- variable pattern === wildcard, which does not go wrong (and produce no log)
vVarTyCheck2 pp@(AsP name _ vsp) csEnv (Just lastCon) = do
  loc <- lift ask
  let (tys, _) = fromJ "impossible. 0xC12." (Map.lookup lastCon csEnv)
      thisTy = tys !! loc
  -- True && b === b. so just call function on inner part
  vVarTyCheck2 vsp csEnv (Just lastCon)

vVarTyCheck2 (VarP var _) csEnv (Just lastCon) = do
  loc <- lift ask
  let (tys , _) = fromJ "impossible. 0xC11." (Map.lookup lastCon csEnv)
      thisTy = tys !! loc
  return True

-- should do more checks
vVarTyCheck2 i _ _ = return True



vVarTyCheckErrMsg :: Int -> String -> Maybe String -> Cons2FieldsEnv ->
  WriterT [String] (Reader Int) Bool
vVarTyCheckErrMsg oldLoc consName maybeCon csEnv = do
  let lastCon = maybe "" id maybeCon
      expectedTy = case Map.lookup lastCon csEnv of
                    Nothing -> mkSimpTyRep "(sorry, not inferred)"
                    Just (expectedTys, _) -> expectedTys !! oldLoc
  tell ["\nType mismatches at the" +^+ count oldLoc +^+ "field of constructor" +^+ lastCon ++
        "\nThe expected type is: " ++ printTypeRep expectedTy ++
        "\nBut find constructor: " ++ consName]
  return False
  where
    -- make some error message for different constructors
    -- intpC :: String -> String
    -- intpC "Singleton" = "the syntax represents list type"
    -- intpC "ConsList"  = "the syntax represents list type"
    -- intpC "EmptyList" = "the syntax represents list type"
    -- intpC "TupleC"    = "the syntax represents Tuple type"
    -- intpC "Just"      = "the syntax represents Maybe type"
    -- intpC "Nothing"   = "the syntax represents Maybe type"
    -- intpC "Left"      = "the syntax represents Either type"
    -- intpC "Right"     = "the syntax represents Either type"
    -- intpC ty          = ty
    count n | last (show n) == '0' = show (n+1) ++ "st"
    count n | last (show n) == '1' = show (n+1) ++ "nd"
    count n | last (show n) == '2' = show (n+1) ++ "rd"
    count n | True = show (n+1) ++ "th"


-- borderline cases
vVarTyCheckB :: Pattern -> -- data to be processed
                ViewType ->        -- the type of the view (type of the group of actions)
                Pattern
vVarTyCheckB (ConP "ConsList" _ [v1, v2, vs]) cc@(TyCon "List" [t]) =
  let v1' = vVarTyCheckB v1 t
      v2' = vVarTyCheckB v2 t
      vs' = vVarTyCheckB vs cc
  in  ConP "ConsList" cc [v1', v2', vs']

vVarTyCheckB (ConP "ConsList" _ [v, vs]) cc@(TyCon "List" [t]) =
  let v' = vVarTyCheckB v t
      vs' = vVarTyCheckB vs cc
  in  ConP "ConsList" cc [v', vs']

vVarTyCheckB (ConP "Singleton" _ [v]) cc@(TyCon "List" [t]) =
  let v' = vVarTyCheckB v t
  in  ConP "Singleton" cc [v']

vVarTyCheckB (ConP "TupleC" _ vvs) cc@(TyCon "TupleT" tts) =
  let vvs' = zipWith (\v t -> vVarTyCheckB v t) vvs tts
  in  ConP "TupleC" cc vvs'

vVarTyCheckB ((AsP uv _ vsp)) vt =
  let vsp' = vVarTyCheckB vsp vt
  in  AsP uv vt vsp'

vVarTyCheckB (VarP uv' _) vt = VarP uv' vt

vVarTyCheckB pat _  = pat

------------------------------------------------------------------------
-- check whether a group of actions use all the production rules.
simpleExhaustCheck :: Program -> TypeNameEnv -> Cons2PatEnv -> Pat2ConsEnv -> Maybe String
simpleExhaustCheck (Program _ grps) tenv n2pEnv p2nEnv =
  case catMaybes (map (checkGrp tenv n2pEnv p2nEnv) grps) of
    [] -> Nothing
    errMsgs -> Just $ "warning: (however, the program may still be correct)\n" ++
                      foldr1c id newlineS errMsgs

-- Maybe (error message)
checkGrp :: TypeNameEnv -> Cons2PatEnv -> Pat2ConsEnv -> Group -> Maybe String
checkGrp tenv n2pEnv p2nEnv (Group (vt , st_) rules) =
  let st = printTypeRep st_ in
  let conNames = sort $ fromJust (Map.lookup st tenv)
      namesWrt  = nubSortedList . sort . filter notNull . map (extPrdName st p2nEnv) $ rules
      -- extPrdName adaptation rules return empty strings. filter (not . null) ignore them.
      lack      = map (\e -> "In group: " ++ printTypeRep vt +^+ "+>" +^+ st ++ "," `newlineS`
                       "production not found: " ++ st +^+ "->" +^+ errMsgRHS e)
                      (conNames \\ namesWrt)
      errMsgRHS name = case Map.lookup name n2pEnv of
                         Nothing -> error "impossible. in errMsgRHS. 0x00."
                         Just a  -> prtPrdFd . fst $ a
  in  case Prelude.null lack of
        True -> Nothing
        False -> Just $  foldr1c id newlineS lack

nubSortedList :: Eq a => [a] -> [a]
nubSortedList [] = []
nubSortedList [x] = [x]
nubSortedList (x:y:z) = if x == y then nubSortedList (y:z) else x: nubSortedList (y:z)

extPrdName :: String -> Pat2ConsEnv -> Rule -> String
extPrdName st p2nEnv (_ , SrcSide _ _ tnts) =
  fromJust (Map.lookup (getProdPat tnts,st) p2nEnv)
