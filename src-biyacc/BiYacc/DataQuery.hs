module BiYacc.DataQuery where

import BiYacc.Language.Def
import BiYacc.Helper.Utils

import Data.Generics

-- if source side has non-linear variable usage
hasDupVars :: SrcSide -> Bool
hasDupVars = not . null . findDup . getVarFromS

hasVVars :: Data a => a -> Bool
hasVVars = not . null . getVarFromV

-- if source side has update variables
hasSVars :: Data a => a -> Bool
hasSVars = not . null . getVarFromS

getGroups :: (Data a) => a -> [Group]
getGroups prog = listify p prog
  where p :: Group -> Bool
        p (Group{}) = True

-- extract patterns of a production rule
getProdPat :: [UpdateUnit] -> [ProdField]
getProdPat = map go
  where
    go :: UpdateUnit -> ProdField
    go (Left t)    = Left t
    go (Right (NUpdate _ ty)) | isPrimTypes' ty = Left (printTypeRep ty)
    go (Right (NUpdate _ ty)) | otherwise       = Right (printTypeRep ty)

    go (Right (NUpdateWithLookAhead _ ty _)) = Right (printTypeRep ty)
    go (Right (Expansion (SrcSide ty _ _)))  = Right (printTypeRep ty)

    go (Right (NoUpdate ty)) | isPrimTypes' ty = Left (printTypeRep ty)
    go (Right (NoUpdate ty)) | otherwise       = Right (printTypeRep ty)

-- collect nonterminals in String representation.
getNTNames :: Data a => a -> [String]
getNTNames = everything (++) (mkQ [] ntquery)
  where
    ntquery :: Either Terminal Update -> [String]
    -- ntquery (Left (Nonterminal st)) = [printTypeRep st]
    ntquery (Right (NUpdate _ st))  = [printTypeRep st]
    ntquery (Right (NUpdateWithLookAhead _ st _)) = [printTypeRep st]
    ntquery _ = []


-- collect view-vars (update vars) in String representation from SrcSide. cannot use listify.
getVarFromS :: Data a => a -> [String]
getVarFromS = everything (++) (mkQ [] vquery)
  where
    vquery :: Either Terminal Update -> [String]
    vquery (Right (NUpdate var st)) = [var]
    vquery (Right (NUpdateWithLookAhead var _ _)) = [var]
    vquery _ = []


-- collect variables from view-side. cannot use listify.
getVarFromV :: (Data a, Typeable a) => a -> [String]
getVarFromV = everything (++) (mkQ [] collect)
  where collect (VarP var _)      = [var]
        collect (AsP var _ _) = [var]
        collect _ = []


getVSideLits :: (Data a, Typeable a) => a -> [String]
getVSideLits = everything (++) (mkQ [] extLit)
  where extLit :: Pattern -> [String]
        extLit (LitP lit) = [extractLitToString lit]
        extLit _ = []


-- getSAllParts :: Data a => a -> [UpdateUnit]
-- getSAllParts = everythingBut (++) (const ([], False) `extQ` go)
--   where
--     go :: UpdateUnit -> ([UpdateUnit],Bool)
--     go s@(UpdateUnitSingle _)   = ([s],False)
--     go s@(UpdateUnitKleene _ _) = ([s],True)
--     go s@(ProtectKleene{}) = ([s],True)
--     go (UpdateUnitFromKleene{}) = error "cannot be UpdateUnitFromKleene. 0xC7."


-- getSKleeneStarParts :: Data a => a -> [UpdateUnit]
-- getSKleeneStarParts = listify isKleeneStarPart

-- isKleeneStarPart :: UpdateUnit -> Bool
-- isKleeneStarPart (UpdateUnitKleene{}) = True
-- isKleeneStarPart (ProtectKleene{}) = True
-- isKleeneStarPart _ = False


saveDesiredVPat :: [String] -> Pattern -> [Pattern]
saveDesiredVPat ns pp@(AsP var _ _) | var `elem` ns = [pp]
saveDesiredVPat ns pp@(VarP var _)      | var `elem` ns = [pp]
saveDesiredVPat ns pp@(LitP l)          | extractLitToString l `elem` ns = [pp]
saveDesiredVPat _ _ = []


isDesiredVPat :: [String] -> Pattern -> Bool
isDesiredVPat names pp@(AsP var _ _) = var `elem` names
isDesiredVPat names pp@(VarP var _)      = var `elem` names
isDesiredVPat names pp@(LitP l)          = extractLitToString l `elem` names
isDesiredVPat _ _ = False


extractSSideTy :: SrcSide -> DataTypeRep
extractSSideTy (SrcSide ty _ _) = ty


extractUpdSrcTy :: UpdateUnit -> DataTypeRep
-- extractUpdSrcTy (UpdateUnitSingle (Left (Nonterminal ty))) = ty
extractUpdSrcTy (Left _) = mkTyRep "BiTuple" [mkSimpTyRep "String", mkSimpTyRep "String"]
extractUpdSrcTy (Right (NUpdate _ ty)) = ty
extractUpdSrcTy (Right (NUpdateWithLookAhead _ ty _ )) = ty
extractUpdSrcTy (Right (Expansion (SrcSide ty _ _))) = ty
extractUpdSrcTy a = error $ "extractUpdSrcTy: " ++ ppShow a
  -- error "unsupported data. extractUpdSrcTy. 0xC3"

extractVarTy :: Pattern -> DataTypeRep
extractVarTy (AsP _ ty _) = ty
extractVarTy (VarP _ ty)      = ty
extractVarTy (ConP _ ty _)     = ty -- warning
extractVarTy (LitP (LitStr _))       = mkSimpTyRep "String"
extractVarTy (LitP (LitInt _))   = mkSimpTyRep "Integer"
extractVarTy (LitP (LitDecimal _)  ) = mkSimpTyRep "Double"
extractVarTy _ = error "cannot extract type. extractVarTy. 0xC2."

extractLitToString :: LitPat -> String
extractLitToString lit = case lit of
  LitStr s     -> show s
  LitInt i     -> show i
  LitDecimal d -> show d
  -- LitBoolPat b    -> show b



---------------- for filters

selPatMatchCmds :: [Cmd] -> [Cmd]
selPatMatchCmds = filter (\f -> case f of CmdPatMatch _ -> True; _ -> False)

selPrioCmds :: [Cmd] -> [Cmd]
selPrioCmds = filter (\f -> case f of CmdPrio _ -> True; _ -> False)

selLAssocCmds :: [Cmd] -> [Cmd]
selLAssocCmds = filter (\f -> case f of CmdLAssoc _ -> True; _ -> False)

selRAssocCmds :: [Cmd] -> [Cmd]
selRAssocCmds = filter (\f -> case f of CmdRAssoc _ -> True; _ -> False)

selNonAssocCmds :: [Cmd] -> [Cmd]
selNonAssocCmds = filter (\f -> case f of CmdNonAssoc _ -> True; _ -> False)

selAssocCmds :: [Cmd] -> [Cmd]
selAssocCmds (CmdLAssoc f : cmds)   = CmdLAssoc   f : selAssocCmds cmds
selAssocCmds (CmdRAssoc f : cmds)   = CmdRAssoc   f : selAssocCmds cmds
selAssocCmds (CmdNonAssoc f : cmds) = CmdNonAssoc f : selAssocCmds cmds
selAssocCmds (_ : cmds) = selAssocCmds cmds
selAssocCmds [] = []
