module BiYacc.Parser.ParserInterface where

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.Parser.TypeDefParser
import BiYacc.Parser.Concrete
import BiYacc.Parser.Directives
import BiYacc.Parser.ActionParser
import BiYacc.Parser.OtherFilters
import BiYacc.DataQuery
import BiYacc.Translation.ISO.GenPrinter
import BiYacc.Translation.TypeInf

import Data.Generics
import Data.Char (toUpper)

import Data.Map as Map hiding (map, foldr)
import qualified Data.Map as Map

import Text.Parsec
import Control.Monad.Reader (runReader)




getTypeDefStr :: Parsec String u (String, SourcePos)
getTypeDefStr = byLexeme (do
  byLexeme (string "#Abstract" <?> "need the keyword \"#Abstract\"")
  pos <- getPosition
  (decls, j) <- try (manyTill anyChar wConcrete >>= \cs -> return (cs , Just "Concrete")) <|>
                try (manyTill anyChar wDirectives >>= \cs -> return (cs , Just "Directives")) <|>
                try (manyTill anyChar wActions >>= \cs -> return (cs , Just "Actions"))
  inp <- getInput
  case j of
    -- just now we consume an additional "Concrete" string. Set it back.
    Just "Concrete" -> setInput ("#Concrete" ++ inp)
    Just "Directives" -> setInput ("#Directives" ++ inp)
    Just "Actions" -> setInput ("#Actions" ++ inp)
  return ("#Abstract\n" ++ decls, pos))

getNontDefStr :: Parsec String u (String, SourcePos)
getNontDefStr = byLexeme (do
  pos <- getPosition
  byLexeme (string "#Concrete" <?> "need the keyword \"#Concrete\"")
  (decls, j) <- try (manyTill anyChar wDirectives >>= \cs -> return (cs , Just "Directives")) <|>
                try (manyTill anyChar wActions >>= \cs -> return (cs , Just "Actions"))
  inp <- getInput
  case j of
    -- just now we consume an additional "Concrete" string. Set it back.
    Just "Directives" -> setInput ("#Directives" ++ inp)
    Just "Actions" -> setInput ("#Actions" ++ inp)
  return ("#Concrete\n" ++ decls, pos))

getActionStr :: Parsec String u (String, SourcePos)
getActionStr = byLexeme (do
  byLexeme (string "#Actions" <?> "need the keyword \"#Action\"")
  pos <- getPosition
  decls <- try (manyTill anyChar (wOtherFilters <|> (eof >> return "")))
  inp <- getInput
  setInput ("#OtherFilters" ++ inp)
  return ("#Actions\n" ++ decls, pos))

--
wConcrete :: Parsec String u String
wConcrete = try (string "#Concrete" <?> "expecting keyword #Concrete")

wDirectives :: Parsec String u String
wDirectives = try (string "#Directives" <?> "expecting keyword #Directives")

wActions :: Parsec String u String
wActions = try (string "#Actions" <?> "expecting keyword #Actions")

wOtherFilters :: Parsec String u String
wOtherFilters = try (string "#OtherFilters" <?> "expecting keyword #OtherFilters")


--
getDirectivesStr :: Parsec String u (String, SourcePos)
getDirectivesStr = do
  pos <- getPosition
  byWhiteSpace
  byLexeme (string "#Directives" <?> "expecting keyword #Directives")
  directives <- manyTill anyChar (try (string "#Actions"))

  actionStr    <- getInput
  setInput ("#Actions\n" ++ actionStr)

  return ("#Directives\n" ++ directives , pos)

-- preprocess a biyacc program. Return the (abstract abstract part, Temp NontDefs of Concrete part,
-- unconsumed Action part and its position, ALL part)
-- In addition, if the user does not write the "Concrete" sytnax part, we will generate it from "Action" groups.
-- Then re-generate and process a new biyacc file with "Concrete" syntax part.
preProcess :: Parsec String u
  (String, (String,SourcePos), (String,SourcePos), (String, SourcePos), (String,SourcePos))
preProcess = do
  byWhiteSpace
  (tyDefStr,tyDefSrcPos) <- getTypeDefStr
  -- concrete part is there
  let conc = try (do
              (ntDefStr, ntSrcPos) <- getNontDefStr
              (directiveStr, directiveSrcPos) <- getDirectivesStr
              (actionStr, actionSrcPos) <- getActionStr
              otherFltStr <- getInput
              otherFltSrcPos <- getPosition
              return (tyDefStr , (ntDefStr,ntSrcPos), (directiveStr,directiveSrcPos)
                     ,(actionStr,actionSrcPos),(otherFltStr,otherFltSrcPos)))

      -- concrete part is missing. generate it from actions. directives part is there.
      direc = try (do
              (directiveStr,directiveSrcPos) <- getDirectivesStr
              dummyPos <- getPosition
              (actionStr, actionSrcPos) <- getActionStr
              otherFltStr <- getInput
              otherFltSrcPos <- getPosition
              let ntDefStr = newlineS "#Concrete" . genPrinter . genProdRulesFromActions .
                                actionParser . actionLexer $ actionStr

              return (tyDefStr, (ntDefStr,dummyPos),(directiveStr,directiveSrcPos)
                     ,(actionStr,actionSrcPos), (otherFltStr,otherFltSrcPos)))

     -- both concrete part and directives part are missing.
      act = try (do
            -- actionStrPos    <- getPosition
            dummyPos <- getPosition
            (actionStr, actionSrcPos) <- getActionStr
            otherFltStr <- getInput
            otherFltSrcPos <- getPosition
            let ntDefStr = newlineS "#Concrete" . genPrinter . genProdRulesFromActions .
                              actionParser . actionLexer $ actionStr
            return (tyDefStr, (ntDefStr,dummyPos), ("",dummyPos)
                   ,(actionStr,actionSrcPos), (otherFltStr,otherFltSrcPos)))
  conc <|> direc <|> act

parseAll :: (String , (String,SourcePos) , (String,SourcePos) , (String,SourcePos) , (String,SourcePos)) ->
  (SynonymMap, DataTypeDecs, NontDefs, [Cmd], Program, Maybe (Doc,[(String,String)]))
parseAll (tyDefStr, (ntDefStr,ntDefPos), (directiveStr,directivePos)
         ,(actionStr,actionSrcPos), (otherFltStr, otherFltSrcPos)) =
  let (synonymMap, absTyDecls) = typeDefParser (typeDefLexer tyDefStr)
      rawNtDefs = map addProdName . either (error . show) id $
                    runParser (pNontDefs ntDefPos) () "" ntDefStr

      (pcEnv, cpEnv, _) = buildEnv rawNtDefs
      ppcEnv = fromList . map (\((k1,k2),v) -> (k1,v)) . toList $ pcEnv
      cmds = either (error . show) id $
              runParser (pDirectives pcEnv directivePos) () "" directiveStr
      rawActions = actionParser . actionLexer $ actionStr
      otherFlts = either (error . show) id $
                    runParser (pOtherFilters otherFltSrcPos) () "" otherFltStr
  in  (synonymMap, absTyDecls, rawNtDefs, cmds, rawActions, otherFlts)


------------ functions for processing production rules
renameBiYaccTy :: NontDefs -> NontDefs
renameBiYaccTy = everywhere (mkT changeETNT)
  where changeETNT :: ProdField -> ProdField
        changeETNT = either (Left . toBiYaccTy) Right


-- add production names to the parsed ProdDef
-- using its nonterminal name and postfix numbers
addProdName :: NontDef -> NontDef
addProdName (NontDef nt prods) = NontDef nt prods'
  where prods' = map go (zip3 (repeat nt) [1..] prods)
        go (nt, n, Prod "CONS_NOT_GENERATED_YET" fields b) =
          Prod (nt ++ show n) fields b
        go (_, _, already) = already

-- the input is tempNontDefs, which does not contain "NullConstructor" and "LayoutField"
buildEnv :: NontDefs -> (Pat2ConsEnv, Cons2PatEnv, TypeNameEnv)
buildEnv rawNtDefs =
  let ntDefs = renameBiYaccTy $ rawNtDefs
      pcEnv = buildPat2ConsEnv ntDefs
      cpEnv = buildCons2PatEnv ntDefs
      tyNameEnv          = buildTyNameEnv ntDefs
  in  (pcEnv, cpEnv, tyNameEnv)


buildCons2PatEnv :: NontDefs -> Cons2PatEnv
buildCons2PatEnv = unions . map go
  where
    go (NontDef ty ctypedefs) = unions (map (go2 ty) ctypedefs)
    go2 ty (Prod key patterns _) = singleton key (patterns,ty)

buildPat2ConsEnv :: NontDefs -> Pat2ConsEnv
buildPat2ConsEnv = unions . map go
  where go (NontDef ty ctypedefs) = unions (map (go2 ty) ctypedefs)
        go2 ty (Prod key patterns _) = (singleton (patterns,ty) key)

buildTyNameEnv :: NontDefs -> TypeNameEnv
buildTyNameEnv = unions . map go
  where
    go (NontDef tyName defs) = builtIn `union` (singleton tyName $ map (\(Prod n _ _) -> n ) defs)
    builtIn = fromList [("String", []), ("BiIdentifierTy", []), ("BiNumericTy", [])]

--------------------
-- change Integer, Double to String. change Name to BiYaccNameTy
progToBiYaccType :: Program -> Program
progToBiYaccType prog = everywhere (mkT go) prog
  where
    go :: Either Terminal Update -> Either Terminal Update
    go (Left st) = Left (toBiYaccTy $ st)
    go (Right (NUpdate uv st)) = Right (NUpdate uv (change st))
    go (Right (NUpdateWithLookAhead uv st inner)) =
      Right (NUpdateWithLookAhead uv (change st) inner)
    go (Right (NoUpdate st)) = Right (NoUpdate (change st))
    go a = a

    change st = mkSimpTyRep . toBiYaccTy . printTypeRep $ st


---------------------------------------------------
-- Add a constructor to the production rule (of the SrcSide of an action).
addProdRuleName :: Pat2ConsEnv -> Program -> Program
addProdRuleName pcEnv prog = everywhere (mkT go) prog
  where
    go :: Rule -> Rule
    go (vside , (SrcSide st _ units)) = (vside , add st pcEnv units)

    add :: SourceType -> Pat2ConsEnv -> [UpdateUnit] -> SrcSide
    add st pcEnv units =
      let pat = getProdPat units
          maybeProdName = Map.lookup (pat, (printTypeRep st)) pcEnv
      in  case maybeProdName of
            Nothing -> error $ "(impossible) cannot find pattern in concrete data type (no such production rule):\n"
                                ++ "(" ++ ppShow pat ++ ", " ++ printTypeRep st ++ ")"
            Just prodName -> SrcSide st prodName (map mkDeepPat units)
      where
        mkDeepPat :: UpdateUnit -> UpdateUnit
        mkDeepPat (Right (Expansion (SrcSide ty _ tnts))) =
          Right (Expansion (add ty pcEnv tnts))
        mkDeepPat (Right (NUpdateWithLookAhead uv st (SrcSide _ _ tnts))) =
          Right (NUpdateWithLookAhead uv st (add st pcEnv tnts))
        mkDeepPat other = other
---------------------------------------------------------------


buildBracketAttrEnv :: NontDefs -> BracketAttrEnv
buildBracketAttrEnv ntDefs = case everything (++) (mkQ [] search) ntDefs of
  []    -> Nothing
  [con] -> Just con
  _:_   -> error "more than one bracket attribute annotations"
  where
    search :: Prod -> [String]
    search (Prod con fs Nothing) = []
    search (Prod con fs (Just BracketAttr)) = [con]


-------------- from action parser

---------- automatically extract production rules from actions --------
genProdRulesFromActions :: Program -> [NontDef]
genProdRulesFromActions (Program entTy grps) =
  let theMap = unionsWith (++) $ map collectTypeDefs grps
  in elems (mapWithKey (\k v -> NontDef (printTypeRep k) v ) theMap)

-- collect maps from data types to their definitons
collectTypeDefs :: Group -> Map DataTypeRep [Prod]
collectTypeDefs (Group (vt,st) rules) = unionsWith (++) $ map (collectTypeDefs2 st) rules
-- map1 may contain Expr --> [Add .. ..,  ..]
-- map2 may contain Expr --> [Sub]
-- merge the two maps will give Expr --> [Add .. .., Sub .. .., .. ..]


collectTypeDefs2 :: SourceType -> Rule -> Map SourceType [Prod]
collectTypeDefs2 st (_ , rhs) =
  let (SrcSide _ _ units) = rhs
      map1 = unions $ map handleDeepUpdate units
      map2 = singleton st [(Prod "NONAMEYET" (map toProdField units) Nothing)]
  in  merge preserveMissing preserveMissing (zipWithMatched (\_ x y -> x ++ y)) map1 map2
-- map1 may contain Expr --> [Add .. ..,  ..] (because deep update strategy may contain new productions)
-- map2 may contain Expr --> [Sub]
-- merge the two maps will give Expr --> [Add .. .., Sub .. .., .. ..]

toProdField :: UpdateUnit -> ProdField
-- toProdField (UpdateUnitSingle (Left (Nonterminal nont)))  = PFSingle (Right (printTypeRep nont))
toProdField (Left t) = (Left t)
toProdField (Right (NUpdate _ nont)) = Right (printTypeRep nont)
toProdField (Right (Expansion (SrcSide ty _ _))) = Right (printTypeRep ty)


handleDeepUpdate :: UpdateUnit -> Map SourceType [Prod]
handleDeepUpdate (Right (Expansion rhs@(SrcSide ty _ _))) =
  collectTypeDefs2 ty (undefined,rhs)
handleDeepUpdate _ = Map.empty


--------------------
-- insert predefined type synonyms to SynonymMap.
insPredefinedTySynonyms :: SynonymMap -> SynonymMap
insPredefinedTySynonyms m =
  insert (mkSimpTyRep "BiNumeric")    (mkTyRep "BiTuple" [mkSimpTyRep "String", mkSimpTyRep "String"]) .
  insert (mkSimpTyRep "BiString")     (mkTyRep "BiTuple" [mkSimpTyRep "String", mkSimpTyRep "String"]) .
  insert (mkSimpTyRep "BiIdentifier") (mkTyRep "BiTuple" [mkSimpTyRep "String", mkSimpTyRep "String"]) $
  m

insPredefinedTypes :: DataTypeDecs -> DataTypeDecs
insPredefinedTypes decs =
  (DataTypeDec (mkTyRep "BiTuple" [TyVar "a", TyVar "b"])
     [DataTypeDef "BiTuple" [TyVar "a", TyVar "b"]]) : decs


upperFst (c:cs) = toUpper c : cs


renamePrimitives :: (Data a, Typeable a) => a -> a
renamePrimitives = everywhere (mkT rename)
  where
    rename :: Pattern -> Pattern
    rename (VarP v t0@(TyCon t [])) | isPrimTypes (printTypeRep t0) =
      VarP v (TyCon ("Bi" ++ upperFst t) [])
    rename a = a



-- some post process of the parse results for generating frontend components:
-- lexer, parser, and naive printer (for flattening cst back to program text)
-- (1) add null cases to each data types yielded from production rules
-- (2) generate data type declarations for nonterminals
processForFrontend :: NontDefs -> (DataTypeDecs, NontDefs)
processForFrontend ntDefs =
  let ntDefs' = addNullConstructor ntDefs
      ntTyDecs' = map nontToDataTypeDec ntDefs'
  in  (ntTyDecs', ntDefs')

addNullConstructor :: NontDefs -> NontDefs
addNullConstructor = everywhere (mkT addNC)
  where addNC :: NontDef -> NontDef
        addNC (NontDef prefix ctypedefs) =
          NontDef prefix (ctypedefs ++ [Prod (prefix ++ "Null") [] Nothing])


-- some post process of the parse results for generating backend components:
-- retentive get and put functions, and tree repairing functions
-- (3) Rename pre-defined primitives identifier, numeric, and string in prog
--     to their type synonyms: BiIdentifier, BiNumeric, BiString.
--     We can rename primitives to their final representations, however,
--     firstly use synonyms make the generation work easier.
-- (4) Merge the type declarations of AST and Productions
--     and substitute type synonyms including synonyms for primitives
--     (BiIdentifier, BiNumeric, and BiString)
-- (5) Generate another abstract representation for #Action part, for later
--     generating retentive get and put functions
processForBackend :: (SynonymMap, DataTypeDecs, DataTypeDecs, NontDefs, Program) ->
  (SynonymMap, DataTypeDecs, Program)
processForBackend (synonymMap, astTyDecs, ntTyDecs, ntDefs, prog) =
  let (Program tlTy grps) = addProdRuleName pcEnv . progToBiYaccType $ prog
      synonymMap' = insPredefinedTySynonyms synonymMap
      tyDecs      = replaceAllSynonyms synonymMap' . insPredefinedTypes $
                    astTyDecs
      -- tyDecs      = replaceAllSynonyms synonymMap' . insPredefinedTypes $
      --               astTyDecs ++ ntTyDecs
      (pcEnv, _ , _) = buildEnv ntDefs
      tlTy' = replaceAllSynonyms synonymMap' $ tlTy
      grps' = map (\g@(Group (vTy, _) _) -> applyTyAnno vTy tyDecs g) .
                replaceAllSynonyms synonymMap' $ grps
  in  (synonymMap', tyDecs, Program tlTy' grps')

-- a generic function replacing all the type synonyms in a piece of data
-- according to a given map (the table variable here)
replaceAllSynonyms :: Data a => SynonymMap -> a -> a
replaceAllSynonyms table = everywhere (mkT (replaceOneSynonym table))

-- replace type synonym by its definition
-- note that the replacement should be run several times to got the final type
-- e.g.: Type A = B; Type B = C. Then A should be evantually represetned by C but not B
replaceOneSynonym :: SynonymMap -> DataTypeRep -> DataTypeRep
replaceOneSynonym table old =
  case Map.lookup old table of
    Nothing -> old
    Just new -> case new of
      t@(TyCon con [])  -> replaceOneSynonym table t
      TyCon con tys -> let tys' = map (replaceOneSynonym table) tys
                       in  replaceOneSynonym table (TyCon con tys')
      t@(TyVar _) -> replaceOneSynonym table t

--
-- generate data type declaraions from productions (nonterminal definitions)
nontToDataTypeDec :: NontDef -> DataTypeDec
nontToDataTypeDec (NontDef nt defs) =
  DataTypeDec (mkTyRep nt []) (map prodToDataType defs)
  where
    prodToDataType :: Prod -> DataTypeDef
    prodToDataType (Prod con fields _) = DataTypeDef con (map go fields)

    -- go = either (\_ -> mkTyRep "String" [])
    -- The prefix "Bi" is for avoiding conflicts with user-defined type Tuple ...
    go = either (\_ -> mkTyRep "BiTuple" [mkSimpTyRep "String", mkSimpTyRep "String"])
                (\t -> mkTyRep t [])

----------------
