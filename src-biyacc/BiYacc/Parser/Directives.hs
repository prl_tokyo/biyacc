module BiYacc.Parser.Directives (pDirectives) where

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import BiYacc.Parser.Concrete

import Data.Map (Map)
import qualified Data.Map as Map
import Text.Parsec
import Text.PrettyPrint as TPP hiding (char)

------------------
-- interpret the directives for parsing and printing
pDirectives :: Pat2ConsEnv -> SourcePos -> Parsec String u [Cmd]
pDirectives pcEnv srcPos = do
  -- posForHappyConf <- getPosition
  setPosition srcPos
  byWhiteSpace
  byLexeme (string "#Directives" <?> "expecting keyword #Directives")

  commentSLine <- optionMaybe commLine
  commentSBlock <- optionMaybe commBlock
  let cmdComments = catMaybes [commentSLine,commentSBlock]


  hasPriDef <- byLexeme ((string "Priority:" >>= return . Just) <|> return Nothing)
  priDef <- case hasPriDef of
                Just _ -> many1 (try (pPriority  pcEnv))
                Nothing -> return []
  hasAssocDef <- byLexeme ((string "Associativity:" >>= return . Just)
                              <|> return Nothing)
  assocDef <- case hasAssocDef of
                Just _ -> do
                  -- There are Left-assoc defs and Right-assoc defs. At most two.
                  assocDef1 <- pAssocs pcEnv
                  assocDef2 <- try (pAssocs pcEnv) <|> return []
                  return (assocDef1 ++ assocDef2)
                Nothing -> return []

  return (cmdComments ++ priDef ++ assocDef)


commLine :: Parsec String u Cmd
commLine = do
  byLexeme (string "LineComment")
  byLexeme (char ':')
  LitStr x <- biStrLit <?> "please use double-quotes for single-line comment symbols"
  eatSemi
  return (CmdCommLine x)

commBlock :: Parsec String u Cmd
commBlock = do
  byLexeme (string "BlockComment")
  byLexeme (char ':')
  LitStr startMark <- biStrLit <?> "please use double-quotes to wrap symbols for multi-line comments"
  LitStr endMark   <- biStrLit <?> "please use double-quotes to wrap symbols for multi-line comments"
  eatSemi
  return (CmdCommBlock startMark endMark)


-- parse pattern matching directives
pPatMatchDef :: Parsec String u Cmd
pPatMatchDef = do
  pat <- pSimplePat
  eatLBrace
  byLexeme (string "FAIL")
  eatRBrace
  eatSemi
  return $ CmdPatMatch pat

-- Simple patterns start with a constructor and contain only constructors and wildcards
pSimplePat :: Parsec String u Pattern
pSimplePat = byLexeme (do
  con <- pConstructor
  args <- pConsArgs
  return $ ConP con (mkSimpTyRep "NO_NEED") args)

pConsArgs :: Parsec String u [Pattern]
pConsArgs =
  many ((eatWildCard >> return (WildP $ mkSimpTyRep "NO_NEED")) <|>
        (eatLParen >> pSimplePat >>= \p -> eatRParen >> return p))


-- a list of pairs containing production name. fst of a pair has higher priority
-- for priority: (x,y) means x <= y; if (x,y) and (y,x) and x === y.
pPriority :: Pat2ConsEnv -> Parsec String u Cmd
pPriority pcEnv = do
  conL <- try (pProd pcEnv) <|> pConstructor
  sy <- (byLexeme (string ">")) <|> (byLexeme (string "<"))
  conR <- try (pProd pcEnv) <|> pConstructor
  byLexeme (string ";")
  case sy of
    ">" -> return (CmdPrio (conL,conR))
    "<" -> return (CmdPrio (conR,conL))



-- parse priority directives
pProd :: Pat2ConsEnv -> Parsec String u ProdCons
pProd pcEnv = do
  nt <- nonterminal
  byLexeme (string "->")
  fields <- many1 parseField
  let con = maybe (error $ "\nproduction not defined:\n" ++ nt ++ " -> " ++ show fields) id
              (Map.lookup (map toBiType fields, nt) pcEnv)
  return con
  where
    toBiType :: ProdField -> ProdField
    toBiType (Right x) = Right . toBiYaccTy $ x
    toBiType a = a


-- The implementation is just for the moment and very unsafe.
-- pAssocOld :: Pat2ConsEnv -> Parsec String u Cmd
-- pAssocOld pcEnv = do
--   fixity <- byLexeme (string "Left:" <|> string "Right:" <|> string "NonAssoc:")
--   con <- try (pProd pcEnv) <|> pConstructor
--   byLexeme (string ";")
--   return $ case fixity of
--     "Left:"     -> CmdLAssoc   con
--     "Right:"    -> CmdRAssoc   con
--     "NonAssoc:" -> CmdNonAssoc con

pAssocs :: Pat2ConsEnv -> Parsec String u [Cmd]
pAssocs pcEnv = do
  fixity <- byLexeme (string "Left:" <|> string "Right:" <|> string "NonAssoc:")
  cons <- (try (pProd pcEnv) <|> pConstructor) `sepBy` byLexeme (string ",")
  byLexeme (string ";")
  return $ case fixity of
    "Left:"     -> map CmdLAssoc   cons
    "Right:"    -> map CmdRAssoc   cons
    -- "NonAssoc:" -> map CmdNonAssoc cons
