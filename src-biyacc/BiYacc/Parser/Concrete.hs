module BiYacc.Parser.Concrete where

import BiYacc.Language.Def
import BiYacc.Helper.Utils


import Text.Parsec hiding (State, newline)

import Data.Map hiding (foldr, map, drop, filter, (\\))
import qualified Data.Map as Map (lookup, empty)

import Control.Monad.State
import Text.Parsec.Token (identifier)

import Data.Generics hiding (empty)

-- import Text.PrettyPrint hiding (empty, char)
import qualified Text.PrettyPrint as TPP

pNontDefs :: SourcePos -> Parsec String u [NontDef]
pNontDefs srcPos = do
  setPosition srcPos
  byWhiteSpace
  byLexeme (string "#Concrete" <?> "expecting keyword #Concrete")
  defs <- many1 datatype
  let (NontDef t _) = head defs
  return defs

datatype :: Parsec String u NontDef
datatype = do
  t <- pConstructor
  defs <- datatypeDefs t
  eatSemi
  return $ NontDef t defs

datatypeDefs :: String -> Parsec String u [Prod]
datatypeDefs typeName = do
  def1 <- (eatTo >> datatypeDef typeName)
  defn <- many (eatOr >> datatypeDef typeName)
  return (def1:defn)

datatypeDef :: String -> Parsec String u Prod
datatypeDef typeName = do
  maybeName <- constructorName <|>
               return (Just "CONS_NOT_GENERATED_YET")
  prdFds <- many1 parseField
  brkAttr   <- bracketAttr <|> return Nothing
  let n = fromMaybe "" maybeName
  return $ Prod n prdFds brkAttr

bracketAttr :: Parsec String u (Maybe BracketAttr)
bracketAttr = do
  sss <- getInput
  byLexeme (string "{#")
  byLexeme (string "Bracket")
  byLexeme (string "#}")
  return $ Just BracketAttr
  <?> "error in parsing disambiguation annotation"

parseField :: Parsec String u ProdField
parseField =
  ((terminal <|> try pBiPrimNont) >>= return . Left) <|>
  (nonterminal >>= return . Right)

constructorName :: Monad m => ParsecT String u m (Maybe String)
constructorName = eatLBrack >> nonterminal >>= \n -> eatRBrack >> return (Just n)
                  <?> "expecting a user-given constructor name"

------------- parser ends -------
