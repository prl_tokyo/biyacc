{-# OPTIONS_GHC -w #-}
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module BiYacc.Parser.ActionParser (actionParser, actionLexer) where

import Data.Char
import Data.Data
import Data.Map as Map (lookup)
import Data.Maybe (fromJust)
import Data.Typeable

import BiYacc.Language.Def
import BiYacc.Helper.Utils
import qualified Data.Array as Happy_Data_Array
import qualified Data.Bits as Bits
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.9

data HappyAbsSyn 
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 (Program)
	| HappyAbsSyn5 ([Group])
	| HappyAbsSyn6 (Group)
	| HappyAbsSyn7 ((ViewType, SourceType))
	| HappyAbsSyn8 (String -> [Rule])
	| HappyAbsSyn9 (DataTypeRep)
	| HappyAbsSyn10 ([DataTypeRep])
	| HappyAbsSyn12 (String)
	| HappyAbsSyn14 (String -> Rule)
	| HappyAbsSyn15 (Pattern)
	| HappyAbsSyn17 ([Pattern])
	| HappyAbsSyn21 (String -> SrcSide)
	| HappyAbsSyn22 (String -> [UpdateUnit])
	| HappyAbsSyn23 (String -> UpdateUnit)
	| HappyAbsSyn25 (String -> Update)
	| HappyAbsSyn27 (String -> (String, SrcSide))

{- to allow type-synonyms as our monads (likely
 - with explicitly-specified bind and return)
 - in Haskell98, it seems that with
 - /type M a = .../, then /(HappyReduction M)/
 - is not allowed.  But Happy is a
 - code-generator that can just substitute it.
type HappyReduction m = 
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> m HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> m HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> m HappyAbsSyn
-}

action_0,
 action_1,
 action_2,
 action_3,
 action_4,
 action_5,
 action_6,
 action_7,
 action_8,
 action_9,
 action_10,
 action_11,
 action_12,
 action_13,
 action_14,
 action_15,
 action_16,
 action_17,
 action_18,
 action_19,
 action_20,
 action_21,
 action_22,
 action_23,
 action_24,
 action_25,
 action_26,
 action_27,
 action_28,
 action_29,
 action_30,
 action_31,
 action_32,
 action_33,
 action_34,
 action_35,
 action_36,
 action_37,
 action_38,
 action_39,
 action_40,
 action_41,
 action_42,
 action_43,
 action_44,
 action_45,
 action_46,
 action_47,
 action_48,
 action_49,
 action_50,
 action_51,
 action_52,
 action_53,
 action_54,
 action_55,
 action_56,
 action_57,
 action_58,
 action_59,
 action_60,
 action_61,
 action_62,
 action_63,
 action_64,
 action_65,
 action_66,
 action_67,
 action_68,
 action_69,
 action_70,
 action_71,
 action_72,
 action_73,
 action_74,
 action_75,
 action_76,
 action_77,
 action_78,
 action_79,
 action_80,
 action_81,
 action_82,
 action_83,
 action_84,
 action_85,
 action_86,
 action_87,
 action_88,
 action_89,
 action_90,
 action_91,
 action_92,
 action_93,
 action_94,
 action_95 :: () => Int -> ({-HappyReduction (HappyIdentity) = -}
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (HappyIdentity) HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (HappyIdentity) HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> (HappyIdentity) HappyAbsSyn)

happyReduce_1,
 happyReduce_2,
 happyReduce_3,
 happyReduce_4,
 happyReduce_5,
 happyReduce_6,
 happyReduce_7,
 happyReduce_8,
 happyReduce_9,
 happyReduce_10,
 happyReduce_11,
 happyReduce_12,
 happyReduce_13,
 happyReduce_14,
 happyReduce_15,
 happyReduce_16,
 happyReduce_17,
 happyReduce_18,
 happyReduce_19,
 happyReduce_20,
 happyReduce_21,
 happyReduce_22,
 happyReduce_23,
 happyReduce_24,
 happyReduce_25,
 happyReduce_26,
 happyReduce_27,
 happyReduce_28,
 happyReduce_29,
 happyReduce_30,
 happyReduce_31,
 happyReduce_32,
 happyReduce_33,
 happyReduce_34,
 happyReduce_35,
 happyReduce_36,
 happyReduce_37,
 happyReduce_38,
 happyReduce_39,
 happyReduce_40,
 happyReduce_41,
 happyReduce_42,
 happyReduce_43,
 happyReduce_44,
 happyReduce_45,
 happyReduce_46,
 happyReduce_47,
 happyReduce_48,
 happyReduce_49,
 happyReduce_50,
 happyReduce_51,
 happyReduce_52 :: () => ({-HappyReduction (HappyIdentity) = -}
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (HappyIdentity) HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (HappyIdentity) HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> (HappyIdentity) HappyAbsSyn)

happyExpList :: Happy_Data_Array.Array Int Int
happyExpList = Happy_Data_Array.listArray (0,155) ([0,32768,0,0,0,1,0,0,32,0,0,0,0,0,0,0,0,256,0,0,17216,0,0,0,32,0,2048,2,0,0,0,0,8192,8,0,0,0,0,32768,0,0,0,1,0,0,8192,0,0,0,0,0,269,0,0,32768,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2048,0,0,42624,1,0,512,0,0,0,0,0,0,4,0,0,0,0,53248,52,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,32,0,0,4096,0,0,47104,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4096,4,0,0,0,0,0,32,0,0,1,0,0,0,0,0,736,0,0,0,0,0,0,0,0,0,0,0,8,0,0,16,0,0,128,0,0,128,0,0,512,0,0,42624,1,0,0,0,0,0,0,0,0,16,0,0,32,0,0,1024,0,0,1024,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,28672,1,0,33280,0,0,0,0,0,0,0,0,0,32,0,0,64,0,16384,0,0,0,64,0,0,0,0,0,2048,0,0,0,0,0,0,0,0,1816,0,0,0,0,0,8192,0,0,0,0,0,29056,0,0,0,0,0,0,0,0,0,0,0,4096,0,0,0,128,0,0,0,0,0,0,0,32768,113,0,0,256,0,0,0,0
	])

{-# NOINLINE happyExpListPerState #-}
happyExpListPerState st =
    token_strs_expected
  where token_strs = ["error","%dummy","%start_actionParser","Program","CRs","CR","GrpHead","GrpBody","Type","TyFields","TyField","NonParametricType","Rules","Rule","ViewTopLevelPat","ConstructorPat","ConsArgs","ConsArg","AsPat","Constants","SrcSide","MixedSeqs","MixedSeq","Terminal","Expansion","Update","ExpansionLookAhead","SrcSideNoUpd","MixedSeqsNoUpd","MixedSeqNoUpd","ExpansionNoUpd","'#Actions'","int","inString","var","cons","';'","'@'","'_'","'\\''","'\"'","'('","')'","'['","']'","'->'","'+>'","';;'","%eof"]
        bit_start = st * 49
        bit_end = (st + 1) * 49
        read_bit = readArrayBit happyExpList
        bits = map read_bit [bit_start..bit_end - 1]
        bits_indexed = zip bits [0..48]
        token_strs_expected = concatMap f bits_indexed
        f (False, _) = []
        f (True, nr) = [token_strs !! nr]

action_0 (32) = happyShift action_2
action_0 (4) = happyGoto action_3
action_0 _ = happyFail (happyExpListPerState 0)

action_1 (32) = happyShift action_2
action_1 _ = happyFail (happyExpListPerState 1)

action_2 (36) = happyShift action_8
action_2 (5) = happyGoto action_4
action_2 (6) = happyGoto action_5
action_2 (7) = happyGoto action_6
action_2 (9) = happyGoto action_7
action_2 _ = happyFail (happyExpListPerState 2)

action_3 (49) = happyAccept
action_3 _ = happyFail (happyExpListPerState 3)

action_4 _ = happyReduce_1

action_5 (36) = happyShift action_8
action_5 (5) = happyGoto action_25
action_5 (6) = happyGoto action_5
action_5 (7) = happyGoto action_6
action_5 (9) = happyGoto action_7
action_5 _ = happyReduce_2

action_6 (33) = happyShift action_21
action_6 (35) = happyShift action_22
action_6 (36) = happyShift action_23
action_6 (41) = happyShift action_24
action_6 (8) = happyGoto action_14
action_6 (13) = happyGoto action_15
action_6 (14) = happyGoto action_16
action_6 (15) = happyGoto action_17
action_6 (16) = happyGoto action_18
action_6 (19) = happyGoto action_19
action_6 (20) = happyGoto action_20
action_6 _ = happyFail (happyExpListPerState 6)

action_7 (47) = happyShift action_13
action_7 _ = happyFail (happyExpListPerState 7)

action_8 (36) = happyShift action_11
action_8 (42) = happyShift action_12
action_8 (10) = happyGoto action_9
action_8 (11) = happyGoto action_10
action_8 _ = happyReduce_8

action_9 _ = happyReduce_7

action_10 (36) = happyShift action_11
action_10 (42) = happyShift action_12
action_10 (10) = happyGoto action_42
action_10 (11) = happyGoto action_10
action_10 _ = happyReduce_8

action_11 _ = happyReduce_10

action_12 (36) = happyShift action_41
action_12 _ = happyFail (happyExpListPerState 12)

action_13 (36) = happyShift action_40
action_13 (12) = happyGoto action_39
action_13 _ = happyFail (happyExpListPerState 13)

action_14 (48) = happyShift action_38
action_14 _ = happyFail (happyExpListPerState 14)

action_15 _ = happyReduce_6

action_16 (33) = happyShift action_21
action_16 (35) = happyShift action_22
action_16 (36) = happyShift action_23
action_16 (41) = happyShift action_24
action_16 (13) = happyGoto action_37
action_16 (14) = happyGoto action_16
action_16 (15) = happyGoto action_17
action_16 (16) = happyGoto action_18
action_16 (19) = happyGoto action_19
action_16 (20) = happyGoto action_20
action_16 _ = happyReduce_13

action_17 (47) = happyShift action_36
action_17 _ = happyFail (happyExpListPerState 17)

action_18 _ = happyReduce_17

action_19 _ = happyReduce_19

action_20 _ = happyReduce_16

action_21 _ = happyReduce_31

action_22 (38) = happyShift action_35
action_22 _ = happyReduce_18

action_23 (33) = happyShift action_21
action_23 (35) = happyShift action_31
action_23 (36) = happyShift action_32
action_23 (39) = happyShift action_33
action_23 (41) = happyShift action_24
action_23 (42) = happyShift action_34
action_23 (17) = happyGoto action_27
action_23 (18) = happyGoto action_28
action_23 (19) = happyGoto action_29
action_23 (20) = happyGoto action_30
action_23 _ = happyReduce_20

action_24 (34) = happyShift action_26
action_24 _ = happyFail (happyExpListPerState 24)

action_25 _ = happyReduce_3

action_26 (41) = happyShift action_57
action_26 _ = happyFail (happyExpListPerState 26)

action_27 _ = happyReduce_21

action_28 (33) = happyShift action_21
action_28 (35) = happyShift action_31
action_28 (36) = happyShift action_32
action_28 (39) = happyShift action_33
action_28 (41) = happyShift action_24
action_28 (42) = happyShift action_34
action_28 (17) = happyGoto action_56
action_28 (18) = happyGoto action_28
action_28 (19) = happyGoto action_29
action_28 (20) = happyGoto action_30
action_28 _ = happyReduce_22

action_29 _ = happyReduce_29

action_30 _ = happyReduce_24

action_31 (38) = happyShift action_35
action_31 _ = happyReduce_25

action_32 _ = happyReduce_27

action_33 _ = happyReduce_26

action_34 (36) = happyShift action_55
action_34 _ = happyFail (happyExpListPerState 34)

action_35 (42) = happyShift action_54
action_35 _ = happyFail (happyExpListPerState 35)

action_36 (40) = happyShift action_50
action_36 (41) = happyShift action_51
action_36 (42) = happyShift action_52
action_36 (44) = happyShift action_53
action_36 (21) = happyGoto action_44
action_36 (22) = happyGoto action_45
action_36 (23) = happyGoto action_46
action_36 (24) = happyGoto action_47
action_36 (25) = happyGoto action_48
action_36 (26) = happyGoto action_49
action_36 _ = happyFail (happyExpListPerState 36)

action_37 _ = happyReduce_14

action_38 _ = happyReduce_4

action_39 _ = happyReduce_5

action_40 _ = happyReduce_12

action_41 (36) = happyShift action_11
action_41 (42) = happyShift action_12
action_41 (10) = happyGoto action_43
action_41 (11) = happyGoto action_10
action_41 _ = happyReduce_8

action_42 _ = happyReduce_9

action_43 (43) = happyShift action_66
action_43 _ = happyFail (happyExpListPerState 43)

action_44 (37) = happyShift action_65
action_44 _ = happyFail (happyExpListPerState 44)

action_45 _ = happyReduce_33

action_46 (40) = happyShift action_50
action_46 (41) = happyShift action_51
action_46 (42) = happyShift action_52
action_46 (44) = happyShift action_53
action_46 (22) = happyGoto action_64
action_46 (23) = happyGoto action_46
action_46 (24) = happyGoto action_47
action_46 (25) = happyGoto action_48
action_46 (26) = happyGoto action_49
action_46 _ = happyReduce_34

action_47 _ = happyReduce_36

action_48 _ = happyReduce_38

action_49 _ = happyReduce_37

action_50 (34) = happyShift action_63
action_50 _ = happyFail (happyExpListPerState 50)

action_51 (34) = happyShift action_62
action_51 _ = happyFail (happyExpListPerState 51)

action_52 (36) = happyShift action_61
action_52 _ = happyFail (happyExpListPerState 52)

action_53 (35) = happyShift action_60
action_53 _ = happyFail (happyExpListPerState 53)

action_54 (36) = happyShift action_23
action_54 (16) = happyGoto action_59
action_54 _ = happyFail (happyExpListPerState 54)

action_55 (33) = happyShift action_21
action_55 (35) = happyShift action_31
action_55 (36) = happyShift action_32
action_55 (39) = happyShift action_33
action_55 (41) = happyShift action_24
action_55 (42) = happyShift action_34
action_55 (17) = happyGoto action_58
action_55 (18) = happyGoto action_28
action_55 (19) = happyGoto action_29
action_55 (20) = happyGoto action_30
action_55 _ = happyFail (happyExpListPerState 55)

action_56 _ = happyReduce_23

action_57 _ = happyReduce_32

action_58 (43) = happyShift action_72
action_58 _ = happyFail (happyExpListPerState 58)

action_59 (43) = happyShift action_71
action_59 _ = happyFail (happyExpListPerState 59)

action_60 (47) = happyShift action_70
action_60 _ = happyFail (happyExpListPerState 60)

action_61 (46) = happyShift action_69
action_61 _ = happyFail (happyExpListPerState 61)

action_62 (41) = happyShift action_68
action_62 _ = happyFail (happyExpListPerState 62)

action_63 (40) = happyShift action_67
action_63 _ = happyFail (happyExpListPerState 63)

action_64 _ = happyReduce_35

action_65 _ = happyReduce_15

action_66 _ = happyReduce_11

action_67 _ = happyReduce_39

action_68 _ = happyReduce_40

action_69 (40) = happyShift action_50
action_69 (41) = happyShift action_51
action_69 (42) = happyShift action_52
action_69 (44) = happyShift action_53
action_69 (21) = happyGoto action_76
action_69 (22) = happyGoto action_45
action_69 (23) = happyGoto action_46
action_69 (24) = happyGoto action_47
action_69 (25) = happyGoto action_48
action_69 (26) = happyGoto action_49
action_69 _ = happyFail (happyExpListPerState 69)

action_70 (36) = happyShift action_74
action_70 (42) = happyShift action_75
action_70 (27) = happyGoto action_73
action_70 _ = happyFail (happyExpListPerState 70)

action_71 _ = happyReduce_30

action_72 _ = happyReduce_28

action_73 (45) = happyShift action_80
action_73 _ = happyFail (happyExpListPerState 73)

action_74 (45) = happyShift action_79
action_74 _ = happyFail (happyExpListPerState 74)

action_75 (36) = happyShift action_78
action_75 _ = happyFail (happyExpListPerState 75)

action_76 (43) = happyShift action_77
action_76 _ = happyFail (happyExpListPerState 76)

action_77 _ = happyReduce_41

action_78 (46) = happyShift action_81
action_78 _ = happyFail (happyExpListPerState 78)

action_79 _ = happyReduce_42

action_80 _ = happyReduce_43

action_81 (35) = happyShift action_87
action_81 (36) = happyShift action_88
action_81 (40) = happyShift action_50
action_81 (41) = happyShift action_51
action_81 (42) = happyShift action_89
action_81 (24) = happyGoto action_82
action_81 (28) = happyGoto action_83
action_81 (29) = happyGoto action_84
action_81 (30) = happyGoto action_85
action_81 (31) = happyGoto action_86
action_81 _ = happyFail (happyExpListPerState 81)

action_82 _ = happyReduce_48

action_83 (43) = happyShift action_92
action_83 _ = happyFail (happyExpListPerState 83)

action_84 _ = happyReduce_45

action_85 (35) = happyShift action_87
action_85 (36) = happyShift action_88
action_85 (40) = happyShift action_50
action_85 (41) = happyShift action_51
action_85 (42) = happyShift action_89
action_85 (24) = happyGoto action_82
action_85 (29) = happyGoto action_91
action_85 (30) = happyGoto action_85
action_85 (31) = happyGoto action_86
action_85 _ = happyReduce_46

action_86 _ = happyReduce_51

action_87 _ = happyReduce_49

action_88 _ = happyReduce_50

action_89 (36) = happyShift action_90
action_89 _ = happyFail (happyExpListPerState 89)

action_90 (46) = happyShift action_93
action_90 _ = happyFail (happyExpListPerState 90)

action_91 _ = happyReduce_47

action_92 _ = happyReduce_44

action_93 (35) = happyShift action_87
action_93 (36) = happyShift action_88
action_93 (40) = happyShift action_50
action_93 (41) = happyShift action_51
action_93 (42) = happyShift action_89
action_93 (24) = happyGoto action_82
action_93 (28) = happyGoto action_94
action_93 (29) = happyGoto action_84
action_93 (30) = happyGoto action_85
action_93 (31) = happyGoto action_86
action_93 _ = happyFail (happyExpListPerState 93)

action_94 (43) = happyShift action_95
action_94 _ = happyFail (happyExpListPerState 94)

action_95 _ = happyReduce_52

happyReduce_1 = happySpecReduce_2  4 happyReduction_1
happyReduction_1 (HappyAbsSyn5  happy_var_2)
	_
	 =  HappyAbsSyn4
		 (let (Group gTy _) = (head happy_var_2)
    in (Program gTy happy_var_2)
	)
happyReduction_1 _ _  = notHappyAtAll 

happyReduce_2 = happySpecReduce_1  5 happyReduction_2
happyReduction_2 (HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn5
		 ([happy_var_1]
	)
happyReduction_2 _  = notHappyAtAll 

happyReduce_3 = happySpecReduce_2  5 happyReduction_3
happyReduction_3 (HappyAbsSyn5  happy_var_2)
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn5
		 (happy_var_1 : happy_var_2
	)
happyReduction_3 _ _  = notHappyAtAll 

happyReduce_4 = happySpecReduce_3  6 happyReduction_4
happyReduction_4 _
	(HappyAbsSyn8  happy_var_2)
	(HappyAbsSyn7  happy_var_1)
	 =  HappyAbsSyn6
		 (let sty = printTypeRep (snd happy_var_1)
    in  Group happy_var_1 (happy_var_2 sty)
	)
happyReduction_4 _ _ _  = notHappyAtAll 

happyReduce_5 = happySpecReduce_3  7 happyReduction_5
happyReduction_5 (HappyAbsSyn12  happy_var_3)
	_
	(HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn7
		 ((happy_var_1, mkSimpTyRep happy_var_3)
	)
happyReduction_5 _ _ _  = notHappyAtAll 

happyReduce_6 = happySpecReduce_1  8 happyReduction_6
happyReduction_6 (HappyAbsSyn8  happy_var_1)
	 =  HappyAbsSyn8
		 (happy_var_1
	)
happyReduction_6 _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_2  9 happyReduction_7
happyReduction_7 (HappyAbsSyn10  happy_var_2)
	(HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn9
		 (TyCon happy_var_1 happy_var_2
	)
happyReduction_7 _ _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_0  10 happyReduction_8
happyReduction_8  =  HappyAbsSyn10
		 ([]
	)

happyReduce_9 = happySpecReduce_2  10 happyReduction_9
happyReduction_9 (HappyAbsSyn10  happy_var_2)
	(HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn10
		 (happy_var_1 : happy_var_2
	)
happyReduction_9 _ _  = notHappyAtAll 

happyReduce_10 = happySpecReduce_1  11 happyReduction_10
happyReduction_10 (HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn9
		 (TyCon happy_var_1 []
	)
happyReduction_10 _  = notHappyAtAll 

happyReduce_11 = happyReduce 4 11 happyReduction_11
happyReduction_11 (_ `HappyStk`
	(HappyAbsSyn10  happy_var_3) `HappyStk`
	(HappyTerminal (TKCons happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn9
		 (TyCon happy_var_2 happy_var_3
	) `HappyStk` happyRest

happyReduce_12 = happySpecReduce_1  12 happyReduction_12
happyReduction_12 (HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn12
		 (happy_var_1
	)
happyReduction_12 _  = notHappyAtAll 

happyReduce_13 = happySpecReduce_1  13 happyReduction_13
happyReduction_13 (HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn8
		 (\sty -> [happy_var_1 sty]
	)
happyReduction_13 _  = notHappyAtAll 

happyReduce_14 = happySpecReduce_2  13 happyReduction_14
happyReduction_14 (HappyAbsSyn8  happy_var_2)
	(HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn8
		 (\sty -> happy_var_1 sty : (happy_var_2 sty)
	)
happyReduction_14 _ _  = notHappyAtAll 

happyReduce_15 = happyReduce 4 14 happyReduction_15
happyReduction_15 (_ `HappyStk`
	(HappyAbsSyn21  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn15  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn14
		 (\sty -> (happy_var_1 , happy_var_3 sty)
	) `HappyStk` happyRest

happyReduce_16 = happySpecReduce_1  15 happyReduction_16
happyReduction_16 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (happy_var_1
	)
happyReduction_16 _  = notHappyAtAll 

happyReduce_17 = happySpecReduce_1  15 happyReduction_17
happyReduction_17 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (happy_var_1
	)
happyReduction_17 _  = notHappyAtAll 

happyReduce_18 = happySpecReduce_1  15 happyReduction_18
happyReduction_18 (HappyTerminal (TKVar  happy_var_1))
	 =  HappyAbsSyn15
		 (VarP happy_var_1 (mkSimpTyRep "UNDEFINED")
	)
happyReduction_18 _  = notHappyAtAll 

happyReduce_19 = happySpecReduce_1  15 happyReduction_19
happyReduction_19 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (happy_var_1
	)
happyReduction_19 _  = notHappyAtAll 

happyReduce_20 = happySpecReduce_1  16 happyReduction_20
happyReduction_20 (HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn15
		 (ConP happy_var_1 (mkSimpTyRep "UNDEFINED") []
	)
happyReduction_20 _  = notHappyAtAll 

happyReduce_21 = happySpecReduce_2  16 happyReduction_21
happyReduction_21 (HappyAbsSyn17  happy_var_2)
	(HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn15
		 (ConP happy_var_1 (mkSimpTyRep "UNDEFINED") happy_var_2
	)
happyReduction_21 _ _  = notHappyAtAll 

happyReduce_22 = happySpecReduce_1  17 happyReduction_22
happyReduction_22 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn17
		 ([happy_var_1]
	)
happyReduction_22 _  = notHappyAtAll 

happyReduce_23 = happySpecReduce_2  17 happyReduction_23
happyReduction_23 (HappyAbsSyn17  happy_var_2)
	(HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn17
		 (happy_var_1 : happy_var_2
	)
happyReduction_23 _ _  = notHappyAtAll 

happyReduce_24 = happySpecReduce_1  18 happyReduction_24
happyReduction_24 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (happy_var_1
	)
happyReduction_24 _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_1  18 happyReduction_25
happyReduction_25 (HappyTerminal (TKVar  happy_var_1))
	 =  HappyAbsSyn15
		 (VarP  happy_var_1 (mkSimpTyRep "UNDEFINED")
	)
happyReduction_25 _  = notHappyAtAll 

happyReduce_26 = happySpecReduce_1  18 happyReduction_26
happyReduction_26 _
	 =  HappyAbsSyn15
		 (WildP (mkSimpTyRep "UNDEFINED")
	)

happyReduce_27 = happySpecReduce_1  18 happyReduction_27
happyReduction_27 (HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn15
		 (ConP  happy_var_1 (mkSimpTyRep "UNDEFINED") []
	)
happyReduction_27 _  = notHappyAtAll 

happyReduce_28 = happyReduce 4 18 happyReduction_28
happyReduction_28 (_ `HappyStk`
	(HappyAbsSyn17  happy_var_3) `HappyStk`
	(HappyTerminal (TKCons happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn15
		 (ConP  happy_var_2 (mkSimpTyRep "UNDEFINED") happy_var_3
	) `HappyStk` happyRest

happyReduce_29 = happySpecReduce_1  18 happyReduction_29
happyReduction_29 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (happy_var_1
	)
happyReduction_29 _  = notHappyAtAll 

happyReduce_30 = happyReduce 5 19 happyReduction_30
happyReduction_30 (_ `HappyStk`
	(HappyAbsSyn15  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TKVar  happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn15
		 (AsP happy_var_1 (mkSimpTyRep "UNDEFINED") happy_var_4
	) `HappyStk` happyRest

happyReduce_31 = happySpecReduce_1  20 happyReduction_31
happyReduction_31 (HappyTerminal (TKInt    happy_var_1))
	 =  HappyAbsSyn15
		 (LitP (LitInt happy_var_1)
	)
happyReduction_31 _  = notHappyAtAll 

happyReduce_32 = happySpecReduce_3  20 happyReduction_32
happyReduction_32 _
	(HappyTerminal (TKInStr  happy_var_2))
	_
	 =  HappyAbsSyn15
		 (LitP (LitStr happy_var_2)
	)
happyReduction_32 _ _ _  = notHappyAtAll 

happyReduce_33 = happySpecReduce_1  21 happyReduction_33
happyReduction_33 (HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn21
		 (\sty -> (SrcSide (mkSimpTyRep sty) "UNDEFINED" (happy_var_1 sty))
	)
happyReduction_33 _  = notHappyAtAll 

happyReduce_34 = happySpecReduce_1  22 happyReduction_34
happyReduction_34 (HappyAbsSyn23  happy_var_1)
	 =  HappyAbsSyn22
		 (\sty -> [happy_var_1 sty]
	)
happyReduction_34 _  = notHappyAtAll 

happyReduce_35 = happySpecReduce_2  22 happyReduction_35
happyReduction_35 (HappyAbsSyn22  happy_var_2)
	(HappyAbsSyn23  happy_var_1)
	 =  HappyAbsSyn22
		 (\sty -> (happy_var_1 sty) : (happy_var_2 sty)
	)
happyReduction_35 _ _  = notHappyAtAll 

happyReduce_36 = happySpecReduce_1  23 happyReduction_36
happyReduction_36 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn23
		 (\sty -> Left happy_var_1
	)
happyReduction_36 _  = notHappyAtAll 

happyReduce_37 = happySpecReduce_1  23 happyReduction_37
happyReduction_37 (HappyAbsSyn25  happy_var_1)
	 =  HappyAbsSyn23
		 (\sty -> Right (happy_var_1 sty)
	)
happyReduction_37 _  = notHappyAtAll 

happyReduce_38 = happySpecReduce_1  23 happyReduction_38
happyReduction_38 (HappyAbsSyn25  happy_var_1)
	 =  HappyAbsSyn23
		 (\sty -> Right (happy_var_1 sty)
	)
happyReduction_38 _  = notHappyAtAll 

happyReduce_39 = happySpecReduce_3  24 happyReduction_39
happyReduction_39 _
	(HappyTerminal (TKInStr  happy_var_2))
	_
	 =  HappyAbsSyn12
		 (happy_var_2
	)
happyReduction_39 _ _ _  = notHappyAtAll 

happyReduce_40 = happySpecReduce_3  24 happyReduction_40
happyReduction_40 _
	(HappyTerminal (TKInStr  happy_var_2))
	_
	 =  HappyAbsSyn12
		 (happy_var_2
	)
happyReduction_40 _ _ _  = notHappyAtAll 

happyReduce_41 = happyReduce 5 25 happyReduction_41
happyReduction_41 (_ `HappyStk`
	(HappyAbsSyn21  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TKCons happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn25
		 (\sty -> Expansion (happy_var_4 happy_var_2)
	) `HappyStk` happyRest

happyReduce_42 = happyReduce 5 26 happyReduction_42
happyReduction_42 (_ `HappyStk`
	(HappyTerminal (TKCons happy_var_4)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TKVar  happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn25
		 (\sty -> NUpdate happy_var_2 (mkSimpTyRep happy_var_4)
	) `HappyStk` happyRest

happyReduce_43 = happyReduce 5 26 happyReduction_43
happyReduction_43 (_ `HappyStk`
	(HappyAbsSyn27  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TKVar  happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn25
		 (\sty ->
      let (nont, ahead) = (happy_var_4 sty)
      in  (NUpdateWithLookAhead happy_var_2 (mkSimpTyRep nont) ahead)
	) `HappyStk` happyRest

happyReduce_44 = happyReduce 5 27 happyReduction_44
happyReduction_44 (_ `HappyStk`
	(HappyAbsSyn21  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TKCons happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn27
		 (\sty -> (happy_var_2, happy_var_4 happy_var_2)
	) `HappyStk` happyRest

happyReduce_45 = happySpecReduce_1  28 happyReduction_45
happyReduction_45 (HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn21
		 (\sty -> SrcSide (mkSimpTyRep sty) "UNDEFINED" (happy_var_1 sty)
	)
happyReduction_45 _  = notHappyAtAll 

happyReduce_46 = happySpecReduce_1  29 happyReduction_46
happyReduction_46 (HappyAbsSyn23  happy_var_1)
	 =  HappyAbsSyn22
		 (\sty -> [happy_var_1 sty]
	)
happyReduction_46 _  = notHappyAtAll 

happyReduce_47 = happySpecReduce_2  29 happyReduction_47
happyReduction_47 (HappyAbsSyn22  happy_var_2)
	(HappyAbsSyn23  happy_var_1)
	 =  HappyAbsSyn22
		 (\sty -> (happy_var_1 sty) : (happy_var_2 sty)
	)
happyReduction_47 _ _  = notHappyAtAll 

happyReduce_48 = happySpecReduce_1  30 happyReduction_48
happyReduction_48 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn23
		 (\sty -> Left happy_var_1
	)
happyReduction_48 _  = notHappyAtAll 

happyReduce_49 = happySpecReduce_1  30 happyReduction_49
happyReduction_49 (HappyTerminal (TKVar  happy_var_1))
	 =  HappyAbsSyn23
		 (\sty -> Left happy_var_1
	)
happyReduction_49 _  = notHappyAtAll 

happyReduce_50 = happySpecReduce_1  30 happyReduction_50
happyReduction_50 (HappyTerminal (TKCons happy_var_1))
	 =  HappyAbsSyn23
		 (\sty -> Right (NoUpdate (mkSimpTyRep happy_var_1))
	)
happyReduction_50 _  = notHappyAtAll 

happyReduce_51 = happySpecReduce_1  30 happyReduction_51
happyReduction_51 (HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn23
		 (\sty -> Right (Expansion (happy_var_1 sty))
	)
happyReduction_51 _  = notHappyAtAll 

happyReduce_52 = happyReduce 5 31 happyReduction_52
happyReduction_52 (_ `HappyStk`
	(HappyAbsSyn21  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TKCons happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn21
		 (\sty -> happy_var_4 happy_var_2
	) `HappyStk` happyRest

happyNewToken action sts stk [] =
	action 49 49 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	TKActionStart -> cont 32;
	TKInt    happy_dollar_dollar -> cont 33;
	TKInStr  happy_dollar_dollar -> cont 34;
	TKVar  happy_dollar_dollar -> cont 35;
	TKCons happy_dollar_dollar -> cont 36;
	TKSemi -> cont 37;
	TKAt -> cont 38;
	TKWild -> cont 39;
	TKQuote -> cont 40;
	TKDQuote -> cont 41;
	TKLParen -> cont 42;
	TKRParen -> cont 43;
	TKLBracket -> cont 44;
	TKRBracket -> cont 45;
	TKExpansion -> cont 46;
	TKEmbed -> cont 47;
	TKDSemi -> cont 48;
	_ -> happyError' ((tk:tks), [])
	}

happyError_ explist 49 tk tks = happyError' (tks, explist)
happyError_ explist _ tk tks = happyError' ((tk:tks), explist)

newtype HappyIdentity a = HappyIdentity a
happyIdentity = HappyIdentity
happyRunIdentity (HappyIdentity a) = a

instance Functor HappyIdentity where
    fmap f (HappyIdentity a) = HappyIdentity (f a)

instance Applicative HappyIdentity where
    pure  = HappyIdentity
    (<*>) = ap
instance Monad HappyIdentity where
    return = pure
    (HappyIdentity p) >>= q = q p

happyThen :: () => HappyIdentity a -> (a -> HappyIdentity b) -> HappyIdentity b
happyThen = (>>=)
happyReturn :: () => a -> HappyIdentity a
happyReturn = (return)
happyThen1 m k tks = (>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> HappyIdentity a
happyReturn1 = \a tks -> (return) a
happyError' :: () => ([(Token)], [String]) -> HappyIdentity a
happyError' = HappyIdentity . (\(tokens, _) -> aParseError tokens)
actionParser tks = happyRunIdentity happySomeParser where
 happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


aParseError :: [Token] -> a
aParseError a = error $ "Parse error.\nFor primitive data, currently it only" ++
  "supports strings and integers.\n" ++
  "Unconsumed 100 tokens: \n" ++ show (take 100 a) ++
  "\nConverted to string:\n" ++ printActionTokens (take 100 a)


data Token =
    TokenEOF
  | TKInt  Integer
  | TKInStr  String
  | TKChar Char
  | TKVar  String
  | TKCons String
  | TKPrim String

  | TKActionStart
  | TKEmbed
  | TKDSemi
  | TKSemi
  | TKPlus
  | TKTimes
  | TKCRArrow
  | TKWild
  | TKLParen
  | TKRParen
  | TKLBracket
  | TKRBracket
--  | TKLBrace
--  | TKRBrace
  | TKExpansion
  | TKQuote
  | TKDQuote
  | TKAt
  deriving (Show, Eq, Ord)


actionLexer :: String -> [Token]
actionLexer [] = []
actionLexer ('-':'-':cs) = lexComment2 cs
actionLexer ('-':'>':cs)  = TKExpansion : actionLexer cs
actionLexer ('_':cs)  = TKWild : actionLexer cs
actionLexer (';':';':cs)  = TKDSemi : actionLexer cs
actionLexer (';':cs)  = TKSemi : actionLexer cs
actionLexer ('(':cs)  = TKLParen   : actionLexer cs
actionLexer (')':cs)  = TKRParen   : actionLexer cs
actionLexer ('[':cs)  = TKLBracket : actionLexer cs
actionLexer (']':cs)  = TKRBracket : actionLexer cs
-- actionLexer ('{':cs)  = TKLBrace   : actionLexer cs
actionLexer ('+':'>':cs) = TKEmbed : actionLexer cs
-- actionLexer ('}':cs)  = TKRBrace   : actionLexer cs
actionLexer ('~':cs)  = TKCRArrow  : actionLexer cs
actionLexer ('@':cs)  = TKAt  : actionLexer cs
actionLexer ('"':cs)  = lexStr1 cs
actionLexer ('\'':cs) = lexStr2 cs
actionLexer ('#':'A':'c':'t':'i':'o':'n':'s':cs) = TKActionStart : actionLexer cs

actionLexer (c:cs)
      | isSpace c = actionLexer cs
      | otherwise = lexData (c:cs)

lexData (c:cs) | isUpper c  = lexCons (c:cs)
lexData (c:cs) | isLower c  = lexVar  (c:cs)
lexData (c:cs) | isNumber c = lexNum  (c:cs)
lexData cs = error $ "Unrecognised data. For primitive data, currently only" ++
  "support strings and integers.\n" ++
  "First 100 unconsumed characters: \n" ++ take 100 cs


lexStr1 cs  =
  let (str,rest) = span (\c -> c /= '"') cs
  in  TKDQuote : TKInStr str : TKDQuote : actionLexer (tail rest)


lexStr2 cs  =
  let (str,rest) = span (\c -> c/= '\'') cs
  in  TKQuote : TKInStr str : TKQuote : actionLexer (tail rest)

lexNum cs =
  let (i,rest) = span isNumber cs
  in  TKInt (read i :: Integer) : actionLexer rest

lexVar cs =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : actionLexer rest

lexCons cs =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : actionLexer rest

lexComment2 :: String -> [Token]
lexComment2 [] = []
lexComment2 (c:cs) | isNewline c = actionLexer cs
lexComment2 (c:cs) | otherwise = lexComment2 cs

isNewline :: Char -> Bool
isNewline '\r' = True
isNewline '\n' = True
isNewline _ = False


printActionTokens :: [Token] -> String
printActionTokens = foldr (\c cs -> c ++ " " ++ cs) " " . map printActionToken

printActionToken :: Token -> String
printActionToken TokenEOF = "END OF FILE"
printActionToken (TKInt  i) = show i
printActionToken (TKInStr s) = s
printActionToken (TKChar c) = show c
printActionToken (TKVar  v) = v
printActionToken (TKCons c) = c
printActionToken (TKPrim p) = p

printActionToken (TKCRArrow) = "~"
printActionToken (TKWild)    = "_"
printActionToken (TKEmbed) = "+>"
printActionToken (TKAt)  = "@"
printActionToken (TKPlus)  = "+"
printActionToken (TKTimes)  = "*"
printActionToken (TKSemi)  = ";"
printActionToken (TKDSemi)  = ";;"
printActionToken (TKLParen)  = "("
printActionToken (TKRParen)  = ")"
printActionToken (TKLBracket) = "["
printActionToken (TKRBracket) = "]"
-- printActionToken (TKLBrace) = "{"
-- printActionToken (TKRBrace) = "}"
printActionToken (TKExpansion) = "->"
printActionToken (TKQuote) = "'"
printActionToken (TKDQuote) = "\""
printActionToken (TKActionStart) = "#Action"

--------------
{-# LINE 1 "templates/GenericTemplate.hs" #-}





































































































































































































-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 











data Happy_IntList = HappyCons Int Happy_IntList




















infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action




indexShortOffAddr arr off = arr Happy_Data_Array.! off


{-# INLINE happyLt #-}
happyLt x y = (x < y)






readArrayBit arr bit =
    Bits.testBit (indexShortOffAddr arr (bit `div` 16)) (bit `mod` 16)






-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             _ = nt :: Int
             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction









happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail explist (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ explist i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail explist i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.









{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.

