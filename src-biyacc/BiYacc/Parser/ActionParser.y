{
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module BiYacc.Parser.ActionParser (actionParser, actionLexer) where

import Data.Char
import Data.Data
import Data.Map as Map (lookup)
import Data.Maybe (fromJust)
import Data.Typeable

import BiYacc.Language.Def
import BiYacc.Helper.Utils

}

%name actionParser
%tokentype { Token }
%error { aParseError }

%token
  '#Actions'      { TKActionStart }
  int             { TKInt    $$ }
  inString        { TKInStr  $$ }
  var             { TKVar  $$ }
  cons            { TKCons $$ }
  ';'             { TKSemi }
  '@'             { TKAt }
  '_'             { TKWild }
  '\''            { TKQuote  }
  '"'             { TKDQuote }
  '('             { TKLParen }
  ')'             { TKRParen }
  '['             { TKLBracket }
  ']'             { TKRBracket }
--  '{'             { TKLBrace }
--  '}'             { TKRBrace }
  '->'            { TKExpansion }
  '+>'            { TKEmbed }
  ';;'            { TKDSemi }

%%

Program :: { Program }
  : '#Actions' CRs  {
    let (Group gTy _) = (head $2)
    in (Program gTy $2) }

CRs :: { [Group] }
  : CR        { [$1] }
  | CR CRs    { $1 : $2 }

CR :: {Group}
  : GrpHead GrpBody ';;' {
    let sty = printTypeRep (snd $1)
    in  Group $1 ($2 sty) }

GrpHead :: {(ViewType, SourceType)}
  : Type '+>' NonParametricType { ($1, mkSimpTyRep $3) }

GrpBody :: { String -> [Rule] }
  : Rules { $1 }

Type :: { DataTypeRep }
  : cons TyFields { TyCon $1 $2}

TyFields :: { [DataTypeRep] }
  : {- empty -}      { [] }
  | TyField TyFields { $1 : $2}

TyField :: { DataTypeRep }
  : cons                  { TyCon $1 [] }
  | '(' cons TyFields ')' { TyCon $2 $3 }
  --  | var                   { TyVar $1    }
  -- No type variables. All type variables need to be instantiated.

NonParametricType :: { String }
  : cons { $1 }


Rules :: { String -> [Rule] }
  : Rule        { \sty -> [$1 sty] }
  | Rule Rules  { \sty -> $1 sty : ($2 sty) }

-- currently only normal rules
-- Rule :: {Rule}
--   : NRule { $1 }

Rule :: { String -> Rule }
  : ViewTopLevelPat '+>' SrcSide  ';'
    { \sty -> ($1 , $3 sty) }

-- currently no view-constraints
ViewTopLevelPat :: { Pattern }
  : Constants           { $1 }
  | ConstructorPat      { $1 }
  | var                 { VarP $1 (mkSimpTyRep "UNDEFINED") }
  | AsPat               { $1 }


ConstructorPat :: { Pattern }
  : cons           { ConP $1 (mkSimpTyRep "UNDEFINED") [] }
  | cons ConsArgs  { ConP $1 (mkSimpTyRep "UNDEFINED") $2 }

ConsArgs :: {[Pattern]}
  : ConsArg           { [$1] }
  | ConsArg ConsArgs  { $1 : $2}

ConsArg :: {Pattern}
  : Constants               { $1 }
  | var                     { VarP  $1 (mkSimpTyRep "UNDEFINED") }
  | '_'                     { WildP (mkSimpTyRep "UNDEFINED") }
  | cons                    { ConP  $1 (mkSimpTyRep "UNDEFINED") [] }
  | '(' cons ConsArgs ')'   { ConP  $2 (mkSimpTyRep "UNDEFINED") $3 }
  | AsPat                   { $1 }

AsPat :: { Pattern }
  : var '@' '(' ConstructorPat ')' { AsP $1 (mkSimpTyRep "UNDEFINED") $4 }

Constants :: { Pattern }
  : int       { LitP (LitInt $1) }
  | '"' inString '"'   { LitP (LitStr $2) }
--  | '\'' inString '\'' { LitP (LitChr $1) }
-- currently only support int and string

SrcSide :: { String -> SrcSide }
  : MixedSeqs { \sty -> (SrcSide (mkSimpTyRep sty) "UNDEFINED" ($1 sty))  }

MixedSeqs :: { String -> [UpdateUnit] }
  : MixedSeq            { \sty -> [$1 sty] }
  | MixedSeq MixedSeqs  { \sty -> ($1 sty) : ($2 sty) }

MixedSeq :: { String -> UpdateUnit }
  : Terminal     { \sty -> Left $1 }
  | Update       { \sty -> Right ($1 sty) }
  | Expansion    { \sty -> Right ($1 sty) }
-- No Nonterminals. All nonterminals shall be properly updated, or expanded.

Terminal :: { String }
  : '\'' inString '\''   { $2 }
  | '"'  inString  '"'   { $2 }


Expansion :: { String -> Update }
  : '(' cons '->' SrcSide ')'  { \sty -> Expansion ($4 $2) }

Update :: { String -> Update }
  : '[' var '+>' cons ']'          { \sty -> NUpdate $2 (mkSimpTyRep $4) }
  | '[' var '+>' ExpansionLookAhead ']'   { \sty ->
      let (nont, ahead) = ($4 sty)
      in  (NUpdateWithLookAhead $2 (mkSimpTyRep nont) ahead) }


-- ExpansionLookAhead is only for more precise pattern maching
-- There is no further update inside. Do not confuse it with SrcSide

ExpansionLookAhead :: { String -> (String, SrcSide) }
  : '(' cons '->' SrcSideNoUpd ')' { \sty -> ($2, $4 $2) }

SrcSideNoUpd :: { String -> SrcSide }
  : MixedSeqsNoUpd { \sty -> SrcSide (mkSimpTyRep sty) "UNDEFINED" ($1 sty) }


MixedSeqsNoUpd :: { String -> [UpdateUnit] }
  : MixedSeqNoUpd                  { \sty -> [$1 sty] }
  | MixedSeqNoUpd MixedSeqsNoUpd   { \sty -> ($1 sty) : ($2 sty) }

MixedSeqNoUpd :: { String -> UpdateUnit }
  : Terminal             { \sty -> Left $1 }
  | var                  { \sty -> Left $1 }
  | cons                 { \sty -> Right (NoUpdate (mkSimpTyRep $1)) } --WARNING
  | ExpansionNoUpd       { \sty -> Right (Expansion ($1 sty)) }

ExpansionNoUpd :: { String -> SrcSide }
  : '(' cons '->' SrcSideNoUpd ')'  { \sty -> $4 $2}


{

aParseError :: [Token] -> a
aParseError a = error $ "Parse error.\nFor primitive data, currently it only" ++
  "supports strings and integers.\n" ++
  "Unconsumed 100 tokens: \n" ++ show (take 100 a) ++
  "\nConverted to string:\n" ++ printActionTokens (take 100 a)


data Token =
    TokenEOF
  | TKInt  Integer
  | TKInStr  String
  | TKChar Char
  | TKVar  String
  | TKCons String
  | TKPrim String

  | TKActionStart
  | TKEmbed
  | TKDSemi
  | TKSemi
  | TKPlus
  | TKTimes
  | TKCRArrow
  | TKWild
  | TKLParen
  | TKRParen
  | TKLBracket
  | TKRBracket
--  | TKLBrace
--  | TKRBrace
  | TKExpansion
  | TKQuote
  | TKDQuote
  | TKAt
  deriving (Show, Eq, Ord)


actionLexer :: String -> [Token]
actionLexer [] = []
actionLexer ('-':'-':cs) = lexComment2 cs
actionLexer ('-':'>':cs)  = TKExpansion : actionLexer cs
actionLexer ('_':cs)  = TKWild : actionLexer cs
actionLexer (';':';':cs)  = TKDSemi : actionLexer cs
actionLexer (';':cs)  = TKSemi : actionLexer cs
actionLexer ('(':cs)  = TKLParen   : actionLexer cs
actionLexer (')':cs)  = TKRParen   : actionLexer cs
actionLexer ('[':cs)  = TKLBracket : actionLexer cs
actionLexer (']':cs)  = TKRBracket : actionLexer cs
-- actionLexer ('{':cs)  = TKLBrace   : actionLexer cs
actionLexer ('+':'>':cs) = TKEmbed : actionLexer cs
-- actionLexer ('}':cs)  = TKRBrace   : actionLexer cs
actionLexer ('~':cs)  = TKCRArrow  : actionLexer cs
actionLexer ('@':cs)  = TKAt  : actionLexer cs
actionLexer ('"':cs)  = lexStr1 cs
actionLexer ('\'':cs) = lexStr2 cs
actionLexer ('#':'A':'c':'t':'i':'o':'n':'s':cs) = TKActionStart : actionLexer cs

actionLexer (c:cs)
      | isSpace c = actionLexer cs
      | otherwise = lexData (c:cs)

lexData (c:cs) | isUpper c  = lexCons (c:cs)
lexData (c:cs) | isLower c  = lexVar  (c:cs)
lexData (c:cs) | isNumber c = lexNum  (c:cs)
lexData cs = error $ "Unrecognised data. For primitive data, currently only" ++
  "support strings and integers.\n" ++
  "First 100 unconsumed characters: \n" ++ take 100 cs


lexStr1 cs  =
  let (str,rest) = span (\c -> c /= '"') cs
  in  TKDQuote : TKInStr str : TKDQuote : actionLexer (tail rest)


lexStr2 cs  =
  let (str,rest) = span (\c -> c/= '\'') cs
  in  TKQuote : TKInStr str : TKQuote : actionLexer (tail rest)

lexNum cs =
  let (i,rest) = span isNumber cs
  in  TKInt (read i :: Integer) : actionLexer rest

lexVar cs =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : actionLexer rest

lexCons cs =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : actionLexer rest

lexComment2 :: String -> [Token]
lexComment2 [] = []
lexComment2 (c:cs) | isNewline c = actionLexer cs
lexComment2 (c:cs) | otherwise = lexComment2 cs

isNewline :: Char -> Bool
isNewline '\r' = True
isNewline '\n' = True
isNewline _ = False


printActionTokens :: [Token] -> String
printActionTokens = foldr (\c cs -> c ++ " " ++ cs) " " . map printActionToken

printActionToken :: Token -> String
printActionToken TokenEOF = "END OF FILE"
printActionToken (TKInt  i) = show i
printActionToken (TKInStr s) = s
printActionToken (TKChar c) = show c
printActionToken (TKVar  v) = v
printActionToken (TKCons c) = c
printActionToken (TKPrim p) = p

printActionToken (TKCRArrow) = "~"
printActionToken (TKWild)    = "_"
printActionToken (TKEmbed) = "+>"
printActionToken (TKAt)  = "@"
printActionToken (TKPlus)  = "+"
printActionToken (TKTimes)  = "*"
printActionToken (TKSemi)  = ";"
printActionToken (TKDSemi)  = ";;"
printActionToken (TKLParen)  = "("
printActionToken (TKRParen)  = ")"
printActionToken (TKLBracket) = "["
printActionToken (TKRBracket) = "]"
-- printActionToken (TKLBrace) = "{"
-- printActionToken (TKRBrace) = "}"
printActionToken (TKExpansion) = "->"
printActionToken (TKQuote) = "'"
printActionToken (TKDQuote) = "\""
printActionToken (TKActionStart) = "#Action"

--------------
}
