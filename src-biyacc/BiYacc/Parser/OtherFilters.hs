module BiYacc.Parser.OtherFilters (pOtherFilters) where

import BiYacc.Language.Def
import Text.PrettyPrint as TPP hiding (char)
import Text.Parsec

-- return (Def-Doc , [(type,name)])
pOtherFilters :: SourcePos -> Parsec String u (Maybe (Doc,[(String,String)]))
pOtherFilters srcPos = do
  setPosition srcPos
  byLexeme (string "#OtherFilters")
  (eof >> return Nothing) <|>
    (do tyFns <- pOFTyNa
        def <- manyTill anyChar (try eof)
        return (Just (text def, tyFns)))


-- parse Other-Filters Types and Names. Written in a list
pOFTyNa :: Parsec String u [(String,String)]
pOFTyNa = do
  byLexeme (char '[')
  many (try (do
    v <- pVar
    byLexeme (string "::")
    byLexeme (string "BiFilter")
    t <- pConstructor
    byLexeme (char ',' <|> char ']')
    return (t,v)))

pVar :: Parsec String u String
pVar = byLexeme (do
  hd <- lower
  tl <- many (lower <|> upper <|> char '_' <|> char '\'')
  return (hd:tl))
