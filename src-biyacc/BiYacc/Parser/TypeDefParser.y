{
{-# LANGUAGE DeriveDataTypeable, ViewPatterns #-}
module BiYacc.Parser.TypeDefParser (typeDefParser, typeDefLexer) where

import Data.Char
import Data.Data
import Data.Map as Map (Map, insert, empty)
import Data.Typeable

import BiYacc.Language.Def

}

%name typeDefParser
%tokentype { Token }
%error { tyParseError }



%token
  var             { TKVar  $$ }
  cons            { TKCons $$ }
  '='             { TKEq }
  '|'             { TKOr }
  '('             { TKLParen }
  ')'             { TKRParen }
  'data'          { TKData }
  'type'          { TKType }
  '#Abstract'     { TKAbstractStart }

%%

Defs     : '#Abstract' SynonymMap TypeDefs { ($2, $3) }

SynonymMap : {- empty -}           { Map.empty }
           | TySynonym SynonymMap  { $1 $2 }

TySynonym  : 'type' cons TyVars '=' cons TyFields  { Map.insert (TyCon $2 $3) (TyCon $5 $6) }

TypeDefs : TypeDef                  { [$1] }
         | TypeDef TypeDefs         { $1 : $2 }


TypeDef : 'data' cons TyVars TyLines  {DataTypeDec (TyCon $2 $3) $4}

TyVars : {- empty -}  { [] }
       | var TyVars   { (TyVar $1) : $2}

TyLines : TyLine         { [$1] }
        | TyLine TyLines { $1 : $2 }

TyLine : '=' cons TyFields { DataTypeDef $2 $3 }
       | '|' cons TyFields { DataTypeDef $2 $3 }

TyFields : {- empty -}      { [] }
         | TyField TyFields { $1 : $2}


TyField : cons    { TyCon $1 [] }
        | var     { TyVar $1 }
        | '(' cons TyFields ')' { TyCon $2 $3 }



{
tyParseError :: [Token] -> a
tyParseError toks = error $ "Parse error in (#Abstract) data type definitions.\n" ++
  "Note: (1) Define all type synonyms before new data types.\n" ++
  "(2) Pre-defined types are only String, Int, and Integer. " ++
  "Currently it does not recognize other Haskell's builtin types.\n\n" ++
  "First 100 unconsumed tokens:\n" ++ show (take 100 toks) ++
  "\nConverted to strings:\n" ++ printTyDefTokens toks


data Token =
    TKInt  Integer
  | TKStr  String
  | TKChar Char
  | TKVar  String
  | TKCons String
  | TKData
  | TKType
  | TKEq
  | TKOr
  | TKLParen
  | TKRParen
  | TKAbstractStart
  deriving (Show, Eq)


-- simple non-total lexer. does not support the case when variable names contain "instance"
typeDefLexer :: String -> [Token]
typeDefLexer [] = []
typeDefLexer ('-':'-':cs) = lexComment cs
typeDefLexer ('=':cs) = TKEq : typeDefLexer cs
typeDefLexer ('|':cs) = TKOr : typeDefLexer cs
typeDefLexer ('(':cs) = TKLParen : typeDefLexer cs
typeDefLexer (')':cs) = TKRParen : typeDefLexer cs
typeDefLexer ('#':'A':'b':'s':'t':'r':'a':'c':'t':cs) = TKAbstractStart : typeDefLexer cs
typeDefLexer ('d':'a':'t':'a':cs) | (isSpace . head $ cs) = TKData : typeDefLexer cs
typeDefLexer ('t':'y':'p':'e':cs) | (isSpace . head $ cs) = TKType : typeDefLexer cs
typeDefLexer (c:cs)
      | isSpace c = typeDefLexer cs
      | isLower c = lexVar  (c:cs) typeDefLexer
      | isUpper c = lexCons (c:cs) typeDefLexer
typeDefLexer cs = error $ "non exhaustive patterns in typeDefLexer. " ++
                  "First untokenised 100 chars:\n" ++ take 100 cs

lexComment :: String -> [Token]
lexComment [] = []
lexComment (c:cs) | isNewline c = typeDefLexer cs
lexComment (c:cs) | otherwise = lexComment cs

isNewline :: Char -> Bool
isNewline '\r' = True
isNewline '\n' = True
isNewline _ = False

lexVar :: String -> (String -> [Token]) -> [Token]
lexVar cs nextLexer =
  let (var,rest) = span isAlphaNum cs
  in  TKVar var : nextLexer rest

lexCons :: String -> (String -> [Token]) -> [Token]
lexCons cs nextLexer =
  let (cons,rest) = span isAlphaNum cs
  in  TKCons cons : nextLexer rest


printTyDefTokens :: [Token] -> String
printTyDefTokens = foldr (\c cs -> c ++ " " ++ cs) " " . map showTok

showTok :: Token -> String
showTok (TKInt  i) = show i
showTok (TKStr  str) = show str
showTok (TKChar c) = show c
showTok (TKVar  v) = v
showTok (TKCons con) = con
showTok TKData = "data"
showTok TKType = "type"
showTok TKEq = "="
showTok TKOr = "|"
showTok TKLParen = "("
showTok TKRParen = ")"
showTok TKAbstractStart = "#Abstract"

}
