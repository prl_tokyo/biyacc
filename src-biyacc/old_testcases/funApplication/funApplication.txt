-- Simple disambiguations do not fully solve the ambiguouties
-- Think of new solutions later

#Abstract

data Arith =
    EqualT Arith Arith
  | Let String  Arith Arith
  | App Arith Arith
  | Var String
  deriving (Show, Eq, Read)


#Concrete

Term   -> Name
        | Term Term
        | "let" Name "=" Term "in" Term
        | Term "=" Term
        | "(" Term ")"
        ;

#Directives

LineComment:   "--"       ;
BlockComment:  "{-" "-}"  ;

Term -> Term Term <<<
  Term -> Term "=" Term ;

Term -> Term "=" Term <<<
  Term -> "let" Name "=" Term "in" Term ;

(Term Term) Term === Term Term Term ;

#Actions

Arith +> Term
Var s           +>  (s +> Name) ;
App a b         +>  (a +> Term)  (b +> Term) ;
Let v x y       +>  "let" (v +> Name) '=' (x +> Term) "in" (y +> Term) ;

EqualT x y   +>  (x +> Term) '=' (y +> Term) ;

e               +>  '(' (e +> Term) ')'  ;
