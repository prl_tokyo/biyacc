# What is this repository for? #

* BiYacc - a tool for generating a reflective printer and its parser in a single program.

----

## Changelog 0.4.0

Compatible with GHC 8.8.1 and Cabal 3.

## Installation

First install *Glasgow Haskell Compiler*.

Then install *happy 1.19.9* by running `cabal install happy-1.19.9` in the command line.
(The latest version 1.19.12 either has a bug in its GLR mode or is incompatible with ghc 8.4.x. I am not sure.)

**If the version of your cabal-install is** ***less 3.0***  
In the root directory where *cabal.project* lies, do the following in order:

1. change the working directory to `src-bigul` and run `cabal isntall`
2. change the working directory to `src-biyacc` and run `cabal install`

**If the version of your cabal-install is** ***greater than 3.0***  
In the root directory where *cabal.project* lies, run the following in order:

1. `cabal configure`
2. `cabal build all`
3. `cabal install lib:bigul --lib`
4. `cabal install biyacc`


----

## Usage
1) generate the executable: type `biyacc BiYaccFile OutputExecutableFile`
2) run the transformations.

for parsing:  type `OutPutExecutableFile get InputFile OutputFile`

for printing: type `OutPutExecutableFile put InputFile1 (code) InputFile2 (AST) OutputFile (code')`

If "OutputFile(updated code)" is ommitted, the "InputFile1" file will be updated.

----

## Try it on the website
Go to
http://www.prg.nii.ac.jp/project/biyacc.html or
http://biyacc.yozora.moe
to try the examples online.
Also, you can install this website on your own computer.



## What BiYacc actually does?
Saying that BiYacc is for bidirectional parsing and reflective printing is wrong, in fact:

- Parsing is to find a tree structure for a grammar in string representation (program text).
- Printing is to re-produce a string representation from a tree structure.

So there is no transformation between two trees in either case, but only transformations between program text and trees. For transformations between program text and its concrete syntax tree (CST), currently we ad hoc generate two programs and wish the two programs to be isomorphisms. There is no guarantee, and even not necessary to make the two transformations bidirectional.

To be precise, BiYacc's main job is to synchronise two algebraic data structures, exactly the same as BiGUL does. But why are parsing and synchronising confused? This has much to do with the origin of the name of BiYacc --- Yacc. Yacc is a parser generator which automatically build CSTs for users' given grammar, and in addition allow some semantic actions to be performed simultaneously. So in practice, users can only get the result of their semantic actions instead of a CST. This is usually a simplified version of CST --- an abstract syntax tree (AST), and in "worse situations" only an integer in examples shown in many tutorials.

Since it is hard to directly modify program text, tools usually make modifications to the tree representation of the program text and produce a new piece of program text. Then we encounter a problem: for a programming language, the parser come with the compiler usually builds an AST instead of a concrete one.
Producing program text from such a tree will lose lots of important information. So the tool had to build CSTs wasting additional effort and modify CSTs instead, which also makes algorithms complex and hard to be performed. Alternatively, it would be easier for tools to design algorithms modifying ASTs and automatically get synchronised CSTs. However, the mapping between CSTs and ASTs is usually not injective, which brings additional difficulties.

Then we have a point: BiYacc may do a good job on synchronising CSTs and ASTs by declaratively writing synchronising actions. In addition, parsers and printers are freely generated all together. In conclusion, using BiYacc to write front end of a compiler, make many other jobs easier or comes for free: resugaring, code refactoring, "language evolution", etc.

We should develop BiYacc and make it better for performing synchronisation between tree structures.
Potentially BiYacc brings insight for research in database represented by trees: in particular XML database.

-------------
Therefore interact parsing and printing with synchronising is just a good application ~

